--[[

Quest "Tortured mind" (birnam:mind)

]]


-- global section
-- --------------

player = 1  -- id defined in chapitre/devilsai.xml
thurio = 4  -- id defined in quest/birnam/Wizard.xml

questStep = "0"

timer = 0

-- functions
-- ---------

function questBegin(addNewElements)

	if questStep == "0" then
		if addNewElements == "true" then
            loadWorld("birnam", "quest/birnam/Wizard.xml", "TorturedMind-Thurio")
		end

		questStep = "1"
	end

end

function questManage()

    if questStep == "1" then
        if Character__interact(player, thurio) then
            pushDialog("birnam", "dialog-mind-thurio")
            questStep = "2"
        end

    elseif questStep == "2" then
        if dialogDisplayed() then
            timer = resetTimer()
            questStep = "3"
        end

    elseif questStep == "3" then
        if getTimeElapsed(timer) > 4000 then
            World__addUtterance("birnam", thurio, "birnam", "utterance-mind-thurio1", 0)
            timer = resetTimer()
            questStep = "4"
        end

    elseif questStep == "4" then
        if getTimeElapsed(timer) > 2000 then
            World__addUtterance("birnam", player, "birnam", "utterance-mind-player", 0)
            timer = resetTimer()
            questStep = "5"
        end

    elseif questStep == "5" then
        if getTimeElapsed(timer) > 5000 then
            World__addUtterance("birnam", thurio, "birnam", "utterance-mind-thurio2", 0)
            timer = resetTimer()
            questStep = "6"
        end

    elseif questStep == "6" then
        if getTimeElapsed(timer) > 1500 then
            Character__setAttribute(thurio, "life", 0)
            timer = resetTimer()
            questStep = "7"
        end

    elseif questStep == "7" then
        if getTimeElapsed(timer) > 4000 then
            loadWorld("birnam", "quest/birnam/Wizard.xml", "TorturedMind-Monsters")
            deleteTaggedItems("birnam", "RescueFluellensCamp-ObstacleForEnemies")
            questStep = "8"
        end
    end
end

function questIsDone()
    return (questStep == "8")
end

function questSave()
	return questStep
end

function questRecoverState(data)
    questStep = data
    timer = resetTimer()
end

function questEnd()
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 1500)
    Character__addExperience(player, 1500)
end
