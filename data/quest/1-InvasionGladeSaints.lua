--[[

Quest "Desecration" (birnam:desecration)

Summary:
  The player must kill all monsters with tag "TheGladeOfTheSaints-Monsters".
  When there are less than 5 remaining monsters, this number is displayed on the screen.
  When the quest is done, the player gains experience.

]]


-- global section
-- --------------

player = 1  -- id defined in chapitre/devilsai.xml
gower  = 2  -- id defined in carte/birnam.xml

checkPoint = 0

questStep = "0"
hasTalkedToGower = "false"
gowerSaidHello = "false"
hasReachedTheCheckPoint = false

remainingMonsters = 1
remainingMonstersDisplayed = 0

timer = 0
timer_hello = 0

-- functions
-- ---------

function questBegin(addNewElements)

	if questStep == "0" then

        timer_hello = resetTimer()

		if addNewElements == "true" then
            loadWorld("birnam", "quest/birnam/FreeIceRoad.xml", "Obstacle-GladeSaints-IceRoad")
		end

		questStep = "1"
	end

    checkPoint = addCheckPoint("birnam", 3652, -880, 10, 100)

end

function questManage()

    if hasTalkedToGower == "false" then
        if gowerSaidHello == "false" and getTimeElapsed(timer_hello) > 3000 then
            gowerSaidHello = "true"
            addJournalEntry("birnam", "desecration", "journal-desecration-title")
            addJournalEntry("birnam", "desecration", "journal-desecration-text1")
            World__addUtterance("birnam", gower, "birnam", "utterance-hello-player", 1, Character__displayedName(player))
        end

        if Character__interact(player, gower) then
            if (gowerSaidHello == "false") then
                addJournalEntry("birnam", "desecration", "journal-desecration-title")
                addJournalEntry("birnam", "desecration", "journal-desecration-text1")
            end
            pushDialog("birnam", "dialog-desecration-introduction")
            addJournalEntry("birnam", "desecration", "journal-desecration-text2")
            hasTalkedToGower = "true"
        end
    end

    if hasReachedTheCheckPoint == false and collision(player, checkPoint) then
        World__addUtterance("birnam", 101, "birnam", "checkpoint-desecration-pileofstones", 0)  -- 101: Pile of stones
        hasReachedTheCheckPoint = true
    end

	if questStep == "1" then

        if getTimeElapsed(timer) > 1000 then
            timer = resetTimer()
            remainingMonsters = getNumberOfItemsByTag("birnam", "TheGladeOfTheSaints-Monsters")
        end

        if remainingMonsters <= 5 and remainingMonsters > 0 then
            if remainingMonsters ~= remainingMonstersDisplayed then
                World__addUtterance(Character__world(player), player, "devilsai", "utterance-remaining-enemies", 1, remainingMonsters)
                remainingMonstersDisplayed = remainingMonsters
			end
		end
        if remainingMonsters == 0 then
			questStep = "2"
            addJournalEntry("birnam", "desecration", "journal-desecration-text3")
		end

	elseif questStep == "2" then
        if Character__interact(player, gower) then
			questStep = "3"
            pushDialog("birnam", "dialog-desecration-end")
            addJournalEntry("birnam", "desecration", "journal-desecration-text3")
            addJournalEntry("birnam", "desecration", "journal-desecration-end")
		end

	elseif questStep == "3" then
        if dialogDisplayed() then
			questStep = "4"
		end

	end

end

function questIsDone()
	return (questStep == "4")
end

function questSave()
    save = questStep .. " " .. hasTalkedToGower .. " " .. gowerSaidHello
	return save
end

function questRecoverState(data)
    _, _, questStep, hasTalkedToGower, gowerSaidHello = string.find(data, "(%d+) (%a+) (%a+)")
end

function questEnd()
	addQuest("quest/1-RescueFluellensCamp.lua")
	addQuest("quest/1-UnknownStone.lua")
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 1000)
    Character__addExperience(player, 1000)
end
