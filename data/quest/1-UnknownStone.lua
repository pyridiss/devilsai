--[[

Quest "Dark energy" (birnam:energy)

]]


-- global section
-- --------------

player = 1  -- id defined in chapitre/devilsai.xml
gower  = 2  -- id defined in carte/birnam.xml

questStep = "0"
hasTheStone = "false"
gowersWorriesLaunched = "false"
gowersWorriesDone     = "false"


-- functions
-- ---------

function questBegin(addNewElements)
    questStep = "1" then
end

function questManage()

    if gowersWorriesLaunched == "false" and questRunning("1-GowersWorries") == true then
        gowersWorriesLaunched = "true"
    elseif gowersWorriesLaunched == "true" and questRunning("1-GowersWorries") == false then
        gowersWorriesDone = "true"
    end

    if questStep == "1" then
        if hasTheStone == "false" and Character__possesses(player, "unidentifiedstone") then
            hasTheStone = "true"
            World__addUtterance(Character__world(player), player, "birnam", "utterance-energy-discover", 0)
            addJournalEntry("birnam", "energy", "journal-energy-title")
            addJournalEntry("birnam", "energy", "journal-energy-text1")
        elseif hasTheStone == "true" and Character__possesses(player, "unidentifiedstone") == false then
            hasTheStone = "false"
        end

        if hasTheStone == "true" and Character__interact(player, gower) == true then
            if gowersWorriesLaunched == "false" or gowersWorriesDone == "true" then
                handOverItem(player, gower, "unidentifiedstone")
                hasTheStone = "false"
                pushDialog("birnam", "dialog-energy-giventogower")
                addJournalEntry("birnam", "energy", "journal-energy-text2")
                questStep = "2"
            end
        end

end

function questIsDone()
    return (questStep == "2")
end

function questSave()
    save = questStep .. " " .. hasTheStone .. " " .. gowersWorriesLaunched .. " " .. gowersWorriesDone
    return save
end

function questRecoverState(data)
    _, _, questStep, hasTheStone, gowersWorriesLaunched, gowersWorriesDone = string.find(data, "(%d+) (%a+) (%a+) (%a+)")
end

function questEnd()
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 2000)
    Character__addExperience(player, 2000)
end
