--[[

Quest "Trade Route" (birnam:route)

]]


-- global section
-- --------------

player   = 1  -- id defined in chapitre/devilsai.xml
gower    = 2  -- id defined in carte/birnam.xml
fluellen = 3  -- id defined in carte/birnam.xml

questStep = "0"

remainingMonsters = 1
timer = 0


-- functions
-- ---------

function questBegin(addNewElements)

	if questStep == "0" then

        if addNewElements == "true" then
            pushDialog("birnam", "dialog-route-advice")
        end

		questStep = "1"
	end

end

function questManage()

	if questStep == "1" then

        if getTimeElapsed(timer) > 1000 then
            timer = resetTimer()
            remainingMonsters = getNumberOfItemsByTag("birnam", "TheIceRoad-Monsters")
        end

        if remainingMonsters == 0 then
            addJournalEntry("birnam", "route", "journal-route-title")
            addJournalEntry("birnam", "route", "journal-route-text1")
			questStep = "2"
		end

	elseif questStep == "2" then
        if Character__interact(player, fluellen) == true then
            pushDialog("birnam", "dialog-route-goseegower")
            addJournalEntry("birnam", "route", "journal-route-text2")
			questStep = "3"
		end

	elseif questStep == "3" then
        if Character__interact(player, gower) == true then
            loadWorld("birnam", "quest/birnam/FreeIceRoad.xml", "FreeIceRoad-Reward")
            pushDialog("birnam", "dialog-route-reward")
            addJournalEntry("birnam", "route", "journal-route-text3")
            addJournalEntry("birnam", "route", "journal-route-end")
			questStep = "4"
		end
	end
end

function questIsDone()
	return (questStep == "4")
end

function questSave()
	return questStep
end

function questRecoverState(data)
    questStep = data
end

function questEnd()
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 1500)
    Character__addExperience(player, 1500)
end
