--[[

Map 1 - Quest "Gower's Worries"

Steps:
1 - Fluellen says that the player should go talk to Gower. The player reaches the check point. Enemies are loaded.
2 - Either Gower is killed (jump to Step 10), or all enemies are killed (jump to Step 20)
10 - Gower says the player must go to Forres. Next quest.
20 - The player must go talk to Fluellen.
21 - Fluellen says the player must go to Forres.
22 - Next quest.

]]


-- global section
-- --------------

player   = 1  -- id defined in chapitre/devilsai.xml
gower    = 2  -- id defined in carte/birnam.xml
fluellen = 3  -- id defined in carte/birnam.xml

checkPoint1 = 0

questStep = "0"

remainingMonsters = 1
timer = 0

-- functions
-- ---------

function questBegin(addNewElements)

	if questStep == "0" then

		checkPoint1 = addCheckPoint("birnam", 3710, -880, 10, 100)

		pushDialog("mis_1.10_intro") -- TODO Find a better name

		questStep = "1"
	end

end

function questManage()

	if questStep == "1" then
        if collision(player, checkPoint1) then
            loadWorld("birnam", "quest/birnam/GowersWorries.xml", "GowersWorries-Warriors")
            loadWorld("birnam", "quest/birnam/GowersWorries.xml", "GowersWorries-Enemies")
			questStep = "2"
		end

	elseif questStep == "2" then

        if getTimeElapsed(timer) > 1000 then
            timer = resetTimer()
            remainingMonsters = getNumberOfItemsByTag("birnam", "GowersWorries-Enemies")
        end

        if remainingMonsters == 0 then
			pushDialog("mis_1.13_ccl")
			pushDialog("mis_1.14_intro")
            addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 4000)
            Character__addExperience(player, 4000)
			questStep = "10"
		end
        if Character__currentHealthStatus(gower, "life") == 0 then
			questStep = "20"
		end

	--elseif questStep == 10 then
		-- Auparavant : Lancement de F1, G1, H1, et 13 !

	elseif questStep == "20" then
        if Character__interact(player, gower) then
			pushDialog("mis_1.E3_intro")
			questStep = "21"
		end

	elseif questStep == "21" then
        if Character__interact(player, fluellen) then
			pushDialog("mis_1.E3_ccl")
			pushDialog("mis_1.E4_intro")
			questStep = "22"
		end
	end

end

function questIsDone()
	return (questStep == "10" or questStep == "22")
end

function questSave()
	return ""
end

function questRecoverState(data)
end

function questEnd()
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 1000)
    Character__addExperience(player, 1000)
end
