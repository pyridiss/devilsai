--[[

Quest "Threatened Friend" (birnam:friend)

Summary:
  The player must reach the Ancient Lands. There, the camp of Fluellen is surrounded by monsters.
  The player must kill all the monsters that Fluellen can see. These monsters are counted once per second.
  If the counter drop under 3, the user is informed that there are only a few monsters remaining. If the counter
  goes back beyond 5, the user is informed that new enemies are comming.
  If Fluellen cannot see any enemy during 5 seconds, he gives the order to open the gate. The gate is removed two
  seconds later.
  The player must go talk to Fluellen. The quest is finished.

]]


-- global section
-- --------------

player   = 1  -- id defined in chapitre/devilsai.xml
fluellen = 3  -- id defined in carte/birnam.xml

checkPoint1 = 0 -- AncientLands entrance
checkPoint2 = 0 -- AncientLands entrance
checkPoint3 = 0 -- Noirefontaine entrance
checkPoint4 = 0 -- Noirefontaine entrance
checkPoint5 = 0 -- Gate of the camp

questStep = "0"
hasReachedTheGate = false
hasSaidFewMonsters = false
timer2HasBeenReset = false

remainingMonsters = 1

timer = 0

-- functions
-- ---------

function questBegin(addNewElements)

	if questStep == "0" then

		-- Add new elements and monsters
        checkPoint1 = addCheckPoint("birnam", 2222, -5304, 35, 100)
        checkPoint2 = addCheckPoint("birnam", 2200, -3630, 490, 10)
        checkPoint3 = addCheckPoint("birnam", 1790, -5301, 35, 100)
        checkPoint4 = addCheckPoint("birnam", 1450, -2281, 35, 200)
        checkPoint5 = addCheckPoint("birnam", 2630, -4307, 35, 250)

        if addNewElements == "true" then
            deleteTaggedItems("birnam", "Obstacle-GladeSaints-IceRoad")

            loadWorld("birnam", "quest/birnam/RescueFluellensCamp.xml", "Obstacle-AncientLands-Birnam")
            loadWorld("birnam", "quest/birnam/RescueFluellensCamp.xml", "Gate-FluellensCamp")
            loadWorld("birnam", "quest/birnam/RescueFluellensCamp.xml", "RescueFluellensCamp-ObstacleForEnemies")

            pushDialog("birnam", "dialog-friend-introduction")
            addJournalEntry("birnam", "friend", "journal-friend-title")
            addJournalEntry("birnam", "friend", "journal-friend-text1")
        end

		questStep = "1"
	end

end

function questManage()

	if questStep == "1" then
        if collision(player, checkPoint1) or collision(player, checkPoint2) then
            deleteTaggedItems("birnam", "TheGladeOfTheSaints-Warriors")
			questStep = "2"
            addJournalEntry("birnam", "friend", "journal-friend-text2")
		end

	elseif questStep == "2" then

        if hasReachedTheGate == false and remainingMonsters > 0 and collision(player, checkPoint5) then
            hasReachedTheGate = true
            World__addUtterance("birnam", fluellen, "birnam", "utterance-friend-fluellen-sorry", 0)
        end

        if getTimeElapsed(timer) > 1000 then
            timer = resetTimer()
            remainingMonsters = Character__numberOfEnemiesInViewField(fluellen)
        end

        if hasSaidFewMonsters == false and remainingMonsters <= 3 then
            World__addUtterance(Character__world(player), player, "birnam", "utterance-friend-player-few-monsters", 0)
            hasSaidFewMonsters = true
        end

        if hasSaidFewMonsters == true and remainingMonsters > 5 then
            World__addUtterance(Character__world(player), player, "birnam", "utterance-friend-player-new-coming", 0)
            hasSaidFewMonsters = false
        end

        if timer2HasBeenReset == false and remainingMonsters == 0 then
            timer2 = resetTimer()
            timer2HasBeenReset = true
        end

        if timer2HasBeenReset == true and getTimeElapsed(timer2) > 5000 then
            World__addUtterance("birnam", fluellen, "birnam", "utterance-friend-fluellen-open-gate", 0)
		end

        if timer2HasBeenReset == true and getTimeElapsed(timer2) > 7000 then
            deleteTaggedItems("birnam", "Gate-FluellensCamp")
			questStep = "3"
            addJournalEntry("birnam", "friend", "journal-friend-text3")
		end

	elseif questStep == "3" then
        if Character__interact(player, fluellen) then
			questStep = "4"
            pushDialog("birnam", "dialog-friend-end")
            addJournalEntry("birnam", "friend", "journal-friend-end")
		end

	elseif questStep == "4" then
        if dialogDisplayed() then
			questStep = "5"
		end

	end

    if collision(player, checkPoint3) or collision(player, checkPoint4) then
        addJournalEntry("birnam", "friend", "journal-friend-checkpoint")
    end

end

function questIsDone()
	return (questStep == "5")
end

function questSave()
	return questStep
end

function questRecoverState(data)
	_, _, questStep = string.find(data, "(%d+)")
end

function questEnd()
    deleteTaggedItems("birnam", "Obstacle-AncientLands-Birnam")
    addQuest("quest/1-KillStolas.lua")
    addQuest("quest/1-FreeIceRoad.lua")
    addQuest("quest/1-Wizard.lua")
    addConsoleEntry("devilsai", "console-experience-gained", 2, Character__displayedName(player), 2000)
    Character__addExperience(player, 2000)
end
