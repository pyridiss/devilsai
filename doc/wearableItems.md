# devilsai game engine – How to create wearable items?

A wearable item refers to an item that can be put in a storage box, in the inventory, or that can be used.
These items are defined in lua files to give them flexibility. However, they must respect some rules in order to be
used with the game engine.


## Operating principle

Each wearable item has its own lua context, except for stacked items. Only identical items can be stacked.

A wearable item can be *active* or *inactive*, that is to say it currently has effects or not. To be active, the
wearable item must be placed in a particular *slot*.


## Mandatory variables and functions

### `name -> string`

The internal name of the item, must match the file name and the language strings.

### `categoryObject -> string`

Returns the category of the item. For the time beeing, it is only used to know if an item is temporary, that is to say
if its effects will expire. If that is the case, categoryObject must return `"temporaire"`.

### `imageFile -> string`

Relative path to the image used in the slot where the item is active, and when the item is moved.

### `iconFile -> string`

Relative path to the image used in the inventory and in storage boxes.

### `currentSlot -> string`

The current slot where the item is.

### `slotForUse -> string`

The slot where the item is active.

### `duree -> number`

The remaining duration before the item expire.

### `quantite -> number`

For stackable items, stores the current number of items stacked.

### `descriptionManuelle -> boolean`

Tells whether a manual description can be got from the language files.

### `descriptionAutomatique -> boolean`

Tells whether the description of the item should contain an auto-defined part with its effects.

### `function active() -> boolean` or `active -> boolean`

Tells whether the item is currently in the slot it needs to be to be active. Most of the time, this global will be
a function that compares `slotForUse` with `currentSlot`.

### `function getObjectProperty(string) -> number`

Given an attribute, returns the effect it can have on the holder if the item is active. This function is used to
create the description of the item.

### `function getCurrentObjectEffect(string) -> number`

Given an attribute, returns the current effect on the holder. It should be `0` if the item is inactive.

### `function stackable() -> boolean` or `stackable -> boolean`

Tells whether the item can be stacked. This global can be a function to return `false` when the item is currently used.

### `function generateRandomObject(number)`

Given a quality, this function can be used to improve the item, or to change the number of items.

`TODO`: *this function should be optional.*

### `objectSave() -> string`

The string will be stored in the savepack when the user saves the game. It will be given back to `objectRecoverState()`
at loading time.

### `function objectRecoverState(string)`

When loading a savepack, this function is called with the string that `objectSave()` had produced at save time.
It can be used to retrieve data.


## Optional variables and functions

### `canImproveSkills -> boolean`

Tells whether an item can change the level of a skill. If this global is not set, it is considered that the item cannot
improve the level of any skill.

### `function getCurrentSkillImprovement(string) -> number`

Given a skill, retuns the extra level currently given by the item. If the item is inactive, it should be `0`.

### `skills -> table{[string] = number, ...}`

Associative array that gives the extra level given by the item for a particular skill. This table is used to create
the description of the item.


## Callable C++ functions from lua

The functions defined in the lua file can call these C++ functions:
- currently, there is no callable C++ functions.
