# devilsai – An Action Role Playing Game

devilsai is an open-source project composed of:
- an Action Role Playing Game engine [devilsai-gameengine]
- a game pack editor [devilsai-editor]
- a game pack "devilsai: broken frontiers" [broken-frontiers]

This project is both a directly playable game and an engine to develop other games.


## Licence

devilsai - An Action Role Playing Game

Copyright (C) 2009-2018  Quentin Henriet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Install

The game needs SFML 2.4, Lua5.2, PhysicsFS, and TinyXML-2.

Installing through CMake should be easy on Windows, Linux, and other platforms supported by the above libraries. You can follow the [SFML tutorial](https://www.sfml-dev.org/tutorials/2.5/compile-with-cmake.php), as SFML uses the same build system.

## Contribute

As an open-source project, devilsai needs help to grow. Il you want to contribute (code, graphics, musics, sounds, translations, ideas, or whatever), you can contact me or use the framagit platform through issues and merge requests.

## Links

Development:    <https://framagit.org/pyridiss/devilsai>

SFML:           <http://www.sfml-dev.org/>

Lua:            <http://www.lua.org>

PhysicsFS:      <http://icculus.org/physfs/>

TinyXML-2:      <http://leethomason.github.io/tinyxml2/>
