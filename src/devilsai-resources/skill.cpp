
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/skill.h"

#include <lua.hpp>
#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/math.h"
#include "textManager/textManager.h"
#include "multimedia/RichText.h"
#include "imageManager/imageManager.h"
#include "musicManager/musicManager.h"
#include "gui/style.h"

#include "../Bibliotheque/luaFunctions.h"
#include "../ElementsCarte/ElementsCarte.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/shaders.h"
#include "devilsai-resources/Species.h"


using namespace tinyxml2;

Skill::Skill(string id)
  : Id(std::move(id)),
    Animation(),
    scriptString(),
    script(nullptr),
    interactionField(),
    numberOfImages(0),
    priority(0),
    step(0),
    levels(1),
    speedAttribute(RunSpeed)
{
}

Skill::Skill(const Skill& other)
  : Id(other.Id),
    Animation(other.Animation),
    scriptString(other.scriptString),
    script(nullptr),
    interactionField(other.interactionField),
    numberOfImages(other.numberOfImages),
    priority(other.priority),
    step(other.step),
    levels(other.levels),
    speedAttribute(other.speedAttribute)
{
    if (!scriptString.empty())
        loadScript();
}

Skill& Skill::operator=(const Skill& right)
{
    Id = right.Id;
    interactionField = right.interactionField;
    Animation = right.Animation;
    scriptString = right.scriptString;
    numberOfImages = right.numberOfImages;
    priority = right.priority;
    step = right.step;
    levels = right.levels;
    speedAttribute = right.speedAttribute;

    if (!scriptString.empty())
        loadScript();

    return *this;
}

Skill::~Skill()
{
    if (script != nullptr) lua_close(script);
}

const string& Skill::name() const
{
    return Id;
}

string Skill::relatedItem() const
{
    if (size_t i = Id.find("use-") ; i != string::npos)
        return Id.substr(i+4);
    return string();
}

void Skill::addImage(double angle, int num, string imageKey)
{
    pair<double, int> key(angle, num);
    Animation.insert(pair<pair<double,int>,string>(key, imageKey));

    if (angle == 0) addImage(2.0 * M_PI, num, imageKey);
}

string Skill::getImageKey(double angle, int num)
{
    string key = Animation.begin()->second;
    double distance = 10;

    for (auto& i : Animation)
    {
        if (i.first.second != num) continue;

        double d = (i.first.first - angle) * (i.first.first - angle);
        if (d < distance)
        {
            distance = d;
            key = i.second;
        }
    }

    return key;
}

void Skill::loadFromXML(XMLHandle &handle, Shader* shader, int hue, int saturation, int luminance, int alpha)
{
    XMLElement *elem = handle.FirstChildElement().ToElement();

    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "properties")
        {
            elem->QueryAttribute("priority", &priority);
            elem->QueryAttribute("step", &step);
            elem->QueryAttribute("numberOfImages", &numberOfImages);

            if (elem->Attribute("speedAttribute"))
                speedAttribute = stringToAttribute(elem->Attribute("speedAttribute"));

            if (elem->Attribute("speed"))
            {
                double sp = 0;
                elem->QueryAttribute("speed", &sp);
                levels[0].extraStats.set(speedAttribute, sp);
            }

            if (elem->BoolAttribute("FixedAngle")) extraState |= FixedAngle;
        }
        if (elemName == "level")
        {
            unsigned level = 1;
            elem->QueryAttribute("level", &level);
            levels.resize(max(level, (unsigned)levels.size()));
            for (XMLElement *elem2 = elem->FirstChildElement() ; elem2 != nullptr ; elem2 = elem2->NextSiblingElement())
            {
                string_view elem2Name = elem2->Name();

                if (elem2Name == "extraStats")
                    levels[level-1].extraStats.loadFromXML(elem2);
                if (elem2Name == "extraStatsAmplifiers")
                    levels[level-1].extraStatsAmplifiers.loadFromXML(elem2);
                if (elem2Name == "unavailability")
                    elem2->QueryAttribute("value", &(levels[level-1].unavailability));
            }
        }
        if (elemName == "extraStats")  // When outside a 'level' element, assume (level == 1)
        {
            levels[0].extraStats.loadFromXML(elem);
        }
        if (elemName == "extraStatsAmplifiers")  // When outside a 'level' element, assume (level == 1)
        {
            levels[0].extraStatsAmplifiers.loadFromXML(elem);
        }
        if (elemName == "unavailability")  // When outside a 'level' element, assume (level == 1)
        {
            elem->QueryAttribute("value", &(levels[0].unavailability));
        }
        if (elemName == "interactionField")
        {
            interactionField.loadFromXML(elem);
        }
        if (elemName == "loadImage")
        {
            string key = elem->Attribute("key");
            string path = elem->Attribute("path");
            imageManager::addImage(tools::hash("skills"), key, path);
        }
        if (elemName == "images")
        {
            double angle = 0;
            int xAlignment = 0, yAlignment = 0;

            elem->QueryAttribute("angle", &angle);
            elem->QueryAttribute("xAlignment", &xAlignment);
            elem->QueryAttribute("yAlignment", &yAlignment);

            if (elem->Attribute("pathToImages"))
            {
                string pathPattern = elem->Attribute("pathToImages");

                for (unsigned i = 0 ; i < numberOfImages ; ++i)
                {
                    string path = pathPattern;
                    string number = tools::math::intToChars(i, 2);
                    path.replace(path.find_first_of('%'), 2, number);
                    string key = Id + "/" + path;
                    imageManager::addImage(tools::hash("individuals"), key, path, Vector2i(xAlignment, yAlignment), shader);
                    if (hue + saturation + luminance != 0)
                        imageManager::changeHSL(tools::hash("individuals"), key, hue, saturation, luminance);
                    if (alpha != 255)
                        imageManager::changeAlphaChannel("individuals"_hash, key, alpha);

                    addImage(angle * M_PI / 180.0, i, key);
                }
            }

            if (elem->Attribute("imageFile"))
            {
                string path = elem->Attribute("imageFile");
                string key = Id + "/" + path;
                imageManager::addImage(tools::hash("individuals"), key, path, Vector2i(xAlignment, yAlignment), shader);
                if (hue + saturation + luminance != 0)
                    imageManager::changeHSL(tools::hash("individuals"), key, hue, saturation, luminance);
                if (alpha != 255)
                    imageManager::changeAlphaChannel("individuals"_hash, key, alpha);
                addImage(angle * M_PI / 180.0, 0, key);
            }
        }
        if (elemName == "useImages")
        {
            string from = elem->Attribute("from");
            Skill* s = devilsai::getResource<Skill>(from);
            if (s) Animation = s->Animation;
            else tools::debug::error("Cannot copy images from skill '" + from + "' to skill '" + Id + "': skill unknown.", "devilsai", __FILENAME__, __LINE__);
        }
        if (elemName == "script")
        {
            scriptString = elem->GetText();
            loadScript();
        }

        elem = elem->NextSiblingElement();
    }
}

void Skill::loadScript()
{
    script = luaL_newstate();
    luaL_openlibs(script);

    luaL_loadbuffer(script, scriptString.c_str(), scriptString.length(), Id.c_str());

    lua_register(script, "Character__addLifetime", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        if (character) character->addLifetime(lua_tonumber(S, 2));

        return 0;
    });

    lua_register(script, "Character__addUtterance", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        if (character) character->addUtterance(textManager::getText(lua_tostring(S, 2), lua_tostring(S, 3)));

        return 0;
    });

    lua_register(script, "Character__angle", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        lua_pushnumber(S, (character ? character->angle() : 0));

        return 1;
    });

    lua_register(script, "Character__changeCurrentSkill", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        if (character) character->Set_Activite(lua_tostring(S, 2));

        return 0;
    });

    lua_register(script, "Character__continueCurrentSkill", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        if (character) character->continueCurrentSkill();

        return 0;
    });

    lua_register(script, "Character__currentHealthStatus", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());
        string_view attribute = lua_tostring(S, 2);
        Attribute a = stringToAttribute(attribute);

        lua_pushnumber(S, (character ? character->currentHealthStatus(a) : 0));

        return 1;
    });

    lua_register(script, "Character__fight", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        lua_pushboolean(S, (character) ? character->fight() : false);
        return 1;
    });

    lua_register(script, "Character__interact", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());
        UnitId other (lua_tonumber(S, 2));

        lua_pushboolean(S, (character && other) ? character->interact(other.unit()) : false);

        return 1;
    });

    lua_register(script, "Character__setAngle", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());
        double value = lua_tonumber(S, 2);

        if (character)
            character->setAngle(value);

        return 0;
    });

    lua_register(script, "Character__setAngleRelativeToUnit", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());
        UnitId ref (lua_tonumber(S, 2));

        if (character)
            character->setAngle(ref);

        return 0;
    });

    lua_register(script, "Character__setAttribute", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());
        string_view attribute = lua_tostring(S, 2);
        double value = lua_tonumber(S, 3);
        Attribute a = stringToAttribute(attribute);

        if (character)
            character->setAttribute(a, value);

        return 0;
    });

    lua_register(script, "Character__setOwner", [](lua_State* S)
    {
        UnitId character (lua_tonumber(S, 1));
        UnitId owner (lua_tonumber(S, 2));

        Character* c = dynamic_cast<Character*>(character.unit());
        Character* o = dynamic_cast<Character*>(owner.unit());

        c->setOwner(o);

        return 0;
    });

    lua_register(script, "Character__targetedItem", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        lua_pushnumber(S, (character ? character->targetedItem().id() : 0));

        return 1;
    });

    lua_register(script, "Character__useItem", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        string item = lua_tostring(S, 2);
        if (character) character->useItem(item);
        return 0;
    });

    lua_register(script, "cout", LUA_cout);

    lua_register(script, "createIndividual", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* ind = dynamic_cast<Character*>(unit.unit());
        Carte* w = (ind) ? ind->world() : nullptr;
        Species* s = devilsai::getResource<Species>(lua_tostring(S, 2));

        if (w != nullptr && s != nullptr)
        {
            Individu* i = new Individu(s, lua_tonumber(S, 3), lua_tonumber(S, 4));
            w->insertItem(i);
            lua_pushnumber(S, i->id().id());
        }
        else lua_pushnumber(S, 0);

        return 1;
    });

    lua_register(script, "isCharacter", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Character* character = dynamic_cast<Character*>(unit.unit());

        lua_pushboolean(S, (character != nullptr));

        return 1;
    });

    lua_register(script, "Player__huntedCharacter", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        Player* player = dynamic_cast<Player*>(unit.unit());

        lua_pushnumber(S, (player) ? player->huntedCharacter() : 0);

        return 1;
    });

    lua_register(script, "playSound", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        string sound = lua_tostring(S, 2);

        if (unit) musicManager::playSound(sound, unit->position().x, unit->position().y, false);

        return 0;
    });

    lua_register(script, "Unit__position__x", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        lua_pushnumber(S, (unit ? unit->position().x : 0));
        return 1;
    });

    lua_register(script, "Unit__position__y", [](lua_State* S)
    {
        UnitId unit (lua_tonumber(S, 1));
        lua_pushnumber(S, (unit ? unit->position().y : 0));
        return 1;
    });

    lua_pcall(script, 0, 0, 0);

    lua_atpanic(script, [](lua_State* S)
    {
        tools::debug::error(lua_tostring(S, -1), "lua", __FILENAME__, __LINE__);
        return 0;
    });

    //Check if the file is well-formed

    lua_getglobal(script, "atBegin");
    if (lua_isfunction(script, -1))
        _state |= BeginScript;
    lua_pop(script, 1);

    lua_getglobal(script, "atEnd");
    if (lua_isfunction(script, -1))
        _state |= EndScript;
    lua_pop(script, 1);

    if (_state == 0)
    {
        tools::debug::error("Invalid lua script for the skill '" + Id + "'", "devilsai", __FILENAME__, __LINE__);
        lua_close(script);
        script = nullptr;
    }
}

bool Skill::atBegin(Individu* owner, unsigned level)
{
    if ((_state & BeginScript) == 0)
        return true;

    lua_getglobal(script, "atBegin");
    lua_pushnumber(script, owner->id().id());
    lua_pushnumber(script, level);
    lua_call(script, 2, 1);
    return lua_toboolean(script, -1);
}

void Skill::atEnd(Individu* owner, unsigned level)
{
    if ((_state & EndScript) == 0)
        return;

    lua_getglobal(script, "atEnd");
    lua_pushnumber(script, owner->id().id());
    lua_pushnumber(script, level);
    lua_call(script, 2, 0);
}

void Skill::displayDescription(sf::RenderWindow& target)
{
    textManager::PlainText description;

    description += "@f[dayroman]@s[20]@c[255,220,255]";
    description += textManager::getText("objects", Id);
    description += " @d@n";
    description += textManager::getText("objects", Id + "-description");

    multimedia::RichText rich;
    rich.setSize(300, 0);
    rich.setDefaultFormatting("liberation", 12, Color(255, 255, 255));
    rich.addFlags(textManager::HAlignCenter);
    rich.create(description);

    float x = gui::mousePosition().x;
    float y = gui::mousePosition().y;
    float w = 300;
    float h = rich.height();

    if (y + h > target.getSize().y)
        y -= h;

    multimedia::applyShaderOnScreen(target, "blur", devilsai::wearableItemShaderInstance, x - 10, y - 10, w + 2*10, h + 2*10);

    rich.display(target, x, y);
}
