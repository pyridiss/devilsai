
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_TRIGGER
#define DEVILSAI_RESOURCES_TRIGGER

#include <string>

#include "tools/vector2d.h"
#include "tools/shape.h"

#include "devilsai-resources/Unit.h"
#include "devilsai-resources/UnitId.h"


using std::string;

namespace sf{
    class RenderTarget;
}

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}

class lua_State;


class Trigger : public Unit
{
    public:
        class Script
        {
            private:
                lua_State* L;

            public:
                Script(const string& file);
                Script(const Script& other) = default;
                Script& operator=(const Script& other) = default;
                ~Script();

                lua_State* get();
        };

    private:
        tools::math::Shape _shape {};
        string _tag {};
        string _triggerScript {};
        string _data {};
        tools::math::Vector2d _position {};
        lua_State* _script {nullptr};
        UnitId _id;
        uint8_t _state {0};

    public:
        Trigger();
        Trigger(const Trigger& other);
        Trigger(Trigger&& other) noexcept = default;
        Trigger& operator=(const Trigger& right) = delete;
        Trigger& operator=(Trigger&& right) = delete;
        ~Trigger() = default;

        const UnitId& id() const;
        const tools::math::Vector2d& position() const;
        const tools::math::Shape& shape() const;
        bool unshaped() const;
        int height() const;
        const string& type() const;
        const string& tag() const;
        int diplomacy() const;
        void manage();
        void move(double x, double y);
        bool destroyed() const;
        void destroy();
        int collision(Unit* other, bool apply);
        void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);
        void display(sf::RenderTarget& target);
};

#endif  // DEVILSAI_RESOURCES_TRIGGER
