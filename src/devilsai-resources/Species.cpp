
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/Species.h"

#include <tinyxml2.h>

#include "ElementsCarte/ElementsCarte.h"

#include "tools/math.h"

#include "textManager/textManager.h"

#include "imageManager/imageManager.h"
#include "musicManager/musicManager.h"

#include "devilsai-resources/manager.h"
#include "devilsai-resources/shaders.h"

using namespace tinyxml2;


void Species::Copie_Element(Individu *elem)
{
    for (auto i : _skills)
    {
        elem->_skills.emplace(i.first, i.second);
    }

    elem->Set_Activite(_behaviors[Behaviors::Random]);
    elem->_shape = size;
    elem->interactionField = interactionField;
    elem->viewField = viewField;

	elem->Diplomatie = Diplomatie;
    elem->_attributes = commonStats;
    elem->setAttribute(Healing, commonStats[HealingPower]);
    elem->lifetime = lifetime;

    if (_state & FixedAngle) elem->setAngle(angle);

    elem->_state = _state;

    inventory.copy(elem->inventory);

    // Generate new items from the prototypes
    for (auto& d : _itemsPrototypes)
    {
        if (rand()%100 < d.probability)
        {
            WearableItem w;
            w.create(d.file);
            w.generateRandomItem(d.quality);
            w.setQuantity(d.quantity);
            elem->inventory.insertItem(std::move(w));
        }
    }

    //Force the updating of attributes
    elem->currentHealthStatus(Strength, true);
}

string Species::definitionFile()
{
    return _definitionFile;
}

void Species::loadFromXML(XMLHandle &handle, const string& file)
{
    Type = handle.ToElement()->Attribute("name");
    _definitionFile = file;

    string archiveFile;
    int hue = 0, saturation = 0, luminance = 0, alpha = 255;
    Shader* shader = nullptr;

    XMLElement *elem = handle.FirstChildElement().ToElement();
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "addImageArchiveFile")
        {
            archiveFile = elem->Attribute("file");
            imageManager::addArchiveFile(archiveFile);
        }
        if (elemName == "colorize")
        {
            imageManager::lockGLMutex(10);
            Glsl::Vec3 r, g, b;
            elem->QueryAttribute("rr", &r.x);
            elem->QueryAttribute("rg", &r.y);
            elem->QueryAttribute("rb", &r.z);
            elem->QueryAttribute("gr", &g.x);
            elem->QueryAttribute("gg", &g.y);
            elem->QueryAttribute("gb", &g.z);
            elem->QueryAttribute("br", &b.x);
            elem->QueryAttribute("bg", &b.y);
            elem->QueryAttribute("bb", &b.z);
            int i = devilsai::newColorizeShaderInstance(r, g, b);
            shader = multimedia::shader("colorize", i);
            imageManager::unlockGLMutex(10);
        }
        if (elemName == "changeImagesHSL")
        {
            elem->QueryAttribute("hue", &hue);
            elem->QueryAttribute("saturation", &saturation);
            elem->QueryAttribute("luminance", &luminance);
            elem->QueryAttribute("alpha", &alpha);
        }
        if (elemName == "addSound")
        {
            string name = elem->Attribute("name");
            musicManager::addSound(name);
        }
        if (elemName == "shape")
        {
            size.loadFromXML(elem);
        }
        if (elemName == "viewField")
        {
            viewField.loadFromXML(elem);
        }
        if (elemName == "attackField")
        {
            interactionField.loadFromXML(elem);
        }
        if (elemName == "characteristics" || elemName == "stats")
        {
            commonStats.loadFromXML(elem);
        }
        if (elemName == "properties")
        {
            elem->QueryAttribute("angle", &angle);
            elem->QueryAttribute("diplomacy", &Diplomatie);
            elem->QueryAttribute("lifetime", &lifetime);
            elem->QueryAttribute("experience", &_experience);
            if (elem->Attribute("corpseImageKey"))
                corpseImageKey = elem->Attribute("corpseImageKey");
            if (elem->BoolAttribute("fixedStats"))      _state |= FixedStats;
            if (elem->BoolAttribute("fixedHealing"))    _state |= FixedHealing;
            if (elem->BoolAttribute("maximumEnergy"))   _state |= MaximumEnergy;
            if (elem->BoolAttribute("useOwnersName"))   _state |= UseOwnersName;
            if (elem->BoolAttribute("fixedAngle"))      _state |= FixedAngle;
        }
        if (elemName == "skill")
        {
            string skillName = elem->Attribute("name");
            Skill* s = devilsai::addResource<Skill>(Type + ":" + skillName, Type + ":" + skillName);

            XMLHandle hdl2(elem);
            s->loadFromXML(hdl2, shader, hue, saturation, luminance, alpha);

            _skills.emplace(skillName, s);
        }
        if (elemName == "skillsManagement")
        {
            if (elem->Attribute("randomBehavior"))
                _behaviors[Behaviors::Random] = elem->Attribute("randomBehavior");
            if (elem->Attribute("blocked"))
                _behaviors[Behaviors::Blocked] = elem->Attribute("blocked");
            if (elem->Attribute("hunting"))
            {
                _behaviors[Behaviors::Hunting] = elem->Attribute("hunting");
                _behaviors[Behaviors::LookingForLife] = elem->Attribute("hunting");
            }
            if (elem->Attribute("hurt"))
                _behaviors[Behaviors::Hurt] = elem->Attribute("hurt");
            if (elem->Attribute("dying"))
                _behaviors[Behaviors::Dying] = elem->Attribute("dying");
            if (elem->Attribute("attacks"))
            {
                string total = elem->Attribute("attacks");
                if (total.find(',') == string::npos)
                {
                    _attacks.push_back(total);
                }
                else
                {
                    size_t first = 0, second = total.find(',');
                    while (second != string::npos)
                    {
                        _attacks.push_back(total.substr(first, second - first));
                        first = second + 1;
                        second = total.find(',', first);
                    }
                    _attacks.push_back(total.substr(first));
                }
            }
        }
        if (elemName == "inventory")
        {
            inventory.loadFromXML(elem);
        }
        if (elemName == "itemPrototype")
        {
            string item = str(elem->Attribute("item"));
            int qly = elem->IntAttribute("quality");
            int qty = elem->IntAttribute("quantity", 1);
            int p = elem->IntAttribute("probability", 100);
            ItemPrototype pr {item, qly, qty, p};
            _itemsPrototypes.push_back(std::move(pr));
        }

        elem = elem->NextSiblingElement();
    }

    if ((_state & UseOwnersName) == 0)
        _displayedName = textManager::getText("species", Type);

    imageManager::removeArchiveFile(archiveFile);
}
