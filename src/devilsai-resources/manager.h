
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_MANAGER
#define DEVILSAI_RESOURCES_MANAGER

#include <string>
#include <vector>
#include <unordered_map>
#include <typeinfo>
#include <typeindex>

namespace devilsai{

inline std::unordered_map<std::type_index, std::unordered_map<std::string, void*>> Resources;

template<typename T>
T* getResource(const string& id)
{
    auto i = Resources.find(type_index(typeid(T)));

    if (i != Resources.end())
    {
        auto j = i->second.find(id);
        if (j != i->second.end())
            return reinterpret_cast<T*>(j->second);
    }

    return nullptr;
}

/**
 * Add a new resource of type \a T and return a pointer to it.
 *
 * The resource is constructed by passing \a args to its constructor.
 * The pointer must not be deleted; the resource manager will handle this.
 * However, deleteResources< \a T >() must be called to clean up everything.
 *
 * \tparam T type of the resource to store.
 * \tparam Args... types of the parameters forwarded to the constructor of the resource.
 * \param id identifier that can be used to retrieve the resource.
 * \param args... parameters forwarded to the constructor of the resource (can be empty).
 */
template<typename T, typename... Args>
T* addResource(const string& id, Args&&... args)
{
    T* r = getResource<T>(id);

    if (r == nullptr)
    {
        r = new T(std::forward<Args>(args)...);
        Resources[type_index(typeid(T))].emplace(id, r);
    }

    return r;
}

template<typename T>
std::vector<T*> getAllResources()
{
    auto i = Resources.find(type_index(typeid(T)));

    if (i != Resources.end())
    {
        std::vector<T*> v;
        v.reserve(i->second.size());
        for (auto j : i->second)
            v.push_back(reinterpret_cast<T*>(j.second));

        return v;
    }

    return std::vector<T*>();
}

/**
 * Apply <tt>f.operator()(\a T*)</tt> to each resource whose type is \a T.
 *
 * \remark This function is designed to be used with a lambda function. The lambda must take a \a T* parameter.
 *
 * \tparam T resource type.
 * \tparam Function type of the function object. Its <tt>operator()</tt> must take a \a T* as parameter.
 *         It is not necessary to specify this template argument, the compiler should deduce it.
 * \param f function object to apply to resources.
 */
template<typename T, typename Function>
void forEachResource(Function f)
{
    auto i = Resources.find(type_index(typeid(T)));

    if (i != Resources.end())
    {
        for (auto& j : i->second)
        {
            f(reinterpret_cast<T*>(j.second));
        }
    }
}

template<typename T>
void deleteResources()
{
    auto i = Resources.find(type_index(typeid(T)));

    if (i != Resources.end())
    {
        for (auto& j : i->second)
            delete reinterpret_cast<T*>(j.second);

        Resources.erase(i);
    }
}

}  // namespace devilsai

#endif  // DEVILSAI_RESOURCES_MANAGER
