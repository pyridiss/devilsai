
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "tools/math.h"
#include "tools/timeManager.h"

#include "textManager/textManager.h"

#include "imageManager/imageManager.h"
#include "musicManager/musicManager.h"

#include "devilsai-resources/CheckPoint.h"
#include "devilsai-resources/Environment.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/Place.h"
#include "devilsai-resources/Species.h"
#include "devilsai-resources/StorageBox.h"
#include "devilsai-resources/Trigger.h"

#include "devilsai-gui/console.h"

#include "Carte.h"
#include "../ElementsCarte/ElementsCarte.h"

#include "Jeu/Jeu.h"

using namespace tinyxml2;


/** FONCTIONS DE LA CLASSE Carte **/

Carte::Carte(string id)
  : elements(),
    collidingItems(),
    onScreenItems(),
    _restartClock(false),
    places(),
    paths(),
    Id(std::move(id)),
    dataFiles(),
    commonDataLoaded(false),
    _stopManagement(false),
    _clock()
{
}

Carte::~Carte()
{
	for (auto& element : elements)
		delete element;

    for (auto& tmp : places)
        delete tmp;

    elements.clear();
    collidingItems.clear();
}

void Carte::insertItem(Unit *elem)
{
    if (elem == nullptr) return;

    elements.push_back(elem);
    onScreenItems.push_back(elem);

    if (!elem->unshaped())
        collidingItems.push_back(elem);

    Individu* i = dynamic_cast<Individu*>(elem);
    if (i != nullptr)
        i->setWorld(this);
}

void Carte::removeItem(Unit *elem)
{
    if (elem == nullptr) return;

    elements.remove(elem);
    onScreenItems.remove(elem);

    if (!elem->unshaped())
    {
        auto i = std::find(collidingItems.begin(), collidingItems.end(), elem);
        if (i != collidingItems.end())
            collidingItems.erase(i);
    }
}

CheckPoint* Carte::addCheckPoint(string liste, int x, int y)
{
    CheckPoint *ind = new CheckPoint;

    ind->setTag(liste);

    ind->move(x, y);

    insertItem(ind);
	return ind;
}

StorageBox* Carte::AjouterCoffre(string liste, int x, int y)
{
    StorageBox *ind = new StorageBox;

    ind->setTag(liste);

    ind->move(x, y);

    insertItem(ind);
	return ind;
}

void Carte::SupprimerListe(string num)
{
    for (auto& tmp : elements)
        if (tmp->tag() == num) tmp->destroy();
}

void Carte::stopManagement()
{
    _stopManagement = true;
}

void Carte::resetClock()
{
    _restartClock = true;
}

void Carte::addUtterance(UnitId s, const textManager::PlainText& t)
{
    size_t hash = std::hash<std::string>{}(t.toStdString());

    auto tmp = _utterances.before_begin(), i = tmp, e = _utterances.end();
    double pos = -30.0;

    while (++tmp != e)
    {
        if (tmp->speaker == s && tmp->hash == hash)  // Already displayed, break
            return;

        if (tmp->speaker == s)
            pos -= tmp->utterance.height() + 10.0;
        i = tmp;
    }

    multimedia::RichText r;
    r.setSize(200, 0);
    r.setDefaultFormatting("liberation-bold", 12, Color(255, 255, 255));
    r.addFlags(textManager::OriginXCenter | textManager::HAlignCenter | textManager::OriginBottom | textManager::Shaded);
    r.create(t);

    Utterance u {std::move(r), hash, 10, pos, s};

    _utterances.insert_after(i, std::move(u));

    // Send the utterance to the console

    Character* c = dynamic_cast<Character*>(s.unit());
    textManager::PlainText consoleText;
    consoleText += "@s[12]";
    if (c)
    {
        consoleText += c->displayedName();
        consoleText += textManager::getText("devilsai", "console-opening-quotation-mark");
    }
    consoleText += t;
    if (c) consoleText += textManager::getText("devilsai", "console-closing-quotation-mark");
    consoleText += " @d";

    addConsoleEntry(consoleText);
}

pair<Unit*, int> Carte::findFirstCollidingItem(Unit* unit, const tools::math::Shape& shape, bool apply)
{
    for (auto& item : collidingItems)
    {
        if (item == unit) continue;
        if (tools::math::intersection(shape, item->shape()))
        {
            int result = item->collision(unit, apply);
            if (result != COLL_OK)
                return pair<Unit*, int>(item, result);
        }
    }
    for (Joueur* p : devilsai::getAllResources<Joueur>())
    {
        if (p->world() == this)
        {
            if (p == unit) continue;
            if (tools::math::intersection(shape, p->shape()))
            {
                int result = p->collision(unit, apply);
                if (result != COLL_OK)
                    return pair<Unit*, int>(p, result);
            }
        }
    }

    return pair<Unit*, int>(nullptr, 0);
}

vector<pair<Unit*, int>> Carte::findAllCollidingItems(Unit* unit, const tools::math::Shape& shape, bool apply)
{
    vector<pair<Unit*, int>> v;

    for (auto& item : collidingItems)
    {
        if (item == unit) continue;
        if (tools::math::intersection(shape, item->shape()))
        {
            int result = item->collision(unit, apply);
            if (result != COLL_OK)
                v.push_back(pair<Unit*, int>(item, result));
        }
    }
    devilsai::forEachResource<Joueur>([&](Joueur *p)
    {
        if (p->world() == this)
        {
            if (p == unit) return;
            if (tools::math::intersection(shape, p->shape()))
            {
                int result = p->collision(unit, apply);
                if (result != COLL_OK)
                    v.push_back(pair<Unit*, int>(p, result));
            }
        }
    });

    return v;
}

bool comparisonBetweenElements(Unit* a, Unit* b)
{
	/* Classement des Élements selon leur TypeClassement
	 * - CLASSEMENT_SOL
	 * - CLASSEMENT_CADAVRE
	 * - CLASSEMENT_NORMAL & CLASSEMENT_HAUT
	 * - CLASSEMENT_NUAGE
	*/
    int aClass = a->height();
    if (aClass == CLASSEMENT_HAUT) aClass = CLASSEMENT_NORMAL;
    int bClass = b->height();
    if (bClass == CLASSEMENT_HAUT) bClass = CLASSEMENT_NORMAL;

	if (aClass < bClass) return true;
	if (aClass > bClass) return false;

	if (a->position().y >= b->position().y) return false;

	return true;
}

void Carte::GestionElements(const View& worldView)
{
    vector<Unit*> deads;

    double max = worldView.getSize().x + 1500;
    _stopManagement = false;

    for (auto& tmp : elements)
    {
        if (abs(tmp->position().x - (double)worldView.getCenter().x) <= max &&
            abs(tmp->position().y - (double)worldView.getCenter().y) <= max)
        {
            tmp->manage();
        }
        if (tmp->destroyed()) deads.push_back(tmp);
        if (_stopManagement) break;
    }

    if (_clock.getElapsedTime().asMilliseconds() > 200 || _restartClock)
    {
        double onScreenWidth = worldView.getSize().x + 300;
        onScreenItems.clear();

        devilsai::forEachResource<Joueur>([this](Joueur* p)
        {
            onScreenItems.push_back(p);
        });

        for (auto& tmp : elements)
        {
            if (abs(tmp->position().x - (double)worldView.getCenter().x) <= onScreenWidth &&
                abs(tmp->position().y - (double)worldView.getCenter().y) <= onScreenWidth)
            {
                onScreenItems.push_back(tmp);
            }
        }

        onScreenItems.sort(comparisonBetweenElements);

        _clock.restart();
        _restartClock = false;
    }

    for (auto d : deads)
    {
        removeItem(d);
        removeUnitId(d->id());

        delete d;
    }
}

void Carte::display(RenderTarget& target)
{
    if (!backgroundImage.empty())
    {
        Vector2f origin = target.getView().getCenter();
        origin.x -= target.getSize().x / 2;
        origin.y -= target.getSize().y / 2;

        imageManager::fillArea(target, "paysage"_hash, backgroundImage, origin.x, origin.y, target.getSize().x, target.getSize().y, 0, 0);
    }

    for (auto& tmp : onScreenItems)
    {
        tmp->display(target);
    }

    for (auto& tmp : _utterances)
    {
        if (!tmp.speaker)
        {
            tmp.duration = 0;
            continue;
        }
        tmp.utterance.display(target, tmp.speaker->position().x, tmp.speaker->position().y + tmp.position);
        tmp.duration -= tools::timeManager::I(1.0/60.0);
    }

    while (_utterances.begin() != _utterances.end() && _utterances.begin()->duration <= 0)
    {
        for (auto& tmp : _utterances)
        {
            if (tmp.speaker == _utterances.begin()->speaker)
            {
                tmp.position += _utterances.begin()->utterance.height() + 10;
            }
        }
        _utterances.pop_front();
    }
}

Unit* Carte::createNewItem(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties)
{
    Unit* newItem = nullptr;

    string itemName = unit->Name();
    if (itemName == "inertItem" || itemName == "environment")
        newItem = new Environment;
    else if (itemName == "individual" || itemName == "character")
        newItem = new Individu;
    else if (itemName == "storageBox")
        newItem = new StorageBox;
    else if (itemName == "trigger")
        newItem = new Trigger;

    if (newItem == nullptr)
    {
        tools::debug::error("Unable to load unit.", "files", __FILENAME__, __LINE__);
        return nullptr;
    }

    newItem->loadFromXML(unit, properties);

    return newItem;
}

void Carte::loadFromFile(string path, string tag)
{
    XMLDocument file;
    file.LoadFile(path.c_str());

    XMLHandle hdl(file);
    XMLElement *elem = hdl.FirstChildElement().FirstChildElement().ToElement();

    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "properties")
        {
            if (elem->Attribute("backgroundImage"))
            {
                backgroundImage = elem->Attribute("backgroundImage");
                imageManager::addImage(tools::hash("paysage"), backgroundImage, backgroundImage);
            }
        }
        if (elemName == "loadDataFile" && !commonDataLoaded)
        {
            string dataFile = elem->Attribute("file");
            loadGameDataFromXML(tools::filesystem::dataDirectory(), dataFile);
            dataFiles.insert(std::move(dataFile));
        }
        if (elemName == "place" && !commonDataLoaded)
        {
            Place *p = new Place;
            p->loadFromXML(elem, nullptr);
            places.push_back(p);
        }
        if (elemName == "path" && !commonDataLoaded)
        {
            pair<tools::math::Vector2d, tools::math::Shape> newPath;

            elem->QueryAttribute("x", &newPath.first.x);
            elem->QueryAttribute("y", &newPath.first.y);

            XMLHandle hdl2(elem);
            XMLElement *elem2 = hdl2.FirstChildElement().ToElement();
            while (elem2)
            {
                string elem2Name = elem2->Name();

                if (elem2Name == "shape")
                {
                    newPath.second.loadFromXML(elem2);
                    newPath.second.setOrigin(&newPath.first);
                }
                if (elem2Name == "inertItem")
                {
                    double repetitionStep = 1;
                    elem2->QueryAttribute("repetition-step", &repetitionStep);

                    if (!newPath.second.isLine() && !newPath.second.isPolyline())
                        break;

                    double createdItems = 0;
                    vector<tools::math::Vector2D<double>> points = newPath.second.pointsOfLine();
                    tools::math::Vector2d currentVector = points[1] - points[0];
                    double currentAngle = tools::math::angle(currentVector.x, currentVector.y);
                    double lengthOnCurrentVector = -repetitionStep;
                    unsigned int index = 1;

                    while (createdItems * repetitionStep < newPath.second.lengthOfLine())
                    {
                        Unit* newItem = createNewItem(elem2, nullptr);

                        if (newItem == nullptr)
                        {
                            tools::debug::error("Error while creating a path.", "files", __FILENAME__, __LINE__);
                            break;
                        }

                        newItem->move(-newItem->position().x, -newItem->position().y);

                        if (currentVector.length() > lengthOnCurrentVector + repetitionStep)
                        {
                            newItem->move(points[index-1].x, points[index-1].y);
                            newItem->move(lengthOnCurrentVector * cos(currentAngle), lengthOnCurrentVector * sin(currentAngle));
                            newItem->move(repetitionStep * cos(currentAngle), repetitionStep * sin(currentAngle));
                            lengthOnCurrentVector += repetitionStep;
                        }
                        else
                        {
                            lengthOnCurrentVector += repetitionStep - currentVector.length();
                            ++index;
                            if (index >= points.size()) break;
                            currentVector = points[index] - points[index-1];

                            while (currentVector.length() <= lengthOnCurrentVector)
                            {
                                lengthOnCurrentVector -= currentVector.length();
                                ++index;
                                if (index >= points.size()) break;
                                currentVector = points[index] - points[index-1];
                            }

                            if (index >= points.size()) break;

                            currentAngle = tools::math::angle(currentVector.x, currentVector.y);
                            newItem->move(points[index-1].x, points[index-1].y);
                            newItem->move(lengthOnCurrentVector * cos(currentAngle), lengthOnCurrentVector * sin(currentAngle));
                        }

                        insertItem(newItem);

                        ++createdItems;
                    }
                }

                elem2 = elem2->NextSiblingElement();
            }

            paths.push_back(std::move(newPath));
        }
        else if (elemName == "items")
        {
            if (tag == "ALL" || (elem->Attribute("tag") && tag == elem->Attribute("tag")))
            {
                XMLHandle hdl2(elem);
                XMLElement *item = hdl2.FirstChildElement().ToElement();
                while (item)
                {
                    insertItem(createNewItem(item, elem));

                    item = item->NextSiblingElement();
                }
            }
        }
        else if (elemName == "randomZone")
        {
            XMLElement* properties = elem;

            if (tag != "ALL" && (elem->Attribute("tag") == 0 || elem->Attribute("tag") != tag))
            {
                elem = elem->NextSiblingElement();
                continue;
            }

            tools::math::Shape* zone = nullptr;
            XMLHandle hdl2(elem);
            XMLElement *item = hdl2.FirstChildElement().ToElement();

            //We create a list in which all genereated items are stored. This way, new items will not collide
            //one another. If this behavior is not wanted, the shape must be forced to none in the XML file.
            list<Unit*> itemsList;

            //The zone can be defined from a place in the current world.
            if (elem->Attribute("place"))
            {
                string placeName = elem->Attribute("place");
                for (auto & p : places)
                {
                    if (p->name() == placeName)
                    {
                        zone = new tools::math::Shape(p->shape());
                        break;
                    }
                }
            }
            else if (elem->Attribute("customZone"))
            {
                if (strcmp(item->Name(), "shape") == 0)
                {
                    zone = new tools::math::Shape();
                    zone->loadFromXML(item);
                }
                item = item->NextSiblingElement();
            }

            if (zone == nullptr)
            {
                tools::debug::error("A randomZone has been defined without a valid zone", "files", __FILENAME__, __LINE__);
                elem = elem->NextSiblingElement();
                continue;
            }

            pair<tools::math::Vector2d, tools::math::Vector2d> box = zone->boundingBox();

            while (item)
            {
                double quantity = 0;
                if (item->Attribute("density"))
                {
                    item->QueryAttribute("density", &quantity);
                    quantity *= zone->area() / 1000000.0;
                }
                if (item->Attribute("quantity"))
                {
                    item->QueryAttribute("quantity", &quantity);
                }

                int counter = 0;

                while (counter < quantity)
                {
                    Unit* newItem = createNewItem(item, properties);

                    if (newItem == nullptr)
                    {
                        tools::debug::error("Error while creating a randomZone.", "files", __FILENAME__, __LINE__);
                        break;
                    }

                    int debugCounter = 0;
                    do
                    {
                        ++debugCounter;

                        //Set a new random position
                        newItem->move(-newItem->position().x, -newItem->position().y);
                        double x = box.first.x + (box.second.x - box.first.x)/10000.0 * (double)(rand()%10000);
                        double y = box.first.y + (box.second.y - box.first.y)/10000.0 * (double)(rand()%10000);
                        newItem->move(x, y);

                        //1. The item must collide with the zone
                        if (!tools::math::intersection(newItem->shape(), *zone)) continue;

                        //2. The item must not collide with paths
                        bool c = false;
                        for (auto& p : paths)
                        {
                            if (tools::math::intersection(newItem->shape(), p.second))
                                c = true;
                        }
                        if (c) continue;

                        //3. The item must not collide with other items already added
                        for (auto& i : itemsList)
                        {
                            if (tools::math::intersection(newItem->shape(), i->shape()))
                                c = true;
                        }
                        if (c) continue;

                        //4. The item must not collide with anything else
                        if (findFirstCollidingItem(newItem, newItem->shape(), false).first == nullptr)
                            break;
                    }
                    while (debugCounter < 100);

                    if (debugCounter == 100 && item->Attribute("quantity"))
                        tools::debug::error("Error while creating randomZone: no place left for " + newItem->type(), "files", __FILENAME__, __LINE__);

                    if (debugCounter < 100)
                        itemsList.push_back(newItem);
                    else delete newItem;

                    ++counter;
                }

                item = item->NextSiblingElement();
            }

            if (zone != nullptr)
                delete zone;

            for (auto& i : itemsList)
                insertItem(i);
        }
        elem = elem->NextSiblingElement();
    }

    commonDataLoaded = true;
}

void Carte::saveToXML(XMLDocument& doc, XMLHandle& handle)
{
    XMLElement* root = handle.ToElement();

    XMLElement* properties = doc.NewElement("properties");
    if (!backgroundImage.empty())
        properties->SetAttribute("backgroundImage", backgroundImage.c_str());
    root->InsertEndChild(properties);

    // dataFiles should contain the files needed for the species of all individuals
    // in the world. However, some individuals may change world or create other
    // individuals which definition is not in these files.
    //  dataFiles will be updated to be sure all species will be loaded.
    for (auto& tmp : elements)
    {
        Individu* ind = dynamic_cast<Individu*>(tmp);
        if (ind != nullptr && ind->species() != nullptr)
            dataFiles.insert(ind->species()->definitionFile());
    }

    for (auto& tmp : dataFiles)
    {
        XMLElement* dataFile = doc.NewElement("loadDataFile");
        dataFile->SetAttribute("file", tmp.c_str());
        root->InsertEndChild(dataFile);
    }

    for (auto& tmp : places)
    {
        tmp->saveToXML(doc, handle);
    }

    for (auto& tmp : paths)
    {
        XMLElement* path = doc.NewElement("path");
        path->SetAttribute("x", tmp.first.x);
        path->SetAttribute("y", tmp.first.y);

        XMLElement* pathShape = doc.NewElement("shape");
        path->InsertEndChild(pathShape);
        XMLHandle shapeHandle(pathShape);
        tmp.second.saveToXML(doc, shapeHandle);

        root->InsertEndChild(path);
    }

    XMLElement* _items = doc.NewElement("items");
    root->InsertEndChild(_items);
    XMLHandle itemsHandle(_items);

    for (auto& tmp : elements)
    {
        tmp->saveToXML(doc, itemsHandle);
    }
}
