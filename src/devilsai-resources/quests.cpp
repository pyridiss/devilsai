
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/quests.h"

#include <lua.hpp>
#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/signals.h"
#include "tools/filesystem.h"
#include "tools/math.h"
#include "tools/timeManager.h"

#include "imageManager/imageManager.h"

#include "textManager/textManager.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/StorageBox.h"

#include "../Bibliotheque/luaFunctions.h"
#include "../ElementsCarte/ElementsCarte.h"
#include "Jeu/options.h"

#include "devilsai-gui/journal.h"
#include "devilsai-gui/console.h"


using namespace tinyxml2;

Quest::Quest(const string& newQuest, const char* args)
  : file(newQuest),
    L(luaL_newstate()),
    done(false)
{
	luaL_openlibs(L);

	luaL_dofile(L, (tools::filesystem::dataDirectory() + file).c_str());

    lua_atpanic(L, [](lua_State* S)
    {
        tools::debug::error(lua_tostring(S, -1), "lua", __FILENAME__, __LINE__);
        return 0;
    });

    //Check if the file is well-formed

    bool fileComplete = true;

    auto check = [&](const char* f)
    {
        lua_getglobal(L, f);
        if (lua_isnil(L, -1))
        {
            tools::debug::error("The quest '" + file + "' does not define the symbol '" + string(f) + "'", "lua", __FILENAME__, __LINE__);
            fileComplete = false;
        }
        lua_pop(L, 1);
    };

    check("questBegin");
    check("questManage");
    check("questIsDone");
    check("questSave");
    check("questRecoverState");
    check("questEnd");

    if (!fileComplete)
    {
        lua_close(L);
        done = true;  // Will disable the lua calls
        return;
    }

    lua_register(L, "addQuest", [](lua_State* S)
    {
        devilsai::addResource<Quest>(lua_tostring(S, 1), lua_tostring(S, 1), "true");
        return 0;
    });

    lua_register(L, "toBlack", [](lua_State* S)
    {
        tools::debug::warning("lua function 'toBlack' is not implemented.", "lua", __FILENAME__, __LINE__);
        return 0;
    });

    lua_register(L, "gameSuccessfullyEnded", [](lua_State* S)
    {
        tools::signals::addSignal("game-successfully-ended");
        tools::signals::addSignal("new-game");
        return 0;
    });

    lua_register(L, "enableCinematics", [](lua_State* S)
    {
        options::addOption<bool>(tools::hash("cinematic-mode"), lua_toboolean(S, 1));
        return 0;
    });

    lua_register(L, "addConsoleEntry", [](lua_State* S)
    {
        textManager::PlainText t = textManager::getText(str(lua_tostring(S, 1)), str(lua_tostring(S, 2)));
        int numberOfParameters = lua_tonumber(S, 3);
        for (int i = 0 ; i < numberOfParameters ; ++i)
        {
            if (lua_isnumber(S, 4 + i))
                t.addParameter(lua_tonumber(S, 4 + i));
            else if (lua_isstring(S, 4 + i))
                t.addParameter(str(lua_tostring(S, 4 + i)));
            else if (lua_islightuserdata(S, 4 + i))
            {
                textManager::PlainText* u = static_cast<textManager::PlainText*>(lua_touserdata(S, 4 + i));
                t.addParameter(*u);
            }
        }

        addConsoleEntry(t);

        return 0;
    });

    lua_register(L, "addJournalEntry", [](lua_State* S)
    {
        addJournalEntry(str(lua_tostring(S, 1)), str(lua_tostring(S, 2)), str(lua_tostring(S, 3)), true);
        return 0;
    });

    lua_register(L, "Character__addExperience", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        if (character) character->gainExperience(lua_tonumber(S, 2));
        return 0;
    });

    lua_register(L, "Character__addSkillLevel", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        if (character) character->addSkillLevel(str(lua_tostring(S, 2)), lua_tonumber(S, 3));
        return 0;
    });

    lua_register(L, "Character__currentHealthStatus", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        string_view attribute = lua_tostring(S, 2);
        Attribute a = stringToAttribute(attribute);
        lua_pushnumber(S, (character ? character->currentHealthStatus(a) : 0));
        return 1;
    });

    lua_register(L, "Character__displayedName", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        lua_pushlightuserdata(S, (character) ? (void*)&(character->displayedName()) : 0);
        return 1;
    });

    lua_register(L, "Character__numberOfEnemiesInViewField", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        lua_pushnumber(S, (character) ? character->numberOfEnemiesInViewField() : 0);
        return 1;
    });

    lua_register(L, "Character__interact", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        Unit* unit = UnitId(lua_tonumber(S, 2)).unit();

        lua_pushboolean(S, (character && unit) ? character->interact(unit) : false);

        return 1;
    });

    lua_register(L, "Character__possesses", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        string item = lua_tostring(S, 2);
        lua_pushboolean(S, (character) ? character->possesses(item) : false);
        return 1;
    });

    lua_register(L, "Character__setAttribute", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        string_view attribute = lua_tostring(S, 2);
        double value = lua_tonumber(S, 3);
        Attribute a = stringToAttribute(attribute);
        if (character) character->setAttribute(a, value);
        return 0;
    });

    lua_register(L, "Character__world", [](lua_State* S)
    {
        Character* character = to<Character>(lua_tonumber(S, 1));
        lua_pushstring(S, (character) ? character->world()->Id.c_str() : "");
        return 1;
    });

    lua_register(L, "collision", [](lua_State* S)
    {
        UnitId u1 (lua_tonumber(S, 1));
        UnitId u2 (lua_tonumber(S, 2));
        lua_pushboolean(S, (u1 && u2) ? tools::math::intersection(u1->shape(), u2->shape()) : false);
        return 1;
    });

    lua_register(L, "deleteTaggedItems", [](lua_State* S)
    {
        Carte* w = devilsai::getResource<Carte>(lua_tostring(S, 1));
        if (w != nullptr)
            w->SupprimerListe(lua_tostring(S, 2));

        return 0;
    });

    lua_register(L, "getNumberOfItemsByTag", [](lua_State* S)
    {
        string world = lua_tostring(S, 1);
        string tag = lua_tostring(S, 2);

        int n = 0;

        Carte* w_ptr = devilsai::getResource<Carte>(world);

        if (w_ptr != nullptr)
        {
            for (auto& i : w_ptr->elements)
            {
                if (i->tag() == tag)
                    ++n;
            }
        }

        lua_pushnumber(S, n);
        return 1;
    });

    lua_register(L, "getTimeElapsed", [](lua_State* S)
    {
        long old = lua_tonumber(S, 1);
        lua_pushnumber(S, tools::timeManager::timeElapsed() - old);
        return 1;
    });

    lua_register(L, "handOverItem", [](lua_State* S)
    {
        Character* c1 = to<Character>(lua_tonumber(S, 1));
        Character* c2 = to<Character>(lua_tonumber(S, 2));

        string object = str(lua_tostring(S, 3));

        if (!c1 || !c2) return 0;

        WearableItem tmp = c1->inventory.pickItem(object);
        if (!tmp.valid()) return 0;

        if (!c2->inventory.insertItem(std::move(tmp)))
        {
            StorageBox *sack = c2->world()->AjouterCoffre("storage-boxes", c2->position().x, c2->position().y);
            sack->sack();
            sack->insertItem(std::move(tmp));
        }

        return 0;
    });

    lua_register(L, "questRunning", [](lua_State* S)
    {
        Quest* q = devilsai::getResource<Quest>(lua_tostring(S, 1));
        bool result = (q != nullptr && !q->done);

        lua_pushboolean(S, result);
        return 1;
    });

    lua_register(L, "resetTimer", [](lua_State* S)
    {
        lua_pushnumber(S, tools::timeManager::timeElapsed());
        return 1;
    });

    lua_register(L, "World__addUtterance", [](lua_State* S)
    {
        World* world = devilsai::getResource<World>(str(lua_tostring(S, 1)));
        UnitId unit(lua_tonumber(S, 2));
        if (!world || !unit) return 0;

        textManager::PlainText t = textManager::getText(str(lua_tostring(S, 3)), str(lua_tostring(S, 4)));
        int numberOfParameters = lua_tonumber(S, 5);
        for (int i = 0 ; i < numberOfParameters ; ++i)
        {
            if (lua_isnumber(S, 6 + i))
                t.addParameter(lua_tonumber(S, 6 + i));
            else if (lua_isstring(S, 6 + i))
                t.addParameter(str(lua_tostring(S, 6 + i)));
            else if (lua_islightuserdata(S, 6 + i))
            {
                textManager::PlainText* u = static_cast<textManager::PlainText*>(lua_touserdata(S, 6 + i));
                t.addParameter(*u);
            }
        }

        world->addUtterance(unit, t);

        return 0;
    });

	lua_register(L, "cout", LUA_cout);
	lua_register(L, "pushDialog", LUA_pushDialog);
	lua_register(L, "dialogDisplayed", LUA_dialogDisplayed);
    lua_register(L, "loadWorld", LUA_loadWorld);
	lua_register(L, "addCheckPoint", LUA_addCheckPoint);

	lua_getglobal(L, "questBegin");
	lua_pushstring(L, args);
	lua_call(L, 1, 0);
}

Quest::~Quest()
{
    lua_close(L);
}

void Quest::manage()
{
    if (done) return;

    lua_getglobal(L, "questManage");
    lua_call(L, 0, 0);

    lua_getglobal(L, "questIsDone");
    lua_call(L, 0, 1);

    done = lua_toboolean(L, -1);
    lua_pop(L, 1);

    if (done)
    {
        lua_getglobal(L, "questEnd");
        lua_call(L, 0, 0);
    }
}

void Quest::save(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement* root)
{
    if (done) return;

    XMLElement* quest = doc.NewElement("quest");
    quest->SetAttribute("file", file.c_str());
    quest->SetAttribute("initialData", "false");

    lua_getglobal(L, "questSave");
    lua_call(L, 0, 1);
    quest->SetAttribute("currentState", lua_tostring(L, -1));

    root->InsertEndChild(quest);
}

void Quest::load(tinyxml2::XMLElement* elem)
{
    if (elem->Attribute("currentState"))
    {
        lua_getglobal(L, "questRecoverState");
        lua_pushstring(L, elem->Attribute("currentState"));
        lua_call(L, 1, 0);
    }
}
