
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef header_carte
#define header_carte

#include <string>
#include <forward_list>
#include <list>
#include <vector>
#include <set>

#include <SFML/Graphics.hpp>

#include <tools/shape.h>
#include <tools/vector2d.h>

#include "multimedia/RichText.h"

#include "devilsai-resources/Unit.h"
#include "devilsai-resources/UnitId.h"

using namespace std;
using namespace sf;

namespace tinyxml2{
    class XMLElement;
    class XMLHandle;
    class XMLDocument;
}

class Place;
class CheckPoint;
class StorageBox;


class Carte
{
    private:
        struct Utterance
        {
            multimedia::RichText utterance {};
            size_t hash {};
            double duration {};
            double position {};
            UnitId speaker {};
        };

    public:
        list<Unit*> elements;

        /*
         * collidingItems will store items which can collide with others.
         * As a vector, it will be more efficient than a list to search for collisions.
         * Moreover, it will avoid take time to check if an item has 'ignoreCollision' set or not.
         */
    private:
        vector <Unit*> collidingItems;
        list <Unit*> onScreenItems;
        forward_list<Utterance> _utterances {};
        bool _restartClock;

    public:
        vector<Place*> places;
        list <pair<tools::math::Vector2d, tools::math::Shape>> paths;
        string Id;
		string backgroundImage = "";
        set <string> dataFiles;
        bool commonDataLoaded;

    private:
        /*
         * _stopManagement must be set when elements are modified outside this class.
         */
        bool _stopManagement;
        Clock _clock;

	//Constructeurs / Destructeurs :
	public:
        Carte(string id);
		~Carte();

	public:
        void insertItem(Unit *elem);
        void removeItem(Unit *elem);
        CheckPoint* addCheckPoint(string liste, int x, int y);
		StorageBox* AjouterCoffre(string liste, int x, int y);

        Unit* createNewItem(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);

		void SupprimerListe(string num);
        void stopManagement();
        void resetClock();

        /**
         * \remark addUterrance() does NOT check if the unit is really in the world. You have to take care of that.
         */
        void addUtterance(UnitId s, const textManager::PlainText& t);

        void loadFromFile(string path, string tag="ALL");
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);

	//Gestion :
	public:
        void GestionElements(const View& worldView);
        void display(RenderTarget& target);

	//Collisions manager
	public:
        pair<Unit*, int> findFirstCollidingItem(Unit* unit, const tools::math::Shape& shape, bool apply);
        vector<pair<Unit*, int>> findAllCollidingItems(Unit* unit, const tools::math::Shape& shape, bool apply);
};

using World = Carte;  // For a further change of name

#endif
