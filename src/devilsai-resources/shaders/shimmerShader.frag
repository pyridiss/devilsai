uniform sampler2D texture;
uniform float value;  // 1000.0 -> 0.0

void main()
{
    float TL, TR, BL, BR;

    if (value > 800.0)       // 1000.0 -> 800.0
    {
        TL = (1000.0 - value) / 200.0;       // 0.0 -> 1.0
        TR = (1000.0 - value) / 1000.0;      // 0.0 -> 0.2
        BL = (1000.0 - value) / 500.0;       // 0.0 -> 0.4
        BR = (1000.0 - value) / 2000.0;      // 0.0 -> 0.1
    }
    else if (value > 500.0)  // 800.0 -> 500.0
    {
        TL = value / 800.0;                  // 1.0 -> 0.625
        TR = 0.2 + (800.0 - value) / 500.0;  // 0.2 -> 0.8
        BL = 0.4;                            // 0.4 -> 0.4
        BR = 0.1 + (800.0 - value) / 400.0;  // 0.1 -> 0.85
    }
    else                     // 500.0 -> 0.0
    {
        TL = 0.625 * value / 500.0;          // 0.625 -> 0.0
        TR = 0.8 * value / 500.0;            // 0.8 -> 0.0
        BL = 0.4 * value / 500.0;            // 0.4 -> 0.0
        BR = 0.85 * value / 500.0;           // 0.85 -> 0.0
    }

    float x = gl_FragCoord.x / 1024.0;
    float y = gl_FragCoord.y / 768.0;

    float alpha = (TL * (pow(x, 2) + pow(y, 2)) + TR * (pow((1.0-x), 2) + pow(y, 2))
                  + BL * (pow(x, 2) + pow((1.0-y), 2)) + BR * (pow((1.0-x), 2) + pow((1.0-y), 2))) / 1.5;

    if (alpha > 1.0) alpha = 1.0;

    gl_FragColor = vec4(texture2D(texture, gl_TexCoord[0].st).xyz, alpha);
}
