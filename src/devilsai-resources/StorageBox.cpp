
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/StorageBox.h"

#include <tinyxml2.h>

#include "tools/timeManager.h"
#include "tools/math.h"
#include "textManager/textManager.h"
#include "imageManager/imageManager.h"

#include "devilsai-resources/shaders.h"


enum StorageBox_State
{
    Unshaped = 1 << 0,
    Inert = 1 << 1,
    Destroyed = 1 << 2,
    CloseWhenEmpty = 1 << 3,
    RemoveWhenEmpty = 1 << 4,
    Highlight = 1 << 5
};


StorageBox::StorageBox()
  : _id(this, 0)
{
    _shape.setOrigin(&_position);
    _objects.setType(Pack::Type::StorageBox);
}

const UnitId& StorageBox::id() const
{
    return _id;
}

const tools::math::Vector2d& StorageBox::position() const
{
    return _position;
}

const tools::math::Shape& StorageBox::shape() const
{
    return _shape;
}

bool StorageBox::unshaped() const
{
    return _state & Unshaped;
}

int StorageBox::height() const
{
    return _height;
}

const string& StorageBox::type() const
{
    return _type;
}

const string& StorageBox::tag() const
{
    return _tag;
}

void StorageBox::setTag(const string& t)
{
    _tag = t;
}

const textManager::PlainText& StorageBox::name()
{
    return _name;
}

int StorageBox::diplomacy() const
{
    return 0;
}

Pack& StorageBox::objects()
{
    return _objects;
}

void StorageBox::manage()
{
    if (_lifetime > 0)
    {
        _lifetime -= tools::timeManager::I(1);
        _lifetime = max(_lifetime, 0.0);
    }
}

void StorageBox::move(double x, double y)
{
    _position.x += x;
    _position.y += y;
}

bool StorageBox::destroyed() const
{
    return (_lifetime == 0);
}

void StorageBox::destroy()
{
    _lifetime = 0;
}

int StorageBox::collision(Unit* elem, [[maybe_unused]] bool apply)
{
    if (elem != nullptr && elem->tag() == "intern")
    {
        if (_state & Inert) return COLL_OK;
        else return COLL_INTER;
    }

    if (_height == CLASSEMENT_NORMAL)
        return COLL_PRIM;

    return COLL_OK;
}

void StorageBox::loadFromXML(tinyxml2::XMLElement* elem, tinyxml2::XMLElement* properties)
{
    using namespace tinyxml2;

    if (elem->Attribute("design"))
        _type = elem->Attribute("design");

    move(elem->DoubleAttribute("x"), elem->DoubleAttribute("y"));

    if (elem->Attribute("tag"))
        _tag = elem->Attribute("tag");
    else if (properties && properties->Attribute("tag"))
        _tag = properties->Attribute("tag");

    if (properties && properties->BoolAttribute("ignoreCollision"))
        _state |= Unshaped;

    if (properties && properties->BoolAttribute("immuable"))
            _height = CLASSEMENT_CADAVRE;

    elem = elem->FirstChildElement();
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "shape")
        {
            _shape.loadFromXML(elem);
        }
        if (elemName == "properties")
        {
            _id.replace(this, elem->UnsignedAttribute("id", 0));
            elem->QueryAttribute("lifetime", &_lifetime);
            if (elem->BoolAttribute("inert"))
                _state |= Inert;
            if (elem->BoolAttribute("ignoreCollision"))
                _state |= Unshaped;
            _height = elem->UnsignedAttribute("classement", _height);
            if (elem->Attribute("imageContainer"))
                _imageContainer = elem->Attribute("imageContainer");
            if (elem->Attribute("name"))
                _name = textManager::getText("species", elem->Attribute("name"));
            if (elem->Attribute("inlineName"))
                _name = textManager::fromStdString(elem->Attribute("inlineName"));
            if (elem->BoolAttribute("closeWhenEmpty"))
                _state |= CloseWhenEmpty;
        }
        if (elemName == "inventory")
        {
            _objects.loadFromXML(elem);
        }

        elem = elem->NextSiblingElement();
    }
}

void StorageBox::saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle)
{
    using namespace tinyxml2;

    XMLElement* root = handle.ToElement();

    XMLElement* storageBox = doc.NewElement("storageBox");
    storageBox->SetAttribute("design", _type.c_str());
    storageBox->SetAttribute("x", _position.x);
    storageBox->SetAttribute("y", _position.y);
    storageBox->SetAttribute("tag", _tag.c_str());

    XMLElement* shape = doc.NewElement("shape");
    storageBox->InsertEndChild(shape);
    XMLHandle shapeHandle(shape);
    _shape.saveToXML(doc, shapeHandle);

    XMLElement* properties = doc.NewElement("properties");
    properties->SetAttribute("id", _id.id());
    properties->SetAttribute("lifetime", _lifetime);
    properties->SetAttribute("inert", (_state & Inert));
    if (unshaped()) properties->SetAttribute("ignoreCollision", true);
    properties->SetAttribute("classement", (unsigned)_height);
    properties->SetAttribute("imageContainer", _imageContainer.c_str());
    properties->SetAttribute("inlineName", _name.toStdString().c_str());
    properties->SetAttribute("closeWhenEmpty", (_state & CloseWhenEmpty));
    storageBox->InsertEndChild(properties);

    XMLHandle hdl(storageBox);
    _objects.saveToXML(doc, hdl);

    root->InsertEndChild(storageBox);
}

void StorageBox::display(sf::RenderTarget& target)
{
    if (!_imageContainer.empty() && !_type.empty())
    {
        sf::Shader* shader = (_state & Highlight) ? multimedia::shader("contrast", devilsai::storageBoxShaderInstance) : nullptr;
        imageManager::display(target, tools::hash(_imageContainer.c_str()), _type, _position.x, _position.y, true, shader);
    }

    _state &= ~Highlight;
}

void StorageBox::highlight()
{
    _state |= Highlight;
}

bool StorageBox::empty() const
{
    return _objects.objects.empty();
}

void StorageBox::close()
{
    if ((_state & CloseWhenEmpty) && empty())
    {
        _state |= Inert;
        if (_imageContainer == "individuals")
            _lifetime = 10000;
    }
    else if ((_state & RemoveWhenEmpty) && empty())
    {
        _lifetime = 0;
    }
}

void StorageBox::corpse(const string& species, const string& key)
{
    _name = textManager::getText("devilsai", "CADAVRE");
    _name.addParameter(textManager::getText("species", species));
    _imageContainer = "individuals";
    _type = key;
    _height = CLASSEMENT_CADAVRE;
    _state |= CloseWhenEmpty;
    _shape.circle(tools::math::Vector2d(0, 0), 20);
}

void StorageBox::sack()
{
    _name = textManager::getText("devilsai", "sack");
    _imageContainer = "paysage";
    _type = "sack";
    _height = CLASSEMENT_CADAVRE;
    _state |= RemoveWhenEmpty;
    _shape.circle(tools::math::Vector2d(0, 0), 5);
}

bool StorageBox::insertItem(WearableItem&& item, string slot)
{
    return _objects.insertItem(std::move(item), slot);
}
