
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_SKILL
#define DEVILSAI_RESOURCES_SKILL

#include <string>
#include <vector>
#include <map>

#include "tools/shape.h"

#include "devilsai-resources/stats.h"

using namespace std;

class lua_State;

namespace sf {
    class RenderWindow;
    class Shader;
}

namespace tinyxml2 {
    class XMLHandle;
}

class Individu;


enum Behaviors
{
    Random,
    Blocked,
    Defense,
    Hunting,
    Attacking,
    LookingForLife,
    Hurt,
    Dying,
    numberOfBehaviors
};


class Skill
{
    public:
        struct Level
        {
            Stats extraStats;
            Stats extraStatsAmplifiers;
            double unavailability;

            Level()
              : extraStats(),
                extraStatsAmplifiers(),
                unavailability(0)
            {
            }
        };

    private:
        enum Skill_State : uint8_t
        {
            BeginScript = 1 << 0,
            EndScript = 1 << 1
        };
        string Id;
        map< pair<double , int> , string > Animation;
        string scriptString;
        lua_State* script;
        uint8_t _state {};

    public:
        tools::math::Shape interactionField;
        unsigned numberOfImages;
        int priority;
        int step;
        vector<Level> levels;
        Attribute speedAttribute;
        uint16_t extraState {};

    public:
        Skill(string id);
        Skill(const Skill& other);
        Skill& operator=(const Skill& right);
        ~Skill();

        /**
         * Returns the name of the skill.
         *
         * \return name of the skill.
         */
        const string& name() const;

        /**
         * Returns the name of the related item, if any.
         *
         * \return name of the related item, or empty string if the skill is not related to an item.
         */
        string relatedItem() const;

        void addImage(double angle, int num, string imageKey);
        string getImageKey(double angle, int num);

        void loadFromXML(tinyxml2::XMLHandle &handle, sf::Shader* shader, int hue, int saturation, int luminance, int alpha);
        void loadScript();

        bool atBegin(Individu* owner, unsigned level);
        void atEnd(Individu* owner, unsigned level);

        void displayDescription(sf::RenderWindow& target);
};

#endif // DEVILSAI_RESOURCES_SKILL
