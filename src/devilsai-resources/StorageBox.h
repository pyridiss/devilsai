
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_STORAGEBOX
#define DEVILSAI_RESOURCES_STORAGEBOX

#define CLASSEMENT_CADAVRE 1
#define CLASSEMENT_NORMAL 2
#define COLL_OK 0
#define COLL_PRIM 2
#define COLL_INTER 4

#include <string>

#include "tools/vector2d.h"
#include "tools/shape.h"

#include "textManager/plainText.h"

#include "devilsai-resources/pack.h"
#include "devilsai-resources/Unit.h"
#include "devilsai-resources/UnitId.h"


using std::string;

namespace sf{
    class RenderTarget;
}

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}


class StorageBox : public Unit
{
    private:
        Pack _objects {};
        textManager::PlainText _name {};
        tools::math::Shape _shape {};
        string _type {};
        string _tag {};
        string _imageContainer {};
        tools::math::Vector2d _position {};
        double _lifetime {-1};
        UnitId _id;
        uint8_t _height {CLASSEMENT_NORMAL};
        uint8_t _state {0};

    public:
        StorageBox();
        StorageBox(const StorageBox& other) = delete;
        StorageBox(StorageBox&& other) = delete;
        StorageBox& operator=(const StorageBox& right) = delete;
        StorageBox& operator=(StorageBox&& right) = delete;
        ~StorageBox() = default;

        const UnitId& id() const;
        const tools::math::Vector2d& position() const;
        const tools::math::Shape& shape() const;
        bool unshaped() const;
        int height() const;
        const string& type() const;
        const string& tag() const;
        void setTag(const string& t);
        const textManager::PlainText& name();
        int diplomacy() const;
        Pack& objects();
        void manage();
        void corpse(const string& species, const string& key);

        /**
         * Defines the storage box as a sack.
         */
        void sack();
        bool empty() const;
        void close();
        void highlight();
        void move(double x, double y);
        bool destroyed() const;
        void destroy();
        int collision(Unit* other, bool apply);
        void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);
        void display(sf::RenderTarget& target);

        /**
         * Insert item in the underlying Pack.
         */
        bool insertItem(WearableItem&& item, string slot = {});
};

#endif  // DEVILSAI_RESOURCES_STORAGEBOX
