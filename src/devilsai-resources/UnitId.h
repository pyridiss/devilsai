
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_UNITID
#define DEVILSAI_RESOURCES_UNITID

#include <unordered_map>

#include "tools/debug.h"

class Unit;

/**
 * \brief Identifier for Units.
 */

class UnitId
{
    private:
        unsigned int _id {};

    public:
        inline UnitId() = default;
        explicit inline UnitId(unsigned int i);
        inline UnitId(Unit* u, unsigned int i);
        inline void replace(Unit* u, unsigned int i);
        inline operator bool();
        inline Unit* operator->();
        inline Unit* unit();
        inline unsigned int id() const;
};

inline bool operator==(const UnitId& left, const UnitId& right)
{
    return (left.id() == right.id());
}

inline bool operator!=(const UnitId& left, const UnitId& right)
{
    return (left.id() != right.id());
}

namespace std{

template<>
struct hash<UnitId>
{
    size_t operator()(const UnitId& i) const
    {
        return (size_t)i.id();
    }
};

template<>
struct equal_to<UnitId>
{
    bool operator()(const UnitId& left, const UnitId& right) const
    {
        return (left.id() == right.id());
    }
};

}  // std

inline std::unordered_map<UnitId, Unit*> units;
inline unsigned int currentId = 10000;


inline UnitId::UnitId(unsigned int i)
  : _id(i)
{
}

inline UnitId::UnitId(Unit* u, unsigned int i)
  : _id(i)
{
    if (_id != 0)
    {
        if (units.find(UnitId(_id)) != units.end())
        {
            tools::debug::error("Cannot set UnitId" + std::to_string(_id) + "; already used.", "devilsai", __FILENAME__, __LINE__);
            _id = 0;
        }
    }

    if (_id == 0)
    {
        while (units.find(UnitId(currentId)) != units.end())
            ++currentId;
        _id = currentId;
    }

    units.emplace(UnitId(_id), u);
}

inline void UnitId::replace(Unit* u, unsigned int i)
{
    if (i == 0) return;

    if (units.find(UnitId(i)) == units.end())
    {
        units.erase(*this);
        units.emplace(UnitId(i), u);
        _id = i;
    }
}

inline UnitId::operator bool()
{
    if (_id == 0) return false;
    if (units.find(*this) != units.end())
        return true;
    _id = 0;
    return false;
}

inline Unit* UnitId::operator->()
{
    return unit();
}

inline Unit* UnitId::unit()
{
    auto i = units.find(*this);
    if (i == units.end())
    {
        _id = 0;
        return nullptr;
    }

    return i->second;
}

inline unsigned int UnitId::id() const
{
    return _id;
}

inline void removeUnitId(const UnitId& i)
{
    units.erase(i);
}

#endif  // DEVILSAI_RESOURCES_UNITID
