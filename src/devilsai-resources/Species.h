
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_SPECIES
#define DEVILSAI_RESOURCES_SPECIES

#include <string>
#include <vector>
#include <unordered_map>

#include "tools/shape.h"
#include "textManager/plainText.h"

#include "devilsai-resources/skill.h"
#include "devilsai-resources/stats.h"
#include "devilsai-resources/pack.h"

namespace tinyxml2{
    class XMLHandle;
}

class Individu;
class Skill;

using std::string;
using std::vector;
using std::unordered_map;


class Species final
{
    private:

        struct ItemPrototype
        {
            string file;
            int quality;
            int quantity;
            int probability;
        };

        string Type {};
        textManager::PlainText _displayedName {};
        tools::math::Shape size {};
        tools::math::Shape viewField {};
        tools::math::Shape interactionField {};
        unordered_map<string, Skill*> _skills {};
        string _behaviors[Behaviors::numberOfBehaviors] {};
        vector<string> _attacks {};
        Stats commonStats {};
        Pack inventory {};
        vector<ItemPrototype> _itemsPrototypes {};
        string corpseImageKey {};
        string _definitionFile {};
        double lifetime {-1};
        double angle {};
        int Diplomatie {};
        unsigned int _experience {};
        uint16_t _state {};

    public:
        Species() = default;
        ~Species() = default;

        void Copie_Element(Individu *elem);
        string definitionFile();

        void loadFromXML(tinyxml2::XMLHandle &handle, const string& file);

    friend class Individu;
};

#endif  // DEVILSAI_RESOURCES_SPECIES
