
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_INVENTORY
#define DEVILSAI_RESOURCES_INVENTORY

#include <string>
#include <vector>

#include "devilsai-resources/wearableItem.h"

using namespace std;

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
    class XMLHandle;
}


class Pack
{
    public:
        /**
         * Types of the Pack.
         */
        enum class Type : uint8_t
        {
            Inventory,  ///< Defines the Pack as the inventory of a character
            StorageBox  ///< Defines the Pack as the set of the items contained in a storage box
        };

    public:
        vector<WearableItem> objects {};
        int _numberOfDefaultSlots {20};
        Type _type {Type::Inventory};

    public:
        Pack() = default;
        Pack(const Pack& other) = delete;
        Pack(Pack&& other) noexcept = default;
        Pack& operator=(const Pack& right) = delete;
        Pack& operator=(Pack&& right) noexcept = default;
        ~Pack() = default;

    private:
        string defaultSlot(int i);
        string nextEmptyDefaultSlot();

    public:
        /**
         * Sets the type of the pack.
         *
         * \param t new type of the pack.
         */
        void setType(Pack::Type t);

        /**
         * Tells if the pack is full or not.
         *
         * \return \a true if the Pack is full (ie. if there is no default slots still empty), \a false otherwise.
         */
        bool isFull();

        /**
         * Returns a pointer to the WearableItem whose current slot is given in parameter.
         *
         * \param slot the slot where to find the WearableItem.
         * \return pointer to a WearableItem if any, \a nullptr otherwhise.
         */
        WearableItem* at(const string& slot);

        /**
         * Returns a pointer to a WearableItem of the given type, if any.
         *
         * \param item name of the item to find.
         * \return pointer to the WearableItem if any, \a nullptr otherwhise.
         */
        WearableItem* find(const string& item);

        /**
         * Returns the quantity of a wearable item contained in the Pack.
         * If the wearable item is active, it will not be counted.
         */
        int quantityOf(const string& wearableItem);

        /**
         * Removes the item from the pack and returns it.
         * If the pack does not contains this item, returns an empty invalid WearableItem.
         *
         * \param item item to pick.
         * \return a WearableItem representing the item picked.
         */
        WearableItem pickItem(const string& item);

        /**
         * Removes the item contained at the given slot from the pack and returns it.
         * If the slot is empty, returns an empty invalid WearableItem.
         *
         * \param slot slot where to pick the item.
         * \return a WearableItem representing the item picked.
         */
        WearableItem pickItemAt(const string& slot);

        /**
         * Insert an item into the pack.
         * If the pack is full, \a item is left unmodified. Otherwise, \a item is moved-from and becomes invalid.
         *
         * \remark this function can allocate more memory to store the new item. It that case, all pointers to items
         * are invalidated.
         *
         * \param item item to put into the pack.
         * \param slot slot where to put the item. If \a slot is empty, the item will be put in the next free default slot.
         *             Default is empty.
         * \return \a true is the item has been inserted successfully, \a false otherwise.
         */
        bool insertItem(WearableItem&& item, string slot = {});

        /**
         * Update the remaining duration of each item.
         * This function should be called once per frame.
         */
        void updateRemainingDurations();

        /**
         * Copy the items in another Pack.
         *
         * \param other reference to the Pack to write the new items.
         */
        void copy(Pack& other);

        void loadFromXML(tinyxml2::XMLElement* elem);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);
};

#endif // DEVILSAI_RESOURCES_INVENTORY
