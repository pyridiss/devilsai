
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/Trigger.h"

#include <tinyxml2.h>
#include <lua.hpp>

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "tools/math.h"
#include "tools/signals.h"

#include "ElementsCarte/ElementsCarte.h"

#include "devilsai-resources/manager.h"

#include "Bibliotheque/luaFunctions.h"


const string Trigger_type {"Trigger"};


Trigger::Trigger()
  : _id(this, 0)
{
    _shape.setOrigin(&_position);
}

const UnitId& Trigger::id() const
{
    return _id;
}

const tools::math::Vector2d& Trigger::position() const
{
    return _position;
}

const tools::math::Shape& Trigger::shape() const
{
    return _shape;
}

bool Trigger::unshaped() const
{
    return false;
}

int Trigger::height() const
{
    return 0;
}

const string& Trigger::type() const
{
    return Trigger_type;
}

const string& Trigger::tag() const
{
    return _tag;
}

int Trigger::diplomacy() const
{
    return 0;
}

void Trigger::manage()
{
}

void Trigger::move(double x, double y)
{
    _position.x += x;
    _position.y += y;
}

bool Trigger::destroyed() const
{
    return (_state == 1);
}

void Trigger::destroy()
{
    _state = 1;
}

int Trigger::collision(Unit* elem, bool apply)
{
    if (apply && elem != nullptr && elem->tag() == "intern")
    {
        lua_getglobal(_script, "mouseHovering");
        lua_pushstring(_script, _data.c_str());
        lua_call(_script, 1, 0);
        return 0;
    }

    lua_getglobal(_script, "collision");
    lua_pushlightuserdata(_script, (void*)elem);
    lua_pushboolean(_script, apply);
    lua_pushstring(_script, _data.c_str());
    lua_call(_script, 3, 1);

    int value = lua_tonumber(_script, -1);

    return value;
}

void Trigger::loadFromXML(tinyxml2::XMLElement* elem, tinyxml2::XMLElement* properties)
{
    using namespace tinyxml2;

    if (elem->Attribute("script"))
    {
        _triggerScript = elem->Attribute("script");
        _script = devilsai::addResource<Trigger::Script>(_triggerScript, _triggerScript)->get();
    }

    move(elem->DoubleAttribute("x"), elem->DoubleAttribute("y"));

    _id.replace(this, elem->UnsignedAttribute("id", 0));

    if (elem->Attribute("tag"))
        _tag = elem->Attribute("tag");
    else if (properties && properties->Attribute("tag"))
        _tag = properties->Attribute("tag");

    if (elem->Attribute("data"))
        _data = elem->Attribute("data");

    elem = elem->FirstChildElement();
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "shape")
            _shape.loadFromXML(elem);

        elem = elem->NextSiblingElement();
    }
}

void Trigger::saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle)
{
    using namespace tinyxml2;

    XMLElement* root = handle.ToElement();

    XMLElement* trigger = doc.NewElement("trigger");
    trigger->SetAttribute("script", _triggerScript.c_str());
    trigger->SetAttribute("x", _position.x);
    trigger->SetAttribute("y", _position.y);
    trigger->SetAttribute("tag", _tag.c_str());
    trigger->SetAttribute("data", _data.c_str());
    trigger->SetAttribute("id", _id.id());

    XMLElement* shape = doc.NewElement("shape");
    trigger->InsertEndChild(shape);
    XMLHandle shapeHandle(shape);
    _shape.saveToXML(doc, shapeHandle);

    root->InsertEndChild(trigger);
}

void Trigger::display([[maybe_unused]] sf::RenderTarget& target)
{
}


Trigger::Script::Script(const string& file)
  : L(luaL_newstate())
{
    luaL_openlibs(L);
    luaL_dofile(L, (tools::filesystem::dataDirectory() + file).c_str());

    lua_atpanic(L, [](lua_State* S)
    {
        tools::debug::error(lua_tostring(S, -1), "lua", __FILENAME__, __LINE__);
        return 0;
    });

    //Check if the file is well-formed

    bool fileComplete = true;

    auto check = [&](const char* f)
    {
        lua_getglobal(L, f);
        if (lua_isnil(L, -1))
        {
            tools::debug::error("The trigger '" + file + "' does not define the symbol '" + string(f) + "'", "lua", __FILENAME__, __LINE__);
            fileComplete = false;
        }
        lua_pop(L, 1);
    };

    check("mouseHovering");
    check("collision");

    if (!fileComplete)
    {
        lua_close(L);
        return;
    }

    lua_register(L, "addSignal", [](lua_State* S)
    {
        tools::signals::addSignal(lua_tostring(S, 1), lua_tostring(S, 2));
        return 0;
    });

    lua_register(L, "Character__setAttribute", [](lua_State* S)
    {
        Unit* unit = static_cast<Unit*>(lua_touserdata(S, 1));
        Character* character = dynamic_cast<Character*>(unit);
        string_view attribute = lua_tostring(S, 2);
        double value = lua_tonumber(S, 3);
        Attribute a = stringToAttribute(attribute);

        if (character)
            character->setAttribute(a, value);

        return 0;
    });

    lua_register(L, "cout", LUA_cout);
    lua_register(L, "moveItemTo", LUA_moveItemTo);

    lua_register(L, "Unit__diplomacy", [](lua_State* S)
    {
        Unit* unit = static_cast<Unit*>(lua_touserdata(S, 1));

        lua_pushnumber(S, (unit ? unit->diplomacy() : 0));

        return 1;
    });
}

Trigger::Script::~Script()
{
    lua_close(L);
}

lua_State* Trigger::Script::get()
{
    return L;
}
