
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/math.h"
#include "tools/timeManager.h"

#include "textManager/textManager.h"

#include "devilsai-resources/pack.h"

using namespace tinyxml2;


string Pack::defaultSlot(int i)
{
    return ((_type == Type::Inventory) ? "inventory" : "storagebox") + tools::math::intToChars(i, 2);
}

string Pack::nextEmptyDefaultSlot()
{
    string slot = defaultSlot(1);
    int key = 2;
    while (at(slot) != nullptr)
    {
        if (key > _numberOfDefaultSlots) return string();
        slot = defaultSlot(key);
        ++key;
    }
    return slot;
}

void Pack::setType(Pack::Type t)
{
    _type = t;
}

bool Pack::isFull()
{
    for (int key = 1 ; key <= _numberOfDefaultSlots ; ++key)
    {
        if (at(defaultSlot(key)) == nullptr) return false;
    }
    return true;
}

WearableItem* Pack::at(const string& slot)
{
    for (auto& i : objects)
        if (i.currentSlot() == slot)
            return &i;

    return nullptr;
}

WearableItem* Pack::find(const string& item)
{
    for (auto& i : objects)
        if (i.name() == item)
            return &i;

    return nullptr;
}

int Pack::quantityOf(const string& wearableItem)
{
    auto i = objects.begin();
    while (i != objects.end() && (i->name() != wearableItem || i->active()))
        ++i;

    if (i != objects.end())
        return i->quantity();

    return 0;
}

WearableItem Pack::pickItem(const string& item)
{
    WearableItem w;
    for (auto i = objects.begin() ; i != objects.end() ; ++i)
    {
        if (i->name() == item)
        {
            w = std::move(*i);
            objects.erase(i);
            break;
        }
    }

    return w;
}

WearableItem Pack::pickItemAt(const string& slot)
{
    WearableItem w;
    for (auto i = objects.begin() ; i != objects.end() ; ++i)
    {
        if (i->currentSlot() == slot)
        {
            w = std::move(*i);
            objects.erase(i);
            break;
        }
    }

    return w;
}

bool Pack::insertItem(WearableItem&& item, string slot)
{
    if (slot.empty())
    {
        if (WearableItem* w = find(item.name()) ; w != nullptr && w->stackable())
        {
            w->setQuantity(w->quantity() + item.quantity());
            WearableItem x = std::move(item);  // destroy the item
            return true;
        }
        else slot = nextEmptyDefaultSlot();
    }

    if (slot.empty()) return false;  // The Pack is full
    if (at(slot)) return false;  // The asked slot is already used

    item.setSlot(slot);
    objects.push_back(std::move(item));
    return true;
}

void Pack::updateRemainingDurations()
{
    for (auto i = objects.begin() ; i != objects.end() ; ++i)
    {
        if (i->active() && i->remainingDuration() > 0)
        {
            i->setRemainingDuration(i->remainingDuration() - tools::timeManager::I(1));

            if (i->remainingDuration() <= 0)
            {
                i = objects.erase(i) - 1;
            }
        }
    }
}

void Pack::copy(Pack& other)
{
    for (auto& i : objects)
        other.insertItem(i.duplicate());
}

void Pack::loadFromXML(XMLElement* elem)
{
    objects.clear();

    _numberOfDefaultSlots = elem->IntAttribute("numberOfDefaultSlots", _numberOfDefaultSlots);

    XMLHandle hdl(elem);
    XMLElement *object = hdl.FirstChildElement().ToElement();
    while (object)
    {
        string objectName = object->Name();

        if (objectName == "addObject")
        {
            string slot = str(object->Attribute("slot"));
            string design = str(object->Attribute("design"));
            int qly = object->IntAttribute("quality", 0);
            int qty = object->IntAttribute("quantity", 1);

            WearableItem w;
            w.create(design);
            w.generateRandomItem(qly);
            w.setQuantity(qty);

            if (object->Attribute("data"))
                w.recoverState(object->Attribute("data"));

            if (!insertItem(std::move(w), slot))
                tools::debug::warning("Cannot add item '" + design + "' in the pack at slot '" + slot + "'", "devilsai", __FILENAME__, __LINE__);
        }
        if (objectName == "createRandomObjects")
        {
            int min = 0, max = 0;
            object->QueryAttribute("min", &min);
            object->QueryAttribute("max", &max);
            int n = tools::math::randomNumber(min, max);

            int designsCount = 0;
            XMLElement* currentDesign = object->FirstChildElement();
            while (currentDesign)
            {
                ++designsCount;
                currentDesign = currentDesign->NextSiblingElement();
            }
            for (int i = 0 ; i < n ; ++i)
            {
                int selectedDesign = rand()%designsCount;
                currentDesign = object->FirstChildElement();
                for (int j = 0 ; j < selectedDesign ; ++j)
                    currentDesign = currentDesign->NextSiblingElement();

                string design = str(currentDesign->Attribute("design"));
                int qly = currentDesign->IntAttribute("quality", 0);
                int qty = currentDesign->IntAttribute("quantity", 1);

                if (design.empty()) design = nextEmptyDefaultSlot();

                WearableItem w;
                w.create(design);
                w.generateRandomItem(qly);
                w.setQuantity(qty);
                if (!insertItem(std::move(w)))
                    tools::debug::warning("Cannot add item '" + design + "' in the pack", "devilsai", __FILENAME__, __LINE__);
            }
        }

        object = object->NextSiblingElement();
    }
}

void Pack::saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle)
{
    XMLElement* root = handle.ToElement();

    XMLElement* inventory = doc.NewElement("inventory");

    for (auto& o : objects)
    {
        XMLElement* object = doc.NewElement("addObject");

        object->SetAttribute("slot", o.currentSlot().c_str());
        object->SetAttribute("design", o.name().c_str());
        object->SetAttribute("data", o.currentState().c_str());

        inventory->InsertEndChild(object);
    }

    root->InsertEndChild(inventory);
}
