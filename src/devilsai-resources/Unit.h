
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_UNIT
#define DEVILSAI_RESOURCES_UNIT

#include <string>

#include "tools/vector2d.h"

using std::string;

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}

namespace tools::math{
    class Shape;
}

class UnitId;

/**
 * \brief Interface for all characters, enemies, environments, etc. that can be put in a world.
 */

class Unit
{
    public:
        /**
         * Destructor.
         */
        virtual ~Unit() = default;

        /**
         * Returns the unique id of the unit.
         */
        virtual const UnitId& id() const = 0;

        /**
         * Returns the position of the unit.
         */
        virtual const tools::math::Vector2d& position() const = 0;

        /**
         * Returns the shape of the ground space occupied by the unit.
         */
        virtual const tools::math::Shape& shape() const = 0;

        /**
         * Tells whether the unit occupy some space on the ground.
         */
        virtual bool unshaped() const = 0;

        /**
         * Returns the height of the unit.
         */
        virtual int height() const = 0;

        /**
         * Returns the type of the unit.
         */
        virtual const string& type() const = 0;

        /**
         * If the unit is tagged, returns its tag.
         */
        virtual const string& tag() const = 0;

        /**
         * Returns the diplomacy of the unit.
         */
        virtual int diplomacy() const = 0;

        /**
         * Manage the unit.
         *
         * This function must be called once per game loop.
         */
        virtual void manage() = 0;

        /**
         * Move the unit.
         *
         * \param x x movement.
         * \param y y movement.
         */
        virtual void move(double x, double y) = 0;

        /**
         * Tells whether the unit has been destroyed and can be deleted.
         */
        virtual bool destroyed() const = 0;

        /**
         * Destroy the unit.
         *
         * After destruction, no memory is freed; it just stops managing the unit and wait
         * for deletion by the owner of the unit.
         */
        virtual void destroy() = 0;

        /**
         * Checks what happen if the unit collide with another, and eventually apply the consequences.
         *
         * \param other the other unit it may collide with.
         * \param apply if set to true, the collision is considered effective and the consequences are applied.
         */
        virtual int collision(Unit* other, bool apply) = 0;

        /**
         * Load the unit from XML code.
         *
         * \param unit main node containing the definition of the unit.
         * \param properties upward node containing some common properties between several units. Can be nullptr.
         */
        virtual void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties) = 0;

        /**
         * Save the unit to a XML file.
         *
         * \param doc file to which the unit mus be saved.
         * \param handle handle in which a new element can be placed to store the data.
         */
        virtual void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle) = 0;

        /**
         * Display the unit on the screen or on another render target.
         *
         * \param target target where the unit must be drawn.
         */
        virtual void display(sf::RenderTarget& target) = 0;
};

#endif  // DEVILSAI_RESOURCES_UNIT
