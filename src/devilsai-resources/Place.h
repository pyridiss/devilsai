
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_PLACE
#define DEVILSAI_RESOURCES_PLACE

#include <string>

#include "tools/vector2d.h"
#include "tools/shape.h"

using std::string;

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}


class Place
{
    private:
        string _name {};
        string _music {};
        tools::math::Shape _shape {};
        uint8_t _diplomacy {0};

    public:
        Place() = default;
        Place(const Place& other) = delete;
        Place(Place&& other) = delete;
        Place& operator=(const Place& right) = delete;
        Place& operator=(Place&& right) = delete;
        ~Place() = default;

        const string& name() const;
        const string& music() const;
        const tools::math::Shape& shape() const;
        int diplomacy() const;
        void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);
};

#endif  // DEVILSAI_RESOURCES_ENVIRONMENT
