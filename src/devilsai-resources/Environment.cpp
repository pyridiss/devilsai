
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/Environment.h"

#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/math.h"
#include "imageManager/imageManager.h"

#include "devilsai-resources/manager.h"


enum Environment_State
{
    Unshaped = 1 << 0,
    CustomShape = 1 << 1,
    Destroyed = 1 << 2
};


Environment::Environment()
  : _id(this, 0)
{
    _shape.setOrigin(&_position);
}

const UnitId& Environment::id() const
{
    return _id;
}

const tools::math::Vector2d& Environment::position() const
{
    return _position;
}

const tools::math::Shape& Environment::shape() const
{
    return _shape;
}

bool Environment::unshaped() const
{
    return _state & Unshaped;
}

int Environment::height() const
{
    return _height;
}

const string& Environment::type() const
{
    return _type;
}

const string& Environment::tag() const
{
    return _tag;
}

int Environment::diplomacy() const
{
    return 0;
}

void Environment::manage()
{
}

void Environment::move(double x, double y)
{
    _position.x += x;
    _position.y += y;
}

bool Environment::destroyed() const
{
    return _state & Destroyed;
}

void Environment::destroy()
{
    _state |= Destroyed;
}

int Environment::collision([[maybe_unused]] Unit *elem, [[maybe_unused]] bool apply)
{
	return COLL_PRIM;
}

void Environment::loadFromXML(tinyxml2::XMLElement* elem, tinyxml2::XMLElement* properties)
{
    using namespace tinyxml2;

    if (elem->Attribute("design"))
    {
        _type = elem->Attribute("design");

        Environment* design = devilsai::getResource<Environment>(_type);
        if (design == nullptr)
        {
            tools::debug::error("Cannot initiate Environment '" + _type + "': design does not exist.", "resources", __FILENAME__, __LINE__);
            return;
        }

        _shape = design->_shape;
        _height = design->_height;
        if (design->unshaped()) _state |= Unshaped;
    }

    if (elem->Attribute("name"))
        _type = elem->Attribute("name");

    move(elem->DoubleAttribute("x"), elem->DoubleAttribute("y"));

    _id.replace(this, elem->UnsignedAttribute("id", 0));

    if (elem->Attribute("tag"))
        _tag = elem->Attribute("tag");
    else if (properties && properties->Attribute("tag"))
        _tag = properties->Attribute("tag");

    if (properties && properties->BoolAttribute("ignoreCollision"))
        _state |= Unshaped;

    if (properties && properties->BoolAttribute("immuable"))
            _height = CLASSEMENT_CADAVRE;

    elem = elem->FirstChildElement();
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "shape")
        {
            _shape.loadFromXML(elem);
            _state |= CustomShape;
        }
        if (elemName == "image")
        {
            int xAlignment = 0, yAlignment = 0;
            string archive = "";
            if (elem->Attribute("archive")) archive = elem->Attribute("archive");
            string path = elem->Attribute("imageFile");
            elem->QueryAttribute("xAlignment", &xAlignment);
            elem->QueryAttribute("yAlignment", &yAlignment);

            if (!archive.empty()) imageManager::addArchiveFile(archive);
            imageManager::addImage("paysage"_hash, _type, path, sf::Vector2i(xAlignment, yAlignment));
            if (!archive.empty()) imageManager::removeArchiveFile(archive);
        }
        if (elemName == "properties")
        {
            if (elem->BoolAttribute("ignoreCollision"))
                _state |= Unshaped;
            _height = elem->UnsignedAttribute("classement", _height);
            _xExtent = elem->UnsignedAttribute("xExtent", _xExtent);
            _yExtent = elem->UnsignedAttribute("yExtent", _yExtent);
        }

        elem = elem->NextSiblingElement();
    }
}

void Environment::saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle)
{
    using namespace tinyxml2;

    XMLElement* root = handle.ToElement();

    XMLElement* environment = doc.NewElement("environment");
    environment->SetAttribute("design", _type.c_str());
    environment->SetAttribute("x", _position.x);
    environment->SetAttribute("y", _position.y);
    environment->SetAttribute("id", _id.id());
    if (!_tag.empty())
        environment->SetAttribute("tag", _tag.c_str());

    if (_state & CustomShape)
    {
        XMLElement* shape = doc.NewElement("shape");
        environment->InsertEndChild(shape);
        XMLHandle shapeHandle(shape);
        _shape.saveToXML(doc, shapeHandle);
    }

    if (unshaped() || _height != CLASSEMENT_NORMAL || _xExtent != 1 || _yExtent != 1)
    {
        XMLElement* properties = doc.NewElement("properties");
        if (unshaped()) properties->SetAttribute("ignoreCollision", true);
        if (_height != CLASSEMENT_NORMAL)
            properties->SetAttribute("classement", (unsigned)_height);
        if (_xExtent != 1)
            properties->SetAttribute("xExtent", (unsigned)_xExtent);
        if (_yExtent != 1)
            properties->SetAttribute("yExtent", (unsigned)_yExtent);
        environment->InsertEndChild(properties);
    }

    root->InsertEndChild(environment);
}

void Environment::display(sf::RenderTarget& target)
{
    if (_xExtent == 1 && _yExtent == 1)
        imageManager::display(target, "paysage"_hash, _type, _position.x, _position.y, true);
    else
        imageManager::displayRepeatedly(target, "paysage"_hash, _type, _position.x, _position.y, _xExtent, _yExtent, true);
}
