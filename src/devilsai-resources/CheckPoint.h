
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_RESOURCES_CHECKPOINT
#define DEVILSAI_RESOURCES_CHECKPOINT

#include <string>

#include "tools/shape.h"
#include "tools/vector2d.h"

#include "devilsai-resources/Unit.h"
#include "devilsai-resources/UnitId.h"

#define COLL_OK 0

using std::string;

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}

const string CheckPoint_type {"CheckPoint"};

class CheckPoint : public Unit
{
    private:
        tools::math::Shape _shape {};
        string _tag {};
        tools::math::Vector2d _position {};
        UnitId _id;
        uint8_t _state {0};

    public:
        CheckPoint()
          : _id(this, 0)
        {
            _shape.setOrigin(&position());
        }
        CheckPoint(const CheckPoint& other) = delete;
        CheckPoint(CheckPoint&& other) = delete;
        CheckPoint& operator=(const CheckPoint& right) = delete;
        CheckPoint& operator=(CheckPoint&& right) = delete;
        ~CheckPoint() = default;

        const UnitId& id() const
        {
            return _id;
        }
        const tools::math::Vector2d& position() const
        {
            return _position;
        }
        const tools::math::Shape& shape() const
        {
            return _shape;
        }
        bool unshaped() const
        {
            return true;
        }
        int height() const
        {
            return 0;
        }
        const string& type() const
        {
            return CheckPoint_type;
        }
        const string& tag() const
        {
            return _tag;
        }
        int diplomacy() const
        {
            return 0;
        }
        void manage()
        {
        }
        void setTag(const string& t)
        {
            _tag = t;
        }
        void move(double x, double y)
        {
            _position.x += x;
            _position.y += y;
        }
        void setShape(const tools::math::Shape& s)
        {
            _shape = s;
        }
        bool destroyed() const
        {
            return (_state == 1);
        }
        void destroy()
        {
            _state = 1;
        }
        int collision([[maybe_unused]] Unit* other, [[maybe_unused]] bool apply)
        {
            return COLL_OK;
        }
        void loadFromXML([[maybe_unused]] tinyxml2::XMLElement* unit, [[maybe_unused]] tinyxml2::XMLElement* properties)
        {
        }
        void saveToXML([[maybe_unused]] tinyxml2::XMLDocument& doc, [[maybe_unused]] tinyxml2::XMLHandle& handle)
        {
        }
        void display([[maybe_unused]] sf::RenderTarget& target)
        {
        }
};

#endif  // DEVILSAI_RESOURCES_CHECKPOINT
