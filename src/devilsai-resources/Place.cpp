
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-resources/Place.h"

#include <tinyxml2.h>

#include "musicManager/musicManager.h"

using namespace tinyxml2;


const string& Place::name() const
{
    return _name;
}

const string& Place::music() const
{
    return _music;
}

const tools::math::Shape& Place::shape() const
{
    return _shape;
}

int Place::diplomacy() const
{
    return _diplomacy;
}

void Place::loadFromXML(tinyxml2::XMLElement* elem, [[maybe_unused]] tinyxml2::XMLElement* properties)
{
    if (elem->Attribute("name"))
        _name = elem->Attribute("name");

    if (elem->Attribute("music"))  // DEPRECATED
    {
        _music = elem->Attribute("music");
        musicManager::addMusic(_music);
    }

    elem = elem->FirstChildElement();
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "shape")
        {
            _shape.loadFromXML(elem);
        }
        if (elemName == "properties")
        {
            _diplomacy = elem->UnsignedAttribute("diplomacy", _diplomacy);
            if (elem->Attribute("music"))
            {
                _music = elem->Attribute("music");
                musicManager::addMusic(_music);
            }
        }

        elem = elem->NextSiblingElement();
    }
}

void Place::saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle)
{
    XMLElement* root = handle.ToElement();

    XMLElement* place = doc.NewElement("place");
    place->SetAttribute("name", _name.c_str());

    XMLElement* properties = doc.NewElement("properties");
    properties->SetAttribute("diplomacy", (unsigned)_diplomacy);
    properties->SetAttribute("music", _music.c_str());
    place->InsertEndChild(properties);

    XMLElement* shape = doc.NewElement("shape");
    place->InsertEndChild(shape);
    XMLHandle shapeHandle(shape);
    _shape.saveToXML(doc, shapeHandle);

    root->InsertEndChild(place);
}
