
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include "multimedia/Area.h"
#include <SFML/System/Vector3.hpp>

using std::string;

namespace sf{
    class Font;
    class RenderTarget;
    class RenderWindow;
    class Shader;
}

namespace multimedia{

// Assets

/**
 * \brief Base class for shaders.
 *
 * The ShaderInstance class and the associated helpers are used to store shaders and use them through
 * a common interface.
 * The multimedia library does not define any shader: a shader must be defined by derivating the
 * ShaderInstance class.
 */
class ShaderInstance
{
    public:

        /**
         * Destructor.
         */
        virtual ~ShaderInstance()
        {
        }

        /**
         * Create a new set of data to which the user can refer with the identifier returned.
         *
         * \return identifier to pass to other functions.
         */
        virtual unsigned int createNewInstance(const std::string& data) = 0;

        /**
         * Return the sf::Shader, already parameterized with the data of the instance.
         *
         * \param instance the instance to use.
         * \return pointer to the sf::Shader.
         */
        virtual sf::Shader* shader(unsigned int instance) = 0;
};

void addFont(const string& name, const string& file);
sf::Font* font(const string& name);

/**
 * Add a new shader to the list of known shaders.
 *
 * \param name the name of the shader.
 * \param i non-owning pointer to the new shader, which can be of any class derived from ShaderInstance.
 */
void addShader(const string& name, ShaderInstance* i);

/**
 * Calls the createNewInstance() function of the shader \a name.
 *
 * An instance stores the data needed to define the varying and uniform variables of a shader.
 *
 * \param name the name of the shader.
 * \param data data used to create the instance.
 * \return an identifier that will be necessary to apply the shader with the good data.
 */
unsigned int createShaderInstance(const std::string& name, const std::string& data);

/**
 * Returns a pointer to the sf::Shader that can directly be used in graphic functions.
 *
 * The shader is already parameterized which the data of the instance.
 *
 * \param name the name of the shader.
 * \param instance the instance of the shader.
 * \return pointer to the sf::Shader.
 */
sf::Shader* shader(const string& name, unsigned int instance);

/**
 * Apply the shader on a part of the screen.
 *
 * \param app    reference to the window in which the shader should be applied.
 * \param shader shader to apply.
 * \param area   area where to apply the shader.
 */
void applyShaderOnScreen(sf::RenderTarget& app, sf::Shader* shader, Area area);

}  // namespace multimedia

namespace imageManager{

class Image;

void addContainer(unsigned int container);
void addImage(unsigned int container, const string& key, const string& file, sf::Vector2i of = sf::Vector2i(0, 0), sf::Shader* sh = nullptr);
void addArchiveFile(string path);
string getCurrentArchiveFile();
void removeArchiveFile(string path);
void changeHSL(unsigned int container, const string& key, double h, double s, double l);
void changeAlphaChannel(unsigned int container, const string& key, int alpha);

/**
 * Displays an image on a target.
 *
 * \param target target to draw on.
 * \param container id of the container containing the image.
 * \param key name of the image.
 * \param x x-coordinate of the position where to display the image.
 * \param y y-coordinate of the position where to display the image.
 * \param atCenter if \a false, (\a x, \a y) is the top-left corner of the area where the image is displayed.
 *                 If \a true, this is the center of the area. Default if \a false.
 * \param states a \a sf::RenderStates object passed to \a target.draw(). Default is \a sf::RenderStates::Default.
 */
void display(sf::RenderTarget& target, unsigned int container, const string& key, float x, float y,
             bool atCenter = false, const sf::RenderStates& states = sf::RenderStates::Default);

void displayStretched(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area, bool atCenter = false, const sf::RenderStates& states = sf::RenderStates::Default);

/**
 * Displays an image in the center of an area, scaled to it, but without distortion.
 *
 * \param target target to draw on.
 * \param container id of the container containing the image.
 * \param key name of the image.
 * \param area area where to draw the image.
 * \param states a \a sf::RenderStates object passed to \a target.draw(). Default is \a sf::RenderStates::Default.
 */
void displayFit(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area,
                const sf::RenderStates& states);

/**
 * Displays an image as many times as needed to completely fill the area, without "overflow".
 *
 * \param target target to draw on.
 * \param container id of the container containing the image.
 * \param key name of the image.
 * \param area area where to draw the image.
 * \param xOrigin x-coordinate of a point in the target which should correspond to the top-left corner of one
 *        of the occurence of the image.
 * \param yOrigin y-coordinate of a point in the target which should correspond to the top-left corner of one
 *        of the occurence of the image.
 * \param states a \a sf::RenderStates object passed to \a target.draw(). Default is \a sf::RenderStates::Default.
 */
void displayTiled(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area,
                  int xOrigin, int yOrigin, const sf::RenderStates& states = sf::RenderStates::Default);

void displayRepeatedly(sf::RenderTarget& target, unsigned int container, const string& key, double x, double y, int xQuantity, int yQuantity, bool atCenter = false, const sf::RenderStates& states = sf::RenderStates::Default);
void lockGLMutex(int id);
void unlockGLMutex(int id);

} //namespace imageManager

#endif //IMAGEMANAGER_H
