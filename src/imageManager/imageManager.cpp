
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imageManager/imageManager.h"

#include <atomic>
#include <thread>
#include <cmath>
#include <unordered_map>

#include <physfs.h>

#include "tools/debug.h"
#include "tools/filesystem.h"

#include "imageManager/image.h"
#include "imageManager/HSL.h"


namespace imageManager{

typedef unordered_map < string, imageManager::Image > Container;
typedef unordered_map < unsigned int, Container > Database;

Database images;
string currentArchiveFile;

std::atomic_flag Mutex_lock = ATOMIC_FLAG_INIT;
std::atomic<int> Mutex_id = 0;
std::thread::id Mutex_locker {};


void addContainer(unsigned int container)
{
    Database::iterator c = images.find(container);

    if (c == images.end())
    {
        Container ctnr;
        images.insert(Database::value_type(container, ctnr));
    }
}

void addImage(unsigned int container, const string& key, const string& file, Vector2i of, Shader* sh)
{
    Database::iterator c = images.find(container);

    if (c == images.end())
    {
        tools::debug::error("Image container has not been created yet for the file \"" + file + "\"", "multimedia", __FILENAME__, __LINE__);
        return;
    }

    Container::iterator i = (*c).second.find(key);

    if (i == (*c).second.end())
    {
        lockGLMutex(100);

        const auto& result = c->second.emplace(key, Image());
        result.first->second.set(file, of);

        if (sh != nullptr)
            result.first->second.applyShader(sh);

        unlockGLMutex(100);
    }
}

void addArchiveFile(string path)
{
    path = tools::filesystem::dataDirectory() + path;

    if (PHYSFS_mount(path.c_str(), NULL, 1) == 0)
    {
        tools::debug::error("Unable to load file: " + path, "multimedia", __FILENAME__, __LINE__);
    }
    else
    {
        currentArchiveFile = path;
    }
}

string getCurrentArchiveFile()
{
    return currentArchiveFile;
}

void removeArchiveFile(string path)
{
    path = tools::filesystem::dataDirectory() + path;
    PHYSFS_unmount(path.c_str());
    currentArchiveFile = "";
}

imageManager::Image* getImage(unsigned int container, const string& key)
{
    Database::iterator c = images.find(container);

    if (c == images.end())
    {
        tools::debug::error("Container not found for the image key \"" + key + "\"", "multimedia", __FILENAME__, __LINE__);
        return nullptr;
    }

    Container::iterator i = (*c).second.find(key);

    if (i == (*c).second.end())
    {
        tools::debug::error("Image not found: \"" + key + "\"", "multimedia", __FILENAME__, __LINE__);
        return nullptr;
    }

    return &(i->second);
}

void changeHSL(unsigned int container, const string& key, double h, double s, double l)
{
    Image* i = getImage(container, key);

    if (i != nullptr)
    {
        HSL hsl(h, s, l);
        Color c = hsl.turnToRGB();
        c.a = i->sprite.getColor().a;
        i->sprite.setColor(c);
    }
}

void changeAlphaChannel(unsigned int container, const string& key, int alpha)
{
    Image* i = getImage(container, key);

    if (i != nullptr)
    {
        Color c = i->sprite.getColor();
        c.a = alpha;
        i->sprite.setColor(c);
    }
}

void display(sf::RenderTarget& target, unsigned int container, const string& key, float x, float y, bool atCenter,
             const sf::RenderStates& states)
{
    Database::iterator c = images.find(container);

    if (c == images.end())
    {
        tools::debug::error("Container not found for the image key \"" + key + "\"", "multimedia", __FILENAME__, __LINE__);
        return;
    }

    Container::iterator i = (*c).second.find(key);

    if (i == (*c).second.end())
    {
        tools::debug::error("Image not found: \"" + key + "\"", "multimedia", __FILENAME__, __LINE__);
        return;
    }

    i->second.display(target, x, y, atCenter, states);
}

void displayStretched(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area, bool atCenter, const sf::RenderStates& states)
{
    Image* i = getImage(container, key);

    if (i == nullptr) return;

    auto w = static_cast<float>(area.w);
    auto h = static_cast<float>(area.h);

    i->sprite.setScale(w / i->texture.getSize().x, h / i->texture.getSize().y);
    i->display(target, area.x, area.y, atCenter, states);
    i->sprite.setScale(1, 1);
}

void displayFit(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area,
                const sf::RenderStates& states)
{
    Image* i = getImage(container, key);

    if (i == nullptr) return;

    auto w = static_cast<float>(area.w);
    auto h = static_cast<float>(area.h);

    float factor = std::min(w / i->texture.getSize().x, h / i->texture.getSize().y);
    i->sprite.setScale(factor, factor);
    i->display(target, area.x + area.w/2, area.y + area.h/2, true, states);
    i->sprite.setScale(1, 1);
}

void displayRepeatedly(sf::RenderTarget& target, unsigned int container, const string& key, double x, double y, int xQuantity, int yQuantity, bool atCenter, const sf::RenderStates& states)
{
    Image* i = getImage(container, key);

    if (i == nullptr) return;

    Vector2u is = i->texture.getSize();
    Vector2i of = i->offset;

    VertexArray area(Quads, 4 * xQuantity * yQuantity);

    if (atCenter)
    {
        x -= is.x * xQuantity / 2.0;
        y -= is.y * yQuantity / 2.0;
    }

    for (int xTmp = 0 ; xTmp < xQuantity ; ++xTmp)
    {
        for (int yTmp = 0 ; yTmp < yQuantity ; ++yTmp)
        {
            int o = 4*(xTmp * yQuantity + yTmp);

            area[o + 0].position = Vector2f(x + xTmp * is.x + of.x, y + yTmp * is.y + of.y);
            area[o + 1].position = Vector2f(x + (xTmp+1) * is.x + of.x, y + yTmp * is.y + of.y);
            area[o + 2].position = Vector2f(x + (xTmp+1) * is.x + of.x, y + (yTmp+1) * is.y + of.y);
            area[o + 3].position = Vector2f(x + xTmp * is.x + of.x, y + (yTmp+1) * is.y + of.y);

            area[o + 0].texCoords = Vector2f(0, 0);
            area[o + 1].texCoords = Vector2f(is.x, 0);
            area[o + 2].texCoords = Vector2f(is.x, is.y);
            area[o + 3].texCoords = Vector2f(0, is.y);
        }
    }

    RenderStates st = states;
    st.texture = &(i->texture);

    target.draw(area, st);
}

void displayTiled(sf::RenderTarget& target, unsigned int container, const string& key, multimedia::Area area,
                  int xOrigin, int yOrigin, const sf::RenderStates& states)
{
    Image* i = getImage(container, key);

    if (i == nullptr) return;

    auto w = static_cast<float>(area.w);
    auto h = static_cast<float>(area.h);

    auto is = i->texture.getSize();
    auto tx = static_cast<int>(is.x);
    auto ty = static_cast<int>(is.y);

    while (xOrigin > area.x) xOrigin -= tx;
    while (yOrigin > area.y) yOrigin -= ty;

    int xOffset = (area.x - xOrigin) % tx;
    int yOffset = (area.y - yOrigin) % ty;

    int xNumber = static_cast<int>(std::ceil(w / tx))
                + ((area.w % tx == 0) ? 1 : 0)
                + (((tx - xOffset) <= area.w % tx) ? 1 : 0);
    int yNumber = static_cast<int>(std::ceil(h / ty))
                + ((area.h % ty == 0) ? 1 : 0)
                + (((ty - yOffset) <= area.h % ty) ? 1 : 0);

    sf::VertexArray array(Quads, static_cast<std::size_t>(4 * xNumber * yNumber));

    for (int xTmp = 0 ; xTmp < xNumber ; ++xTmp)
    {
        for (int yTmp = 0 ; yTmp < yNumber ; ++yTmp)
        {
            auto o = static_cast<std::size_t>(4*(xTmp * yNumber + yTmp));

            array[o + 0].position = sf::Vector2f(std::max(area.x, area.x - xOffset + tx * xTmp),
                                                 std::max(area.y, area.y - yOffset + ty * yTmp));
            array[o + 1].position = sf::Vector2f(std::min(tx * ((area.x-xOrigin) / tx + xTmp+1) + xOrigin,
                                                          (area.x + area.w)),
                                                 array[o].position.y);
            array[o + 2].position = sf::Vector2f(array[o+1].position.x,
                                                 std::min(ty * ((area.y-yOrigin) / ty + yTmp+1) + yOrigin,
                                                          (area.y + area.h)));
            array[o + 3].position = sf::Vector2f(array[o].position.x, array[o+2].position.y);

            array[o + 0].texCoords = sf::Vector2f((xTmp == 0) ? xOffset : 0 , (yTmp == 0) ? yOffset : 0);
            array[o + 1].texCoords = sf::Vector2f((xTmp == xNumber-1) ? (area.w + xOffset) % tx : tx,
                                                  array[o].texCoords.y);
            array[o + 2].texCoords = sf::Vector2f(array[o+1].texCoords.x,
                                                  (yTmp == yNumber-1) ? (area.h + yOffset) % ty : ty);
            array[o + 3].texCoords = sf::Vector2f(array[o].texCoords.x, array[o+2].texCoords.y);
        }
    }

    sf::RenderStates st = states;
    st.texture = &(i->texture);

    target.draw(array, st);
}

void lockGLMutex(int id)
{
    while (Mutex_lock.test_and_set(std::memory_order_acquire))
    {
        // Skip the lock if the current thread is the locker of the mutex
        if (std::this_thread::get_id() == Mutex_locker)
            return;

        // Wait for some time...
        std::this_thread::sleep_for(1us);
    }

    Mutex_id.store(id, std::memory_order_release);
    Mutex_locker = std::this_thread::get_id();
}

void unlockGLMutex(int id)
{
    if (Mutex_id.load(std::memory_order_acquire) != id)
        return;

    Mutex_lock.clear(std::memory_order_release);
}

} //namespace imageManager
