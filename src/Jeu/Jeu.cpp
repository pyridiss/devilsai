
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <thread>

#include <SFML/Audio.hpp>

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "tools/timeManager.h"
#include "tools/signals.h"
#include "tools/math.h"
#include "textManager/textManager.h"

#include "gui/style.h"
#include "gui/window.h"
#include "gui/textWidget.h"
#include "gui/MainWindow.h"

#include "imageManager/imageManager.h"

#include "musicManager/musicManager.h"

#include "../ElementsCarte/ElementsCarte.h"
#include "Jeu.h"

#include "devilsai-gui/conversation.h"
#include "devilsai-gui/inventory.h"
#include "devilsai-gui/journal.h"
#include "devilsai-gui/console.h"
#include "devilsai-gui/storageBox.h"
#include "devilsai-gui/skillPanel.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/CheckPoint.h"
#include "devilsai-resources/Environment.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/Place.h"
#include "devilsai-resources/quests.h"
#include "devilsai-resources/Species.h"
#include "devilsai-resources/StorageBox.h"
#include "devilsai-resources/Trigger.h"

#include "options.h"

enum GameState
{
    LeftPanelOpened = 1 << 0,
    ShowCharacter   = (1 << 0) + (1 << 1),
    ShowInventory   = (1 << 0) + (1 << 2),
    ShowJournal     = (1 << 0) + (1 << 3),
    ShowSkills      = (1 << 0) + (1 << 4),
    LeftPanels      = (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4),
    ShowConsole     = 1 << 5,
    ShowStorageBox  = 1 << 6,
    LeftButtonPressed  = 1 << 7,
    RightButtonPressed = 1 << 8,
    LeftButtonReleased  = 1 << 9,
    RightButtonReleased = 1 << 10
};

void mainLoop(gui::MainWindow& app)
{
    uint16_t state = 0;

    Joueur* player = nullptr;
    bool managementActivated = false;
    string savePack;
    Place* currentPlace = nullptr;

    gui::Window mainMenuWindow("gui/main-menu.xml", app);
    gui::Window ingameToolbar("gui/ingame-toolbar.xml", app);
    gui::Window inventoryWindow("gui/inventory.xml", app);
    gui::Window storageBoxWindow("gui/storage-box.xml", app);
    gui::Window characterWindow("gui/character.xml", app);

    gui::Widget* fps = ingameToolbar.widget("fps");
    gui::Widget* tooltip = ingameToolbar.widget("tooltip");
    gui::Widget* playerDescription = characterWindow.widget("text");

    initConsole(app);
    initConversation(app);
    initJournal(app);
    initSkillPanel(app);

    tools::signals::addSignal("main-menu:disable-load-game");

    options::initLoadGameWindow(mainMenuWindow);
    options::initOptionsWindow(mainMenuWindow);

    tools::signals::addSignal("main-menu");

    View worldView(FloatRect(0, 0, app.getSize().x, app.getSize().y));
    worldView.setViewport(sf::FloatRect(0, 0, 1, 1));

    CheckPoint cursor;
    cursor.setTag("intern");
    tools::math::Shape s;
    s.circle(tools::math::Vector2d(0, 0), 5);
    cursor.setShape(s);

    CheckPoint screen;
    screen.setTag("intern");
    s.circle(tools::math::Vector2d(0, 0), app.getSize().x/2 * sqrt(2));
    screen.setShape(s);

    Individu* underCursor = nullptr;
    StorageBox* storageBoxUnderCursor = nullptr;
    StorageBox* openStorageBox = nullptr;
    UnitId selectedStorageBox {};
    bool inGame = false;
    bool loadingResources = false;
    bool cursorIsInWorld = false;
    bool showTooltip = false;
    bool cinematicMode = false;

    if (options::option<bool>(tools::hash("show-console")))
        state |= ShowConsole;

    auto closeStorageBox = [&]()
    {
        selectedStorageBox = UnitId(0);
        state &= ~ShowStorageBox;

        if (openStorageBox != nullptr)
        {
            openStorageBox->close();
            openStorageBox = nullptr;
        }
    };

    musicManager::playMusic(mainMenuWindow.music);

	while (true)
	{
        //1. Events & Signals

        if (loadingResources) imageManager::lockGLMutex(1);

        if (state & LeftButtonReleased)
            state &= ~ (LeftButtonPressed | LeftButtonReleased);
        if (state & RightButtonReleased)
            state &= ~ (RightButtonPressed | RightButtonReleased);

        Event event;
        while (app.pollEvent(event))
        {
            // Common window management

            if (event.type == Event::MouseMoved)
                gui::updateMousePosition(app);

            if (event.type == Event::Closed || (event.type == Event::KeyReleased && Keyboard::isKeyPressed(Keyboard::F4) && Keyboard::isKeyPressed(Keyboard::LAlt)))
                tools::signals::addSignal("exit");

            // Gui windows

            app.handleUserEvent(event);

            if (inGame)
            {
                ingameToolbar.manage(event);
                manageConversation(event);

                if ((state & ShowInventory) == ShowInventory)
                    manageInventoryScreen(inventoryWindow, event, player);
                if ((state & ShowJournal) == ShowJournal)
                    manageJournal(event);
                if ((state & ShowSkills) == ShowSkills)
                    manageSkillPanel(event, player);
                if ((state & ShowInventory) != ShowInventory && (state & ShowSkills) != ShowSkills)
                    manageSkillbar(event, player);
                if ((state & ShowStorageBox) == ShowStorageBox)
                    manageStorageBoxScreen(storageBoxWindow, event, player->inventory, openStorageBox);
                if ((state & ShowConsole) == ShowConsole && (state & LeftPanelOpened) == 0 && (state & ShowStorageBox) == 0)
                    manageConsole(event);
            }
            else
            {
                mainMenuWindow.manage(event);
            }

            // In-game events

            if (event.type == Event::MouseButtonPressed && cursorIsInWorld && !cinematicMode)
            {
                if (event.mouseButton.button == Mouse::Button::Left)
                    state |= LeftButtonPressed;
                if (event.mouseButton.button == Mouse::Button::Right)
                    state |= RightButtonPressed;

                closeStorageBox();

                //Retrieve the current individual under the cursor
                player->userSelection((underCursor) ? underCursor->id() : UnitId(0));

                //Retrieve the current storage box under the cursor
                if (storageBoxUnderCursor != nullptr && event.mouseButton.button == Mouse::Button::Left)
                    selectedStorageBox = storageBoxUnderCursor->id();
            }

            if (event.type == Event::MouseButtonReleased)
            {
                if (event.mouseButton.button == Mouse::Button::Left)
                    state |= LeftButtonReleased;
                if (event.mouseButton.button == Mouse::Button::Right)
                    state |= RightButtonReleased;
            }

            if (event.type == Event::KeyPressed || event.type == Event::KeyReleased)
                closeStorageBox();
        }

        app.checkTriggers();
        if (inGame)
        {
            ingameToolbar.checkTriggers();
            characterWindow.checkTriggers();
            checkSkillPanelTriggers();
        }
        else
        {
            mainMenuWindow.checkTriggers();
        }

        if (inGame && managementActivated)
            manageConversation();

        worldView.reset(FloatRect(0, 0, app.getSize().x, app.getSize().y - 106));
        worldView.setViewport(FloatRect(0, 50.f/(float)app.getSize().y, 1, (float)(app.getSize().y-106)/(float)app.getSize().y));

        if (loadingResources) imageManager::unlockGLMutex(1);

        if ((state & LeftPanelOpened) == LeftPanelOpened)
        {
            worldView.reset(FloatRect(0, 0, app.getSize().x/2.f, app.getSize().y - 106));
            worldView.setViewport(sf::FloatRect(0.5f, 50.f/(float)app.getSize().y, 0.5f, (float)(app.getSize().y-106)/(float)app.getSize().y));
        }
        if ((state & ShowStorageBox) == ShowStorageBox)
        {
            worldView.reset(FloatRect(0, 0, app.getSize().x, app.getSize().y - 306));
            worldView.setViewport(sf::FloatRect(0, 50.f/(float)app.getSize().y, 1, (float)(app.getSize().y - 306)/(float)app.getSize().y));
        }

        tools::signals::Signal signal = tools::signals::getNextSignal();
        while (signal.first != "")
        {
            if (signal.first == "error-occured") {
                //TODO: Do something here
            }
            if (signal.first == "main-menu")
            {
                inGame = false;
                managementActivated = false;
                musicManager::playMusic(mainMenuWindow.music);
            }
            if (signal.first == "pause-game") {
                managementActivated = false;
            }
            if (signal.first == "resume-game") {
                managementActivated = true;
            }
            if (signal.first == "savegame") {
                saveGame(savePack, player->displayedName());
            }
            if (signal.first == "rest")
            {
                player->Repos();
            }

            if (signal.first == "game-successfully-ended") {
                //TODO: Do something here
            }
            if (signal.first == "player-dead") {
                managementActivated = false;
            }
            if (signal.first == "clear-gamedata") {
                Clean_Partie();
            }
            if (signal.first == "start-new-game") {
                loadGameDataFileAsync(tools::filesystem::dataDirectory(), signal.second);
                savePack = options::getNewSavePack();
                loadingResources = true;
            }
            if (signal.first == "game-started") {
                player = devilsai::getResource<Joueur>("player");
                inGame = true;
                managementActivated = true;
                loadingResources = false;
            }
            if (signal.first == "change-player-name") {
                if (!signal.second.empty())
                    player->setCustomDisplayedName(textManager::fromStdString(signal.second));
            }

            if (signal.first == "delete-game")
            {
                options::deleteSavePack(signal.second);
                options::initLoadGameWindow(mainMenuWindow);
            }

            if (signal.first == "start-loaded-game") {
                savePack = signal.second;
                loadGameDataFileAsync(tools::filesystem::getSaveDirectoryPath() + savePack, "gamedata.xml");
                loadingResources = true;
            }
            if (signal.first.find("option-change") != string::npos)
            {
                options::changeOption(signal.first, signal.second);
            }
            if (signal.first == "screen-character") {
                state = (state & ~LeftPanels) | (((state & ShowCharacter) == ShowCharacter) ? 0 : ShowCharacter);
            }
            if (signal.first == "screen-equipment") {
                state = (state & ~LeftPanels) | (((state & ShowInventory) == ShowInventory) ? 0 : ShowInventory);
            }
            if (signal.first == "screen-skills") {
                state = (state & ~LeftPanels) | (((state & ShowSkills) == ShowSkills) ? 0 : ShowSkills);
            }
            if (signal.first == "screen-journal") {
                state = (state & ~LeftPanels) | (((state & ShowJournal) == ShowJournal) ? 0 : ShowJournal);
            }
            if (signal.first == "exit")
            {
                return;
            }
            if (signal.first == "player-skill") {
                player->Set_Activite(signal.second);
            }
            if (signal.first == "add-tooltip") {
                gui::optionType o;
                o.set<textManager::PlainText>(textManager::getText("places", signal.second));
                tooltip->setValue(o);
                showTooltip = true;
            }
            if (signal.first == "enable-cinematic-mode") {
                cinematicMode = true;
            }
            if (signal.first == "disable-cinematic-mode") {
                cinematicMode = false;
            }

            tools::signals::removeSignal();
            signal = tools::signals::getNextSignal();
        }

        if (!inGame)
        {
            managementActivated = false;

            if (loadingResources) imageManager::lockGLMutex(2);

            app.displayGui();
            mainMenuWindow.display(app);
            tools::timeManager::frameDone();
            app.display();

            if (loadingResources)
            {
                imageManager::unlockGLMutex(2);
                // Give priority to the loading process
                std::this_thread::sleep_for(20ms);
            }

            musicManager::manageRunningMusics();

            continue;
        }

        //2. Management

        if (!cinematicMode && cursorIsInWorld)
        {
            if (state & LeftButtonPressed)
                player->userRequest(leftClickSkill(), cursor.position(), false);
            else if (state & RightButtonPressed)
                player->userRequest(rightClickSkill(), cursor.position(), true);
        }

        if (managementActivated) devilsai::forEachResource<Joueur>([](Joueur* p)
        {
            p->manage();
        });

        worldView.setCenter((int)player->position().x, (int)player->position().y - 100 * ((state & ShowStorageBox) == ShowStorageBox));

        if (managementActivated)
        {
            devilsai::forEachResource<Carte>([&worldView](Carte* w)
            {
                w->GestionElements(worldView);
            });
        }

        //Screen position update
        screen.move(-screen.position().x + player->position().x,
                    -screen.position().y + player->position().y);

        //Mouse cursor
        cursor.move(-cursor.position().x + app.mapPixelToCoords(gui::mousePosition(), worldView).x,
                    -cursor.position().y + app.mapPixelToCoords(gui::mousePosition(), worldView).y);

        if (gui::mousePosition().x >= app.getSize().x * worldView.getViewport().left &&
            gui::mousePosition().y >= app.getSize().y * worldView.getViewport().top &&
            gui::mousePosition().x <= app.getSize().x * (worldView.getViewport().left + worldView.getViewport().width) &&
            gui::mousePosition().y <= app.getSize().y * (worldView.getViewport().top + worldView.getViewport().height))
            cursorIsInWorld = true;
        else cursorIsInWorld = false;

        underCursor = nullptr;
        storageBoxUnderCursor = nullptr;

        if (cursorIsInWorld)
        {
            auto result = player->world()->findFirstCollidingItem(&cursor, cursor.shape(), true);
            if (result.second == COLL_PRIM_MVT)
                underCursor = dynamic_cast<Individu*>(result.first);
            else if (result.second == COLL_INTER)
                storageBoxUnderCursor = dynamic_cast<StorageBox*>(result.first);
        }

        if (selectedStorageBox.unit() != nullptr)
        {
            if (tools::math::intersection(player->interactionField, selectedStorageBox->shape()))
            {
                player->stopAutomoving();
                openStorageBox = dynamic_cast<StorageBox*>(selectedStorageBox.unit());
                state |= ShowStorageBox;
                state &= ~LeftPanels;
            }
            else
            {
                if (openStorageBox == nullptr)
                    player->automove(selectedStorageBox->position());
                else closeStorageBox();
            }
        }

        //3. Display

        app.clear();
        app.setView(worldView);

        player->world()->display(app);

        if (underCursor != nullptr) underCursor->displayLifeGauge(app);

        if (storageBoxUnderCursor != nullptr) storageBoxUnderCursor->highlight();

        if (showTooltip)
        {
            int x = gui::mousePosition().x + 10;
            int y = gui::mousePosition().y + 10;
            tooltip->setPosition(x, y);
            tooltip->show();
        }
        else tooltip->hide();

        showTooltip = false;

        if (Keyboard::isKeyPressed(Keyboard::LAlt))
        {
            auto result = player->world()->findAllCollidingItems(&screen, screen.shape(), false);
            for (auto& v : result)
            {
                if (v.second == COLL_INTER)
                {
                    StorageBox* c = dynamic_cast<StorageBox*>(v.first);
                    if (c != nullptr) c->highlight();
                }
            }
        }

        if (player->targetedItem())
        {
            Individu* ind = dynamic_cast<Individu*>(player->targetedItem().unit());
            if (ind != nullptr && ind != underCursor) ind->displayLifeGauge(app);
        }

        app.setView(View(FloatRect(0, 0, app.getSize().x, app.getSize().y)));

        if (!options::option<bool>(tools::hash("cinematic-mode")))
        {
            player->displayHealthStatus(app, 30, 65);
        }

        displayConversation(app);

        if ((state & ShowInventory) == ShowInventory)
            displayInventoryScreen(inventoryWindow, app, player);
        if ((state & ShowJournal) == ShowJournal)
            displayJournal(app);
        if ((state & ShowSkills) == ShowSkills)
            displaySkillPanel(app, player);

        if ((state & ShowCharacter) == ShowCharacter)
        {
            gui::optionType o;
            o.set<textManager::PlainText>(player->characterDescription());
            playerDescription->setValue(o);
            characterWindow.display(app);
        }
        if ((state & ShowStorageBox) == ShowStorageBox)
            displayStorageBoxScreen(storageBoxWindow, app, player->inventory, openStorageBox);
        if ((state & ShowConsole) == ShowConsole && (state & LeftPanelOpened) == 0 && (state & ShowStorageBox) == 0)
            displayConsole(app);

        if ((state & ShowInventory) != ShowInventory && (state & ShowSkills) != ShowSkills)  // These screens display the skillbar by themselves
            displaySkillbar(app, player);

        app.displayGui();
        gui::optionType o;
        o.set<textManager::PlainText>(tools::timeManager::getFPS());
        fps->setValue(o);

        ingameToolbar.display(app);

		//2. GESTION DES MISSIONS

        devilsai::forEachResource<Quest>([](Quest* q)
        {
            q->manage();
        });

		//4. CHANGEMENTS DE LIEU

        if (currentPlace == nullptr || !intersection(player->shape(), currentPlace->shape()))
        {
            for (auto& p : player->world()->places)
            {
                if (intersection(p->shape(), player->shape()))
                {
                    currentPlace = p;

                    if (p->diplomacy() != player->diplomacy())
                        tools::signals::addSignal("ingame-toolbar:disable-rest");
                    else
                        tools::signals::addSignal("ingame-toolbar:enable-rest");

                    tools::signals::addSignal("place-changed", textManager::getText("places", p->name()).toStdString());

                    if (!p->music().empty())
                        musicManager::playMusic(p->music());

                    break;
                }
            }
        }

        tools::timeManager::frameDone();
        app.display();
        sf::Listener::setPosition(player->position().x, 0.f, player->position().y);
        sf::Listener::setDirection(cos(player->angle()), 0.f, sin(player->angle()));
        musicManager::manageRunningMusics();
	}
}

void Clean_Partie()
{
    clearConversation();

    devilsai::deleteResources<Species>();
    devilsai::deleteResources<Skill>();
    devilsai::deleteResources<Environment>();
    devilsai::deleteResources<Carte>();
    devilsai::deleteResources<Joueur>();
    devilsai::deleteResources<Trigger::Script>();
    devilsai::deleteResources<Quest>();
}
