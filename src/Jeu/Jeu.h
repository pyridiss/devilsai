
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef header_jeu
#define header_jeu

#include <string>

using std::string;

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}
namespace textManager{
    class PlainText;
}

namespace gui{
    class MainWindow;
}

void createWindow();

void mainLoop(gui::MainWindow& app);
void Clean_Partie();

void saveGame(const string& savePack, const textManager::PlainText& playerName);
void saveGameDataToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);
void loadGameDataFileAsync(const string& dataDirectory, const string& mainFile);
void loadGameDataFromXML(const string& dataDirectory, const string& mainFile);

#endif
