
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_SCROLLINGLIST_H
#define GUI_SCROLLINGLIST_H

#include <vector>

#include "gui/widget.h"

using namespace std;

namespace gui{

class ScrollingList : public Widget
{
    private:
        vector<pair<multimedia::RichText, string>> entries {};
        unsigned index {0};
        int scrolling {0};
        bool mouseScrolling = false;
        int firstPixel {0};

    public:
        ScrollingList();
        ~ScrollingList() = default;

    public:
        void addEntry(textManager::PlainText& entry, string data);
        void removeAllEntries();

        bool mouseHovering();
        int mouseHoveringIndex();
        bool mouseHoveringScrollbar();

        /**
         * Handles an event.
         *
         * \param event event to handle.
         * \return \a true if the event has been consumed, \a false otherwise.
         */
        bool handleUserEvent(sf::Event& event) override;

        void setValue(std::any v);

        void display(sf::RenderWindow& app);

        /**
         * Loads the properties of the scrolling list from a XML file.
         *
         * \param elem XML element to load the properties from.
         */
        void loadFromXMLElement(tinyxml2::XMLElement* elem) override;

    private:
        void setScrolling(int s);
};

} //namespace gui

#endif // GUI_SCROLLINGLIST_H
