
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2016  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_WINDOW_H
#define GUI_WINDOW_H

#include <any>
#include <string>
#include <list>
#include <unordered_map>
#include <vector>
#include <queue>

#include "ext/priority_vector.h"

#include "gui/widget.h"

using namespace std;

namespace gui{

enum Actions { SendSignal, ModifyValue, ModifyEmbeddedData, Enable, Disable, Show, Hide, Focus, Unfocus, NoAction };

class Window : public Widget
{
    private:
        struct Trigger
        {
            Widget*     sender       {nullptr};
            Widget*     dataProvider {nullptr};
            Widget*     target       {nullptr};
            Event       event        {Event::NoEvent};
            std::any    eventData    {};
            Actions     action       {NoAction};
            std::string signal       {};
            std::string dataName     {};

            Trigger() = default;
            Trigger(const Trigger& other) = delete;
            Trigger(Trigger&& other) = default;
            Trigger& operator=(const Trigger& right) = delete;
            Trigger& operator=(Trigger&& right) = delete;
        };
        struct WindowEvent
        {
            Widget*  widget {nullptr};
            Event    event  {Event::NoEvent};
            std::any data   {};

            WindowEvent() = default;
            WindowEvent(Widget* s, Event e, std::any d)
              : widget(s), event(e), data(d)
            { }
            WindowEvent(const WindowEvent& other) = delete;
            WindowEvent(WindowEvent&& other) = default;
            WindowEvent& operator=(const WindowEvent& right) = delete;
            WindowEvent& operator=(WindowEvent&& right) = delete;
        };

    protected:
        Window*                            _parent;
        ext::chars_map<Widget*>            _widgets         {};
        ext::chars_map<Window*>            _subwindows      {};
        std::vector<Trigger>               _triggers        {};
        std::vector<WindowEvent>           _events          {};
        std::vector<sf::Keyboard::Key>     _watchedKeys     {};
        std::queue<tools::signals::Signal> _capturedSignals {};

        ext::priority_vector<Widget*> _orderedWidgets {};

        Widget*                            _focusedWidget   {nullptr};

    public:
        Window(Widget* parent) : Widget(parent), _parent{dynamic_cast<Window*>(parent)} {}
        Window(const Window&)                = delete;
        Window(Window&&) noexcept            = default;
        Window& operator=(const Window&)     = delete;
        Window& operator=(Window&&) noexcept = default;
        ~Window() override;

    public:
        void display(RenderWindow& app);
        void checkTriggers();
        const map<string,Widget*>& getWidgets();
        void setValue(std::any v);
        void setValue(const std::string& w, std::any v);
        void askFocus(Widget* w, bool value);
        void widgetEvent(Widget* s, Event e, std::any d = 0) override;

        bool mouseHovering();

        /**
         * Handles an event.
         *
         * \param event event to handle.
         * \return \a true if the event has been consumed, \a false otherwise.
         */
        bool handleUserEvent(sf::Event& event) override;

        void loadFromFile(string path);
        void loadFromXML(tinyxml2::XMLElement* elem);

    protected:
        Widget* widget(const std::string& name);
};

} //namespace gui

#endif // GUI_WINDOW_H

