
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gui/widget.h"

#include <tinyxml2.h>

#include "tools/debug.h"

#include "ext/flags.h"

#include "tools/debug_xml.h"
#include "tools/math.h"

#include "textManager/textManager.h"

#include "gui/window.h"
#include "gui/style.h"

#include "imageManager/imageManager.h"

using namespace tinyxml2;

namespace gui{

enum class WidgetFlags
{
    OriginXCenter                 = 1 <<  0,
    OriginRight                   = 1 <<  1,
    OriginYCenter                 = 1 <<  2,
    OriginBottom                  = 1 <<  3,
    XPositionRelativeToCenter     = 1 <<  4,
    XPositionRelativeToRight      = 1 <<  5,
    YPositionRelativeToCenter     = 1 <<  6,
    YPositionRelativeToBottom     = 1 <<  7,
    XPositionRelativeToScreenSize = 1 <<  8,
    YPositionRelativeToScreenSize = 1 <<  9,
    WidthRelativeToScreenSize     = 1 << 10,
    WidthMeansFixedMargin         = 1 << 11,
    HeightRelativeToScreenSize    = 1 << 12,
    HeightMeansFixedMargin        = 1 << 13,
    Hidden                        = 1 << 14,
    Disabled                      = 1 << 15
};


const std::string& Widget::name()
{
    return _name;
}

void Widget::show()
{
    _flags &= ~(WidgetFlags::Hidden | WidgetFlags::Disabled);
    onShow();
}

void Widget::hide()
{
    _flags |= (WidgetFlags::Hidden | WidgetFlags::Disabled);
    unfocus();
}

bool Widget::hidden() const
{
    return _flags % WidgetFlags::Hidden;
}

void Widget::enable()
{
    _flags &= ~WidgetFlags::Disabled;
}

void Widget::disable()
{
    _flags |= WidgetFlags::Disabled;
}

bool Widget::disabled() const
{
    return _flags % WidgetFlags::Disabled;
}

void Widget::focus()
{
    _parent->askFocus(this, true);
    show();
}

void Widget::unfocus()
{
    _parent->askFocus(this, false);
}

int Widget::left() const
{
    return _x;
}

int Widget::top() const
{
    return _y;
}

int Widget::width() const
{
    return _width;
}

int Widget::height() const
{
    return _height;
}

void Widget::setTransform(sf::Transform t)
{
    _transform = t;
    move();
    auto p = _transform.transformPoint(_x, _y);
    _x = p.x;
    _y = p.y;
}

sf::Transform Widget::transform() const
{
    return _transform;
}

void Widget::move()
{
    _x = _xParameter + _parent->left();

    if (_flags % WidgetFlags::XPositionRelativeToCenter)
        _x = _parent->left() + _parent->width()/2 + _xParameter;
    if (_flags % WidgetFlags::XPositionRelativeToRight)
        _x = _parent->left() + _parent->width() - _xParameter;
    if (_flags % WidgetFlags::XPositionRelativeToScreenSize)
        _x = _parent->left() + _xParameter * _parent->width() / 100.0;

    if (_flags % WidgetFlags::OriginXCenter)
        _x -= width()/2;
    if (_flags % WidgetFlags::OriginRight)
        _x = _x - width();

    _y = _yParameter + _parent->top();

    if (_flags % WidgetFlags::YPositionRelativeToCenter)
        _y = _parent->top() + _parent->height()/2 + _yParameter;
    if (_flags % WidgetFlags::YPositionRelativeToBottom)
        _y = _parent->top() + _parent->height() - _yParameter;
    if (_flags % WidgetFlags::YPositionRelativeToScreenSize)
        _y = _parent->top() + _yParameter * _parent->height() / 100.0;

    if (_flags % WidgetFlags::OriginYCenter)
        _y -= height()/2;
    if (_flags % WidgetFlags::OriginBottom)
        _y = _y - height();

    onMove();
}

void Widget::resize()
{
    _width = _widthParameter;
    _height = _heightParameter;

    if (_flags % WidgetFlags::WidthRelativeToScreenSize)
        _width = _parent->width() * _widthParameter / 100.0;

    if (_flags % WidgetFlags::WidthMeansFixedMargin)
        _width = _parent->width() - _widthParameter;

    if (_flags % WidgetFlags::HeightRelativeToScreenSize)
        _height = _parent->height() * _heightParameter / 100.0;

    if (_flags % WidgetFlags::HeightMeansFixedMargin)
        _height = _parent->height() - _heightParameter;

    move();  // Update the position

    onResize();
}

void Widget::setPosition(int x, int y)
{
    _xParameter = x;
    _yParameter = y;
    move();
}

void Widget::setSize(int w, int h)
{
    _flags &= ~(WidgetFlags::WidthRelativeToScreenSize | WidgetFlags::WidthMeansFixedMargin |
                WidgetFlags::HeightRelativeToScreenSize | WidgetFlags::HeightMeansFixedMargin);

    _width = _widthParameter = w;
    _height = _heightParameter = h;

    move();  // Update the position
}

void Widget::setStyle(const char* style)
{
    _style = Style::get(style);
}

void Widget::addEmbeddedData(string name, std::any value)
{
    _embeddedData.erase(name);
    _embeddedData.emplace(std::move(name), std::move(value));
}

std::any& Widget::embeddedData(const string& name)
{
    return _embeddedData[name];
}

void Widget::loadFromXMLElement(const char* file, tinyxml2::XMLElement* elem)
{
    _name = elem->Attribute("name");
    setStyle(elem->Attribute("style"));

    elem->QueryAttribute("x", &_xParameter);
    elem->QueryAttribute("y", &_yParameter);
    elem->QueryAttribute("width", &_widthParameter);
    elem->QueryAttribute("height", &_heightParameter);

    if (elem->Attribute("OriginXCenter"))
        _flags |= WidgetFlags::OriginXCenter;
    if (elem->Attribute("OriginRight"))
        _flags |= WidgetFlags::OriginRight;
    if (elem->Attribute("OriginYCenter"))
        _flags |= WidgetFlags::OriginYCenter;
    if (elem->Attribute("OriginBottom"))
        _flags |= WidgetFlags::OriginBottom;
    if (elem->Attribute("OriginCenter"))
        _flags |= (WidgetFlags::OriginXCenter | WidgetFlags::OriginYCenter);

    if (elem->Attribute("XPositionRelativeToCenter"))
        _flags |= WidgetFlags::XPositionRelativeToCenter;
    if (elem->Attribute("XPositionRelativeToRight"))
        _flags |= WidgetFlags::XPositionRelativeToRight;
    if (elem->Attribute("YPositionRelativeToCenter"))
        _flags |= WidgetFlags::YPositionRelativeToCenter;
    if (elem->Attribute("YPositionRelativeToBottom"))
        _flags |= WidgetFlags::YPositionRelativeToBottom;
    if (elem->Attribute("PositionRelativeToCenter"))
        _flags |= (WidgetFlags::XPositionRelativeToCenter | WidgetFlags::YPositionRelativeToCenter);
    if (elem->Attribute("XPositionRelativeToScreenSize"))
        _flags |= WidgetFlags::XPositionRelativeToScreenSize;
    if (elem->Attribute("YPositionRelativeToScreenSize"))
        _flags |= WidgetFlags::YPositionRelativeToScreenSize;
    if (elem->Attribute("PositionRelativeToScreenSize"))
        _flags |= (WidgetFlags::XPositionRelativeToScreenSize | WidgetFlags::YPositionRelativeToScreenSize);

    if (elem->Attribute("WidthRelativeToScreenSize"))
        _flags |= WidgetFlags::WidthRelativeToScreenSize;
    if (elem->Attribute("WidthMeansFixedMargin"))
        _flags |= WidgetFlags::WidthMeansFixedMargin;
    if (elem->Attribute("HeightRelativeToScreenSize"))
        _flags |= WidgetFlags::HeightRelativeToScreenSize;
    if (elem->Attribute("HeightMeansFixedMargin"))
        _flags |= WidgetFlags::HeightMeansFixedMargin;

    if (elem->Attribute("Fullscreen"))
    {
        _widthParameter = 100;
        _heightParameter = 100;
        _flags |= (WidgetFlags::WidthRelativeToScreenSize | WidgetFlags::HeightRelativeToScreenSize);
    }

    if (elem->Attribute("Hidden"))
        _flags |= (WidgetFlags::Hidden | WidgetFlags::Disabled);

    if (elem->Attribute("VerticalScrollBar"))
        addFlags(VerticalScrollBar);

    XMLHandle hdl(elem);
    XMLElement *subElem = hdl.FirstChildElement().ToElement();
    while (subElem)
    {
        string_view subElemName = subElem->Name();

        if (subElemName == "addEmbeddedData" && subElem->Attribute("name") && subElem->Attribute("value"))
        {
            const char* name = subElem->Attribute("name");

            if (subElem->Attribute("type", "plaintext"))
                addEmbeddedData(name, textManager::PlainText(subElem->Attribute("value")));
            else if (subElem->Attribute("type", "float"))
                addEmbeddedData(name, subElem->FloatAttribute("value"));
            else
                addEmbeddedData(name, string(subElem->Attribute("value")));
        }

        subElem = subElem->NextSiblingElement();
    }

    // Update the position and the size
    move();
    resize();
}

} //namespace gui
