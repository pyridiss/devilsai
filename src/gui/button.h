
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2016  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

#include "gui/widget.h"

namespace gui{

class Button : public Widget
{
    private:
        bool autoRelease = true;

    public:
        Button();
        ~Button() = default;

    public:
        void setAutoRelease(bool a);

        bool mouseHovering();
        bool activated(sf::Event& event);

        /**
         * Handles an event.
         *
         * \param event event to handle.
         * \return \a true if the event has been consumed, \a false otherwise.
         */
        bool handleUserEvent(sf::Event& event) override;

        void setValue(std::any v);

        /**
         * Loads the properties of the button from a XML file.
         *
         * \param elem XML element to load the properties from.
         */
        void loadFromXMLElement(tinyxml2::XMLElement* elem) override;
};

} //namespace gui

#endif // GUI_BUTTON_H

