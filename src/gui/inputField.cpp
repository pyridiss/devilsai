
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tools/math.h"

#include "textManager/plainText.h"
#include "textManager/textManager.h"

#include "gui/inputField.h"
#include "gui/style.h"

namespace gui{

InputField::InputField()
  : input()
{
    _text.setDefaultFormatting(parameter<string>(tools::hash("input-text-font")),
                               parameter<int>(tools::hash("input-text-size")),
                               parameter<Color>(tools::hash("input-text-color")));

    _text.addFlags(textManager::HAlignCenter | textManager::VAlignCenter);
}

bool InputField::mouseHovering()
{
    if (mousePosition().x >= left() && mousePosition().x <= left() + width() &&
        mousePosition().y >= top() && mousePosition().y <= top() + height())
    {
        return true;
    }

    return false;
}

bool InputField::handleUserEvent(sf::Event& event)
{
    if (event.type == Event::TextEntered)
    {
        switch(event.text.unicode)
        {
            case 8 :    if (input.size() > 0) input.resize(input.size()-1);
                        break;  // Backspace
            case 13 :   break;  // Return
            case 27 :   input.clear();
                        break;  // Escape
            default :   input += event.text.unicode;
                        break;
        }

        setText(input);
        addEmbeddedData("value", textManager::PlainText(input).toStdString());

        return true;
    }

    return false;
}

void InputField::setValue(std::any v)
{
    if (v.type() == typeid(textManager::PlainText))
    {
        input = std::any_cast<textManager::PlainText>(v).aggregatedText();
        addEmbeddedData("value", std::any_cast<textManager::PlainText>(v).toStdString());
    }
    else if (v.type() == typeid(string))
    {
        input = textManager::PlainText(std::any_cast<string>(v)).aggregatedText();
        addEmbeddedData("value", std::any_cast<string>(v));
    }

    setText(input);
}

void InputField::loadFromXMLElement(tinyxml2::XMLElement* elem)
{
    Widget::loadFromXMLElement(elem);
}

} //namespace gui
