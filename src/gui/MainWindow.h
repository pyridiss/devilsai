
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_MAINWINDOW
#define GUI_MAINWINDOW

#include "ext/reverse_container.h"

#include "multimedia/Area.h"

#include "gui/window.h"


namespace gui{

/**
 * \brief A sf::RenderWindow that integrates the gui library of devilsai.
 *
 * MainWindow derives from gui::Window and sf::RenderWindow. This class is used to integrate the gui library
 * into the application's RenderWindow. This class should be used just like sf::RenderWindow to create new
 * windows. It just supports more functionality to load gui::Window's.
 */
class MainWindow : public Window, public sf::RenderWindow
{
    private:
        multimedia::Point _mouse {};

    public:
        /**
         * Overrides Widget::left() for screens.
         *
         * \return always 0.
         */
        int left() const override
        {
            return 0;
        }

        /**
         * Overrides Widget::top() for screens.
         *
         * \return always 0.
         */
        int top() const override
        {
            return 0;
        }

        /**
         * Returns the width of the main window.
         *
         * \return width of the main window.
         */
        int width() const override
        {
            return getSize().x;
        }

        /**
         * Returns the height of the main window.
         *
         * \return height of the main window.
         */
        int height() const override
        {
            return getSize().y;
        }

        /**
         * Loads a gui::Window from a file.
         *
         * The MainWindow class handles all the gui::Window's of the program. As gui::Window can be derived to
         * add custom behaviors, the function loadWindow() is templated to allow to load a window of any type
         * derived from gui::Window.
         *
         * As for widgets, the gui::Window's can be ordered with a "z-coordinate".
         *
         * \tparam T type of the window to create. Default is gui::Window.
         * \param filename path to the XML file describing the window.
         * \param z z-coordinate of the window. Default is 0.
         * \return pointer to the newly created window.
         */
        template <typename T = gui::Window, class = typename std::enable_if<std::is_base_of<gui::Window, T>::value>::type>
        T* loadWindow(const std::string& filename, int z = 0)
        {
            T* w = new T(filename, this);
            _subwindows.emplace(filename, w);
            _orderedWidgets.emplace(z, w);
            return w;
        }

        /**
         * Handles an event if the MainWindow has the focus.
         *
         * \param event event to handle.
         * \return \a true if the event has been consumed, \a false otherwise.
         */
        bool handleUserEvent(sf::Event& event) override
        {
            if (!hasFocus()) return false;

            if (event.type == sf::Event::MouseMoved)
                updateMousePosition();

            if (event.type == sf::Event::Resized)
            {
                setView(sf::View(sf::FloatRect(0, 0, getSize().x, getSize().y)));
                for (auto& [key, sw] : _subwindows)
                    sw->resize();
            }

            return Window::handleUserEvent(event);
        }

        /**
         * Calls Window::checkTriggers() if the MainWindow has the focus.
         */
        void checkTriggers()
        {
            if (!hasFocus()) return;

            Window::checkTriggers();
        }

        /**
         * Displays the GUI on the screen.
         */
        void displayGui()
        {
            for (auto& [p, w] : ext::reverse(_orderedWidgets))
                w->display(*this);
        }

        /**
         * Returns the position of the mouse relatively to the MainWindow.
         *
         * This is an override of Widget::mouse(). Calling a widget's mouse() will call its parent's mouse()
         * until this override is called.
         *
         * \return position of the mouse.
         */
        multimedia::Point mouse() const override
        {
            return _mouse;
        }

    private:
        void updateMousePosition()
        {
            auto m = sf::Mouse::getPosition(*this);
            _mouse.x = m.x;
            _mouse.y = m.y;
        }
        void display(RenderWindow& app) override {}

    public:
        using sf::RenderWindow::display;
};

}  // namespace gui

#endif  // GUI_MAINWINDOW
