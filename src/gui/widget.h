
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2016  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_WIDGET_H
#define GUI_WIDGET_H

#include <any>
#include <string>
#include <unordered_map>

#include "tools/debug.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "multimedia/Area.h"

#include "gui/Style.h"
#include "tools/signals.h"
#include "textManager/plainText.h"
#include "multimedia/RichText.h"

using namespace std;
using namespace sf;

namespace tinyxml2{
    class XMLElement;
}

namespace gui{

enum class WidgetFlags;


enum class Event
{
    WidgetActivated,
    WidgetActivatedLeftClick,
    WidgetActivatedRightClick,
    WidgetDeactivated,
    WidgetMouseOver,
    WidgetValueChanged,
    KeyPressed,
    KeyReleased,
    KeyHeld,
    SignalCaptured,
    NoEvent
};

enum Flags
{
    OriginXCenter = 1 << 0,
    OriginRight = 1 << 1,
    OriginYCenter = 1 << 2,
    OriginBottom = 1 << 3,
    XPositionRelativeToCenter = 1 << 4,
    XPositionRelativeToRight = 1 << 5,
    YPositionRelativeToCenter = 1 << 6,
    YPositionRelativeToBottom = 1 << 7,
    XPositionRelativeToScreenSize = 1 << 8,
    YPositionRelativeToScreenSize = 1 << 9,
    WidthRelativeToScreenSize = 1 << 10,
    WidthMeansFixedMargin = 1 << 11,
    HeightRelativeToScreenSize = 1 << 12,
    HeightMeansFixedMargin = 1 << 13,
    AdjustSizeToText = 1 << 14,
    AdjustBackgroundToSize = 1 << 15,
    RepeatBackgroundToFitSize = 1 << 16,
    VerticalScrollBar = 1 << 17,
    VerticalScrollBarInUse = 1 << 18,
    Hidden = 1 << 19,
    Disabled = 1 << 20,
    Activated = 1 << 21,
    MouseOver = 1 << 22,
    CustomTextShader = 1 << 23
};

class Widget
{
    private:
        Widget*       _parent;
        std::string   _name            {};
        int           _x               {};
        int           _y               {};
        int           _width           {};
        int           _height          {};
        sf::Transform _transform       {};
        int           _xParameter      {};
        int           _yParameter      {};
        int           _widthParameter  {};
        int           _heightParameter {};
        WidgetFlags   _flags           {};

    protected:
        Style*        _style           {nullptr};
        StyleStatus   _status          {};

    public:
        explicit Widget(Widget* parent) : _parent(parent) {}
        Widget(const Widget&)                = delete;
        Widget(Widget&&) noexcept            = default;
        Widget& operator=(const Widget&)     = delete;
        Widget& operator=(Widget&&) noexcept = default;
        virtual ~Widget()                    = default;
        std::unordered_map<std::string, std::any> _embeddedData {};

    public:

        /**
         * Returns the name of the widget.
         *
         * \return name of the widget.
         */
        const std::string& name();

        /**
         * Unhides and enables the widget.
         */
        void show();

        /**
         * Hides, disables and unfocus the widget.
         */
        void hide();

        /**
         * Tells whether the widget is hidden.
         *
         * \return \a true if the widget is hidden, \a false otherwise.
         */
        bool hidden() const;

        /**
         * Enables the widget.
         */
        void enable();

        /**
         * Disables the widget.
         */
        void disable();

        /**
         * Tells whether the widget is disabled.
         *
         * \return \a true if the widget is disabled, \a false otherwise.
         */
        bool disabled() const;

        /**
         * Asks the parent for the focus, enables and shows the widget.
         */
        void focus();

        /**
         * Removes the focus.
         */
        void unfocus();

        /**
         * Returns the left bound of the widget.
         *
         * The left bound is defined related to the whole window (not the parent).
         *
         * \return left bound of the widget.
         */
        virtual int left() const;

        /**
         * Returns the top bound of the widget.
         *
         * The top bound is defined related to the whole window (not the parent).
         *
         * \return top bound of the widget.
         */
        virtual int top() const;

        /**
         * Returns the width of the widget.
         *
         * \return width of the widget.
         */
        virtual int width() const;

        /**
         * Returns the height of the widget.
         *
         * \return height of the widget.
         */
        virtual int height() const;

        /**
         * Adds a transformation to the widget. Only translations are correctly handled.
         *
         * \param t transformation to apply.
         */
        void setTransform(sf::Transform t);

        /**
         * Gets the current transformation applied to the widget. Only translations are correctly handled.
         *
         * \return current applied transformation.
         */
        sf::Transform transform() const;

        /**
         * Updates the position of the widget.
         *
         * This function is called by the parent widget to update the position of the widget.
         */
        virtual void move();

        /**
         * Updates the size of the widget.
         *
         * This function is called by the parent widget to update the size of the widget.
         */
        virtual void resize();

        /**
         * Sets the position of the widget.
         *
         * The position is relative to the parent. Flags can be used to set the origin of the position.
         *
         * \param x new x-coordinate of the widget.
         * \param y new y-coordinate of the widget.
         */
        void setPosition(int x, int y);

        /**
         * Sets the size of the widget.
         *
         * If this function is used, the size will be fixed and the flags that can change the size will be removed.
         *
         * \param w new width of the widget.
         * \param h new height of the widget.
         */
        void setSize(int w, int h);

        /**
         * Virtual function called when the widget is moved.
         *
         * This function is defined in the class Widget but does nothing. It can be overriden in derived class to
         * adopt a particular behavior when the widget is moved.
         */
        virtual void onMove() {}

        /**
         * Virtual function called when the widget is resized.
         *
         * This function is defined in the class Widget but does nothing. It can be overriden in derived class to
         * adopt a particular behavior when the widget is resized.
         */
        virtual void onResize() {}

        /**
         * Virtual function called when the widget is shown, i.e. when Widget::show() is called.
         *
         * This function is defined in the class Widget but does nothing. It can be overriden in derived class to
         * adopt a particular behavior when the widget is shown.
         */
        virtual void onShow() {}

        /**
         * Sets the style of the widget.
         *
         * \param style name of the new style to use.
         */
        void setStyle(const char* style);

        /**
         * Returns the position of the mouse relatively to the window.
         *
         * Calling a widget's mouse() will call its parent's mouse() until an override actually returns the position
         * of the mouse. Such an override is defined in the MainWindow class.
         *
         * \return position of the mouse.
         */
        virtual multimedia::Point mouse() const
        {
            return _parent->mouse();
        }

        /**
         * Adds an embedded value to the widget.
         *
         * Widgets can carry any data of any type. Each piece of data stored is given a name used to retrieve it later.
         *
         * \param name name of the value to store.
         * \param value object to store.
         */
        void addEmbeddedData(string name, std::any value);

        /**
         * Returns an embedded value casted to its type.
         *
         * Given a name and a type, this function will checked if an embedded value corresponds.
         * If the value does not exist or has a different type, a warning is emitted and an empty value of type \a T
         * is returned.
         *
         * \param name name of the value to find
         * \tparam T type of the value to find
         * \return corresponding value if found, empty value otherwise.
         */
        template<typename T>
        T embeddedData(const string& name)
        {
            auto& v = _embeddedData[name];
            if (v.has_value() && v.type() == typeid(T))
            {
                return std::any_cast<T>(v);
            }
            else
            {
                tools::debug::warning("The current widget does not embed a value named " + name + " of type " + string(typeid(T).name()), "gui", __FILENAME__, __LINE__);
                return T();
            }
        }

        /**
         * Returns a reference to an embedded value as a std::any.
         *
         * If the value does not exists, it is created.
         *
         * \param name name of the value to find.
         * \return reference to the corresponding value.
         */
        std::any& embeddedData(const string& name);

        /**
         * Loads the properties of the widget from a XML file.
         *
         * This function is virtual to let the derived classes load their own properties.
         * However, the redefinitions should first call this one to load the properties related to the position
         * and the size.
         *
         * \param file name of the XML file (for debugging purposes).
         * \param elem XML element to load the properties from.
         */
        virtual void loadFromXMLElement(const char* file, tinyxml2::XMLElement* elem);

        virtual bool mouseHovering() = 0;

        virtual void setValue(std::any v) = 0;

        virtual bool activated(sf::Event& event) { return false; }

        /**
         * Pure virtual function that handles an event.
         *
         * The function must be defined in derived classes.
         *
         * If the widget handles (consumes) the event, this function should return \a true to avoid
         * another widget to handle it too.
         *
         * \param event event to handle.
         * \return \a true if the event has been consumed, \a false otherwise.
         */
        virtual bool handleUserEvent(sf::Event& event) = 0;

        /**
         * Pure virtual function to draw the widget on the screen.
         *
         * This function must be defined in derived classes.
         *
         * \param app target to draw on.
         * \param states states to apply when drawing.
         */
        virtual void display(sf::RenderTarget& app, const sf::RenderStates& states = sf::RenderStates::Default) = 0;

    protected:

        /**
         * Creates a widget-related event and pass it to the parent widget, until a parent widget handles it by
         * overriding this function.
         *
         * \param s widget that emits the event
         * \param e event to create
         * \param d data linked to the event
         */
        virtual void widgetEvent(Widget* s, Event e, std::any d = 0)
        {
            _parent->widgetEvent(s, e, std::move(d));
        }

        /**
         * Helper function that calls Widget::widgetEvent(this, e, d).
         *
         * Use this helper instead of the complete form to avoid confusion when a widget wants to handle the events of
         * its children (by overriding the complete form) and send some events to its own parent.
         *
         * \param e event to create
         * \param d data linked to the event
         */
        void widgetEvent(Event e, std::any d = 0)
        {
            Widget::widgetEvent(this, e, std::move(d));
        }
};

} //namespace gui

#endif // GUI_WIDGET_H

