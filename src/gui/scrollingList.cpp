
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include "tools/math.h"

#include "textManager/plainText.h"

#include "gui/scrollingList.h"
#include "gui/style.h"

namespace gui{

static constexpr int margin = 5;
static constexpr int line_height = 20;
static constexpr int scrollbar_width = 20;

ScrollingList::ScrollingList()
{
    _text.setDefaultFormatting(parameter<string>(tools::hash("text-font")),
                               parameter<int>(tools::hash("text-size")),
                               parameter<Color>(tools::hash("text-color")));
}

void ScrollingList::addEntry(textManager::PlainText& entry, string data)
{
    //The first entry is auto-selected.
    if (entries.empty())
        addEmbeddedData("value", data);

    multimedia::RichText t;
    t.setSize(width() - scrollbar_width, line_height);
    t.setDefaultFormatting(parameter<string>("list-text-font"_hash),
                           parameter<int>("list-text-size"_hash),
                           parameter<Color>("list-text-color"_hash));
    t.create(entry);

    entries.emplace_back(std::move(t), data);
}

void ScrollingList::removeAllEntries()
{
    entries.clear();
    index = 0;
    scrolling = 0;
    firstPixel = 0;
}

bool ScrollingList::mouseHovering()
{
    if (mousePosition().x >= left() && mousePosition().x <= left() + width() &&
        mousePosition().y >= top() && mousePosition().y <= top() + height())
    {
        return true;
    }

    return false;
}

int ScrollingList::mouseHoveringIndex()
{
    if (mousePosition().x >= left() && mousePosition().x <= left() + width() - scrollbar_width &&
        mousePosition().y >= top() + margin && mousePosition().y <= top() + height() - 2 * margin)
    {
        int i = (mousePosition().y + firstPixel - (top() + margin)) / line_height;
        return (i >= (int)entries.size()) ? -1 : i;
    }

    return -1;
}

bool ScrollingList::mouseHoveringScrollbar()
{
    int r = left() + width();
    return (mousePosition().x >= r - scrollbar_width && mousePosition().x <= r &&
            mousePosition().y >= top() && mousePosition().y <= top() + height());
}

void ScrollingList::setScrolling(int s)
{
    scrolling = std::clamp(s, 0, 1000);
    firstPixel = round(scrolling / 1000.0 * (line_height * (entries.size()+1) - height() - 2*margin));
    firstPixel = max(firstPixel, 0);
}

bool ScrollingList::handleUserEvent(sf::Event& event)
{
    if (mouseScrolling)
    {
        if (event.type == Event::MouseButtonReleased)
        {
            mouseScrolling = false;
            return true;
        }
        else if (event.type == Event::MouseMoved)
        {
            setScrolling(1000.0 * (double)(mousePosition().y - top()) / (double)height());
            return true;
        }
    }

    if (!mouseHovering()) return false;

    if (event.type == Event::MouseButtonPressed)
    {
        if (int i = mouseHoveringIndex() ; i != -1)
        {
            index = i;
            addEmbeddedData("value", entries[index].second);
            return true;
        }
        else if (mouseHoveringScrollbar())
        {
            mouseScrolling = true;
            setScrolling(1000.0 * (double)(mousePosition().y - top()) / (double)height());
            return true;
        }
    }
    else if (event.type == Event::MouseWheelScrolled)
    {
        setScrolling(scrolling - round(1000.0 * event.mouseWheelScroll.delta * line_height / (double)(line_height*(entries.size()+1) - height() - 2*margin)));
        return true;
    }

    return false;
}

void ScrollingList::setValue(std::any v)
{
    index = 0;
    while (index < entries.size() && entries[index].second != std::any_cast<string>(v))
        ++index;

    addEmbeddedData("value", entries[index].second);
}

void ScrollingList::display(RenderWindow& app)
{
    Widget::display(app);

    RectangleShape drawing(Vector2f(width(), height()));
    drawing.setPosition(left(), top());
    drawing.setFillColor(parameter<Color>(tools::hash("list-background-color")));
    drawing.setOutlineColor(Color(48, 48, 48, 255));
    drawing.setOutlineThickness(1);
    app.draw(drawing);

    RectangleShape bar(Vector2f(4, height() - 16));
    bar.setPosition(left() + width() - 12, top() + 8);
    bar.setFillColor(Color::White);
    app.draw(bar);

    CircleShape cursor(6);
    cursor.setPosition(left() + width() - 16, top() + scrolling / 1000.0 * (height()-12));
    cursor.setFillColor(Color::White);
    cursor.setOutlineColor(Color(48, 48, 48, 255));
    cursor.setOutlineThickness(1);
    app.draw(cursor);

    View newView(FloatRect(0, firstPixel, width(), height()-2*margin));
    newView.setViewport(FloatRect((float)left()/(float)app.getSize().x, (float)(top()+margin)/(float)app.getSize().y,
                                  width()/(float)app.getSize().x, (height()-2*margin)/(float)app.getSize().y));

    View currentView = app.getView();
    app.setView(newView);

    if (int h_index = mouseHoveringIndex() ; h_index != -1)
    {
        RectangleShape highlight(Vector2f(width() - scrollbar_width, line_height));
        highlight.setPosition(0, h_index * line_height);
        highlight.setFillColor(parameter<Color>(tools::hash("list-highlight-color")));
        app.draw(highlight);
    }

    RectangleShape selected(Vector2f(width() - scrollbar_width, line_height));
    selected.setPosition(0, index * line_height);
    selected.setFillColor(parameter<Color>("list-selected-color"_hash));
    app.draw(selected);

    int currentY = 0;
    for (auto& e : entries)
    {
        e.first.display(app, 5, currentY);
        currentY += line_height;
    }

    app.setView(currentView);
}

void ScrollingList::loadFromXMLElement(tinyxml2::XMLElement* elem)
{
    Widget::loadFromXMLElement(elem);
}

} //namespace gui
