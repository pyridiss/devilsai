
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "gui/Style.h"

#include "ext/chars_map.h"
#include "ext/flags.h"

#include <SFML/Graphics.hpp>
#include <tinyxml2.h>

#include "tools/debug_xml.h"
#include "tools/filesystem.h"
#include "tools/math.h"

#include "multimedia/colors.h"
#include "imageManager/imageManager.h"
#include "musicManager/musicManager.h"
#include "textManager/plainText.h"
#include "multimedia/RichText.h"
#include "textManager/textManager.h"

#include "gui/widget.h"


namespace gui{

enum class StyleFlags
{
    TextFont            = 1 <<  0,
    TextSize            = 1 <<  1,
    TextColor           = 1 <<  2,
    TextShader          = 1 <<  3,
    TextShadow          = 1 <<  4,
    BackgroundColor     = 1 <<  5,
    BackgroundImage     = 1 <<  6,
    BackgroundShader    = 1 <<  7,
    TextHAlignCenter    = 1 <<  8,
    TextVAlignCenter    = 1 <<  9,
    AdjustSizeToText    = 1 << 10,
    BackgroundStretched = 1 << 11,
    BackgroundTiled     = 1 << 12,
    BackgroundFit       = 1 << 13
};

enum class ActionType
{
    ChangeStyle,
    ResetShaderInstance,
    PlaySound
};


namespace{
    ext::chars_map<Style> styles;
}


void Style::formatText(multimedia::RichText& richText, const textManager::PlainText& source) const
{
    richText.setDefaultFormatting(textFont(), textSize(), textColor());

    if (textVAlignCenter()) { richText.addFlags(textManager::VAlignCenter); }
    if (textHAlignCenter()) { richText.addFlags(textManager::HAlignCenter); }
    if (adjustSizeToText()) { richText.setSize(0, 0); }

    richText.create(source);
}

void Style::displayBackground(sf::RenderTarget& target, multimedia::Area area, StyleStatus& status,
                              const sf::RenderStates& states) const
{
    const Style* style = this;

    if (status.event.hash)
    {
        if (Style* s = findChild(status.event) ; s != nullptr)
            style = s;
    }

    sf::Shader* shader = nullptr;
    if (style->hasBackgroundShader()) { shader = style->backgroundShader(status); }
    else if (hasBackgroundShader())   { shader = backgroundShader(status); }

    sf::RenderStates st = states;
    st.shader = shader;

    if (style->hasBackgroundColor())  { style->displayBackgroundColor(target, area, st); }
    else if (hasBackgroundColor())    { displayBackgroundColor(target, area, st); }

    if (style->hasBackgroundImage())  { style->displayBackgroundImage(target, area, st); }
    else if (hasBackgroundImage())    { displayBackgroundImage(target, area, st); }

    if (!style->hasBackgroundColor() && !hasBackgroundColor() &&
        !style->hasBackgroundImage() && !hasBackgroundImage() &&
        shader != nullptr)
    {
        multimedia::applyShaderOnScreen(target, shader, area);
    }
}

void Style::displayText(multimedia::RichText& text, sf::RenderTarget& target, multimedia::Area area,
                        multimedia::Point origin, StyleStatus& status, const sf::RenderStates& states) const
{
    const Style* style = this;

    if (status.event.hash)
    {
        if (Style* s = findChild(status.event) ; s != nullptr)
            style = s;
    }

    const sf::Font* f = style->textFont();
    if (f == nullptr) f = textFont();

    int s = style->textSize();
    if (s == 0) s = textSize();

    sf::Color c = style->textColor();
    if (c == sf::Color::Transparent) c = textColor();

    text.setDefaultFormatting(f, s, c);

    if (_flags % StyleFlags::TextShadow)
        text.addFlags(textManager::Shaded);

    sf::Shader* shader = style->textShader(status);
    if (shader == nullptr) shader = textShader(status);

    sf::RenderStates st = states;
    st.shader = shader;

    text.display(target, area, origin, st);
}

void Style::event(StyleStatus& status, const char* event, int priority) const
{
    if (_parent) _parent->event(status, event, priority);

    if (priority < status.priority) return;

    ext::chars_map_key_type i(event);
    if (i == status.event) return;

    status.priority = priority;

    for (auto& e : _onEvent) if (e.event == i)
    {
        if (e.action == ActionType::ChangeStyle)
        {
            status.event = i;
        }
        else if (e.action == ActionType::ResetShaderInstance)
        {
            status.eraseInstance(StyleStatus::Instance::Type::Text, this);
        }
        else if (e.action == ActionType::PlaySound)
        {
            musicManager::playSound(std::any_cast<std::string>(e.data), 0, 0, true);
        }
    }
}

void Style::clearEvent(StyleStatus& status, int priority) const
{
    if (status.priority == priority)
    {
        status.event.hash = 0;
        status.priority = 0;
    }
}

bool Style::adjustSizeToText() const
{
    if (_flags % StyleFlags::AdjustSizeToText) return true;
    if (_parent) return _parent->adjustSizeToText();
    return false;
}

void Style::setParent(Style* parent)
{
    _parent = parent;
}

void Style::addChild(const char* event, Style* child)
{
    Event e {ext::chars_map_key_type(event), ActionType::ChangeStyle, child};
    _onEvent.push_back(std::move(e));
}

Style* Style::findChild(ext::chars_map_key_type child) const
{
    for (auto& e : _onEvent) if (e.event == child)
    {
        if (e.action == ActionType::ChangeStyle)
            return std::any_cast<Style*>(e.data);
    }

    if (_parent) return _parent->findChild(child);

    return nullptr;
}

void Style::loadFromXML(const char* file, tinyxml2::XMLElement* elem)
{
    debug::XMLCheckAttribute(file, elem, "name");
    _name = elem->Attribute("name");

    if (elem->Attribute("parent"))  // TODO(quentin): ability to link to a parent not loaded yet.
    {
        if (auto s = styles.find(ext::chars_map_key_type(elem->Attribute("parent"))) ; s != styles.end())
            setParent(&(s->second));
    }

    tinyxml2::XMLElement* attribute = elem->FirstChildElement();
    while (attribute)
    {
        std::string_view attributeName = attribute->Name();

        if (attributeName == "text")
        {
            if (attribute->Attribute("font"))
            {
                _flags |= StyleFlags::TextFont;
                _textFont = multimedia::font(attribute->Attribute("font"));
            }
            if (attribute->Attribute("size"))
            {
                _flags |= StyleFlags::TextSize;
                _textSize = attribute->IntAttribute("size");
            }
            if (attribute->Attribute("color"))
            {
                _flags |= StyleFlags::TextColor;
                _textColor = multimedia::getColor(attribute->Attribute("color"));
            }
            if (attribute->Attribute("shader"))
            {
                _flags |= StyleFlags::TextShader;
                _textShader = attribute->Attribute("shader");
            }
            if (attribute->Attribute("Shadow", "1"))
            {
                _flags |= StyleFlags::TextShadow;
            }
            if (attribute->Attribute("AdjustSizeToText", "1"))
            {
                _flags |= StyleFlags::AdjustSizeToText;
            }
            if (attribute->Attribute("HAlignCenter", "1"))
            {
                _flags |= StyleFlags::TextHAlignCenter;
            }
            if (attribute->Attribute("VAlignCenter", "1"))
            {
                _flags |= StyleFlags::TextVAlignCenter;
            }
        }
        else if (attributeName == "background")
        {
            if (attribute->Attribute("color"))
            {
                _flags |= StyleFlags::BackgroundColor;
                _backgroundColor = multimedia::getColor(attribute->Attribute("color"));
            }
            if (attribute->Attribute("path"))
            {
                _flags |= StyleFlags::BackgroundImage;
                _backgroundImage = attribute->Attribute("path");
                imageManager::addImage(tools::hash("gui"), _backgroundImage, _backgroundImage);
            }
            if (attribute->Attribute("shader"))
            {
                _flags |= StyleFlags::BackgroundShader;
                _backgroundShader = attribute->Attribute("shader");
            }
            if (attribute->Attribute("shader-data"))
            {
                _backgroundShaderData = attribute->Attribute("shader-data");
            }
            if (attribute->Attribute("BackgroundStretched", "1"))
            {
                _flags |= StyleFlags::BackgroundStretched;
            }
            if (attribute->Attribute("BackgroundTiled", "1"))
            {
                _flags |= StyleFlags::BackgroundTiled;
            }
            if (attribute->Attribute("BackgroundFit", "1"))
            {
                _flags |= StyleFlags::BackgroundFit;
            }
        }
        else if (attributeName == "parameter")
        {
            debug::XMLCheckAttribute(file, attribute, "name");
            debug::XMLCheckAttribute(file, attribute, "type");

            std::string name = _name + "/" + attribute->Attribute("name");
            if (attribute->Attribute("type", "integer"))
            {
                debug::XMLCheckAttribute<int>(file, attribute, "value");
                parameterize<int>(name.c_str(), attribute->IntAttribute("value"));
            }
            else if (attribute->Attribute("type", "unsigned"))
            {
                debug::XMLCheckAttribute<unsigned>(file, attribute, "value");
                parameterize<unsigned>(name.c_str(), attribute->UnsignedAttribute("value"));
            }
            else if (attribute->Attribute("type", "string"))
            {
                debug::XMLCheckAttribute(file, attribute, "value");
                parameterize<std::string>(name.c_str(), attribute->Attribute("value"));
            }
        }
        else if (attributeName.compare(0, 2, "on") == 0 && !attribute->Attribute("style"))
        {
            if (attribute->Attribute("ResetShaderInstance", "1"))
            {
                Event e {ext::chars_map_key_type(attributeName.substr(2).data()), ActionType::ResetShaderInstance, 0};
                _onEvent.push_back(std::move(e));
            }
            else if (attribute->Attribute("play-sound"))
            {
                std::string sound = attribute->Attribute("play-sound");
                musicManager::addSound(sound);
                Event e {ext::chars_map_key_type(attributeName.substr(2).data()), ActionType::PlaySound, sound};
                _onEvent.push_back(std::move(e));
            }
        }

        attribute = attribute->NextSiblingElement();
    }
}

const sf::Font* Style::textFont() const
{
    if (_flags % StyleFlags::TextFont) return _textFont;
    if (_parent) return _parent->textFont();
    return nullptr;
}

int Style::textSize() const
{
    if (_flags % StyleFlags::TextSize) return _textSize;
    if (_parent) return _parent->textSize();
    return 0;
}

sf::Color Style::textColor() const
{
    if (_flags % StyleFlags::TextColor) return _textColor;
    if (_parent) return _parent->textColor();
    return sf::Color::Transparent;
}

sf::Shader* Style::textShader(StyleStatus& status) const
{
    if (!(_flags % StyleFlags::TextShader))
    {
        if (_parent) return _parent->textShader(status);
        return nullptr;
    }

    unsigned instance = 0;
    if (auto i = status.getInstance(StyleStatus::Instance::Type::Text, this) ; i.first)
    {
        instance = i.second;
    }
    else
    {
        instance = multimedia::createShaderInstance(_textShader, _textShaderData);
        status.addInstance(StyleStatus::Instance::Type::Text, this, instance);
    }

    return multimedia::shader(_textShader, instance);
}

bool Style::hasBackgroundColor() const
{
    if (_flags % StyleFlags::BackgroundColor) return true;
    if (_parent) return _parent->hasBackgroundColor();
    return false;
}

sf::Color Style::backgroundColor() const
{
    if (_flags % StyleFlags::BackgroundColor) return _backgroundColor;
    if (_parent) return _parent->backgroundColor();
    return sf::Color::Transparent;
}

bool Style::hasBackgroundImage() const
{
    if (_flags % StyleFlags::BackgroundImage) return true;
    if (_parent) return _parent->hasBackgroundImage();
    return false;
}

const std::string& Style::backgroundImage() const
{
    if (_flags % StyleFlags::BackgroundImage) return _backgroundImage;
    if (_parent) return _parent->backgroundImage();
    return _backgroundImage;
}

bool Style::hasBackgroundShader() const
{
    if (_flags % StyleFlags::BackgroundShader) return true;
    if (_parent) return _parent->hasBackgroundShader();
    return false;
}

sf::Shader* Style::backgroundShader(StyleStatus& status) const
{
    if (!(_flags % StyleFlags::BackgroundShader) && _parent) return _parent->backgroundShader(status);

    unsigned instance = 0;
    if (auto i = status.getInstance(StyleStatus::Instance::Type::Background, this) ; i.first)
    {
        instance = i.second;
    }
    else
    {
        instance = multimedia::createShaderInstance(_backgroundShader, _backgroundShaderData);
        status.addInstance(StyleStatus::Instance::Type::Background, this, instance);
    }

    return multimedia::shader(_backgroundShader, instance);
}

bool Style::textVAlignCenter() const
{
    if (_flags % StyleFlags::TextVAlignCenter) return true;
    if (_parent) return _parent->textVAlignCenter();
    return false;
}

bool Style::textHAlignCenter() const
{
    if (_flags % StyleFlags::TextHAlignCenter) return true;
    if (_parent) return _parent->textHAlignCenter();
    return false;
}

void Style::displayBackgroundColor(sf::RenderTarget& target, multimedia::Area area,
                                   const sf::RenderStates& states) const
{
    sf::RectangleShape drawing(sf::Vector2f(area.w, area.h));
    drawing.setPosition(area.x, area.y);
    drawing.setFillColor(backgroundColor());
    target.draw(drawing, states);
}

void Style::displayBackgroundImage(sf::RenderTarget& target, multimedia::Area area,
                                   const sf::RenderStates& states) const
{
    const std::string& background = backgroundImage();

    if (_flags % StyleFlags::BackgroundStretched)
        imageManager::displayStretched(target, "gui"_hash, background, area, false, states);

    else if (_flags % StyleFlags::BackgroundTiled)
        imageManager::displayTiled(target, "gui"_hash, background, area, area.x, area.y, states);

    else if (_flags % StyleFlags::BackgroundFit)
        imageManager::displayFit(target, "gui"_hash, background, area, states);

    else
        imageManager::display(target, "gui"_hash, background, area.x, area.y, false, states);
}

void Style::loadFromXMLFile(const char* file)
{
    struct link
    {
        Style* parent;
        const char* parentName;
        const char* event;
        const char* child;
    };
    std::vector<link> links;


    tinyxml2::XMLDocument xml;
    xml.LoadFile(file);

    tinyxml2::XMLHandle hdl(xml);
    tinyxml2::XMLElement* elem = hdl.FirstChildElement().FirstChildElement().ToElement();

    while (elem)
    {
        std::string_view elemName = elem->Name();

        if (elemName == "font")
        {
            debug::XMLCheckAttribute(file, elem, "name");
            debug::XMLCheckAttribute(file, elem, "file");
            multimedia::addFont(elem->Attribute("name"), elem->Attribute("file"));
        }

        if (elemName == "color")
        {
            debug::XMLCheckAttribute(file, elem, "name");
            debug::XMLCheckAttribute(file, elem, "value");
            multimedia::addColor(elem->Attribute("name"), elem->Attribute("value"));
        }

        else if (elemName == "style")
        {
            debug::XMLCheckAttribute(file, elem, "name");
            const char* name = elem->Attribute("name");
            auto [styleIterator, insertion] = styles.emplace(ext::chars_map_key_type(name), Style());
            Style* style = &(styleIterator->second);

            style->loadFromXML(file, elem);

            tinyxml2::XMLElement* attribute = elem->FirstChildElement();
            while (attribute)
            {
                std::string_view attributeName = attribute->Name();

                if (attributeName.compare(0, 2, "on") == 0 && attribute->Attribute("style"))
                {
                    const char* event = attributeName.substr(2).data();
                    const char* child = attribute->Attribute("style");
                    links.push_back(link{style, name, event, child});
                }
                attribute = attribute->NextSiblingElement();
            }

        }

        elem = elem->NextSiblingElement();
    }

    // Make links between the styles
    for (auto& l : links)
    {
        if (auto child = styles.find(ext::chars_map_key_type(l.child)) ; child != styles.end())
        {
            l.parent->addChild(l.event, &(child->second));
        }
        else
        {
            debug::warn << "Error in XML file " << file << ": "
                        << "Could not find the style '" << l.child << "' required by the style '" << l.parentName << "'"
                        << debug::end;
        }
    }
}

Style* Style::get(const char* style)
{
    if (auto s = styles.find(ext::chars_map_key_type(style)) ; s != styles.end())
        return &(s->second);

    debug::warn << __FILENAME__ << ":" << __LINE__ << ": Could not find the style '" << style << "'." << debug::end;

    return nullptr;
}

} //namespace gui
