
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gui/button.h"

#include "tools/math.h"

#include "textManager/textManager.h"
#include "musicManager/musicManager.h"
#include "gui/window.h"
#include "gui/style.h"

namespace gui{

Button::Button()
{
    _text.setDefaultFormatting(parameter<string>(tools::hash("button-text-font")),
                               parameter<int>(tools::hash("button-text-size")),
                               parameter<Color>(tools::hash("button-text-color")));
    _text.addFlags(textManager::HAlignCenter | textManager::VAlignCenter);
}

void Button::setAutoRelease(bool a)
{
    autoRelease = a;
}

bool Button::mouseHovering()
{
    if ((_flags & Disabled) == Disabled)
        return false;

    if (mousePosition().x >= left() && mousePosition().x <= left() + width() &&
        mousePosition().y >= top() && mousePosition().y <= top() + height())
    {
        _flags |= MouseOver;
        return true;
    }
    else _flags &= ~MouseOver;

//     if (autoRelease) currentState = "normal";
//     else if (currentState == "hover") currentState = "normal";

    return false;
}

bool Button::activated(Event& event)
{
    if ((_flags & Disabled) == Disabled)
        return false;

    if (mouseHovering())
    {
        if (event.type == Event::MouseButtonPressed)
        {
            musicManager::playSound("Click", 0, 0, true);

            if (autoRelease)
            {
                _flags |= Activated;
                return false; //We wait for MouseButtonReleased
            }
            else
            {
                _flags ^= Activated;
                _parent->addEvent(this, Event::WidgetActivated, embeddedData("value"));
                return true;
            }
        }
        if (event.type == Event::MouseButtonReleased)
        {
            if ((_flags & Activated) == Activated)
            {
                if (autoRelease)
                {
                    _flags &= ~Activated;
                    _parent->addEvent(this, Event::WidgetActivated, embeddedData("value"));
                    return true;
                }
                else //MouseButtonReleased must not be used
                {
                    return (_flags & Activated);
                }
            }
            else return false;
        }

        if (!autoRelease && (_flags & Activated) == Activated)
            return true;

        return false;
    }

    if (!autoRelease && (_flags & Activated) == Activated)
        return true;

    return false;
}

bool Button::handleUserEvent(sf::Event& event)
{
    if (disabled())
        return false;

    if (mouseHovering())
    {
        if (event.type == Event::MouseButtonPressed)
        {
            if (autoRelease)
            {
                _flags |= Activated;
            }
            else
            {
                _flags ^= Activated;
                _parent->addEvent(this, WidgetActivated, 0);
                if (event.mouseButton.button == Mouse::Left)
                    _parent->addEvent(this, WidgetActivatedLeftClick, 0);
                else if (event.mouseButton.button == Mouse::Right)
                    _parent->addEvent(this, WidgetActivatedRightClick, 0);
            }
            return true;
        }
        if (event.type == Event::MouseButtonReleased)
        {
            if (autoRelease && (_flags & Activated))
            {
                _flags &= ~Activated;
                _parent->addEvent(this, WidgetActivated, 0);
                if (event.mouseButton.button == Mouse::Left)
                    _parent->addEvent(this, WidgetActivatedLeftClick, 0);
                else if (event.mouseButton.button == Mouse::Right)
                    _parent->addEvent(this, WidgetActivatedRightClick, 0);
            }
            return true;
        }
    }

    return false;
}

void Button::setValue(std::any v)
{
}

void Button::loadFromXMLElement(tinyxml2::XMLElement* elem)
{
    Widget::loadFromXMLElement(elem);
}

} //namespace gui
