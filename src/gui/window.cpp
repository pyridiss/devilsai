
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gui/window.h"

#include <tinyxml2.h>

#include "ext/reverse_container.h"

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "tools/signals.h"
#include "tools/timeManager.h"
#include "tools/math.h"
#include "textManager/textManager.h"

#include "imageManager/imageManager.h"

#include "musicManager/musicManager.h"

#include "gui/textWidget.h"
#include "gui/button.h"
#include "gui/inputField.h"
#include "gui/dropDownList.h"
#include "gui/scrollingList.h"
#include "gui/progressBar.h"
#include "gui/style.h"

using namespace tinyxml2;

namespace gui{

Window::~Window()
{
    for (auto& w : _widgets)
        delete w.second;
    for (auto& s : _subwindows)
        delete s.second;
}

void Window::display(RenderWindow& app)
{
    if ((_flags & Hidden) == Hidden)
        return;

    if (!_backgroundShader.empty())
        multimedia::applyShaderOnScreen(app, _backgroundShader, _backgroundShaderInstance, left(), top(), width(), height());

    if (!_background.empty())
    {
        if ((_flags & AdjustBackgroundToSize) == AdjustBackgroundToSize)
        {
            imageManager::displayStretched(app, "gui"_hash, _background, left(), top(), width(), height());
        }
        else if ((_flags & RepeatBackgroundToFitSize) == RepeatBackgroundToFitSize)
        {
            imageManager::fillArea(app, "gui"_hash, _background, left(), top(), width(), height(), left(), top());
        }
        else
        {
            imageManager::display(app, tools::hash("gui"), _background, left(), top());
        }
    }

    for (auto& [p, w] : ext::reverse(_orderedWidgets))
        w->display(app);
}

bool Window::handleUserEvent(sf::Event& event)
{
    if ((_flags & Hidden) == Hidden)
        return;

    bool handled = false;

    if (_focusedWidget != nullptr)
    {
        handled = _focusedWidget->handleUserEvent(event);
    }
    else
    {
        for (auto& [p, w] : _orderedWidgets)
        {
            if (w->handleUserEvent(event))
            {
                _orderedWidgets.put_at_first_position(w);

                handled = true;
                break;  // If a widget has handled the event, stop there
            }
        }
    }

    if (!handled) for (auto i : _watchedKeys)  // Handle keyboard events only if subwindows and widgets did not
    {
        if (event.type == Event::KeyPressed && event.key.code == i)
        {
            handled = true;
            addEvent(nullptr, Event::KeyPressed, i);
        }
        if (event.type == Event::KeyReleased && event.key.code == i)
        {
            handled = true;
            addEvent(nullptr, Event::KeyReleased, i);
        }
    }

    return handled;
}

void Window::checkTriggers()
{
    while (!_capturedSignals.empty())
    {
        addEvent(nullptr, Event::SignalCaptured, _capturedSignals.front());
        _capturedSignals.pop();
    }

    for (auto i : _watchedKeys)
    {
        if (Keyboard::isKeyPressed(i))
        {
            addEvent(nullptr, Event::KeyHeld, i);
        }
    }

    for (auto& e : _events) for (auto& t : _triggers) if (e.widget == t.sender && e.event == t.event)
    {
        if (t.event == Event::KeyPressed || t.event == Event::KeyReleased || t.event == Event::KeyHeld)
        {
            if (std::any_cast<sf::Keyboard::Key>(t.eventData) != std::any_cast<sf::Keyboard::Key>(e.data))
                continue;
        }
        if (t.event == Event::SignalCaptured)
        {
            if (std::any_cast<string>(t.eventData) != std::any_cast<tools::signals::Signal>(e.data).first)
                continue;
        }

        switch (t.action)
        {
            case SendSignal:
            {
                string value;
                if (t.dataProvider != nullptr)
                    value = t.dataProvider->embeddedData<string>(t.dataName);
                tools::signals::addSignal(t.signal, value);
                break;
            }
            case ModifyValue:
            {
                std::any o = e.data;
                if (t.dataProvider != nullptr)
                    o = t.dataProvider->embeddedData(t.dataName);
                else if (e.data.type() == typeid(tools::signals::Signal))
                    o = std::any_cast<tools::signals::Signal>(e.data).second;

                if (t.target != nullptr)
                    t.target->setValue(o);

                break;
            }
            case ModifyEmbeddedData:
            {
                std::any o = e.data;
                if (t.dataProvider != nullptr)
                    o = t.dataProvider->embeddedData(t.dataName);
                else if (e.data.type() == typeid(tools::signals::Signal))
                    o = std::any_cast<tools::signals::Signal>(e.data).second;

                if (t.target != nullptr)
                    t.target->addEmbeddedData(t.signal, o);

                break;
            }
            case Enable:
                if (t.target != nullptr)
                    t.target->removeFlags(Disabled);
                break;
            case Disable:
                if (t.target != nullptr)
                    t.target->addFlags(Disabled);
                break;
            case Show:
                if (t.target != nullptr)
                    t.target->removeFlags(Hidden);
                break;
            case Hide:
                if (t.target != nullptr)
                {
                    t.target->addFlags(Hidden);
                    askFocus(t.target, false);
                }
                break;
            case Focus:
                if (t.target != nullptr)
                {
                    t.target->removeFlags(Hidden);
                    askFocus(t.target, true);
                }
                break;
            case Unfocus:
                if (t.target != nullptr)
                    askFocus(t.target, false);
                break;
            default:
                break;
        }
    }

    for (auto [key, value] : _subwindows)
        value->checkTriggers();

    _events.clear();
}

Widget* Window::widget(const std::string& name)
{
    if (auto i = name.find("/") ; i != string::npos)
    {
        string w = name.substr(0, i);
        ext::chars_map_key_type k {w.c_str()};

        if (w == "parent" && _parent)
        {
            return _parent->widget(name.substr(i+1, string::npos));
        }
        else
        {
            if (auto sw = _subwindows.find(k) ; sw != _subwindows.end())
                return sw->second->widget(name.substr(i+1, string::npos));
        }
    }
    else
    {
        ext::chars_map_key_type k {name.c_str()};

        if (auto w = _widgets.find(k) ; w != _widgets.end())
            return w->second;

        if (auto sw = _subwindows.find(k) ; sw != _subwindows.end())
            return sw->second;
    }

    debug::warn << "Unknown widget '" << name << "' in window '" << Widget::name() << "'." << debug::end;
    return nullptr;
}

const std::unordered_map<std::string, Widget*>& Window::getWidgets()
{
    return _widgets;
}

void Window::setValue(std::any v)
{
}

void Window::setValue(const std::string& w, std::any v)
{
    widget(w)->setValue(v);
}

void Window::widgetEvent(Widget* s, Event e, std::any d)
{
    _events.emplace_back(s, e, d);
}

void Window::askFocus(Widget* w, bool value)
{
    if (_focusedWidget != nullptr && _focusedWidget != w)
        return;

    if (value) _orderedWidgets.put_at_first_position(w);

    _focusedWidget = (value) ? w : nullptr;
}

bool Window::mouseHovering()
{
    if ((_flags & Disabled) == Disabled)
        return false;

    if (mousePosition().x >= left() && mousePosition().x <= left() + width() &&
        mousePosition().y >= top() && mousePosition().y <= top() + height())
    {
        return true;
    }

    return false;
}

void Window::loadFromFile(string path)
{
    path = tools::filesystem::dataDirectory() + path;

    XMLDocument file;
    file.LoadFile(path.c_str());

    XMLHandle hdl(file);
    loadFromXML(hdl.FirstChildElement().FirstChildElement().ToElement());
}

void Window::loadFromXML(XMLElement *elem)
{
    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "loadImage")
        {
            string key = elem->Attribute("key");
            string pathToImage = elem->Attribute("path");
            imageManager::addContainer(tools::hash("gui"));
            imageManager::addImage(tools::hash("gui"), key, pathToImage);
        }

        if (elemName == "properties")
        {
            loadFromXMLElement(elem);

            if (elem->Attribute("music"))
            {
                music = elem->Attribute("music");
                musicManager::addMusic(music);
            }
        }

        if (elemName == "widget")
        {
            string widgetName = elem->Attribute("name");
            string type = elem->Attribute("type");
            int z = elem->IntAttribute("z");
            Widget* widget = nullptr;
            if (type == "text")         widget = new TextWidget();
            if (type == "button")       widget = new Button();
            if (type == "input-field")  widget = new InputField();
            if (type == "drop-down-list") widget = new DropDownList();
            if (type == "scrolling-list") widget = new ScrollingList();
            if (type == "progress-bar") widget = new ProgressBar();

            if (widget == nullptr)
            {
                tools::debug::error("Unknown widget: " + type, "gui", __FILENAME__, __LINE__);
                elem = elem->NextSiblingElement();
                continue;
            }

            _widgets.emplace(std::move(widgetName), widget);
            _orderedWidgets.emplace(z, widget);

            widget->setParent(this);

            widget->loadFromXMLElement(elem);
        }

        if (elemName == "subwindow")
        {
            string_view subwindowName = elem->Attribute("name");
            int z = elem->IntAttribute("z");

            Window* subwindow = new Window();

            _subwindows.emplace(subwindowName, subwindow);
            _orderedWidgets.emplace(z, subwindow);

            subwindow->setParent(this);

            subwindow->loadFromXML(elem->FirstChildElement());
        }

        if (elemName == "trigger")
        {
            Trigger t;

            if (elem->Attribute("sender"))
                t.sender = widget(elem->Attribute("sender"));

            if      (elem->Attribute("event", "WidgetActivated"))    t.event = Event::WidgetActivated;
            else if (elem->Attribute("event", "WidgetValueChanged")) t.event = Event::WidgetValueChanged;
            else if (elem->Attribute("event", "KeyPressed"))         t.event = Event::KeyPressed;
            else if (elem->Attribute("event", "KeyReleased"))        t.event = Event::KeyReleased;
            else if (elem->Attribute("event", "KeyHeld"))            t.event = Event::KeyHeld;
            else if (elem->Attribute("event", "SignalCaptured"))     t.event = Event::SignalCaptured;

            if (elem->Attribute("eventDataType") && elem->Attribute("eventData"))
            {
                string_view dataType = elem->Attribute("eventDataType");

                if (dataType == "string")
                {
                    t.eventData = str(elem->Attribute("eventData"));
                }
                else if (dataType == "key")
                {
                    int k = 0;
                    elem->QueryAttribute("eventData", &k);
                    t.eventData = static_cast<Keyboard::Key>(k);
                    _watchedKeys.push_back(static_cast<Keyboard::Key>(k));
                }
            }

            if (elem->Attribute("action", "SendSignal")) t.action = SendSignal;
            else if (elem->Attribute("action", "ModifyValue")) t.action = ModifyValue;
            else if (elem->Attribute("action", "ModifyEmbeddedData")) t.action = ModifyEmbeddedData;
            else if (elem->Attribute("action", "Enable")) t.action = Enable;
            else if (elem->Attribute("action", "Disable")) t.action = Disable;
            else if (elem->Attribute("action", "Show")) t.action = Show;
            else if (elem->Attribute("action", "Hide")) t.action = Hide;
            else if (elem->Attribute("action", "Focus")) t.action = Focus;
            else if (elem->Attribute("action", "Unfocus")) t.action = Unfocus;

            if (elem->Attribute("signal"))
                t.signal = elem->Attribute("signal");

            if (elem->Attribute("dataProvider"))
                t.dataProvider = widget(elem->Attribute("dataProvider"));

            if (elem->Attribute("dataName"))
                t.dataName = elem->Attribute("dataName");

            if (elem->Attribute("target"))
                t.target = widget(elem->Attribute("target"));

            if (t.event == Event::SignalCaptured)
                tools::signals::registerListener(std::any_cast<string>(t.eventData), &_capturedSignals);

            _triggers.push_back(std::move(t));
        }

        elem = elem->NextSiblingElement();
    }
}

} //namespace gui
