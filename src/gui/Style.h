
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUI_STYLE
#define GUI_STYLE

#include <any>
#include <string>
#include <vector>

#include "ext/chars_map.h"

#include <SFML/Graphics.hpp>

#include "imageManager/imageManager.h"


namespace tinyxml2{
    class XMLElement;
}

namespace multimedia{
    class RichText;
}

namespace textManager{
    class PlainText;
}

namespace gui{

class Widget;
class Style;
enum class StyleFlags;
enum class ActionType;




/**
 * Helper structure that save some information about a particular widget needed to apply the stype correctly.
 *
 * Each widget must have a StyleStatus object to use the style. It stores some information about the shaders
 * defined in the style. Passing this object is often required to use Style's functions. It should never be
 * modified, only Style should use it.
 */
class StyleStatus
{
    struct Instance
    {
        enum class Type : uint8_t {Text, Background};
        const Style* style    {nullptr};
        unsigned int instance {};
        Type         type     {Type::Text};
    };

    ext::chars_map_key_type event     {""};
    std::vector<Instance>   instances {};
    int                     priority  {};

    std::pair<bool, unsigned int> getInstance(Instance::Type t, const Style* s)
    {
        for (auto& i : instances) if (i.type == t && i.style == s)
            return std::make_pair(true, i.instance);
        return std::make_pair(false, 0);
    }

    void addInstance(Instance::Type t, const Style* s, unsigned int i)
    {
        instances.push_back({s, i, t});
    }

    void eraseInstance(Instance::Type t, const Style* s)
    {
        for (auto i = instances.begin() ; i != instances.end() ; ++i) if (i->type == t && i->style == s)
        {
            instances.erase(i);
            return;
        }
    }

    friend class Style;
};

/**
 * Defines a style to be applied to widgets.
 *
 * Each widget contains a pointer to a style. Styles are globally loaded from XML files and each widget defined
 * must point to a style. Styles are used to defined the appearence of the texts, of the backgrounds, of shaders,
 * and how this appearence should evolve when a particular event occurs. Styles can also store "extra" information
 * for a particular class of widgets.
 *
 * Hierarchies of styles can be defined: when a style does not define a parameter, parent's parameter is used.
 *
 * Each style define one state of the widget; when an event occurs, another style can be defined to describe the
 * new appearence of the widget. A link between the two styles is made to do this. Some other things can be done
 * when an event occurs, for example playing a sound.
 */
class Style
{
    private:

        struct Event
        {
            ext::chars_map_key_type event  {""};
            ActionType              action {};
            std::any                data   {};
        };

        std::string        _name                 {};
        Style*             _parent               {nullptr};
        const sf::Font*    _textFont             {};
        int                _textSize             {};
        sf::Color          _textColor            {};
        std::string        _textShader           {};
        std::string        _textShaderData       {};
        sf::Color          _backgroundColor      {};
        std::string        _backgroundImage      {};
        std::string        _backgroundShader     {};
        std::string        _backgroundShaderData {};
        std::vector<Event> _onEvent              {};
        StyleFlags         _flags                {};

    public:

        /**
         * Constructs the given RichText using the given PlainText, using the format of the Style.
         *
         * \param richText RichText to construct.
         * \param source   source of the text.
         */
        void formatText(multimedia::RichText& richText, const textManager::PlainText& source) const;

        /**
         * Displays the background of the widget using the format of the Style.
         *
         * \param target target to draw on.
         * \param area   area of the target where to draw the background.
         * \param status StyleStatus object of the widget.
         * \param states sf::RenderStates passed to the drawing functions. If the Style defines a shader,
         *               the original shader will be replaced.
         */
        void displayBackground(sf::RenderTarget& target, multimedia::Area area, StyleStatus& status,
                               const sf::RenderStates& states) const;

        /**
         * Displays the text using the format of the Style.
         *
         * \param text   text to draw.
         * \param target target to draw on.
         * \param area   area of the target where to draw the text, passed to RichText::display().
         * \param origin origin of the text where to start drawing, passed to RichText::display().
         * \param status StyleStatus object of the widget.
         * \param states sf::RenderStates passed to the drawing functions. If the Style defines a shader,
         *               the original shader will be replaced.
         */
        void displayText(multimedia::RichText& text, sf::RenderTarget& target, multimedia::Area area,
                         multimedia::Point origin, StyleStatus& status, const sf::RenderStates& states) const;

        /**
         * Declares that an event occured.
         *
         * This function must be called when an event that can change the style occur.
         *
         * \param status   StyleStatus object of the widget.
         * \param event    name of the event that occured.
         * \param priority priority of the event. An event with a smaller priority would be discarded.
         */
        void event(StyleStatus& status, const char* event, int priority) const;

        /**
         * Resets the state of the Style.
         *
         * \param status   StyleStatus object of the widget.
         * \param priority priority of the event to clear. If the given priority is different from the priority of
         *                 the current event, the clear will be discarded.
         */
        void clearEvent(StyleStatus& status, int priority) const;

        /**
         * Returns a parameter of the style.
         *
         * Parameters are 'extra' data embedded in the Style that defines a particular type of widgets.
         * This function can be used to retrieve such parameters.
         *
         * \tparam T   type of the parameter to return.
         * \param name name of the parameter to return.
         */
        template<class T>
        T parameter(const char* name) const
        {
            std::string n = _name + "/";
            n += name;

            auto p = param<T>(n.c_str());
            if (p.first) return p.second;

            if (_parent) return _parent->parameter<T>(name);

            return T();
        }

        /**
         * Tells whether the Style recommands that the widget should be resized to the size of its text.
         *
         * \return \a true if this parameter is set, \a false otherwise.
         */
        bool adjustSizeToText() const;

    private:

        void setParent(Style* parent);
        void addChild(const char* event, Style* child);
        Style* findChild(ext::chars_map_key_type child) const;

        void loadFromXML(const char* file, tinyxml2::XMLElement* elem);

        const sf::Font* textFont() const;
        int textSize() const;
        sf::Color textColor() const;
        sf::Shader* textShader(StyleStatus& status) const;

        bool hasBackgroundColor() const;
        sf::Color backgroundColor() const;

        bool hasBackgroundImage() const;
        const std::string& backgroundImage() const;

        bool hasBackgroundShader() const;
        sf::Shader* backgroundShader(StyleStatus& status) const;

        bool textVAlignCenter() const;
        bool textHAlignCenter() const;

        void displayBackgroundColor(sf::RenderTarget& target, multimedia::Area area,
                                    const sf::RenderStates& states) const;
        void displayBackgroundImage(sf::RenderTarget& target, multimedia::Area area,
                                    const sf::RenderStates& states) const;

    public:

        /**
         * Loads Styles from an XML file.
         *
         * \param file file to read.
         */
        static void loadFromXMLFile(const char* file);

        /**
         * Returns the Style with the given name.
         *
         * \param style name of the style to return.
         * \return pointer to the Style if found, \a nullptr otherwise.
         */
        static Style* get(const char* style);

    private:

        inline static ext::chars_map<std::any> parameters {};

        template<typename T>
        static void parameterize(const char* name, T value)
        {
            parameters.emplace(ext::chars_map_key_type(name), value);
        }

        template<typename T>
        static std::pair<bool, T> param(const char* name)
        {
            const auto& i = parameters.find(ext::chars_map_key_type(name));
            if (i != parameters.end() && i->second.type() == typeid(T))
                return std::make_pair(true, std::any_cast<T>(i->second));

            return std::make_pair(false, T());
        }
};

}  // namespace gui

#endif  // GUI_STYLE
