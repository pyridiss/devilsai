
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Jeu/Jeu.h"

#include <thread>

#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "tools/signals.h"
#include "tools/math.h"

#include "imageManager/imageManager.h"
#include "textManager/textManager.h"

#include "devilsai-gui/journal.h"
#include "devilsai-gui/skillPanel.h"

#include "ElementsCarte/ElementsCarte.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/Environment.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/quests.h"
#include "devilsai-resources/Species.h"
#include "Jeu/options.h"

using namespace tinyxml2;


list< pair <string, string> > textFiles;

thread LoadGameThread;


void saveGame(const string& savePack, const textManager::PlainText& playerName)
{
    options::updateSavePack(savePack, playerName);

    string path = tools::filesystem::getSaveDirectoryPath() + savePack;

    //'files' will store the filenames to create index.xml
    list<string> files;


    //gamedata.xml
    XMLDocument gamedata_xml;

    XMLElement* gamedata_xml_root = gamedata_xml.NewElement("gamedata");
    gamedata_xml.InsertFirstChild(gamedata_xml_root);

    XMLHandle gamedata_xml_handle(gamedata_xml_root);
    saveGameDataToXML(gamedata_xml, gamedata_xml_handle);

    gamedata_xml.SaveFile((path + "gamedata.xml").c_str());
    files.push_back("gamedata.xml");


    //worlds xml files
    for (const auto& w : devilsai::getAllResources<Carte>())
    {
        XMLDocument world_xml;

        XMLElement* world_xml_root = world_xml.NewElement("world");
        world_xml.InsertFirstChild(world_xml_root);

        XMLHandle world_xml_handle(world_xml_root);
        w->saveToXML(world_xml, world_xml_handle);

        world_xml.SaveFile((path + w->Id + ".xml").c_str());
        files.push_back(w->Id + ".xml");
    }


    //index.xml
    XMLDocument index_xml;

    XMLElement* index_xml_root = index_xml.NewElement("files");
    index_xml.InsertFirstChild(index_xml_root);

    for (auto& f : files)
    {
        XMLElement* fileName = index_xml.NewElement("file");
        fileName->SetAttribute("path", f.c_str());
        index_xml_root->InsertEndChild(fileName);
    }

    index_xml.SaveFile((path + "index.xml").c_str());
}

void saveGameDataToXML(XMLDocument& doc, XMLHandle& handle)
{
    XMLElement* root = handle.ToElement();

    for (auto& t : textFiles)
    {
        XMLElement* textElem = doc.NewElement("loadTextFile");
        textElem->SetAttribute("name", t.first.c_str());
        textElem->SetAttribute("file", t.second.c_str());
        root->InsertEndChild(textElem);
    }

    for (auto& w : devilsai::getAllResources<Carte>())
    {
        XMLElement* worldElem = doc.NewElement("loadWorld");
        worldElem->SetAttribute("file", (w->Id + ".xml").c_str());
        worldElem->SetAttribute("name", (w->Id).c_str());
        worldElem->SetAttribute("localFile", true);
        root->InsertEndChild(worldElem);
    }


    devilsai::forEachResource<Joueur>([&root, &doc, &handle](Joueur* p)
    {
        p->saveToXML(doc, handle);
        root->LastChildElement()->SetAttribute("world", p->world()->Id.c_str());
        string playerName = p->displayedName().toStdString();
        root->LastChildElement()->SetAttribute("playerName", playerName.c_str());
    });

    devilsai::forEachResource<Quest>([&doc, &root](Quest* q)
    {
        q->save(doc, root);
    });

    XMLElement* journal = doc.NewElement("journal");
    XMLHandle h(journal);
    saveJournalToXML(doc, h);
    root->InsertEndChild(journal);

    XMLElement* skillbar = doc.NewElement("skillbar");
    saveSkillbarToXML(doc, skillbar);
    root->InsertEndChild(skillbar);
}

void loadGameDataFileAsync(const string& dataDirectory, const string& mainFile)
{
    if (!tools::filesystem::checkFile(dataDirectory + mainFile))
    {
        tools::signals::addSignal("unreachable-file");
        tools::signals::addSignal("main-menu");
        return;
    }

    LoadGameThread = thread([&]()
    {
        loadGameDataFromXML(dataDirectory, mainFile);
        // Lock the mutex to destroy the thread-local storage.
        imageManager::lockGLMutex(5);
    });

    thread t([]()
    {
        LoadGameThread.join();

        // Unlock the mutex when the thread-local storage is destoyed.
        imageManager::unlockGLMutex(5);

        tools::signals::addSignal("game-started");
    });
    t.detach();
}

void loadGameDataFromXML(const string& dataDirectory, const string& mainFile)
{
    XMLDocument file;
    file.LoadFile((dataDirectory + mainFile).c_str());

    XMLHandle hdl(file);
    XMLElement *elem = hdl.FirstChildElement().FirstChildElement().ToElement();

    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "loadTextFile")
        {
            textManager::loadFile(elem->Attribute("name"), elem->Attribute("file"), options::option<string>(tools::hash("language")));
            textFiles.emplace_back(elem->Attribute("name"), elem->Attribute("file"));
        }

        if (elemName == "species")
        {
            string speciesName = elem->Attribute("name");

            if (devilsai::getResource<Species>(speciesName) == nullptr)
            {
                Species *s = devilsai::addResource<Species>(speciesName);
                XMLHandle hdl2(elem);
                s->loadFromXML(hdl2, mainFile);
            }
        }

        if (elemName == "inertItemDesign")
        {
            string designName = elem->Attribute("name");

            if (devilsai::getResource<Environment>(designName) == nullptr)
            {
                Environment* design = devilsai::addResource<Environment>(designName);
                design->loadFromXML(elem, nullptr);
            }
        }

        if (elemName == "loadWorld")
        {
            string worldFile = elem->Attribute("file");
            string worldName = elem->Attribute("name");
            string tag = "ALL";
            if (elem->Attribute("tag")) tag = elem->Attribute("tag");

            Carte* w = devilsai::addResource<Carte>(worldName, worldName);  // just get it, if it already exists
            w->loadFromFile(dataDirectory + worldFile, tag);
        }

        if (elemName == "individual" || elemName == "character")
        {
            if (!elem->Attribute("world"))
            {
                tools::debug::error("Attribute 'world' is not defined in player declaration (" + mainFile + ").", "files", __FILENAME__, __LINE__);
                continue;
            }

            Joueur* player = devilsai::addResource<Joueur>(elem->Attribute("name"));

            player->setWorld(devilsai::getResource<Carte>(elem->Attribute("world")));

            if (player->world() == nullptr)
            {
                tools::debug::error("Player is declared in an unknown world: " + string(elem->Attribute("world")), "files", __FILENAME__, __LINE__);
                continue;
            }

            player->loadFromXML(elem, nullptr);

            if (elem->Attribute("playerName"))
                player->setCustomDisplayedName(textManager::fromStdString(string(elem->Attribute("playerName"))));
        }

        if (elemName == "quest")
        {
            Quest* q = devilsai::addResource<Quest>(elem->Attribute("file"), elem->Attribute("file"), elem->Attribute("initialData"));
            q->load(elem);
        }

        if (elemName == "journal")
        {
            XMLHandle h(elem);
            loadJournalFromXML(h);
        }

        if (elemName == "skillbar")
            loadSkillbarFromXML(elem);

        elem = elem->NextSiblingElement();
    }
}
