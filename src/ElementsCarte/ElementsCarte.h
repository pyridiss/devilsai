
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef header_elementscarte
#define header_elementscarte

#include <string>
#include <unordered_map>

#include <SFML/System.hpp>

#include "tools/vector2d.h"
#include "tools/shape.h"
#include "textManager/plainText.h"

#include "devilsai-resources/CheckPoint.h"
#include "devilsai-resources/skill.h"
#include "devilsai-resources/stats.h"
#include "devilsai-resources/pack.h"
#include "devilsai-resources/Unit.h"
#include "devilsai-resources/UnitId.h"

#define DIPLOM_NEUTRE       0
#define DIPLOM_ALLIE        1
#define DIPLOM_ENNEMI       2
#define DIPLOM_BREAKABLE    3

#define CLASSEMENT_SOL      0
#define CLASSEMENT_CADAVRE  1
#define CLASSEMENT_NORMAL   2
#define CLASSEMENT_HAUT     3
#define CLASSEMENT_NUAGE    4

#define COLL_OK             0
#define COLL_END            1
#define COLL_PRIM           2
#define COLL_PRIM_MVT       3
#define COLL_INTER          4
#define COLL_ATT            6
#define COLL_LIFE           7
#define COLL_VIS            8


using namespace std;
using namespace sf;

class Carte;
class Element_Carte;
class Individu;
class Species;

namespace tinyxml2{
    class XMLHandle;
    class XMLDocument;
}

/** INDIVIDUS **/


enum Character_State : uint16_t
{
    FixedStats = 1 << 0,
    FixedHealing = 1 << 1,
    MaximumEnergy = 1 << 2,
    FixedAngle = 1 << 3,
    UseOwnersName = 1 << 5,
    Dead = 1 << 6,
    Unshaped = 1 << 7
};


class Element_Carte : public Unit
{
	//Objet :
	public:
        UnitId Id;
		string Type         = "DEFAUT";
		string Liste        = "DEFAUT";
        int Diplomatie    = 0;
        double lifetime     = -1;
		int TypeClassement    = CLASSEMENT_NORMAL;

	//Constructeurs / Destructeurs :
	public:
		Element_Carte();
        Element_Carte& operator=(const Element_Carte& right) = delete;
        Element_Carte& operator=(Element_Carte&& right) = delete;
        virtual ~Element_Carte() = default;

	//Gestion :
	public:

        // Some temporary adaptations for the new interface Unit:

        const UnitId& id() const
        {
            return Id;
        }

        int height() const
        {
            return TypeClassement;
        }

        const string& type() const
        {
            return Type;
        }

        const string& tag() const
        {
            return Liste;
        }

        int diplomacy() const
        {
            return Diplomatie;
        }
};

class Individu : public Element_Carte
{
    public:
        struct SkillProperties
        {
            Skill* skill;
            double unavailability;
            unsigned level;
            int extraLevel;

            SkillProperties(Skill* s)
              : skill(s),
                unavailability(0),
                level(1),
                extraLevel(0)
            {
            }

            Skill* operator->()
            {
                return skill;
            }
        };
        struct SkillAccess
        {
            private:
                SkillProperties* p;

            public:
                SkillAccess()
                  : p(nullptr)
                {
                }

                SkillAccess(SkillProperties* s)
                  : p(s)
                {
                }

                Skill* operator->() const
                {
                    return p->skill;
                }

                bool operator==(const SkillAccess& right)
                {
                    return (p == right.p);
                }

                bool operator!=(const SkillAccess& right)
                {
                    return (p != right.p);
                }

                double& unavailability() const
                {
                    return p->unavailability;
                }

                unsigned level() const
                {
                    return max(0, (int)(p->level + p->extraLevel));
                }

                unsigned& skillLevel() const
                {
                    return p->level;
                }

                double extraStats(Attribute a) const
                {
                    if (level() == 0) return 0;
                    return p->skill->levels[level()-1].extraStats[a];
                }

                double extraStatsAmplifiers(Attribute a) const
                {
                    if (level() == 0) return 0;
                    return p->skill->levels[level()-1].extraStatsAmplifiers[a];
                }

                double maxUnavailability() const
                {
                    if (level() == 0) return 0;
                    return p->skill->levels[level()-1].unavailability;
                }

                bool none() const
                {
                    return (p == nullptr || level() == 0);
                }
        };

	public:
        Behaviors Comportement = Behaviors::Random;
        Behaviors NouveauComportement = Behaviors::Random;

	protected:
        tools::math::Shape _shape {};
        tools::math::Vector2d _position {};
        double _animationFrame;
        double _timer;
        UnitId _targetedItem {};
        SkillAccess _currentSkill;
        Stats _currentHealthStatus;
        Stats _attributes;
        string* _behaviors;
        vector<string>* _attacks;
        unordered_map<string, SkillProperties> _skills;
        Species* _species;
        textManager::PlainText* _displayedName;
        string* _corpseImageKey;
        unsigned int _experience = 0;
        string* _extraDataFile;
        Clock _clock;
        bool _owned {false};
        UnitId _owner {};
        Carte* _world;
        uint16_t _state {};
        double _angle {0};

	public:
        tools::math::Shape interactionField;
        tools::math::Shape viewField;
        vector<pair<Unit*, int>> seenItems;

        tools::math::Shape pathToTarget;
        tools::math::Shape validityOfPath;

        Pack inventory;

	//Constructeurs / Destructeurs :
	public:
		Individu();
        Individu(Species* s, double x, double y);
        Individu(const Individu& other) = delete;
        Individu(Individu&& other) = delete;
        Individu& operator=(const Individu& right) = delete;
        Individu& operator=(Individu&& right) = delete;
        virtual ~Individu();

	//Getters :
    protected:
        bool unshaped() const;
        bool destroyed() const;
        string& behavior(Behaviors b);
        vector<string>& attacks();
        void createCustomBehaviors();
        string& corpseImageKey();
        void createCorpse();

	public:
        const tools::math::Vector2d& position() const;
        const tools::math::Shape& shape() const;
        void move(double x, double y);
        UnitId targetedItem();
        virtual bool interact(Unit* item);
        Stats& attributes();
        SkillAccess skill(const string& s);
        const textManager::PlainText& displayedName();
        unsigned int experience();

        /**
         * Returns whether the character owns an item of given type.
         *
         * \param item item to test.
         * \return true if the character possesses the item, false otherwise.
         */
        bool possesses(const string& item);

        /**
         * If the character possesses an item of type \a item, uses it.
         *
         * \param item item to use.
         */
        void useItem(const string& item);

        void addUtterance(const textManager::PlainText& t);

	//Gestion :
	public:
        void manage();
        void MouvementAleatoire(bool newDirection);
        virtual void findPath(const tools::math::Vector2d& destination);
        virtual bool MouvementChasse(Unit *elem);
        void Gestion_Recuperation();
        bool Set_Activite(const string& nv);
        bool Set_Activite(Behaviors b);
        void continueCurrentSkill();

        /**
         * \brief Launch a fight against the targeted item.
         * \return true if the targeted item has been successfully hurt, false otherwise.
         */
        bool fight();
        int collision(Unit *elem, bool apply);
        void nextAnimationFrame();
        void gainExperience(int exp);
        void setOwner(Individu* o);
        Individu* owner();
        virtual bool improveAttribute(Attribute a, int chance, Individu* enemy);
        void addSkillLevel(const string& skillName, int quantity);
        void setWorld(Carte* w);
        Carte* world();
        double angle();

        /**
         * \brief Set the angle of the character.
         * Unlike the two other overloads, this one does not check if FixedAngle is set in the state of the
         * character or in the state of the current skill. This function is the only way to force a new angle.
         *
         * \param a angle, in radians, to set to the character.
         */
        void setAngle(double a);

        /** \brief Set the angle of the character to make it look at a point.
         *  Before setting the angle, this overload checks if the character can change its angle.
         *
         * \param v point which the character must look at.
         */
        void setAngle(tools::math::Vector2d v);

        /**
         * \brief Set the angle of the character to make it look at another unit.
         *  Before setting the angle, this overload checks if the character can change its angle.
         *
         * \param u unit that the character must look at.
         */
        void setAngle(UnitId u);
        int numberOfEnemiesInViewField();
        void addLifetime(double l);
        void destroy();

	public:
        int currentHealthStatus(Attribute a, bool forceUpdate = false);
        void setAttribute(Attribute a, double value);
        virtual void modifyHealthStatus(Attribute a, double value);
        Species* species();
        void setCustomDisplayedName(const textManager::PlainText& newName);

    public:
        void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);

	//Affichage
	public:
        void display(RenderTarget& target);
        void displayLifeGauge(RenderTarget& target);

    friend class Species;
};

class Joueur : public Individu
{
    private:
        Stats _displayedAttributes;
        sf::Clock _aStarClock {};
        bool _automove = false;
        tools::math::Vector2d _automoveEndpoint;
        bool _hunting = false;
        UnitId _hunted {};
        CheckPoint _fakeHunted {};
        string _skillForHunted;
        double _wakingTime;
        bool _tired;

	public:
		Joueur();
        Joueur(const Joueur& other) = delete;
        Joueur(Joueur&& right) = delete;
        Joueur& operator=(const Joueur& right) = delete;
        Joueur& operator=(Joueur&& right) = delete;
        ~Joueur() = default;

    private:
        bool doNextStep();

	public:
        void manage() override;
		void Gestion_Statistiques();
		void Repos();
        bool interact(Unit* item);
        void findPath(const tools::math::Vector2d& destination) override;

        /**
         * Sets a skill requested by the user.
         *
         * \param skill skill requested by the user.
         * \param point point in the world where the skill is requested.
         * \param force if set to false, the Random Behavior will be used instead of the \a skill if the player is not
         * hunting anyone or if the hunted character has the same diplomacy; otherwise, force the skill. Note that the
         * skill itself can refuse the change of skill in its atBegin() script function.
         */
        void userRequest(const string& skill, tools::math::Vector2D<double> point, bool force);

        /**
         * Sets a character selected by the user.
         *
         * \param other UnitId of the selected character.
         */
        void userSelection(UnitId other);

        bool improveAttribute(Attribute a, int chance, Individu* enemy);
        void resetDisplayedAttributes();

        void automove(const tools::math::Vector2d& p);
        UnitId huntedCharacter();
        void stopAutomoving();
        void stopHunting();

        void loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties);
        void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle);

        void displayHealthStatus(RenderTarget& target, int x, int y);

        textManager::PlainText characterDescription();
};

/**
 * Helper function used to convert from an insigned int representing a Unit to a pointer of the corresponding class
 * type.
 *
 * \param i id of the Unit
 * \tparam T class of the Unit
 * \return pointer of type \a T* pointing to the Unit
 */
template<typename T>
inline T* to(unsigned int i)
{
    static_assert(std::is_base_of<Unit, T>::value);
    UnitId c (i);
    return dynamic_cast<T*>(c.unit());
}

using Character = Individu;  // For a further change of name
using Player    = Joueur;    // For a further change of name

#endif
