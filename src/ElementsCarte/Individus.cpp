
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include "ElementsCarte.h"

#include "tools/debug.h"
#include "tools/timeManager.h"
#include "tools/math.h"
#include "tools/aStar.h"
#include "imageManager/imageManager.h"
#include "multimedia/RichText.h"
#include "textManager/textManager.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/Species.h"
#include "devilsai-resources/StorageBox.h"

#include "devilsai-gui/console.h"


Individu::Individu()
    : Element_Carte(),
    _animationFrame(0),
    _timer(0),
    _currentSkill(nullptr),
    _currentHealthStatus(),
    _attributes(),
    _behaviors(nullptr),
    _attacks(nullptr),
    _skills(),
    _species(nullptr),
    _displayedName(nullptr),
    _corpseImageKey(nullptr),
    _extraDataFile(nullptr),
    _clock(),
    _owner(),
    _world(),
    interactionField(),
    viewField(),
    seenItems(),
    pathToTarget(),
    validityOfPath(),
    inventory()
{
    _shape.setOrigin(&_position);
	TypeClassement = CLASSEMENT_NORMAL;
    _angle = tools::math::randomNumber(0.0, 2 * M_PI);
    interactionField.setOrigin(&position());
    viewField.setOrigin(&position());
    validityOfPath.circle(tools::math::Vector2d(0, 0), 20);
    inventory.setType(Pack::Type::Inventory);
}

Individu::Individu(Species* s, double x, double y)
  : Individu()
{
    _species = s;
    _species->Copie_Element(this);
    Type = _species->Type;
    move(x, y);
}

Individu::~Individu()
{
    if (_behaviors != nullptr)
        delete[] _behaviors;

    if (_attacks != nullptr)
        delete _attacks;

    if (_displayedName != nullptr)
        delete _displayedName;

    if (_corpseImageKey != nullptr)
        delete _corpseImageKey;

    if (_extraDataFile != nullptr)
        delete _extraDataFile;
}

void Character::destroy()
{
    lifetime = 0;
}

bool Character::destroyed() const
{
    return (lifetime == 0);
}

void Character::move(double x, double y)
{
    _position.x += x;
    _position.y += y;
}

const tools::math::Vector2d& Character::position() const
{
    return _position;
}

const tools::math::Shape& Character::shape() const
{
    return _shape;
}

bool Individu::Set_Activite(const string& nv)
{
    if (skill(nv).none()) return false;
    if (skill(nv).unavailability() > 0) return false;

    if (_currentSkill.none())
    {
        _currentSkill = skill(nv);
        _currentSkill->atBegin(this, _currentSkill.level());
        //Force the update of health status, as skills can change the speed
        currentHealthStatus(Strength, true);
        _animationFrame = 0;
        return true;
    }

    if (_currentSkill->priority > skill(nv)->priority && _animationFrame <= _currentSkill->numberOfImages)
        return false;

    if (_currentSkill == skill(nv)) return true;

    if (!skill(nv)->atBegin(this, skill(nv).level()))
        return false;

    _currentSkill = skill(nv);

    _timer = 0;

    //Force the update of health status, as skills can change the speed
    currentHealthStatus(Strength, true);

    if (_currentSkill->priority > 0)
        _animationFrame = 0;

    return true;
}

bool Individu::Set_Activite(Behaviors b)
{
    return Set_Activite(behavior(b));
}

void Character::continueCurrentSkill()
{
    while (_animationFrame > _currentSkill->numberOfImages)
        _animationFrame -= _currentSkill->numberOfImages;
}

int Individu::collision([[maybe_unused]] Unit *elem, [[maybe_unused]] bool apply)
{
    return COLL_PRIM_MVT;
}

void Individu::nextAnimationFrame()
{
    if (_currentSkill->step != 0)
        _animationFrame += currentHealthStatus(_currentSkill->speedAttribute) * _timer / abs((double)_currentSkill->step);
    else
        _animationFrame += currentHealthStatus(_currentSkill->speedAttribute) * _timer / 10.0;

    if (_currentSkill == skill(behavior(Behaviors::Dying)) && (unsigned)_animationFrame >= _currentSkill->numberOfImages-1)
    {
        _animationFrame = _currentSkill->numberOfImages-1;
        _state |= Dead;
    }

    if (_animationFrame > _currentSkill->numberOfImages)
    {
        _currentSkill->atEnd(this, _currentSkill.level());
        _currentSkill.unavailability() = _currentSkill.maxUnavailability();
        Set_Activite(Behaviors::Random);
    }

    while (_animationFrame > _currentSkill->numberOfImages)
        _animationFrame -= _currentSkill->numberOfImages;
}

UnitId Individu::targetedItem()
{
    return _targetedItem;
}

bool Individu::interact(Unit* item)
{
    return intersection(interactionField, item->shape());
}

bool Character::unshaped() const
{
    return (_state & Unshaped);
}

string& Individu::behavior(Behaviors b)
{
    if (_behaviors != nullptr)
        return _behaviors[b];

    if (_species != nullptr)
        return _species->_behaviors[b];

    //In case there is no custom behaviors nor species...
    createCustomBehaviors();
    return _behaviors[b];
}

void Individu::createCustomBehaviors()
{
    _behaviors = new string[Behaviors::numberOfBehaviors];
}

vector<string>& Individu::attacks()
{
    if (_attacks != nullptr)
        return *_attacks;

    if (_species != nullptr)
        return _species->_attacks;

    //In case there is no custom attacks nor species...
    _attacks = new vector<string>;
    return *_attacks;
}

string& Individu::corpseImageKey()
{
    if (_corpseImageKey != nullptr)
        return *_corpseImageKey;

    if (_species != nullptr)
        return _species->corpseImageKey;

    //In case there is no custom corpse image key nor species...
    _corpseImageKey = new string;
    return *_corpseImageKey;
}

void Individu::createCorpse()
{
    if (corpseImageKey().empty()) return;  // No image key for the corpse means that there is no corpse to create.

    StorageBox *corpse = world()->AjouterCoffre("storage-boxes", position().x, position().y);
    corpse->corpse(Type, corpseImageKey());

    for (auto& i : inventory.objects)
        corpse->insertItem(std::move(i));

    inventory.objects.clear();

    corpse->close();
}

Stats& Individu::attributes()
{
    return _attributes;
}

Individu::SkillAccess Individu::skill(const string& s)
{
    auto i = _skills.find(s);
    if (i != _skills.end()) return SkillAccess(&i->second);

    return SkillAccess(nullptr);
}

const textManager::PlainText& Individu::displayedName()
{
    if ((_state & UseOwnersName) && _owner)
        return owner()->displayedName();

    if (_displayedName != nullptr)
        return *_displayedName;

    if (_species != nullptr)
        return _species->_displayedName;

    //In case there is no custom name nor species...
    setCustomDisplayedName(textManager::PlainText());
    return *_displayedName;
}

unsigned int Individu::experience()
{
    if (_species != nullptr)
        return _experience + _species->_experience;

    return _experience;
}

bool Character::possesses(const string& item)
{
    return inventory.find(item);
}

void Character::useItem(const string& item)
{
    if (WearableItem* i = inventory.find(item) ; i != nullptr)
    {
        if (i->quantity() > 1)
        {
            WearableItem d = i->duplicate();
            d.setQuantity(1);
            i->setQuantity(i->quantity() - 1);

            if (!inventory.insertItem(std::move(d), d.requiredSlot()))
                i->setQuantity(i->quantity() + 1);
        }
        else i->setSlot(i->requiredSlot());
    }
}

double Character::angle()
{
    return _angle;
}

void Character::setAngle(double a)
{
    _angle = a;
}

void Character::setAngle(tools::math::Vector2d v)
{
    if (((_state | _currentSkill->extraState) & FixedAngle) == 0)
        _angle = tools::math::angle(v.x - _position.x, v.y - _position.y);
}

void Character::setAngle(UnitId u)
{
    if (!u) return;
    if (((_state | _currentSkill->extraState) & FixedAngle) == 0)
        _angle = tools::math::angle(u->position().x - _position.x, u->position().y - _position.y);
}

void Individu::Gestion_Recuperation()
{
    double t = tools::timeManager::I(1.0);
    double life = currentHealthStatus(Life);

    modifyHealthStatus(Life, currentHealthStatus(Healing)/1000.0 * t);
    modifyHealthStatus(Energy, currentHealthStatus(Healing)/1000.0 * t);

    if (currentHealthStatus(Life) == 0 && life != 0)
    {
        textManager::PlainText text = textManager::getText("devilsai", "console-died-from-wounds");
        textManager::PlainText c;
        if (Diplomatie == DIPLOM_ALLIE)
            c += "@c[128,255,128]";
        if (Diplomatie == DIPLOM_ENNEMI)
            c += "@c[255,128,128]";
        c += displayedName();
        text.addParameter(c);
        addConsoleEntry(text);
    }

    if (currentHealthStatus(Healing) > 95)
    {
        modifyHealthStatus(Life, t);
        modifyHealthStatus(Energy, t);
    }

    double diff = currentHealthStatus(HealingPower) + (currentHealthStatus(Life)-800.0)/100.0 - currentHealthStatus(Healing);
    modifyHealthStatus(Healing, diff / 1000.0 * t);

    //Test de récupération forcée (potion, …)
    if ((_state & FixedHealing) || abs(currentHealthStatus(HealingPower)) >= 95)
        setAttribute(Healing, currentHealthStatus(HealingPower));

    if (_state & MaximumEnergy) setAttribute(Energy, 1000);

    inventory.updateRemainingDurations();

    //Update the unavailability of the skills
    for (auto& i : _skills)
    {
        i.second.unavailability = max(0.0, i.second.unavailability - t/60.0);
        if (i.second.unavailability < 0.1)
        {
            if (string item = i.second->relatedItem() ; !item.empty())
            {  // The skill remains unavailable if the character do no have the related item anymore
                if (inventory.find(item) == nullptr) i.second.unavailability = 0.1;
            }
        }
    }
}

int Individu::currentHealthStatus(Attribute a, bool forceUpdate)
{
    //Update values
    if (forceUpdate || _clock.getElapsedTime().asSeconds() > 1)
    {
        // First, check if some wearable items may improve the level of the skills

        for (auto& s : _skills)
            s.second.extraLevel = 0;

        for (auto& o : inventory.objects)
        {
            for (auto& s : _skills)
                s.second.extraLevel += o.currentSkillImprovement(s.first);
        }

        // Now, update the _currentHealthStatus array

        _currentHealthStatus = _attributes;

        for (int i = Strength ; i != numberOfAttributes ; ++i)
        {
            Attribute att = static_cast<Attribute>(i);
            AttributeAmplifier attAmplifier = static_cast<AttributeAmplifier>(i);

            if (att != HealingPower && (_state & FixedStats) == 0)
                _currentHealthStatus.set(att, _currentHealthStatus[att] / 2.0 * (1 + 1.2*_currentHealthStatus[Life]/1000.0));

            for (auto& o : inventory.objects)
            {
                double add = o.currentObjectEffect(attributeToString(att));
                double mul = o.currentObjectEffect(attributeAmplifierToString(attAmplifier));
                _currentHealthStatus.add(att, add + _attributes[att] * mul / 100.0);
            }

            if (!_currentSkill.none())
            {
                _currentHealthStatus.add(att, _currentSkill.extraStats(att));
                _currentHealthStatus.add(att, _currentSkill.extraStatsAmplifiers(att) * _attributes[att] / 100.0);
            }
        }

        _clock.restart();
    }

    //Return asked value
    return floor(_currentHealthStatus[a]);
}

void Individu::setAttribute(Attribute a, double value)
{
    if (a == Life || a == Energy)
    {
        value = min(max(0.0, value), 1000.0);
        _currentHealthStatus.set(a, value);
        _attributes.set(a, value);
    }
    else if (a == Healing)
    {
        if (_state & FixedHealing) return;
        value = min(max(-100.0, value), 100.0);
        _currentHealthStatus.set(a, value);
        _attributes.set(a, value);
    }
    else if (a == Precision)
    {
        value = min(max(0.0, value), 100.0);
        _attributes.set(a, value);
        currentHealthStatus(a, true);
    }
    else
    {
        value = max(0.0, value);
        _attributes.set(a, value);
        currentHealthStatus(a, true);
    }
}

void Individu::modifyHealthStatus(Attribute a, double value)
{
    if (a == Life || a == Energy)
    {
        value = min(max(-_currentHealthStatus[a], value), 1000.0 - _currentHealthStatus[a]);
        _currentHealthStatus.add(a, value);
        _attributes.add(a, value);
    }
    if (a == Healing && (_state & FixedHealing) == 0)
    {
        value = min(max(-100.0 - _currentHealthStatus[a], value), 100.0 - _currentHealthStatus[a]);
        _currentHealthStatus.add(a, value);
        _attributes.add(a, value);
    }
}

Species* Individu::species()
{
    return _species;
}

void Individu::setCustomDisplayedName(const textManager::PlainText& newName)
{
    if (_displayedName == nullptr)
        _displayedName = new textManager::PlainText;

    *_displayedName = newName;
}

void Character::addUtterance(const textManager::PlainText& t)
{
    _world->addUtterance(Id, t);
}

int Character::numberOfEnemiesInViewField()
{
    int otherDiplomacy = (Diplomatie == DIPLOM_ENNEMI) ? DIPLOM_ALLIE : DIPLOM_ENNEMI;

    // Some of the units may have been killed since the last update of seenItems, it is safer to update it
    seenItems = world()->findAllCollidingItems(this, viewField, false);

    return std::count_if(seenItems.begin(), seenItems.end(), [&](const pair<Unit*, int>& i)
        { return (i.first->diplomacy() == otherDiplomacy); });
}

void Individu::findPath(const tools::math::Vector2d& destination)
{
    tools::aStar::clear();

    tools::aStar::setPoints(position(), destination);
    tools::aStar::setField(viewField);

    tools::aStar::setNodesProperties(_shape.span(), 8, 30, 150);

    vector<pair<const tools::math::Shape*, int>> obstacles;
    for (auto& i : seenItems)
    {
        if (i.second == COLL_PRIM)          obstacles.emplace_back(&i.first->shape(), 100000);
        else if (i.second == COLL_PRIM_MVT) obstacles.emplace_back(&i.first->shape(), 30000);
    }

    tools::aStar::setObstacles(obstacles);

    pathToTarget = tools::aStar::aStar();
}

bool Character::fight()
{
    Individu *enemy = dynamic_cast<Individu*>(_targetedItem.unit());

    if (enemy == nullptr || enemy->diplomacy() == Diplomatie) return false;

    _currentSkill->interactionField.setOrigin(&position());
    _currentSkill->interactionField.updateDirection(_angle);

    if (!tools::math::intersection(_currentSkill->interactionField, enemy->shape())) return false;

    //Force the updating of attributes
    currentHealthStatus(Strength, true);
    enemy->currentHealthStatus(Strength, true);

    double Att_Agilite = currentHealthStatus(Agility);
    double Att_Intelli = currentHealthStatus(Intellect);
    double Att_Puissance = currentHealthStatus(Power);
    if (Att_Agilite <= 0) Att_Agilite = 1;
    if (Att_Intelli <= 0) Att_Intelli = 1;
    if (Att_Puissance <= 0) Att_Puissance = 1;

    double TauxReussite = (1.0 + (Att_Agilite - enemy->currentHealthStatus(Agility))/Att_Agilite) * 50.0;
    if (TauxReussite > 95) TauxReussite = 95;
    if (TauxReussite < 5) TauxReussite = 5;

    bool Succes = (rand()%100 < TauxReussite) ? true : false;

    if (Succes)
    {
        double Degats = currentHealthStatus(Strength);

        double TauxEsquive = (1.0 + (enemy->currentHealthStatus(Dodge) - Att_Agilite)/Att_Agilite) * 50.0;
        bool Esquive = (rand()%100 < TauxEsquive) ? true : false;

        if (Esquive)
        {
            Degats = 0;
            enemy->improveAttribute(Dodge, 10, this);
        }
        else
        {
            double TauxCritique = (1 + (Att_Intelli - enemy->currentHealthStatus(Charisma))/Att_Intelli) * 10.0;
            bool Critique = (rand()%100 < TauxCritique) ? true : false;
            if (Critique)
            {
                Degats *= 1.5;
                modifyHealthStatus(Energy, -10);
                enemy->modifyHealthStatus(Healing, -30);
                improveAttribute(Agility, 10, enemy);
                improveAttribute(Charisma, 10, enemy);
                enemy->improveAttribute(Constitution, -10, this);
                enemy->improveAttribute(Charisma, -10, this);
            }
            else if (currentHealthStatus(Precision) < 100)
            {
                double reduction = rand() % (int)(100 - currentHealthStatus(Precision) + 1);
                Degats *= (100.0 - reduction)/100.0;
            }

            Degats *= (1 + (Att_Puissance - enemy->currentHealthStatus(Constitution))/Att_Puissance)/2.0;
            if (Degats < 5) Degats = 5;

            if (enemy->_currentSkill == enemy->skill(enemy->behavior(Behaviors::Hurt)))
            {
                enemy->improveAttribute(Strength, -10, this);
                enemy->improveAttribute(Power, -10, this);
                enemy->improveAttribute(Dodge, -10, this);
            }
        }

        enemy->modifyHealthStatus(Life, -Degats);
        enemy->modifyHealthStatus(Healing, -3-Degats/20);

        if (enemy->currentHealthStatus(Life) > 0)
            gainExperience(Degats / 5000. * enemy->experience());
        else gainExperience(enemy->experience());

        textManager::PlainText resultText = textManager::getText("devilsai", "console-damageDone");

        textManager::PlainText a;
        if (Diplomatie == DIPLOM_ALLIE)
            a += "@c[128,255,128]";
        if (Diplomatie == DIPLOM_ENNEMI)
            a += "@c[255,128,128]";
        a += displayedName();

        textManager::PlainText b;
        if (enemy->Diplomatie == DIPLOM_ALLIE)
            b += "@c[128,255,128]";
        if (enemy->Diplomatie == DIPLOM_ENNEMI)
            b += "@c[255,128,128]";
        b += enemy->displayedName();

        resultText.addParameter(a);
        resultText.addParameter((int)Degats);
        resultText.addParameter(b);

        if (enemy->currentHealthStatus(Life) == 0)
        {
            resultText += " @n";
            resultText += textManager::getText("devilsai", "console-has-killed");
            resultText.addParameter(a);
            resultText.addParameter(b);
        }
        if (Esquive)
        {
            resultText = textManager::getText("devilsai", "console-has-dodged");
            resultText.addParameter(b);
            resultText.addParameter(a);
        }

        addConsoleEntry(resultText);

        if (Degats) enemy->Set_Activite(enemy->behavior(Behaviors::Hurt));

        return Degats;
    }
    else return false;
}

void Individu::setOwner(Individu* o)
{
    if (o == this) return;

    _owner = o->id();
    _owned = true;
    Diplomatie = _owner->diplomacy();
}

Individu* Individu::owner()
{
    return dynamic_cast<Individu*>(_owner.unit());
}

void Individu::gainExperience(int exp)
{
    for (int i = 0 ; i < 1 + exp / 100 ; ++i)
    {
        improveAttribute(Intellect, 1, nullptr);
        improveAttribute(Strength, 1, nullptr);
        improveAttribute(Power, 1, nullptr);
        improveAttribute(Constitution, 1, nullptr);
    }

    _experience += exp;
    if (owner() != nullptr) owner()->gainExperience(exp);
}

bool Individu::improveAttribute(Attribute a, int chance, Individu* enemy)
{
    if (chance == 0 || rand()%100 >= abs(chance))
        return false;

    auto c1 = [&](Attribute s)
    {
        double x = currentHealthStatus(s);
        double y = atan(log(1+x) * M_PI) / M_PI;
        return min(1.0, y);
    };
    auto c2 = [&](Attribute s, Attribute o)
    {
        if (enemy == nullptr) return 0.0;
        double x = enemy->currentHealthStatus(o) / max(1, currentHealthStatus(s));
        double y = atan(log(1+x) * M_PI) / M_PI;
        return min(1.0, y);
    };

    double result = 0;

    if (chance > 0) switch (a)
    {
        case Strength:
            [[fallthrough]]
        case Power:
            [[fallthrough]]
        case Intellect:
            [[fallthrough]]
        case Constitution:
            result = c1(Intellect);
            break;
        case Agility:
            [[fallthrough]]
        case Charisma:
            result = c2(a, a);
            break;
        case Dodge:
            result = c2(Dodge, Agility);
            break;
        default:
            break;
    }
    else switch (a)
    {
        case Strength:
            [[fallthrough]]
        case Power:
            [[fallthrough]]
        case Constitution:
            [[fallthrough]]
        case Dodge:
            result = - c2(Constitution, Power);
            break;
        case Agility:
            [[fallthrough]]
        case Intellect:
            result = - (70.0-currentHealthStatus(Energy))/100000.0*tools::timeManager::I(1);
            break;
        case Charisma:
            result = - c2(Charisma, Charisma);
            break;
        default:
            break;
    }

    _attributes.add(a, result);

    return (result != 0);
}

void Individu::addSkillLevel(const string& skillName, int quantity)
{
    auto s = skill(skillName);
    if (!s.none())
    {
        s.skillLevel() += quantity;
    }
}

void Individu::setWorld(Carte* w)
{
    _world = w;
}

Carte* Individu::world()
{
    return _world;
}

void Character::addLifetime(double l)
{
    lifetime += l;

    if (lifetime < 0)
        lifetime = 0;
}

void Individu::display(RenderTarget& target)
{
    if (_currentSkill.none()) return;

    imageManager::display(target, tools::hash("individuals"), _currentSkill->getImageKey(_angle, _animationFrame), position().x, position().y, true);
}

void Individu::displayLifeGauge(RenderTarget& target)
{
    if (Diplomatie == DIPLOM_NEUTRE || Diplomatie == DIPLOM_BREAKABLE) return;

    //x and y are used to convert position to integers; otherwise, the text may be blurred
    int x = position().x;
    int y = position().y;

    multimedia::RichText individualName;
    individualName.setSize(160, 0);

    if (Diplomatie == DIPLOM_ALLIE)
        individualName.setDefaultFormatting("liberation", 10, Color(128, 255, 128));
    else
        individualName.setDefaultFormatting("liberation", 10, Color(255, 255, 255));

    individualName.addFlags(textManager::HAlignCenter | textManager::OriginXCenter);
    individualName.create(displayedName());
    individualName.display(target, x, y + 20);

    RectangleShape background(Vector2f(50, 4));
    background.setPosition(x - 25, y + 35);
    background.setFillColor(Color(0, 0, 0, 175));
    target.draw(background);

    RectangleShape foreground(Vector2f(currentHealthStatus(Life)/20, 4));
    foreground.setPosition(x - 25, y + 35);
    foreground.setFillColor(Color(228, 0, 0, 255));
    target.draw(foreground);
}
