
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tinyxml2.h>

#include <cmath>

#include "tools/math.h"
#include "tools/signals.h"

#include "ElementsCarte.h"

#include "tools/aStar.h"
#include "tools/timeManager.h"
#include "textManager/textManager.h"
#include "multimedia/RichText.h"
#include "imageManager/imageManager.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/shaders.h"

#include "devilsai-gui/console.h"


using namespace tinyxml2;


/** FONCTIONS DE LA CLASSE Joueur **/

Joueur::Joueur()
  : Individu(),
    _displayedAttributes(),
    _automoveEndpoint(),
    _skillForHunted(),
    _wakingTime(0),
    _tired(false)
{
    tools::math::Shape s;
    s.circle(tools::math::Vector2d(0, 0), 10);
    _fakeHunted.setShape(s);
}

bool Joueur::interact(Unit* item)
{
    return (_hunted.unit() == item && intersection(interactionField, _hunted->shape()));
}

void Joueur::Repos()
{
    setAttribute(Life, 1000);
    setAttribute(Energy, 1000);
    _wakingTime = 0;
}

void Joueur::Gestion_Statistiques()
{
	//3. Perte d'énergie selon durée depuis repos
    if (_wakingTime > currentHealthStatus(Constitution)) modifyHealthStatus(Energy, -tools::timeManager::I(_wakingTime-currentHealthStatus(Constitution))/10000);

	//4. Gain & Perte d'énergie par activité
	if (_currentSkill == skill(behavior(Behaviors::Random)))
        modifyHealthStatus(Energy, tools::timeManager::I(currentHealthStatus(Life)/25000) /*+ (LieuVillage == "village")*tools::timeManager::I(currentHealthStatus(Statistiques::Life)/10000)*/);
	if (_currentSkill == skill(behavior(Behaviors::Hunting)))
        modifyHealthStatus(Energy, -tools::timeManager::I(0.05/currentHealthStatus(Constitution)));

	//6. Durée d'éveil
    _wakingTime += tools::timeManager::I(0.0005);

	//7. Fatigue extrême
    if (currentHealthStatus(Energy) > 150)
    {
        _tired = false;
    }
    else if (!_tired)
    {
        addConsoleEntry(textManager::getText("devilsai", "FATIGUE"));
        _tired = true;
    }
	if (currentHealthStatus(Energy) < 70 && currentHealthStatus(Energy) > 10)
	{
        improveAttribute(Agility, -1, nullptr);
        improveAttribute(Intellect, -1, nullptr);
	}

    //Update interactionField of skills
    for (auto& s : _skills)
    {
        s.second->interactionField.setOrigin(&position());
        s.second->interactionField.updateDirection(_angle);
    }
}

bool Joueur::improveAttribute(Attribute a, int chance, Individu* enemy)
{
    if (Individu::improveAttribute(a, chance, enemy) == 0) return false;

    switch (a)
    {
        case Strength:
            [[fallthrough]]
        case Constitution:
            if (chance < 0) _world->addUtterance(Id, textManager::getText("devilsai", "player-serious-injury"));
            break;
        case Agility:
            if (chance > 0) _world->addUtterance(Id, textManager::getText("devilsai", "player-critical-hit"));
            break;
        case Dodge:
            if (chance > 0) _world->addUtterance(Id, textManager::getText("devilsai", "player-dodged-strike"));
            break;
        default:
            break;
    }

    if (int diff = round(_attributes[a]) - _displayedAttributes[a] ; diff != 0)
    {
        char text[50] = "console-attributeImprovement-";
        strcat(text, attributeToString(a));
        textManager::PlainText t(textManager::getText("devilsai", text));

        string v = ((diff > 0) ? "@c[128,255,128]" : "@c[225,128,128]") + tools::math::intToChars(diff, 0, true) + "@d";
        t.addParameter(v);

        _world->addUtterance(Id, t);
    }

    _displayedAttributes.set(a, round(_attributes[a]));

    return true;
}

void Joueur::resetDisplayedAttributes()
{
    for (int a = Strength ; a != numberOfAttributes ; ++a)
    {
        Attribute att = static_cast<Attribute>(a);
        _displayedAttributes.set(att, round(_attributes[att]));
    }
}

void Player::userRequest(const string& skill, tools::math::Vector2D<double> point, bool force)
{
    if (!_hunted)
    {
        setAngle(point);
        if (force) Set_Activite(skill);
        else automove(point);
    }
    else if (_hunted != id())
    {
        _hunting = true;
        if (_hunted->diplomacy() == DIPLOM_NEUTRE)
            _skillForHunted = behavior(Behaviors::Random);
        else if (force || _hunted->diplomacy() != Diplomatie)
            _skillForHunted = skill;
        else
            _skillForHunted = behavior(Behaviors::Random);

        setAngle(_hunted);

        tools::math::Shape c;
        c.circle(_hunted->position(), 2);
        if (!tools::math::intersection(c, validityOfPath))
        {
            validityOfPath.circle(_hunted->position(), 5);
            pathToTarget.none();  // Restart A*
        }
    }
}

void Player::userSelection(UnitId other)
{
    _hunted = other;
    _hunting = (_hunted) && (_hunted != id());
}

void Joueur::automove(const tools::math::Vector2d& p)
{
    _automove = true;
    _automoveEndpoint = p;

    tools::math::Shape c;
    c.circle(p, 2);
    if (!tools::math::intersection(c, validityOfPath))
    {
        validityOfPath.circle(p, 5);
        pathToTarget.none();  // Restart A*
    }
}

UnitId Player::huntedCharacter()
{
    if (!_hunting || _hunted == id()) return UnitId(0);
    return _hunted;
}

void Joueur::stopHunting()
{
    _hunting = false;
}

void Joueur::stopAutomoving()
{
    _automove = false;
}

void Joueur::findPath(const tools::math::Vector2d& destination)
{
    tools::aStar::clear();

    tools::aStar::setPoints(position(), destination);
    tools::aStar::setField(viewField);

    tools::aStar::setNodesProperties(_shape.span(), 16, 20, 500);

    vector<pair<const tools::math::Shape*, int>> obstacles;
    for (auto& i : seenItems)
    {
        if (i.second == COLL_PRIM || i.second == COLL_PRIM_MVT)
            obstacles.emplace_back(&i.first->shape(), 100000);
    }

    tools::aStar::setObstacles(obstacles);

    pathToTarget = tools::aStar::aStar();

    // If A* doesn't find a way, try the straight line
    if (!pathToTarget.isLine() && !pathToTarget.isPolyline())
    {
        pathToTarget.line(destination, position(), _shape.span() / 2.0);
        setAngle(destination);
    }
}

bool Player::doNextStep()
{
    tools::math::Vector2D<double> target;

    if (_automove) target = _fakeHunted.position();
    else if (_hunted) target = _hunted->position();
    else  // no unit to hunt, but a skill has been 'forced' and requires movement
    {
        target.x = _position.x + 10 * cos(_angle);
        target.y = _position.y + 10 * sin(_angle);
    }

    if ((!pathToTarget.isLine() && !pathToTarget.isPolyline()) || _aStarClock.getElapsedTime().asSeconds() > 1)
    {
        pathToTarget.line(target, position(), _shape.span() / 2.0);
        bool obstacle = false;
        for (auto& i : seenItems)
        {
            if (i.first->position() == target) continue;
            if (tools::math::intersection(pathToTarget, i.first->shape()))
            {
                obstacle = true;
                break;
            }
        }

        if (obstacle) findPath(target);

        _aStarClock.restart();
    }

    vector<tools::math::Vector2D<double>> points = pathToTarget.pointsOfLine();
    tools::math::Vector2d step = points[points.size() - 2];

    setAngle(step);

    double distanceTraveled = currentHealthStatus(_currentSkill->speedAttribute) * _timer * tools::math::signof(_currentSkill->step);
    distanceTraveled = min(distanceTraveled, tools::math::distance(position(), step));
    move(cos(_angle) * distanceTraveled, sin(_angle) * distanceTraveled);

    if (tools::math::distanceSquare(step, position()) < 100)
        pathToTarget.removeLastPointOfLine();

    return (pathToTarget.isLine() || pathToTarget.isPolyline());
}

void Joueur::loadFromXML(tinyxml2::XMLElement* unit, tinyxml2::XMLElement* properties)
{
    Individu::loadFromXML(unit, properties);
    resetDisplayedAttributes();
}

void Joueur::saveToXML(XMLDocument& doc, XMLHandle& handle)
{
    Individu::saveToXML(doc, handle);
}

textManager::PlainText Joueur::characterDescription()
{
    textManager::PlainText d = textManager::getText("devilsai", "player-description");

    d.addParameter(currentHealthStatus(Life));
    d.addParameter(currentHealthStatus(Energy));
    d.addParameter(currentHealthStatus(Healing));
    d.addParameter(experience());

    d.addParameter(currentHealthStatus(Strength));
    d.addParameter(round(attributes()[Strength]));
    d.addParameter(currentHealthStatus(Power));
    d.addParameter(round(attributes()[Power]));
    d.addParameter(currentHealthStatus(Agility));
    d.addParameter(round(attributes()[Agility]));
    d.addParameter(currentHealthStatus(Intellect));
    d.addParameter(round(attributes()[Intellect]));

    d.addParameter(currentHealthStatus(Constitution));
    d.addParameter(round(attributes()[Constitution]));
    d.addParameter(currentHealthStatus(Charisma));
    d.addParameter(round(attributes()[Charisma]));
    d.addParameter(currentHealthStatus(Dodge));
    d.addParameter(round(attributes()[Dodge]));
    d.addParameter(currentHealthStatus(HealingPower));

    return d;
}

void Joueur::displayHealthStatus(RenderTarget& target, int x, int y)
{
    // Health gauges - Life, Energy and Healing

    Shader* lifeShader = (currentHealthStatus(Life) < 100)
        ? multimedia::shader("blink", devilsai::warnLifeShaderInstance)
        : multimedia::shader("contrast", devilsai::lifeGaugeShaderInstance);

    imageManager::display(target, "gui"_hash, "healthgauge-life", x, y + 20, false, lifeShader);
    imageManager::fillArea(target, "gui"_hash, "healthgauge-life", x, y + 20, ceil(currentHealthStatus(Life) / 6.67), 10, x, y + 20);

    Shader* energyShader = (currentHealthStatus(Energy) < 100)
        ? multimedia::shader("blink", devilsai::warnEnergyShaderInstance)
        : multimedia::shader("contrast", devilsai::energyGaugeShaderInstance);

    imageManager::display(target, "gui"_hash, "healthgauge-energy", x, y + 32, false, energyShader);
    imageManager::fillArea(target, "gui"_hash, "healthgauge-energy", x, y + 32, ceil(currentHealthStatus(Energy) / 6.67), 10, x, y + 32);

    imageManager::fillArea(target, "gui"_hash, "healthgauge-healing",
        (int)(x + 75 - max(0.0, -currentHealthStatus(Healing)/1.33)), y + 46, (int)(abs(currentHealthStatus(Healing))/1.33), 10, x, y + 46);

    // Text (name and state)

    textManager::PlainText playerStateText;

    playerStateText += "@s[14]@c[128,255,128]";
    playerStateText += displayedName();
    playerStateText += " @d@n[44]"; // Make place for the gauges.

    int l = currentHealthStatus(Life) + currentHealthStatus(Healing) * 10;

    if (currentHealthStatus(Life) == 0)
        playerStateText += textManager::getText("devilsai", "player-health-dead");
    else if (l >= 900)
        playerStateText += textManager::getText("devilsai", "player-health-1");
    else if (l >= 650)
        playerStateText += textManager::getText("devilsai", "player-health-2");
    else if (l >= 300)
        playerStateText += textManager::getText("devilsai", "player-health-3");
    else if (l >= 100)
        playerStateText += textManager::getText("devilsai", "player-health-4");
    else
        playerStateText += textManager::getText("devilsai", "player-health-5");

    if (_tired)
    {
        playerStateText += " @n";
        playerStateText += textManager::getText("devilsai", "player-health-tired");
    }

    for (auto& i : inventory.objects)
    {
        if (i.active() && i.temporary())
        {
            playerStateText += " @n";
            playerStateText += textManager::getText("devilsai", "SOUS_EFFET");
            playerStateText.addParameter(textManager::getText("objects", i.name()));
        }
    }

    multimedia::RichText playerState;
    playerState.setSize(200, 0);
    playerState.setDefaultFormatting("liberation", 11, Color(255, 255, 255));
    playerState.addFlags(textManager::HAlignCenter | textManager::OriginXCenter);
    playerState.create(playerStateText);
    playerState.display(target, x + 75, y);
}
