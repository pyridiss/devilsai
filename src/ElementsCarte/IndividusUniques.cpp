
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include <tinyxml2.h>

#include "tools/debug.h"
#include "tools/filesystem.h"
#include "textManager/textManager.h"

#include "imageManager/imageManager.h"
#include "musicManager/musicManager.h"

#include "ElementsCarte.h"

#include "devilsai-resources/manager.h"
#include "devilsai-resources/shaders.h"
#include "devilsai-resources/Species.h"

#include "Jeu/Jeu.h"

using namespace tinyxml2;


void Individu::loadFromXML(tinyxml2::XMLElement* elem, tinyxml2::XMLElement* properties)
{
    string archiveFile;
    int hue = 0, saturation = 0, luminance = 0, alpha = 255;
    Shader* shader = nullptr;


    if (elem->Attribute("name"))
    {
        Type = elem->Attribute("name");
        setCustomDisplayedName(textManager::getText("species", Type));
    }

    if (elem->Attribute("species"))
    {
        Type = elem->Attribute("species");
        _species = devilsai::getResource<Species>(Type);
        if (_species == nullptr)
        {
            tools::debug::error("This class has not been loaded: " + Type, "files", __FILENAME__, __LINE__);
            return;
        }
        _species->Copie_Element(this);
    }

    move(elem->DoubleAttribute("x"), elem->DoubleAttribute("y"));

    if (elem->Attribute("tag"))
        Liste = elem->Attribute("tag");
    else if (properties && properties->Attribute("tag"))
        Liste = properties->Attribute("tag");

    if (properties && properties->BoolAttribute("ignoreCollision"))
        _state |= Unshaped;

    if (properties && properties->BoolAttribute("immuable"))
            TypeClassement = CLASSEMENT_CADAVRE;

    if (elem->Attribute("loadFromDataFile"))
    {
        //Another file must be loaded to complete the unique profile.
        _extraDataFile = new string;
        *_extraDataFile = elem->Attribute("loadFromDataFile");

        //First, give this file to gamedata because it can contain species used by the unique
        loadGameDataFromXML(tools::filesystem::dataDirectory(), *_extraDataFile);

        //We need to load this file and find the XMLElement named "uniqueData" to continue the loading
        XMLDocument file;
        file.LoadFile((tools::filesystem::dataDirectory() + *_extraDataFile).c_str());
        XMLHandle hdl(file);
        XMLHandle hdl2 = hdl.FirstChildElement().FirstChildElement("uniqueData");

        //We can call loadFromXML() with this other file
        loadFromXML(hdl2.ToElement(), nullptr);
    }

    elem = elem->FirstChildElement();

    while (elem)
    {
        string elemName = elem->Name();

        if (elemName == "addImageArchiveFile")
        {
            archiveFile = elem->Attribute("file");
            imageManager::addArchiveFile(archiveFile);
        }
        if (elemName == "colorize")
        {
            imageManager::lockGLMutex(11);
            Glsl::Vec3 r, g, b;
            elem->QueryAttribute("rr", &r.x);
            elem->QueryAttribute("rg", &r.y);
            elem->QueryAttribute("rb", &r.z);
            elem->QueryAttribute("gr", &g.x);
            elem->QueryAttribute("gg", &g.y);
            elem->QueryAttribute("gb", &g.z);
            elem->QueryAttribute("br", &b.x);
            elem->QueryAttribute("bg", &b.y);
            elem->QueryAttribute("bb", &b.z);
            int i = devilsai::newColorizeShaderInstance(r, g, b);
            shader = multimedia::shader("colorize", i);
            imageManager::unlockGLMutex(11);
        }
        if (elemName == "changeImagesHSL")
        {
            elem->QueryAttribute("hue", &hue);
            elem->QueryAttribute("saturation", &saturation);
            elem->QueryAttribute("luminance", &luminance);
            elem->QueryAttribute("alpha", &alpha);
        }
        if (elemName == "addSound")
        {
            string name = elem->Attribute("name");
            musicManager::addSound(name);
        }

        if (elemName == "shape")            _shape.loadFromXML(elem);
        if (elemName == "viewField")        viewField.loadFromXML(elem);
        if (elemName == "interactionField") interactionField.loadFromXML(elem);

        if (elemName == "currentHealthStatus")
        {
            _currentHealthStatus.loadFromXML(elem);
        }
        if (elemName == "stats")
        {
            _attributes.loadFromXML(elem);
            //Force the updating of _currentHealthStatus
            currentHealthStatus(Life, true);
        }

        if (elemName == "properties")
        {
            Id.replace(this, elem->UnsignedAttribute("id", 0));
            elem->QueryAttribute("lifetime", &lifetime);
            elem->QueryAttribute("angle", &_angle);
            if (elem->BoolAttribute("ignoreCollision"))
                _state |= Unshaped;
            elem->QueryAttribute("classement", &TypeClassement);
            elem->QueryAttribute("diplomacy", &Diplomatie);
            elem->QueryAttribute("experience", &_experience);
            if (elem->Attribute("corpseImageKey"))
                corpseImageKey() = elem->Attribute("corpseImageKey");
            if (elem->Attribute("owner"))
            {
                _owner = UnitId(elem->UnsignedAttribute("owner", 0));
                _owned = true;
            }
            if (elem->BoolAttribute("fixedStats"))      _state |= FixedStats;
            if (elem->BoolAttribute("fixedHealing"))    _state |= FixedHealing;
            if (elem->BoolAttribute("maximumEnergy"))   _state |= MaximumEnergy;
            if (elem->BoolAttribute("fixedAngle"))      _state |= FixedAngle;
        }
        if (elemName == "skill")
        {
            string skillName = elem->Attribute("name");

            Skill *s = devilsai::getResource<Skill>(Type + ":" + skillName);

            if (s == nullptr)
            {
                s = devilsai::addResource<Skill>(Type + ":" + skillName, Type + ":" + skillName);

                XMLHandle hdl2(elem);
                s->loadFromXML(hdl2, shader, hue, saturation, luminance, alpha);
            }

            SkillProperties p(s);

            elem->QueryAttribute("level", &p.level);
            elem->QueryAttribute("extraLevel", &p.extraLevel);
            elem->QueryAttribute("unavailability", &p.unavailability);

            _skills.emplace(skillName, std::move(p));
        }
        if (elemName == "skillsManagement")
        {
            createCustomBehaviors();
            if (elem->Attribute("randomBehavior"))
                behavior(Behaviors::Random) = elem->Attribute("randomBehavior");
            if (elem->Attribute("blocked"))
                behavior(Behaviors::Blocked) = elem->Attribute("blocked");
            if (elem->Attribute("hunting"))
            {
                behavior(Behaviors::Hunting) = elem->Attribute("hunting");
                behavior(Behaviors::LookingForLife) = elem->Attribute("hunting");
            }
            if (elem->Attribute("hurt"))
                behavior(Behaviors::Hurt) = elem->Attribute("hurt");
            if (elem->Attribute("dying"))
                behavior(Behaviors::Dying) = elem->Attribute("dying");
            if (elem->Attribute("attacks"))
            {
                string total = elem->Attribute("attacks");
                if (total.find(',') == string::npos)
                {
                    attacks().push_back(total);
                }
                else
                {
                    size_t first = 0, second = total.find(',');
                    while (second != string::npos)
                    {
                        attacks().push_back(total.substr(first, second - first));
                        first = second + 1;
                        second = total.find(',', first);
                    }
                    attacks().push_back(total.substr(first));
                }
            }

            Set_Activite(behavior(Behaviors::Random));
        }
        if (elemName == "inventory")
        {
            inventory.loadFromXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    imageManager::removeArchiveFile(archiveFile);
}

void Individu::saveToXML(XMLDocument& doc, XMLHandle& handle)
{
    XMLElement* root = handle.ToElement();

    XMLElement* unique = doc.NewElement("character");

    if (_extraDataFile != nullptr)
        unique->SetAttribute("loadFromDataFile", _extraDataFile->c_str());

    if (_species != nullptr)
        unique->SetAttribute("species", Type.c_str());
    else
        unique->SetAttribute("name", Type.c_str());

    unique->SetAttribute("x", position().x);
    unique->SetAttribute("y", position().y);
    unique->SetAttribute("tag", Liste.c_str());

    XMLElement* stats = doc.NewElement("currentHealthStatus");
    unique->InsertEndChild(stats);
    XMLHandle statsHandle(stats);
    _currentHealthStatus.saveToXML(doc, statsHandle);

    XMLElement* attributesElem = doc.NewElement("stats");
    unique->InsertEndChild(attributesElem);
    XMLHandle attributesHandle(attributesElem);
    _attributes.saveToXML(doc, attributesHandle);

    XMLElement* properties = doc.NewElement("properties");
    properties->SetAttribute("id", Id.id());

    if (lifetime != -1)
        properties->SetAttribute("lifetime", lifetime);

    properties->SetAttribute("angle", _angle);

    if (unshaped()) properties->SetAttribute("ignoreCollision", true);
    properties->SetAttribute("classement", TypeClassement);
    properties->SetAttribute("diplomacy", Diplomatie);
    properties->SetAttribute("experience", _experience);

    if (_corpseImageKey != nullptr)
        properties->SetAttribute("corpseImageKey", _corpseImageKey->c_str());

    if (owner() != nullptr)
        properties->SetAttribute("owner", owner()->Id.id());

    if (_state & FixedStats) properties->SetAttribute("fixedStats", true);
    if (_state & FixedHealing) properties->SetAttribute("fixedHealing", true);
    if (_state & MaximumEnergy) properties->SetAttribute("maximumEnergy", true);
    if (_state & FixedAngle) properties->SetAttribute("fixedAngle", true);
    if (_state & UseOwnersName) properties->SetAttribute("useOwnersName", true);

    unique->InsertEndChild(properties);

    for (auto i : _skills)
    {
        XMLElement* skill = doc.NewElement("skill");
        skill->SetAttribute("name", i.first.c_str());
        skill->SetAttribute("level", i.second.level);
        skill->SetAttribute("extraLevel", i.second.extraLevel);
        skill->SetAttribute("unavailability", i.second.unavailability);
        unique->InsertEndChild(skill);
    }

    XMLHandle hdl(unique);
    inventory.saveToXML(doc, hdl);

    root->InsertEndChild(unique);
}
