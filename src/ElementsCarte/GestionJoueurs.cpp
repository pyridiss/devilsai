
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include "tools/signals.h"
#include "tools/math.h"
#include "tools/timeManager.h"

#include "devilsai-resources/Carte.h"
#include "ElementsCarte.h"


void Player::manage()
{
		// 2. Vérifie que le personnage est toujours en vie !

	if (currentHealthStatus(Life) <= 0)
	{
        Set_Activite(behavior(Behaviors::Dying));
        if ((int)_animationFrame == 8) tools::signals::addSignal("player-dead");
	}

    Gestion_Recuperation();
	Gestion_Statistiques();

		// 3. Gestion du temps : Mouvement ou pas

    nextAnimationFrame();

    if (!Set_Activite(behavior(Behaviors::Random)))
        _automove = false;

    if (_hunting && _hunted)
    {
        // Stop the hunt if the character is dead; otherwise, set the skill if close enough, or run to it if too far.
        Character* c = dynamic_cast<Character*>(_hunted.unit());
        if (c == nullptr || c->currentHealthStatus(Life) <= 0)
        {
            _hunting = false;
            _hunted = Id;  // A zero would trigger an automove (see userRequest())
        }
        else if (tools::math::intersection(skill(_skillForHunted)->interactionField, _hunted->shape()))
        {
            Set_Activite(_skillForHunted);
            _skillForHunted = behavior(Behaviors::Random);
            _hunting = false;
            _automove = false;
        }
        else automove(_hunted->position());
    }
    else _hunting = false;

    if (_automove)
    {
        _fakeHunted.move(_automoveEndpoint.x - _fakeHunted.position().x, _automoveEndpoint.y - _fakeHunted.position().y);
        Set_Activite(Behaviors::Hunting);

        //Check if we are close enough to the endPoint
        if (tools::math::distance(_automoveEndpoint, position()) < 1.2 * _currentSkill->step)
        {
            _automove = false;
            Set_Activite(behavior(Behaviors::Random));
        }
    }

    seenItems = world()->findAllCollidingItems(this, viewField, false);

    tools::math::Vector2d oldPosition = position();

    if (_currentSkill->step != 0)  // A new position must be found
    {
        _timer = tools::timeManager::I(1/20.0); //Set_Activite() reset the _timer...

        if (!doNextStep())
        {
            _automove = false;
            _hunting = false;
        }

        bool collision = false;
        int Iteration2 = 0;
        while (Iteration2 < 4)
        {
            collision = false;
            Unit* other;
            for (auto& i : seenItems)
            {
                if (tools::math::intersection(_shape, i.first->shape()))
                {
                    int c = i.first->collision(this, false);
                    if (c == COLL_PRIM || c == COLL_PRIM_MVT)
                    {
                        collision = true;
                        other = i.first;
                        break;
                    }
                }
            }
            if (collision == false) break;

            double angleWithOther = tools::math::angle(other->position().x - position().x, other->position().y - position().y);
            move(-0.5 * cos(angleWithOther), -0.5 * sin(angleWithOther));
            ++Iteration2;
        }

        if (collision)
        {
            move(oldPosition.x - position().x, oldPosition.y - position().y);
            Set_Activite(behavior(Behaviors::Blocked));
        }
    }

    _targetedItem = UnitId(0);
    double angleDifference = 100;
    for (auto& i : seenItems)
    {
        if (tools::math::intersection(_shape, i.first->shape()))
        {
            i.first->collision(this, true);
        }
        if (tools::math::intersection(_currentSkill->interactionField, i.first->shape()))
        {
            if (abs(tools::math::angle(i.first->position().x - position().x, i.first->position().y - position().y) - _angle) < angleDifference)
            {
                angleDifference = abs(tools::math::angle(i.first->position().x - position().x, i.first->position().y - position().y) - _angle);
                _targetedItem = i.first->id();
            }
        }
    }

    // The individual selected by the user is more important than the one found by the collision manager
    if (_hunted && _hunted != id())
        _targetedItem = _hunted;

    _timer = tools::timeManager::I(1/20.0); //Set_Activite() reset the _timer...

    interactionField.updateDirection(_angle);
    _currentSkill->interactionField.updateDirection(_angle);

    return;
}
