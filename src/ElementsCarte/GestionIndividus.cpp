
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include "tools/debug.h"
#include "tools/timeManager.h"
#include "tools/math.h"

#include "devilsai-resources/Carte.h"
#include "ElementsCarte.h"


/** FONCTIONS DE GESTION DE LA CLASSE Individu **/

void Character::manage()
{
    if (lifetime > 0)  // Character has a finite life
    {
        lifetime -= tools::timeManager::I(1);

        if (lifetime < 0)
            setAttribute(Life, 0);
    }

    if (_owned && !_owner)  // Owner has been killed
    {
        setAttribute(Life, 0);
    }

    if (currentHealthStatus(Life) == 0)  // Life reached 0, set the 'dying' animation
    {
        setAttribute(Energy, 0);
        setAttribute(Healing, 0);
        Diplomatie = DIPLOM_NEUTRE;
        _shape.circle(tools::math::Vector2d(0, 0), 0);

        if (!Set_Activite(Behaviors::Dying))
            _state |= Dead;
    }

    if (_state & Dead)  // State set at the end of the 'dying' animation
    {
        createCorpse();
        lifetime = 0;
        _state &= ~Dead;
    }

    if (lifetime == 0)  // The character will be destroyed by the world he is in; stop here the management.
    {
        return;
    }

    if (_currentSkill.none())  // Should not happen, but may prevent a crash in case of problem
    {
        return;
    }

	Gestion_Recuperation();

	//Gestion du temps :
    double speed = currentHealthStatus(_currentSkill->speedAttribute);
    _timer += tools::timeManager::I(1/20.0);
    if (_timer * speed < ((_currentSkill->step != 0) ? _currentSkill->step : 10))
        return;

    nextAnimationFrame();

	if (NouveauComportement != -1) Comportement = NouveauComportement;

	int Iteration = 0;
    string attackToUse;

    bool findNewPosition = false;

    if (!_targetedItem)
        Comportement = Behaviors::Random;

    Set_Activite(behavior(Comportement));

    if (_currentSkill->step != 0) findNewPosition = true;

    if (_targetedItem) setAngle(_targetedItem);

    seenItems = world()->findAllCollidingItems(this, viewField, false);

    tools::math::Vector2d oldPosition = position();

    while (findNewPosition)
    {
        switch (Comportement)
        {
            case Behaviors::Hunting :
                [[fallthrough]]
            case Behaviors::LookingForLife :
                MouvementChasse(_targetedItem.unit());
                break;
            default:
                MouvementAleatoire(Iteration);
                break;
        }

        bool collision = false;
        int Iteration2 = 0;
        while (Iteration2 < 5)
        {
            collision = false;
            Unit* other;
            for (auto& i : seenItems)
            {
                if (tools::math::intersection(_shape, i.first->shape()))
                {
                    int c = i.first->collision(this, false);
                    if (c == COLL_PRIM || c == COLL_PRIM_MVT)
                    {
                        collision = true;
                        other = i.first;
                        break;
                    }
                }
            }
            if (collision == false) break;

            double angleWithOther = tools::math::angle(other->position().x - position().x, other->position().y - position().y);
            move(-2 * cos(angleWithOther), -2 * sin(angleWithOther));
            ++Iteration2;
        }

        if (collision)
        {
            //Le mouvement précédent a entraîné une collision primaire : retour en arrière.
            move(oldPosition.x - position().x, oldPosition.y - position().y);

            //If cannot reach the hunted item, it is better to stop than to stomp.
            if (Comportement == Behaviors::Hunting)
                Iteration = 4;

            if (Iteration == 4) //Aucun mouvement valable n'a été trouvé
            {
                findNewPosition = false;
                Set_Activite(behavior(Behaviors::Blocked));
            }
        }
        else
            findNewPosition = false;

        ++Iteration;
    }

    NouveauComportement = Behaviors::Random;
    _targetedItem = UnitId(0);

    for (auto& i : seenItems)
    {
        if (tools::math::intersection(_shape, i.first->shape()))
        {
            i.first->collision(this, true);
        }
        switch (i.second)
        {
            case COLL_PRIM_MVT:
                if (i.first->diplomacy() != DIPLOM_NEUTRE && i.first->diplomacy() != DIPLOM_BREAKABLE && i.first->diplomacy() != Diplomatie)
                {
                    bool canUseAttack = false;
                    for (auto& attack : attacks())
                    {
                        if (skill(attack).none())
                        {
                            tools::debug::error("This skill does not exists : " + Type + ":" + attack, "items", __FILENAME__, __LINE__);
                            continue;
                        }
                        if (skill(attack).unavailability() > 0)
                            continue;

                        skill(attack)->interactionField.setOrigin(&position());
                        skill(attack)->interactionField.updateDirection(_angle);

                        if (tools::math::intersection(skill(attack)->interactionField, i.first->shape()))
                        {
                            canUseAttack = true;
                            if (Behaviors::Attacking > NouveauComportement)
                            {
                                NouveauComportement = Behaviors::Attacking;
                                _targetedItem = i.first->id();
                                attackToUse = attack;
                            }
                        }
                    }

                    if (!canUseAttack)
                    {
                        if (Behaviors::Hunting > NouveauComportement && (_state & FixedAngle) == 0)
                        {
                            NouveauComportement = Behaviors::Hunting;
                            _targetedItem = i.first->id();
                        }
                        else if (NouveauComportement == Behaviors::Hunting)
                        {
                            if (tools::math::distance(position(), i.first->position()) < tools::math::distance(position(), _targetedItem->position()))
                                _targetedItem = i.first->id();
                        }
                    }
                }
                break;
            case COLL_LIFE:
                if (currentHealthStatus(Life) < 150 || currentHealthStatus(Healing) < -15)
                {
                    if (Behaviors::LookingForLife > NouveauComportement)
                    {
                        NouveauComportement = Behaviors::LookingForLife;
                        _targetedItem = i.first->id();
                    }
                }
            default:
                break;
        }
    }

    if (NouveauComportement == Behaviors::Attacking)
        Set_Activite(attackToUse);

    _timer = 0;

    return;
}

void Individu::MouvementAleatoire(bool newDirection)
{
    if (((_state | _currentSkill->extraState) & FixedAngle) == 0)
    {
        if (newDirection)
            _angle = tools::math::randomNumber(0.0, 2.0 * M_PI);
        _angle += tools::math::randomNumber_BinomialLaw(-10.0, 10.0);
    }

    while (_angle < 0) _angle += 2.0 * M_PI;
    while (_angle > 2.0 * M_PI) _angle -= 2.0 * M_PI;

    double distanceTraveled = currentHealthStatus(_currentSkill->speedAttribute) * _timer * tools::math::signof(_currentSkill->step);
    move(cos(_angle) * distanceTraveled, sin(_angle) * distanceTraveled);
}

bool Individu::MouvementChasse(Unit *elem)
{
    if (!tools::math::intersection(elem->shape(), validityOfPath))
    {
        pathToTarget.line(elem->position(), position(), _shape.span() / 2.0);
        bool obstacle = false;
        for (auto& i : seenItems)
        {
            if (i.first == elem) continue;
            if (tools::math::intersection(pathToTarget, i.first->shape()))
            {
                obstacle = true;
                break;
            }
        }

        if (obstacle)
        {
            findPath(elem->position());
            validityOfPath.circle(elem->position(), 20);
        }
        else
            validityOfPath.circle(elem->position(), 5);
    }

    if (pathToTarget.isLine() || pathToTarget.isPolyline())
    {
        vector<tools::math::Vector2D<double>> points = pathToTarget.pointsOfLine();
        tools::math::Vector2d step = points[points.size() - 2];
        setAngle(step);
        double distanceTraveled = currentHealthStatus(_currentSkill->speedAttribute) * _timer * tools::math::signof(_currentSkill->step);
        move(cos(_angle) * distanceTraveled, sin(_angle) * distanceTraveled);
        if (tools::math::distanceSquare(step, position()) < 100)
            pathToTarget.removeLastPointOfLine();
    }
    else
    {
        Set_Activite(behavior(Behaviors::Blocked));
        setAngle(elem->id());

        return false;
    }

	return true;
}
