
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_CHARS_MAP
#define EXT_CHARS_MAP

#include <unordered_map>
#include "ext/chars_sequence_index.h"


namespace ext{

/**
 * Type definition to simplify the use of ext::chars_sequence_index in std::unordered_map.
 *
 * chars_map defines an std::unordered_map which key is a ext::chars_sequence_index.
 */
template<typename T>
using chars_map = std::unordered_map<chars_sequence_index, T, chars_sequence_hash>;

/**
 * Type definition to simplify the use of ext::chars_sequence_index in std::unordered_map.
 *
 * chars_map_key_type is an alias for ext::chars_sequence_index, the key of a chars_map.
 */
using chars_map_key_type = chars_sequence_index;

}  // namespace ext

#endif  // EXT_CHARS_MAP
