
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_FLAGS
#define EXT_FLAGS

#include <type_traits>

/**
 * Defines operator~ for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T operator~ (const T& op)
{
    return static_cast<T>(~static_cast<typename std::underlying_type<T>::type>(op));
}

/**
 * Defines operator| for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T operator| (const T& left, const T& right)
{
    return static_cast<T>(static_cast<typename std::underlying_type<T>::type>(left) | static_cast<typename std::underlying_type<T>::type>(right));
}

/**
 * Defines operator|= for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T& operator|= (T& left, const T& right)
{
    return (left = left | right);
}

/**
 * Defines operator& for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T operator& (const T& left, const T& right)
{
    return static_cast<T>(static_cast<typename std::underlying_type<T>::type>(left) & static_cast<typename std::underlying_type<T>::type>(right));
}

/**
 * Defines operator &= for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T& operator&= (T& left, const T& right)
{
    return (left = left & right);
}

/**
 * Defines operator^ for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T operator^ (const T& left, const T& right)
{
    return static_cast<T>(static_cast<typename std::underlying_type<T>::type>(left) ^ static_cast<typename std::underlying_type<T>::type>(right));
}

/**
 * Defines operator ^= for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr T& operator^= (T& left, const T& right)
{
    return (left = left ^ right);
}

/**
 * Defines an operator "contains the flags" for enum classes.
 */
template <typename T, class = typename std::enable_if<std::is_enum<T>::value>::type>
constexpr bool operator% (const T& left, const T& right)
{
    return (right == (left & right));
}

#endif  // EXT_FLAGS
