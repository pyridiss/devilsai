
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_CHARS_SEQUENCE_INDEX
#define EXT_CHARS_SEQUENCE_INDEX

#include <cstddef>

namespace ext{

/**
 * \brief Hashes any sequence of characters so that it can be used as index in associative containers.
 *
 * The class ensures that if only ASCII characters are used, the hash will be the same, whatever the types of
 * the characters are (char, unsigned int,...).
 *
 * The characters sequence is not saved and cannot be retrieved.
 *
 * To use this class, the hash struct of the container must be set to clars_sequence_hash.
 *
 * Example:
 * \code
 * std::unordered_map<ext::chars_sequence_index, myMappedType, ext::chars_sequence_hash> myMap;
 * \endcode
 */
struct chars_sequence_index
{
    size_t hash {0}; /**< Value of the hash */

    /**
     * Constructor.
     *
     * Hashes the sequence of characters.
     *
     * \param begin pointer to the first character to read.
     * \param end pointer to one-past the last character to read. Default is nullptr, in the case of a null-terminated
     * string.
     * \tparam T type of characters.
     */
    template<typename T>
    explicit chars_sequence_index(T* begin, T* end = nullptr)
    {
        while (begin != end && *begin != 0)
        {
            hash = static_cast<char>(*begin) + (hash << 6) + (hash << 16) - hash;
            ++begin;
        }
    }

    /**
     * Comparison operator.
     *
     * Only compares hashes.
     */
    bool operator==(const chars_sequence_index& other) const noexcept
    {
        return hash == other.hash;
    }
};

/**
 * \brief Helper for associative containers using chars_sequence_index as index.
 * \see chars_sequence_index
 */
struct chars_sequence_hash
{
    size_t operator()(const chars_sequence_index& v) const noexcept
    {
        return v.hash;
    }
};

}  // namespace ext

#endif  // EXT_CHARS_SEQUENCE_INDEX
