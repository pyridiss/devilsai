
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_CAST
#define EXT_CAST

namespace ext{

/**
 * \brief Casts a sequence of characters to an integer.
 *
 * If the string pointed-to by \a begin is null-terminated, the parameter \a end is optional. Otherwise, the function
 * reads every character of the sequence [begin, end). The end character is not read.
 *
 * The characters 'comma' and 'dot' can be used as a decimal separator. The character minus can be used for negative
 * numbers.
 *
 * If the string contains other caracters, before, after or in the middle of the number, they are simply ignored.
 *
 * For example, the string "aaa -u123,01 fff. 45" will be interpreted as the number -1230145.
 *
 * \remark There is no check on the validity of the sequence, or on the ability of a plain int to
 * store the value reprensented by the sequence of characters.
 *
 * \param begin pointer to the first character to read.
 * \param end pointer to one-past the last character to read. Default is nullptr, in the case of a null-terminated
 * string.
 * \tparam T type of characters.
 * \return integer value reprensented by the sequence [begin, end).
 */
template <typename T>
int chars_to_int(const T* begin, const T* end = nullptr)
{
    int res = 0;
    int sign = 1;

    while (begin != end && *begin != 0)
    {
        if (*begin == 0x2D && res == 0)  // minus
        {
            sign = -1;
        }
        else if (*begin >= 0x30 && *begin <= 0x39)  // digits
        {
            res = res * 10 + (*begin - 0x30);
        }
        ++begin;
    }

    return res * sign;
}

/**
 * \brief Casts a sequence of characters to a double.
 *
 * If the string pointed-to by \a begin is null-terminated, the parameter \a end is optional. Otherwise, the function
 * reads every character of the sequence [begin, end). The end character is not read.
 *
 * The characters 'comma' and 'dot' can be used as a decimal separator. The character minus can be used for negative
 * numbers.
 *
 * If the string contains other caracters, before, after or in the middle of the number, they are simply ignored.
 *
 * For example, the string "aaa -u123,01 fff. 45" will be interpreted as the number -123.0145.
 *
 * \remark There is no check on the validity of the sequence, or on the ability of a double to store the value
 * reprensented by the sequence of characters.
 *
 * \param begin pointer to the first character to read.
 * \param end pointer to one-past the last character to read. Default is nullptr, in the case of a null-terminated
 * string.
 * \tparam T type of characters.
 * \return interpreted number as a double.
 */
template <typename T>
double chars_to_double(const T* begin, const T* end = nullptr)
{
    int res_int = 0;
    int res_frac = 0;
    int dec = 0;
    int sign = 1;

    int* current_res = &res_int;

    while (begin != end && *begin != 0)
    {
        if (*begin == 0x2D && *current_res == 0)  // minus
        {
            sign = -1;
        }
        else if (*begin >= 0x30 && *begin <= 0x39)  // digits
        {
            *current_res = *current_res * 10 + (*begin - 0x30);
            dec *= 10;
        }
        else if (*begin == 0x2C || *begin == 0x2E)  // comma or dot
        {
            current_res = &res_frac;
            if (!dec) dec = 1;
        }
        ++begin;
    }

    double res = res_int + (dec ? ((double)res_frac/(double)dec) : 0);

    return res * sign;
}

}  // namespace ext

#endif  // EXT_CAST
