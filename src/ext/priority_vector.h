
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_PRIORITY_VECTOR
#define EXT_PRIORITY_VECTOR

#include <vector>


namespace ext{

/**
 * \brief Defines a vector where the elements can be reordered.
 *
 * Each element of a priority_vector is given a priority (positive integer). If the element A has a higher priority
 * than the element B, A will be before B in the range [begin(), end()]. If A and B have the same priority, the
 * order is not guaranteed but one of the elements can be put at the first position (of all the elements having the
 * same priority) using put_at_first_position().
 */
template <typename T>
class priority_vector : public std::vector<std::pair<size_t, T>>
{
    using base_class = std::vector<std::pair<size_t, T>>;

    public:
        /**
         * Updates the priority of an element.
         *
         * If the element is not in the vector, nothing happen.
         *
         * \param element element for which the priority must be changed.
         * \param new_priority new priority of the element.
         */
        void set_new_priority(const T& element, size_t new_priority)
        {
            for (auto i = base_class::begin(), e = base_class::end() ; i != e ; ++i)
            {
                if (i->second == element)
                {
                    T tmp {std::move(i->second)};
                    if (new_priority > i->first)  // Priority has increased; move towards the top
                    {
                        typename base_class::reverse_iterator j(i+1);
                        for ( ; j+1 != base_class::rend() ; ++j)
                        {
                            if ((j+1)->first < new_priority)
                                *j = std::move(*(j+1));
                            else break;
                        }
                        *j = std::make_pair(std::move(new_priority), std::move(tmp));
                        break;
                    }
                    if (new_priority < i->first)  // Priority has decreased; move toward the bottom
                    {
                        for ( ; i+1 != base_class::end() ; ++i)
                        {
                            if ((i+1)->first > new_priority)
                                *i = std::move(*(i+1));
                            else break;
                        }
                        *i = std::make_pair(new_priority, std::move(tmp));
                        break;
                    }
                }
            }
        }

        /**
         * Reorders the vector so that the element \a element will be the first of its priority category.
         *
         * If the element is not in the vector, nothing happens.
         *
         * \param element element to move at the first position of its priority category.
         */
        void put_at_first_position(const T& element)
        {
            for (auto i = base_class::begin(), e = base_class::end() ; i != e ; ++i)
            {
                if (i->second == element)
                {
                    typename base_class::value_type tmp {std::move(*i)};

                    typename base_class::reverse_iterator j(i+1);
                    for ( ; j+1 != base_class::rend() ; ++j)
                    {
                        if ((j+1)->first == i->first)
                            *j = std::move(*(j+1));
                        else break;
                    }
                    *j = std::move(tmp);
                    break;
                }
            }
        };

        /**
         * Adds a new element at the end of the vector, with a priority of 0.
         *
         * \param element new element to add.
         */
        void push_back(const T& element)
        {
            base_class::push_back(std::make_pair(0, element));
        }

        /**
         * Moves a new element at the end of the vector, with a priority of 0.
         *
         * \param element new element to add into the vector.
         */
        void push_back(T&& element)
        {
            base_class::push_back(std::make_pair(0, std::move(element)));
        }

        /**
         * Constructs a new element in the vector with the given priority and the given arguments.
         *
         * \param priority priority of the new element.
         * \param args arguments to construct the new element.
         */
        template <typename... Args>
        void emplace(size_t priority, Args&&... args)
        {
            if (priority != 0) for (auto i = base_class::begin(), e = base_class::end() ; i != e ; ++i)
            {
                if (i->first <= priority)
                {
                    base_class::emplace(i, priority, args...);
                    return;
                }
            }
            base_class::emplace_back(priority, args...);
        }

    private:
        using base_class::emplace_back;
        using base_class::emplace;
};

}  // namespace ext

#endif  // EXT_PRIORITY_VECTOR
