
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXT_REVERSE_CONTAINER
#define EXT_REVERSE_CONTAINER

#include <iterator>


namespace ext{

/**
 * Helper struct to reverse a container.
 *
 * \see reversion_wrapper<T> reverse (T&& iterable)
 */
template <typename T>
struct reversion_wrapper{
    T& iterable;
};

/**
 * Helper function to reverse a container.
 *
 * \see reversion_wrapper<T> reverse (T&& iterable)
 */
template <typename T>
auto begin(reversion_wrapper<T> w)
{
    return std::rbegin(w.iterable);
}

/**
 * Helper function to reverse a container.
 *
 * \see reversion_wrapper<T> reverse (T&& iterable)
 */
template <typename T>
auto end(reversion_wrapper<T> w)
{
    return std::rend(w.iterable);
}

/**
 * Reverses a container.
 *
 * This function is mainly made to be used in range-based for loops. Given a container \a c,
 * the syntax \code for (auto& i : c) \endcode executes a for loop from \c begin(c) to \c end(c);
 * with this function, the syntax \code for (auto& i : reverse(c)) \endcode can be used to
 * executes the loop from \c rbegin(c) to \c rend(c).
 *
 * \param iterable container to reverse.
 * \return a reversion_wrapper that can be used to loop from the end to the begin of the container.
 */
template <typename T>
reversion_wrapper<T> reverse (T&& iterable)
{
    return { iterable };
}

}  // namespace ext

#endif  // EXT_REVERSE_CONTAINER
