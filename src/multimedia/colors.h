
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_COLORS
#define MULTIMEDIA_COLORS

#include <unordered_map>

#include <SFML/Graphics/Color.hpp>

#include "ext/cast.h"
#include "ext/chars_sequence_index.h"


namespace multimedia{

/**
 * \brief Container for named colors.
 *
 * The container should not be used directly, use multimedia::getColor() and multimedia::addColor() instead.
 */
inline std::unordered_map<ext::chars_sequence_index, sf::Color, ext::chars_sequence_hash> colors;

/**
 * \brief Returns a color from a sequence of characters.
 *
 * The sequence of characters can represent either the name of a color added with multimedia::addColor(), or the
 * components of the color separated by commas. For example, a valid sequence is "128,64,64,192" (r,g,b,a).
 *
 * If the string pointed-to by \a begin is null-terminated, the parameter \a end is optional. Otherwise, the function
 * reads every character of the sequence [begin, end). The end character is not read.
 *
 * \param begin pointer to the first character to read.
 * \param end pointer to one-past the last character to read. Default is nullptr, in the case of a null-terminated
 * string.
 * \tparam T type of characters.
 * \return if the sequence of character is a known name, the color corresponding to that name. Otherwise, a color
 * whose components are interpreted from that sequence.
 */
template <typename T>
sf::Color getColor(const T* begin, const T* end = nullptr)
{
    // Check if the sequence of characters matches a string in the container
    auto v = colors.find(ext::chars_sequence_index(begin, end));

    if (v != colors.end())
        return v->second;


    // No match; try to read the color components
    int c[4] = {0, 0, 0, 255};
    const T* start = begin;
    int current = 0;

    while (begin != end && *begin != 0 && current < 4)
    {
        if (*begin == 0x2C)  // comma
        {
            c[current] = ext::chars_to_int(start, begin);
            ++current;
            start = begin+1;
        }
        ++begin;
    }
    if (current < 4)
        c[current] = ext::chars_to_int(start, end);  // the last one


    return sf::Color(c[0], c[1], c[2], c[3]);
}

/**
 * \brief Bounds a name to a color.
 *
 * Where colors are needed, a name can be used instead. This function records a name and the color linked to.
 * The color can then be get using multimedia::getColor().
 *
 * The name of the color must be an ASCII sequence of characters. Indeed, the name can be encoded in different
 * encodings, but the internal hash used to record the name will be the same in all encodings if the name uses
 * only ASCII characters. See also ext::chars_sequence_index.
 *
 * The name must be a null-terminated sequence. The color is represented by [begin, end). If the string pointed-to by
 * \a begin is null-terminated, the parameter \a end is optional. Otherwise, the function reads every character of
 * the sequence [begin, end). The end character is not read.
 *
 * The color is read using multimedia::getColor().
 *
 * \param name null-terminated sequence of characters representing the name of the color.
 * \param begin pointer to the first character to read to get the color.
 * \param end pointer to one-past the last character to read to get the color. Default is nullptr, in the case of a
 * null-terminated string.
 * \tparam T type of characters.
 */
template <typename T>
void addColor(const T* name, const T* begin, const T* end = nullptr)
{
    colors.emplace(ext::chars_sequence_index(name), getColor(begin, end));
}

}  // namespace multimedia

#endif  // MULTIMEDIA_COLORS
