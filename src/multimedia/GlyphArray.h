
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_GLYPHARRAY
#define MULTIMEDIA_GLYPHARRAY

#include <vector>
#include <unordered_map>

#include <SFML/Graphics.hpp>

namespace std
{
    /**
     * Specialization og std::hash used for
     * std::unordered_map< std::pair<const sf::Font*, int>, std::vector<sf::Vertex> > GlyphArray::_vertices
     */
    template<>
    struct hash<pair<const sf::Font*, int>>
    {
        size_t operator()(pair<const sf::Font*, int> const& s) const noexcept
        {
            return (size_t)s.first + s.second;
        }
    };
}

namespace multimedia{

/**
 * \brief Displays a multimedia::RichText on the screen.
 *
 * Internally, a GlyphArray object only contains the characters of a unique line. To create a multi-line text,
 * the object manages (and owns) a pointer to the 'next' GlyphArray object corresponding to the next line.
 * The lines can communicate and "exchange" words if necessary.
 *
 * This class has been made to be used internally by multimedia::RichText. No other user is targeted.
 */

class GlyphArray
{
    private:
        /**
         * \brief Contains all information about a character that should be rendered on screen.
         */
        struct Index
        {
            size_t index;
            unsigned int character;
            const sf::Font* font;
            int size;
            sf::Color color;
            size_t vertexIndex;
            int baseline;
            int right;

            /**
             * Returns the x-coordinate of the left side of the character.
             *
             * \return x-coordinate of the left side of the character.
             */
            int left();

            /**
             * Returns the width of the characters, in pixels.
             *
             * \return number of pixels taken by the character.
             */
            float advance();
        };

        std::vector<Index> _indexes {};  ///< Ordered vector of characters, containing the index and the formatting

        std::unordered_map<
            std::pair<const sf::Font*, int>,  // The key of the map sets the font and the size of the characters
            std::vector<sf::Vertex>           // A vector of vertices is created for each pair of <Font, Size>.
                                              // The color is stored inside the sf::Vertex class.
        > _vertices {};                       /**< Container used to store all the vertices for all the characters*/

        int _width {};
        int _yPosition {};
        int _topMargin {};
        int _lineSpacing {};
        int _maxSize {};

        float _textWidth {};

        float _yMin {};
        float _yMax {};

        GlyphArray* next {nullptr}, *previous{nullptr};

        uint16_t _flags {};
        bool _updated {false};
        bool _aligned{false};

    public:
        /**
         * Default constructor.
         */
        GlyphArray() = default;

        /**
         * Move constructor.
         */
        GlyphArray(GlyphArray&& other) noexcept;

        /**
         * Move operator.
         */
        GlyphArray& operator=(GlyphArray&& right) noexcept;

        /**
         * Destructor.
         */
        ~GlyphArray();

    private:
        GlyphArray(const GlyphArray&) = delete;
        GlyphArray& operator=(const GlyphArray&) = delete;

    public:
        /**
         * Sets the maximum width that the text can take.
         *
         * If not set or set to 0, the text will take as much space as needed.
         *
         * \param w maximum width of the text.
         */
        void setMaximumWidth(int w);

        /**
         * Adds new flags to set the properties of the RichText.
         * Allowed flags are flags from the textManager::Flags enum.
         *
         * \param newFlags flags to add.
         */
        void setFlags(uint16_t f);

        /**
         * Returns the width of the text.
         * If a maximum width has been fixed by setMaximumWidth(), this function returns that size.
         * Otherwise, this function returns the width that the text takes to be fully displayed on the screen.
         *
         * \return width of the text.
         */
        int width();

        /**
         * Returns the height that the text takes to be fully displayed on the screen.
         *
         * \return height of the text.
         */
        int height();

        /**
         * Clears the data stored in the GlyphArray. It does not clear the properties (maximum width, flags...)
         */
        void clear();

        /**
         * Adds a new character at the given position with given formatting.
         *
         * The index of the following character are automatically updated.
         *
         * \param cursor index where to put the new character.
         * \param c      character to add.
         * \param f      font to use.
         * \param s      size of the character.
         * \param col    color of the character.
         * \param force  default is \a false and should NOT be set to \a true (internal use only).
         */
        void add(size_t cursor, unsigned int c, const sf::Font* f, int s, const sf::Color& col, bool force = false);

        /**
         * Adds a NewLine character at the given position with given formatting.
         *
         * The index of the following character are automatically updated.
         *
         * \param cursor index where to put the new character.
         * \param f      font to use.
         * \param s      size of the character.
         * \param col    color of the character.
         * \param h      top margin to add to the new line created.
         * \param force  default is \a false and should NOT be set to \a true (internal use only).
         */
        void addNewLine(size_t cursor, const sf::Font* f, int s, const sf::Color& col, int h, bool force = false);

        /**
         * Removes the character at the given position.
         *
         * The index of the following character are automatically updated.
         *
         * \param cursor      index of the character to remove.
         * \param indexOffset if set, the index of the following characters will be decreased by this value.
         *                    if not set or set to 0, the offset is calculated with DML_sizeof().
         * \param force       default is \a false and should NOT be set to \a true (internal use only).
         */
        void remove(size_t cursor, int indexOffset = 0, bool force = false);

        /**
         * Finds the nearest character of a point (\a x, \a y) on the screen.
         *
         * \param x x-coordinate of the point
         * \param y y-coordinate of the point
         * \return std::tuple composed of: 1) the index of the character; 2) font of the character; 3) size of the
         *         character; 4) color of the character.
         */
        std::tuple<size_t, const sf::Font*, int, sf::Color> findCursor(int x, int y);

        /**
         * Returns the bottom-left position of the character which index is \a cursor.
         *
         * \param cursor index of the character.
         * \return std::pair composed of: 1) x-coordinate of the position; 2) y-coordinate of the position.
         */
        std::pair<int, int> cursorPosition(size_t cursor);

        /**
         * Returns the index of the displayed character preceding the given cursor.
         *
         * \param cursor index from which to search for the previous character.
         * \return std::pair composed of: 1) index of the resulting character; 2) resulting character.
         */
        std::pair<size_t, unsigned int> getPreviousCharacter(size_t cursor);

        /**
         * Returns the index of the displayed character following the given cursor.
         *
         * \param cursor index from which to search for the next character.
         * \return std::pair composed of: 1) index of the resulting character; 2) resulting character.
         */
        std::pair<size_t, unsigned int> getCharacter(size_t cursor);

        /**
         * Updates the representation of the characters.
         *
         * This function should be called after an update of the text is complete.
         */
        void update();

        /**
         * Displays the characters on the \a target.
         *
         * The glyphs must be placed on the target through the sf::RenderStates object.
         *
         * If the text contains several lines, the lines that do not intersect with the area defined by \a yMinimum
         * and \a yMaximum are not displayed.
         *
         * \param target    target to draw on.
         * \param yMinimum  minimum y-coordinate of the area to draw.
         * \param yMaximum  maximum y-coordinate of the area to draw.
         * \param states    states to apply when drawing.
         */
        void display(sf::RenderTarget& target, int yMinimum, int yMaximum,
                     const sf::RenderStates& states);

    private:
        std::vector<GlyphArray::Index>::iterator findIndex(size_t cursor);
        void createNextLine(int top);
        void removeNextLine();
        void moveLinePosition(int offset);
        int lengthOfFirstWord();
        int left();
        void realignToLeft();
        void changeIndexes(size_t cursor, int offset);
        void dumpToNextLine(size_t cutIndex);
        void stealFirstWordOfNextLine();
        void drawGlyphs(sf::RenderTarget& target, const sf::RenderStates& states);
        template<typename F>
        void applyToGlyphVertices(const Index& i, F f);
};

}  // namespace multimedia

#endif  // MULTIMEDIA_GLYPHARRAY
