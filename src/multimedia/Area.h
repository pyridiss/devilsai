
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_AREA
#define MULTIMEDIA_AREA


namespace multimedia{

/**
 * \brief Defines a point in a window.
 */
struct Point
{
    int x {}; /**< x-coordinate of the point.**/
    int y {}; /**< y-coordinate of the point.**/
};

/**
 * \brief Defines an axis-aligned rectangle area of a window.
 */
struct Area
{
    int x {}; /**< x-coordinate of the top-left corner of the area.**/
    int y {}; /**< y-coordinate of the top-left corner of the area.**/
    int w {}; /**< width of the area.**/
    int h {}; /**< height of the area.**/

    /**
     * Tells whether a point is inside the area (bounds included).
     *
     * \param p point to check.
     * \return \a true if the point is inside the area, \a false otherwise.
     */
    bool in(Point p)
    {
        return (p.x >= x && p.y >= y && p.x <= x+w && p.y <= y+h);
    }
};

}  // namespace multimedia

#endif  // MULTIMEDIA_AREA
