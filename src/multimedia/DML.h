
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_DML
#define MULTIMEDIA_DML

// Unicode code names
constexpr unsigned int BackSpace      = 0x08;
constexpr unsigned int NewLine        = 0x0A;
constexpr unsigned int CarriageReturn = 0x0D;
constexpr unsigned int Space          = 0x20;
constexpr unsigned int AtSign         = 0x40;
constexpr unsigned int Delete         = 0x7F;

// DML markups
constexpr unsigned int DML_MarkupSign               = 0x40;

constexpr unsigned int DML_markup_AtSign            = 0x40;
constexpr unsigned int DML_markup_ParameterBegin    = 0x5B;
constexpr unsigned int DML_markup_ParameterEnd      = 0x5D;
constexpr unsigned int DML_markup_Color             = 0x63;
constexpr unsigned int DML_markup_DefaultFormatting = 0x64;
constexpr unsigned int DML_markup_Font              = 0x66;
constexpr unsigned int DML_markup_NewLine           = 0x6E;
constexpr unsigned int DML_markup_Size              = 0x73;

constexpr int DML_sizeof(unsigned int c)
{
    if (c == NewLine) return 2;
    if (c == AtSign)  return 2;
    return 1;
}

#endif  // MULTIMEDIA_DML
