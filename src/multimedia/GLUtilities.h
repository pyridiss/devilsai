
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_GLUTILITIES
#define MULTIMEDIA_GLUTILITIES

#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>

#include "multimedia/Area.h"


namespace multimedia{

/**
 * Restricts the drawing on an area of the target (using glScissor()).
 *
 * \param target target on which restrictions apply.
 * \param area   area where drawing is still allowed.
 *
 * \see unclipView().
 */
inline void clipView(sf::RenderTarget& target, Area area)
{
    target.setActive();
    glEnable(GL_SCISSOR_TEST);
    glScissor(area.x, target.getSize().y - area.y - area.h, area.w, area.h);
}

/**
 * Removes any restriction on the drawing on the target.
 *
 * \param target target on which the restrictions must be removed.
 *
 * \see clipView().
 */
inline void unclipView(sf::RenderTarget& target)
{
    target.setActive();
    glDisable(GL_SCISSOR_TEST);
}

}  // namespace multimedia

#endif  // MULTIMEDIA_GLUTILITIES
