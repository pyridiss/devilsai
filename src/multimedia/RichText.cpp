
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "multimedia/RichText.h"

#include "ext/cast.h"

#include "multimedia/colors.h"
#include "multimedia/DML.h"
#include "multimedia/GLUtilities.h"
#include "multimedia/GlyphArray.h"
#include "textManager/textManager.h"
#include "textManager/plainText.h"

#include "tools/graphicBuffer.h"

#include "imageManager/imageManager.h"


using namespace sf;
using namespace textManager;

namespace multimedia{

void RichText::setSize(int w, int h)
{
    _height = h;
    _glyphArray.setMaximumWidth(w);
}

void RichText::setDefaultFormatting(const std::string& f, int s, const sf::Color& c)
{
    setDefaultFormatting(font(f), s, c);
}

void RichText::setDefaultFormatting(const sf::Font* f, int s, const sf::Color& c)
{
    if (_defaultFont != f || _defaultSize != s || _defaultColor != c)
    {
        _defaultFont = f;
        _defaultSize = s;
        _defaultColor = c;

        _cursorFont = _defaultFont;
        _cursorSize = _defaultSize;
        _cursorColor = _defaultColor;

        _cursor = 0;
        auto t = std::move(_text);
        _text.clear();
        _glyphArray.clear();

        interpretAsDML(t);
    }
}

void RichText::addFlags(uint16_t newFlags)
{
    _flags |= newFlags;
    _glyphArray.setFlags(_flags);
}

int RichText::width()
{
    return _glyphArray.width();
}

int RichText::height()
{
    return (_height) ? _height : _glyphArray.height();
}

void RichText::create(const PlainText& src)
{
    _text.clear();
    _glyphArray.clear();
    _cursor = 0;
    interpretAsDML(src);
}

void RichText::addAtCursor(unsigned int c)
{
    if (_cursorFont == nullptr)
        return;

    imageManager::lockGLMutex(201);

    switch (c)
    {
        case BackSpace:
        case Delete:
        {
            auto [index, character] = (c == BackSpace ? _glyphArray.getPreviousCharacter(_cursor) : _glyphArray.getCharacter(_cursor));

            if (index == (size_t)-1) break;  // No character to remove

            int length = DML_sizeof(character);

            if (_text[index+length] == DML_markup_ParameterBegin)
                length += _text.find(DML_markup_ParameterEnd, index+length) - (index+length) + 1;

            _text.erase(index, length);
            _glyphArray.remove(index, length);
            _cursor = index;

            break;
        }
        case CarriageReturn:
        case NewLine:
            _text.insert(_cursor, {DML_MarkupSign, DML_markup_NewLine});
            _glyphArray.addNewLine(_cursor, _cursorFont, _cursorSize, _cursorColor, 0);
            _cursor += 2;
            break;

        case AtSign:
            _text.insert(_cursor, {DML_MarkupSign, DML_markup_AtSign});
            _glyphArray.add(_cursor, c, _cursorFont, _cursorSize, _cursorColor);
            _cursor += 2;
            break;

        default:
            _text.insert(_cursor, {c});
            _glyphArray.add(_cursor, c, _cursorFont, _cursorSize, _cursorColor);
            ++_cursor;
            break;
    }

    _glyphArray.update();

    tie(_cursorX, _cursorY) = _glyphArray.cursorPosition(_cursor);

    imageManager::unlockGLMutex(201);
}

void RichText::append(const PlainText& src)
{
    _cursor = _text.size();
    interpretAsDML(src);
}

void RichText::append(unsigned int c)
{
    _cursor = _text.size();
    addAtCursor(c);
}

std::basic_string<unsigned int> RichText::text()
{
    return _text;
}

void RichText::setCursor(int x, int y)
{
    tie(_cursor, _cursorFont, _cursorSize, _cursorColor) = _glyphArray.findCursor(x, y);
    tie(_cursorX, _cursorY) = _glyphArray.cursorPosition(_cursor);
}

void RichText::interpretAsDML(const PlainText& src)
{
    if (_cursorFont == nullptr)
        return;

    const basic_string<unsigned int>& str = src.aggregatedText();
    if (str.empty()) return;

    _text.insert(_cursor, str);

    // RichText::interpretAsDML() uses some OpenGL stuff. It may be called concurrently, so we lock the GL mutex.
    imageManager::lockGLMutex(200);

    const size_t npos = basic_string<unsigned>::npos;

    for (auto begin = str.begin(), c = begin, end = str.end() ; c != end ; ++c)
    {
        if (*c == DML_MarkupSign)
        {
            unsigned type = ((c+1) != end) ? *(c+1) : 0;

            size_t parameterBegin = npos, parameterEnd = npos;
            if ((c+2) != end && *(c+2) == DML_markup_ParameterBegin)
            {
                parameterBegin = (c - begin) + 2;
                parameterEnd = str.find(DML_markup_ParameterEnd, c - begin);
            }

            switch(type)
            {
                case DML_markup_AtSign:
                {
                    _glyphArray.add(_cursor, AtSign, _cursorFont, _cursorSize, _cursorColor);
                    ++c;
                    break;
                }
                case DML_markup_NewLine:
                {
                    int height = 2;
                    if (parameterBegin != npos && parameterEnd != npos)
                        height = ext::chars_to_int(&*(begin + parameterBegin + 1), &*(begin + parameterEnd));
                    else ++c;

                    _glyphArray.addNewLine(_cursor, _cursorFont, _cursorSize, _cursorColor, height);

                    break;
                }
                case DML_markup_Font:
                {
                    if (parameterBegin == npos || parameterEnd == npos) break;

                    string newFont;
                    for (size_t i = parameterBegin+1 ; i < parameterEnd ; ++i)
                        newFont.push_back(*(begin+i));

                    _cursorFont = font(newFont);

                    break;
                }
                case DML_markup_Size:
                {
                    if (parameterBegin != npos && parameterEnd != npos)
                        _cursorSize = ext::chars_to_int(&*(begin + parameterBegin + 1), &*(begin + parameterEnd));
                    break;
                }
                case DML_markup_Color:
                {
                    if (parameterBegin != npos && parameterEnd != npos)
                        _cursorColor = getColor(&str[parameterBegin + 1], &str[parameterEnd]);
                    break;
                }
                case DML_markup_DefaultFormatting:
                    _cursorFont = _defaultFont;
                    _cursorSize = _defaultSize;
                    _cursorColor = _defaultColor;
                    ++c;
                    break;

                default:
                    break;
            }

            ++_cursor;

            if (parameterBegin != npos && parameterEnd != npos)
            {
                _cursor += parameterEnd - parameterBegin + 1;
                c = begin + parameterEnd;
            }
        }
        else  // normal character, control characters should not be used in strings
        {
            _glyphArray.add(_cursor, *c, _cursorFont, _cursorSize, _cursorColor);
        }

        ++_cursor;
    }

    _glyphArray.update();

    tie(_cursorX, _cursorY) = _glyphArray.cursorPosition(_cursor);

    // Release the GL mutex
    imageManager::unlockGLMutex(200);
}

void RichText::display(sf::RenderTarget& target, Area area, Point origin, const sf::RenderStates& states)
{
    bool clip = (area.w < _glyphArray.width() || area.h < _glyphArray.height());

    if (clip) clipView(target, area);

    // Adapt the position according to the flags

    if (_flags & OriginRight)
        area.x -= width();
    else if (_flags & OriginXCenter)
        area.x -= width() / 2.f;

    if (_flags & OriginBottom)
        area.y -= height();
    else if (_flags & OriginYCenter)
        area.y -= height() / 2.f;

    if (_flags & VAlignCenter)
        area.y += (height() - _glyphArray.height()) / 2.f;


    sf::RenderStates st = states;
    st.transform.translate(area.x - origin.x, area.y - origin.y);

    int minY = origin.y;
    int maxY = origin.y + area.h;

    if (st.shader == nullptr)
    {
        _glyphArray.display(target, minY, maxY, st);
    }
    else
    {
        RenderTexture& tex = tools::graphicBuffer::buffer();
        tex.clear(Color::Transparent);

        sf::RenderStates st2 = st;
        st2.shader = nullptr;
        _glyphArray.display(tex, minY, maxY, st2);

        tex.display();

        Sprite sprite(tex.getTexture());
        target.draw(sprite, st.shader);
    }

    drawCursor(target);

    if (clip) unclipView(target);
}

void RichText::drawCursor(sf::RenderTarget& target)
{
    if (_flags & ShowCursor)
    {
        RectangleShape cursor(Vector2f(1, _cursorSize));
        cursor.setPosition(_cursorX, _cursorY - _cursorSize);
        cursor.setFillColor(_cursorColor);
        target.draw(cursor);
    }
}

}  // namespace multimedia
