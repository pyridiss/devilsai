
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "multimedia/GlyphArray.h"

#include <cmath>

#include "multimedia/DML.h"
#include "textManager/textManager.h"


using namespace sf;
using namespace textManager;

namespace multimedia{

int GlyphArray::Index::left()
{
    return right - round(advance());
}

float GlyphArray::Index::advance()
{
    if (character == NewLine) return 1.f;
    return font->getGlyph(character, size, false).advance;
}

GlyphArray::~GlyphArray()
{
    if (next) delete next;
}

GlyphArray::GlyphArray(GlyphArray&& other) noexcept
  : _indexes    {std::move(other._indexes)},
    _vertices   {std::move(other._vertices)},
    _width      {other._width},
    _yPosition  {other._yPosition},
    _topMargin  {other._topMargin},
    _lineSpacing{other._lineSpacing},
    _maxSize    {other._maxSize},
    _textWidth  {other._textWidth},
    _yMin       {other._yMin},
    _yMax       {other._yMax},
    next        {other.next},
    previous    {other.previous},
    _flags      {other._flags},
    _updated    {other._updated},
    _aligned    {other._aligned}
{
    other.next = other.previous = nullptr;
}

GlyphArray& GlyphArray::operator=(GlyphArray&& right) noexcept
{
    _indexes     = std::move(right._indexes);
    _vertices    = std::move(right._vertices);
    _width       = right._width;
    _yPosition   = right._yPosition;
    _topMargin   = right._topMargin;
    _lineSpacing = right._lineSpacing;
    _maxSize     = right._maxSize;
    _textWidth   = right._textWidth;
    _yMin        = right._yMin;
    _yMax        = right._yMax;
    next         = right.next;
    previous     = right.previous;
    _flags       = right._flags;
    _updated     = right._updated;
    _aligned     = right._aligned;

    right.next = right.previous = nullptr;

    return *this;
}

void GlyphArray::setMaximumWidth(int w)
{
    _width = w;
    _updated = false;
}

void GlyphArray::setFlags(uint16_t f)
{
    _flags = f;
}

int GlyphArray::width()
{
    if (_width) return _width;
    else return max((int)round(_textWidth), (next) ? next->width() : 0);
}

int GlyphArray::height()
{
    if (next) return next->height();

    return _yPosition + _topMargin + _lineSpacing;
}

void GlyphArray::clear()
{
    delete next;

    _vertices.clear();
    _indexes.clear();
    _textWidth = _yMin = _yMax = 0;
    next = nullptr;
}

void GlyphArray::add(size_t cursor, unsigned int c, const sf::Font* f, int s, const sf::Color& col, bool force)
{
    if (c == NewLine) return addNewLine(cursor, f, s, col, 0, force);

    auto w = findIndex(cursor);

    if (!force && w == _indexes.end() && next)
        return next->add(cursor, c, f, s, col);


    const Glyph& glyph = f->getGlyph(c, s, false);
    float advance = glyph.advance;

    // Make room for the new glyph
    for (auto& i : _indexes) if (i.index >= cursor)
    {
        applyToGlyphVertices(i, [advance](Vertex& v)
        {
            v.position.x += advance;
        });
    }
    for (auto ww = w ; ww != _indexes.end() ; ++ww)
        ww->right += advance;

    // Increment the index of the following characters
    if (!force) changeIndexes(cursor, DML_sizeof(c));

    // Create the new glyph
    _maxSize = max(s, _maxSize);
    int y = _yPosition + _topMargin + _maxSize;

    if (c != Space)
    {
        auto& v = _vertices[make_pair(f, s)];
        float x = (w == _indexes.begin()) ? 0 : (w-1)->right;
        unsigned int prev = (w == _indexes.begin()) ? 0 : (w-1)->character;
        x += f->getKerning(prev, c, s);

        float padding = 1.0;

        float left   = glyph.bounds.left - padding;
        float top    = glyph.bounds.top - padding;
        float right  = glyph.bounds.left + glyph.bounds.width + padding;
        float bottom = glyph.bounds.top  + glyph.bounds.height + padding;

        float u1 = static_cast<float>(glyph.textureRect.left) - padding;
        float v1 = static_cast<float>(glyph.textureRect.top) - padding;
        float u2 = static_cast<float>(glyph.textureRect.left + glyph.textureRect.width) + padding;
        float v2 = static_cast<float>(glyph.textureRect.top  + glyph.textureRect.height) + padding;

        v.push_back(sf::Vertex(sf::Vector2f(x + left , y + top   ), col, sf::Vector2f(u1, v1)));
        v.push_back(sf::Vertex(sf::Vector2f(x + right, y + top   ), col, sf::Vector2f(u2, v1)));
        v.push_back(sf::Vertex(sf::Vector2f(x + left , y + bottom), col, sf::Vector2f(u1, v2)));
        v.push_back(sf::Vertex(sf::Vector2f(x + left , y + bottom), col, sf::Vector2f(u1, v2)));
        v.push_back(sf::Vertex(sf::Vector2f(x + right, y + top   ), col, sf::Vector2f(u2, v1)));
        v.push_back(sf::Vertex(sf::Vector2f(x + right, y + bottom), col, sf::Vector2f(u2, v2)));

        int cright = round((w == _indexes.begin()) ? advance : (w-1)->right + advance);
        _indexes.emplace(w, Index{cursor, c, f, s, col, v.size()-6, y, cright});
    }
    else
    {
        int cright = round((w == _indexes.begin()) ? advance : (w-1)->right + advance);
        _indexes.emplace(w, Index{cursor, c, f, s, col, 0, y, cright});
    }

    // Update data
    _textWidth += advance;
    _updated = false;
}

void GlyphArray::addNewLine(size_t cursor, const sf::Font* f, int s, const sf::Color& col, int h, bool force)
{
    auto w = findIndex(cursor);

    if (!force && w == _indexes.end() && next)
        return next->addNewLine(cursor, f, s, col, h);

    // Increment the index of the following characters
    if (!force)
        changeIndexes(cursor, DML_sizeof(NewLine));

    // Create the index for the newline
    int right = (w == _indexes.begin()) ? 0 : (w-1)->right + 1;
    _indexes.emplace(w, Index{cursor, NewLine, f, s, col, 0, 0, right});

    if (!force || next == nullptr) createNextLine(h);

    // Move the rest of the line to the next one
    dumpToNextLine(cursor);

    _updated = false;
}

void GlyphArray::remove(size_t cursor, int indexOffset, bool force)
{
    auto w = findIndex(cursor);

    if (!force && w == _indexes.end() && next)
        return next->remove(cursor, indexOffset);

    if (w == _indexes.end()) return;

    if (w->character != Space && w->character != NewLine)
    {
        auto& v = _vertices[make_pair(w->font, w->size)];
        v.erase(v.begin() + w->vertexIndex, v.begin() + w->vertexIndex + 6);
        for (auto& j : _indexes)
            if (j.font == w->font && j.size == w->size && j.vertexIndex > w->vertexIndex)
                j.vertexIndex -= 6;
    }

    float advance = w->advance();

    for (auto& i : _indexes) if (i.index > cursor)
    {
        applyToGlyphVertices(i, [advance](Vertex& v)
        {
            v.position.x -= advance;
        });
    }
    for (auto ww = w ; ww != _indexes.end() ; ++ww)
        ww->right -= advance;

    // Decrement the index of the following characters
    if (!force)
    {
        int change = (indexOffset == 0 ? DML_sizeof(w->character) : indexOffset);
        changeIndexes(cursor, -change);
    }

    if (w->character == NewLine && next && next->_indexes.empty())
        removeNextLine();

    // If the removed character is a new line, remove the top margin of the next line
    if (w->character == NewLine && next)
    {
        next->_topMargin = 0;
        next->_updated = false;
    }

    // If the removed character is a space that breaks the line, steal the first word of the next line
    if (w->character == Space && w->index == _indexes.back().index)
        stealFirstWordOfNextLine();

    _indexes.erase(w);

    _textWidth -= advance;
    _updated = false;

    if (_indexes.empty() && previous)
        previous->removeNextLine();  // No character left; suicide.
}


std::tuple<size_t, const sf::Font*, int, sf::Color> GlyphArray::findCursor(int x, int y)
{
    if (y > _yMax && next) return next->findCursor(x, y);

    if (y <= _yMax)
    {
        for (auto i = _indexes.begin() ; i != _indexes.end() ; ++i)
        {
            if (x > i->right) continue;

            if (x <= i->left() + i->advance()/2)  // Before the character i
                return make_tuple(i->index, i->font, i->size, i->color);
            else
            {
                if (i+1 != _indexes.end())  // Between the character i and the character i+1, using the properties of the character i
                    return make_tuple((i+1)->index, i->font, i->size, i->color);
            }
        }
    }

    // Jumps here if the cursor should be displayed at the end of the current line
    if (!_indexes.empty())
    {
        if (_indexes.back().character == NewLine || _indexes.back().character == Space)  // Place the cursor just before
            return make_tuple(_indexes.back().index, _indexes.back().font, _indexes.back().size, _indexes.back().color);
        else
            return make_tuple(_indexes.back().index + DML_sizeof(_indexes.back().character), _indexes.back().font, _indexes.back().size, _indexes.back().color);
    }

    // No text in this line, use the previous one
    if (previous)
    {
        auto& i = previous->_indexes.back();
        return make_tuple(i.index + DML_sizeof(i.character), i.font, i.size, i.color);
    }

    // Aargh... no other solution
    return make_tuple(0, nullptr, 12, Color::Black);
}

std::pair<int, int> GlyphArray::cursorPosition(size_t cursor)
{
    auto w = findIndex(cursor);

    if (w == _indexes.end() && next)
        return next->cursorPosition(cursor);

    if (_indexes.empty())
    {
        int x = 0, y = _yMin;
        if (_flags & HAlignCenter) x = width()/2;
        else if (_flags & HAlignRight) x = width();
        if (previous) y += previous->_indexes.back().size;
        return make_pair(x, y);
    }

    if (w == _indexes.begin())
        return make_pair(w->left(), w->baseline);
    else
        return make_pair((w-1)->right, (w-1)->baseline);
}

std::pair<size_t, unsigned int> GlyphArray::getPreviousCharacter(size_t cursor)
{
    auto w = findIndex(cursor);

    if (w == _indexes.end() && next)
        return next->getPreviousCharacter(cursor);

    if (_indexes.empty())
    {
        if (previous) return make_pair(previous->_indexes.back().index, previous->_indexes.back().character);
        return make_pair<size_t, unsigned int>(-1, 0);
    }

    if (w == _indexes.begin())
    {
        if (previous && !previous->_indexes.empty())
            return make_pair(previous->_indexes.back().index, previous->_indexes.back().character);
        else return make_pair<size_t, unsigned int>(-1, 0);
    }
    else return make_pair((w-1)->index, (w-1)->character);
}

std::pair<size_t, unsigned int> GlyphArray::getCharacter(size_t cursor)
{
    auto w = findIndex(cursor);

    if (w == _indexes.end() && next)
        return next->getCharacter(cursor);

    if (_indexes.empty() || w == _indexes.end()) return make_pair<size_t, unsigned int>(-1, 0);

    return make_pair(w->index, w->character);
}

void GlyphArray::update()
{
    // Cut the line if it is too short to contain all characters
    if (_width != 0 && _textWidth > _width)
    {
        if (next == nullptr || (!_indexes.empty() && _indexes.back().character == NewLine))
            createNextLine(0);

        // Find the space where to cut
        auto sp = find_if(_indexes.rbegin(), _indexes.rend(), [w = _width](Index& i)
        {
            return (i.character == Space && i.right < w);
        });
        if (sp != _indexes.rend()) dumpToNextLine(sp->index);

        _updated = false;
    }

    // Check if the first words of the next line could "come back" to the current line
    while (_width != 0 && !_indexes.empty() && _indexes.back().character != NewLine &&
           next && next->lengthOfFirstWord() < _width - _textWidth)
    {
        stealFirstWordOfNextLine();
    }

    // Align at center or at right if asked
    if (_width != 0 && !_updated && (_flags & (HAlignRight | HAlignCenter)))
    {
        realignToLeft();

        float x = _width - _textWidth - 1;
        if (_flags & HAlignCenter) x = round(x/2);

        for (auto& [key, value] : _vertices)
        for (auto& v : value)
        {
            v.position.x += x;
        }
        for (auto& i : _indexes)
            i.right += x;

        _aligned = true;
    }

    // Justify the words if asked
    if (_width != 0 && !_updated && (_flags & HAlignJustify) && !_indexes.empty() &&
        _indexes.back().character != NewLine && next)
    {
        realignToLeft();

        float extraSpace = _width - _indexes.back().right;

        float spaces = count_if(_indexes.begin(), _indexes.end(), [](Index& i)
        {
            return i.character == Space;
        });

        if (_indexes.back().character == Space)  // The trailing space should 'disappear' from the result
        {
            extraSpace -= _indexes.back().font->getGlyph(Space, _indexes.back().size, false).advance;
            --spaces;
        }

        if (spaces)
        {
            float s = extraSpace / spaces;
            int n = 0;

            for (auto& i : _indexes)
            {
                if (i.character == Space)
                    ++n;

                i.right += round(n * s);
                applyToGlyphVertices(i, [n, s](Vertex& v)
                {
                    v.position.x += round(n * s);
                });
            }
        }

        _aligned = true;
    }

    // Align the glyphs vertically if there is different sizes of glyphs; move the next lines if needed
    if (!_updated)
    {
        int m = 0;
        float h = 0;
        for (auto& i : _indexes)
        {
            if (i.size > m)
            {
                m = i.size;
                h = i.font->getLineSpacing(m);
            }
        }

        float newBaseline = _yPosition + _topMargin + m;
        for (auto& i : _indexes)
        {
            if (i.baseline != newBaseline)
            {
                applyToGlyphVertices(i, [newBaseline, b = i.baseline](Vertex& v)
                {
                    v.position.y += newBaseline - b;
                });
                i.baseline = newBaseline;
            }
        }
        _maxSize = m;

        if (next && next->_yPosition != _yPosition + _topMargin + h + 3)
            next->moveLinePosition(_yPosition + _topMargin + h + 3 - next->_yPosition);
        _lineSpacing = h;
    }

    // Recalc the minimum and maximum y
    if (!_updated)
    {
        _yMin = _yPosition + _topMargin;
        _yMax = _yMin;

        if (!_indexes.empty())
            _yMax += _indexes.begin()->font->getLineSpacing(_indexes.begin()->size);

        for (auto [key, value] : _vertices)
        {
            for (auto& v : value)
            {
                _yMin = min(_yMin, v.position.y);
                _yMax = max(_yMax, v.position.y);
            }
        }
    }

    _updated = true;

    // Update the next line
    if (next) next->update();
}
void GlyphArray::display(sf::RenderTarget& target, int yMinimum, int yMaximum,
                         const sf::RenderStates& states)
{
    if ((_yMin >= yMinimum && _yMin <= yMaximum) ||
        (_yMax >= yMinimum && _yMax <= yMaximum) ||
        (_yMin <= yMinimum && _yMax >= yMaximum))
    {
        drawGlyphs(target, states);
    }

    if (next) next->display(target, yMinimum, yMaximum, states);
}

std::vector<GlyphArray::Index>::iterator GlyphArray::findIndex(size_t cursor)
{
    if (!_indexes.empty() && _indexes.back().index < cursor) return _indexes.end();

    return find_if(_indexes.begin(), _indexes.end(), [cursor](Index& i) {
        return i.index >= cursor;
    });
}

void GlyphArray::createNextLine(int top)
{
    GlyphArray* tmp = next;
    next = new GlyphArray;
    next->_width = _width;
    next->_flags = _flags;
    next->_yPosition = _yPosition + _topMargin + _lineSpacing + 3;
    next->_topMargin = top;
    next->next = tmp;
    if (tmp) tmp->moveLinePosition(top + 3);
    if (tmp) tmp->previous = next;
    next->previous = this;
}

void GlyphArray::removeNextLine()
{
    if (!next) return;

    GlyphArray* tmp = next->next;
    if (tmp)
    {
        tmp->moveLinePosition(-(next->_topMargin + next->_lineSpacing) - 3);
        tmp->previous = this;
    }

    next->next = nullptr;
    delete next;

    next = tmp;
}

void GlyphArray::moveLinePosition(int offset)
{
    _yPosition += offset;
    _yMin += offset;
    _yMax += offset;

    for (auto& i : _indexes)
        i.baseline += offset;

    for (auto& [key, value] : _vertices)
    for (auto& v : value)
    {
        v.position.y += offset;
    }
    if (next) next->moveLinePosition(offset);
}

int GlyphArray::lengthOfFirstWord()
{
    if (_indexes.empty()) return 0;

    for (auto& i : _indexes)
    {
        if (i.character == Space || i.character == NewLine)
            return i.right - left();
    }

    return _indexes.back().right - left();
}

int GlyphArray::left()
{
    if (_flags & HAlignRight)
    {
        if (_indexes.empty()) return _width - 1;
        else return _indexes.begin()->left();
    }
    else if (_flags & HAlignCenter)
    {
        if (_indexes.empty()) return _width / 2;
        else return _indexes.begin()->left();
    }
    else  // Left-aligned or Justified
    {
        return 0;
    }
}

void GlyphArray::realignToLeft()
{
    // _aligned is set when the default (left) alignment has been modified.
    // No need to realign to left if the alignment has not changed.
    if (!_aligned) return;

    _textWidth = 0;
    unsigned int prev = 0;
    for (auto& i : _indexes)
    {
        float advance = 0;
        int x = _textWidth;

        if (i.character != NewLine)
        {
            const Glyph& glyph = i.font->getGlyph(i.character, i.size, false);
            advance = glyph.advance;
            x += i.font->getKerning(prev, i.character, i.size);
            x += glyph.bounds.left - 1.0;
        }
        else
        {
            advance = 1;
        }

        if (i.character != Space && i.character != NewLine)
        {
            int c = _vertices[make_pair(i.font, i.size)][i.vertexIndex].position.x;
            int offset = x - c;

            applyToGlyphVertices(i, [offset](Vertex& v)
            {
                v.position.x += offset;
            });
        }

        prev = i.character;
        i.right = _textWidth + round(advance);
        _textWidth += advance;
    }

    _aligned = false;
}

void GlyphArray::changeIndexes(size_t cursor, int offset)
{
    for (auto& i : _indexes)
        if (i.index >= cursor) i.index += offset;

    if (next) next->changeIndexes(cursor, offset);
}

void GlyphArray::dumpToNextLine(size_t cutIndex)
{
    for (auto& i : _indexes)
    {
        if (i.index > cutIndex)
        {
            if (i.character == Space) next->add(i.index, Space, i.font, i.size, i.color, true);
            else if (i.character == NewLine) next->addNewLine(i.index, i.font, i.size, i.color, 0, true);
            else
            {
                auto& v = _vertices[make_pair(i.font, i.size)];
                v.erase(v.begin() + i.vertexIndex, v.begin() + i.vertexIndex + 6);
                for (auto& j : _indexes)
                    if (j.font == i.font && j.size == i.size && j.vertexIndex > i.vertexIndex)
                        j.vertexIndex -= 6;

                next->add(i.index, i.character, i.font, i.size, i.color, true);
            }
        }
    }
    while (!_indexes.empty() && _indexes.back().index > cutIndex)
    {
        _indexes.pop_back();
    }

    // Recalc _textWidth
    _textWidth = 0;
    for (auto& i : _indexes)
        _textWidth += i.advance();
}

void GlyphArray::stealFirstWordOfNextLine()
{
    unsigned int stolen = 0;
    while (next &&  !next->_indexes.empty() && stolen != Space && stolen != NewLine)
    {
        Index& i = *next->_indexes.begin();
        stolen = i.character;
        add(i.index, i.character, i.font, i.size, i.color, true);
        next->remove(i.index, 0, true);
        _updated = false;
    }
}

void GlyphArray::drawGlyphs(sf::RenderTarget& target, const sf::RenderStates& states)
{
    if (_flags & Shaded)
    {
        RenderStates st = states;
        st.transform.translate(1, 1);
        st.blendMode.colorSrcFactor = sf::BlendMode::Factor::Zero;

        for (auto& [key, value] : _vertices)
        {
            st.texture = &key.first->getTexture(key.second);

            target.draw(&value[0], value.size(), sf::Triangles, st);
        }
    }

    RenderStates st = states;
    for (auto& [key, value] : _vertices)
    {
        st.texture = &key.first->getTexture(key.second);
        target.draw(&value[0], value.size(), sf::Triangles, st);
    }
}

template<typename F>
void GlyphArray::applyToGlyphVertices(const Index& i, F f)
{
    if (i.character == Space || i.character == NewLine) return;

    auto& v = _vertices[make_pair(i.font, i.size)];
    f(v[i.vertexIndex  ]);
    f(v[i.vertexIndex+1]);
    f(v[i.vertexIndex+2]);
    f(v[i.vertexIndex+3]);
    f(v[i.vertexIndex+4]);
    f(v[i.vertexIndex+5]);
}

}  // namespace multimedia
