
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIMEDIA_RICHTEXT
#define MULTIMEDIA_RICHTEXT

#include <SFML/Graphics.hpp>

#include "multimedia/Area.h"
#include "multimedia/GlyphArray.h"


namespace textManager{
class PlainText;
}

namespace multimedia{

/**
 * \brief Defines a text with formatting.
 *
 * The RichText class manages a text of any length containing formatting characters. It translates those formatting
 * characters to display the text on the screen though a textManager::GlyphArray. It manages a cursor so that the text
 * can be (partly) edited.
 */

class RichText
{
    private:
        std::basic_string<unsigned int> _text {};
        GlyphArray _glyphArray {};

        int _height {};
        uint16_t _flags {};

        const sf::Font*         _defaultFont {nullptr};
        int _defaultSize {};
        sf::Color _defaultColor {};

        size_t _cursor {};
        const sf::Font*         _cursorFont  {nullptr};
        int _cursorSize {};
        sf::Color _cursorColor {};
        int _cursorX {};
        int _cursorY {};

    public:
        /**
         * Default constructor.
         */
        RichText() = default;

        /**
         * Move constructor.
         */
        RichText(RichText&& other) noexcept = default;

        /**
         * Move operator.
         */
        RichText& operator=(RichText&& right) noexcept = default;

        /**
         * Default destructor.
         */
        ~RichText() = default;

    private:
        RichText(const RichText&) = delete;
        RichText& operator=(const RichText&) = delete;

    public:
        /**
         * Sets the size of the area that can be used to display the text.
         *
         * \param w width of the area. If set to 0, the text will take as much space as needed.
         * \param h height of the area. If set and if the flag VAlignCenter is set too, the text will be
         *          vertically centered on the area.
         */
        void setSize(int w, int h);

        /**
         * Sets the properties to use when the formatting is not explicited in the text.
         *
         * \param f name of the font.
         * \param s size of the text.
         * \param c color of the text.
         */
        void setDefaultFormatting(const std::string& f, int s, const sf::Color& c);

        /**
         * Sets the properties to use when the formatting is not explicited in the text.
         *
         * \param f pointer to the font.
         * \param s size of the text.
         * \param c color of the text.
         */
        void setDefaultFormatting(const sf::Font* f, int s, const sf::Color& c);

        /**
         * Adds new flags to set the properties of the RichText.
         * Allowed flags are flags from the textManager::Flags enum.
         *
         * \param newFlags flags to add.
         */
        void addFlags(uint16_t newFlags);

        /**
         * Returns the width of the text.
         * If a width has been fixed by setSize(), this function returns that size.
         * Otherwise or if setSize() has been called with a width equal to zero, this function returns the width
         * that the text takes to be fully displayed on the screen.
         *
         * \return width of the text.
         */
        int width();

        /**
         * Returns the height of the text.
         * If a height has been fixed by setSize(), this function returns that size.
         * Otherwise or if setSize() has been called with a width equal to zero, this function returns the height
         * that the text takes to be fully displayed on the screen.
         *
         * \return height of the text.
         */
        int height();

        /**
         * Clears the current content of the RichText and replaces it with a new string of characters.
         * The RichText class does not store the parameters like PlainText does, the function
         * PlainText::aggregatedText() is called to extract the characters to store and display.
         *
         * \param src PlainText containing the new characters to add.
         */
        void create(const textManager::PlainText& src);

        /**
         * Adds a character at the current position of the cursor.
         *
         * \param c character to add.
         */
        void addAtCursor(unsigned int c);

        /**
         * Adds a string of characters at the end of the text.
         * The RichText class does not store the parameters like PlainText does, the function
         * PlainText::aggregatedText() is called to extract the characters to store and display.
         *
         * \param src PlainText containing the new characters to add.
         */
        void append(const textManager::PlainText& src);

        /**
         * Appends a character at the end of the text.
         *
         * \param c character to add.
         */
        void append(unsigned int c);

        /**
         * Returns the text as a string.
         *
         * \return text as std::basic_string<unsigned int>
         */
        std::basic_string<unsigned int> text();

        /**
         * Sets the position of the cursor from its coordinates (typically a click).
         *
         * \param x x position of the cursor.
         * \param y y position of the cursor.
         */
        void setCursor(int x, int y);

        /**
         * Displays the text on the \a target.
         *
         * An area where to draw to text must be defined. The text may be truncated if the area is too small.
         *
         * \remark If the area is smaller than the size of the text, this function may call multimedia::clipView().
         *
         * \param target target to draw on.
         * \param area   area where the text must be displayed. If the size of the area is smaller than the size
         *               of the text, the text will be truncated.
         * \param origin offset between the starting point of the text and the top-left corner of the area.
         * \param states states to apply when drawing.
         */
        void display(sf::RenderTarget& target, Area area, Point origin,
                     const sf::RenderStates& states = sf::RenderStates::Default);

    private:
        void interpretAsDML(const textManager::PlainText& src);
        void drawCursor(sf::RenderTarget& target);
};

}  // namespace multimedia

#endif // MULTIMEDIA_RICHTEXT
