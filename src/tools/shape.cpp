
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include <tinyxml2.h>
#include <SFML/Graphics.hpp>

#include "tools/debug.h"
#include "tools/math.h"
#include "tools/vector2d.h"
#include "tools/shape.h"

using namespace std;
using namespace tinyxml2;
using namespace sf;

namespace tools{

namespace math{

Vector2d _absoluteOrigin(0, 0);

Vector2d* absoluteOrigin()
{
    return &_absoluteOrigin;
}

Shape::Shape()
{
}

Shape::Shape(const Shape& s)
  : profile(None),
    _origin(absoluteOrigin()),
    box(s.box),
    next(nullptr)
{
    setUnion(s);
    if (s.next != nullptr)
        next = new Shape(*(s.next));
}

Shape& Shape::operator=(const Shape& right)
{
    // Do not copy origin
    box = right.box;
    setUnion(right);
    if (right.next != nullptr)
    {
        next = new Shape(*(right.next));
        next->setOrigin(_origin);
    }

    return *this;
}

Shape::~Shape()
{
    if (profile == Profiles::Polyline)
        _polyline.points.~vector<math::Vector2D<double>>();

    if (next != nullptr)
        delete next;
}

void Shape::setUnion(Profiles p)
{
    if (p == Polyline && profile != Polyline)
    {
        new(&_polyline.points) vector<math::Vector2D<double>> {};
    }
    else if (profile == Polyline && p != Polyline)
    {
        _polyline.points.~vector<math::Vector2D<double>>();
    }

    profile = p;
}

void Shape::setUnion(const Shape& other)
{
    switch (other.profile)
    {
        case Point:
            _point = other._point;
            break;
        case Circle:
            _circle = other._circle;
            break;
        case Triangle:
            _triangle = other._triangle;
            break;
        case Rectangle:
            _rectangle = other._rectangle;
            break;
        case Line:
            _line = other._line;
            break;
        case Polyline:
            new(&_polyline.points) vector<Vector2D<double>>(other._polyline.points);
            _polyline.thickness = other._polyline.thickness;
            break;
        case Arc:
            _arc = other._arc;
            break;
        default:
            break;
    }

    profile = other.profile;
}

void Shape::setOrigin(const Vector2d* o)
{
    _origin = o;
    if (next != nullptr) next->setOrigin(o);
}

const Vector2d* Shape::origin() const
{
    return _origin;
}

void Shape::none()
{
    setUnion(Profiles::None);
}

void Shape::point(Vector2D<double> p)
{
    setUnion(Profiles::Point);
    _point.point = p;

    box.first.x = p.x - 1;
    box.first.y = p.y - 1;
    box.second.x = p.x + 1;
    box.second.y = p.y + 1;
}

void Shape::circle(Vector2D<double> p, double radius)
{
    if (radius <= 0)
    {
        setUnion(Profiles::None);
        return;
    }

    setUnion(Profiles::Circle);
    _circle.center = p;
    _circle.radius = radius;

    box.first.x = p.x - radius;
    box.first.y = p.y - radius;
    box.second.x = p.x + radius;
    box.second.y = p.y + radius;
}

void Shape::triangle(Vector2D<double> p1, Vector2D<double> p2, Vector2D<double> p3)
{
    if (p1 == p2 || p1 == p3 || p2 == p3)
    {
        profile = Profiles::None;
        return;
    }

    setUnion(Profiles::Triangle);
    _triangle.points[0] = p1;
    _triangle.points[1] = p2;
    _triangle.points[2] = p3;

    box.first.x = minimum(p1.x, p2.x, p3.x);
    box.first.y = minimum(p1.y, p2.y, p3.y);
    box.second.x = maximum(p1.x, p2.x, p3.x);
    box.second.y = maximum(p1.y, p2.y, p3.y);
}

void Shape::rectangle(Vector2D<double> p1, Vector2D<double> p2, Vector2D<double> p3)
{
    if (p1 == p2 || p1 == p3 || p2 == p3)
    {
        setUnion(Profiles::None);
        return;
    }

    setUnion(Profiles::Rectangle);
    _rectangle.points[0] = p1;
    _rectangle.points[1] = p2;
    _rectangle.points[2] = p2 + p3 - p1;
    _rectangle.points[3] = p3;

    box.first.x = minimum(p1.x, p2.x, p3.x, _rectangle.points[2].x);
    box.first.y = minimum(p1.y, p2.y, p3.y, _rectangle.points[2].y);
    box.second.x = maximum(p1.x, p2.x, p3.x, _rectangle.points[2].x);
    box.second.y = maximum(p1.y, p2.y, p3.y, _rectangle.points[2].y);
}

void Shape::line(Vector2D<double> p1, Vector2D<double> p2, int thickness)
{
    setUnion(Profiles::Line);
    _line.points[0] = p1;
    _line.points[1] = p2;
    _line.length = sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
    _line.direction = angle(p2.x - p1.x, p2.y - p1.y);
    _line.thickness = thickness;

    box.first.x = minimum(p1.x, p2.x) - thickness;
    box.first.y = minimum(p1.y, p2.y) - thickness;
    box.second.x = maximum(p1.x, p2.x) + thickness;
    box.second.y = maximum(p1.y, p2.y) + thickness;
}

void Shape::line(Vector2D<double> p, double length, double direction, int thickness)
{
    setUnion(Profiles::Line);
    _line.points[0] = p;
    _line.points[1] = Vector2D<double>(p.x + length * cos(direction), p.y + length * sin(direction));
    _line.length = length;
    _line.direction = direction;
    _line.thickness = thickness;

    box.first.x = minimum(_line.points[0].x, _line.points[1].x) - thickness;
    box.first.y = minimum(_line.points[0].y, _line.points[1].y) - thickness;
    box.second.x = maximum(_line.points[0].x, _line.points[1].x) + thickness;
    box.second.y = maximum(_line.points[0].y, _line.points[1].y) + thickness;
}

void Shape::polyline(const vector<Vector2d>& p, int thickness)
{
    if (p.size() == 0)
    {
        tools::debug::error("The polyline is empty.", "tools::math", __FILENAME__, __LINE__);
        return;
    }
    else if (p.size() == 1)
    {
        return circle(p[0], thickness);
    }
    else if (p.size() == 2)
    {
        return line(p[0], p[1], thickness);
    }

    setUnion(Profiles::Polyline);
    _polyline.points = p;
    _polyline.thickness = thickness;

    box.first.x = p[0].x;
    box.first.y = p[0].y;
    box.second.x = p[0].x;
    box.second.y = p[0].y;

    for (auto& pt : _polyline.points)
    {
        if (pt.x - thickness < box.first.x) box.first.x = pt.x - thickness;
        if (pt.y - thickness < box.first.y) box.first.y = pt.y - thickness;
        if (pt.x + thickness > box.second.x) box.second.x = pt.x + thickness;
        if (pt.y + thickness > box.second.y) box.second.y = pt.y + thickness;
    }
}

void Shape::arc(Vector2D<double> p, double radius, double direction, double opening)
{
    setUnion(Profiles::Arc);
    _arc.center = p;
    _arc.radius = radius;
    _arc.direction = direction;
    _arc.opening = opening;

    box.first.x = p.x - radius;
    box.first.y = p.y - radius;
    box.second.x = p.x + radius;
    box.second.y = p.y + radius;
}

bool Shape::isLine()
{
    return (profile == Profiles::Line);
}

bool Shape::isPolyline()
{
    return (profile == Profiles::Polyline);
}

double Shape::lengthOfLine()
{
    if (profile == Profiles::Line)
        return distance(_line.points[0], _line.points[1]);

    else if (profile == Profiles::Polyline)
    {
        double l = 0;
        for (unsigned i = 1 ; i < _polyline.points.size() ; ++i)
            l += distance(_polyline.points[i-1], _polyline.points[i]);

        return l;
    }

    return 0;
}

std::vector<Vector2D<double>> Shape::pointsOfLine()
{
    if (profile == Profiles::Line)
    {
        vector<Vector2D<double>> v {_line.points[0], _line.points[1]};
        return v;
    }

    else if (profile == Profiles::Polyline)
        return _polyline.points;

    return vector<Vector2D<double>> {};
}

double Shape::area()
{
    switch(profile)
    {
        case Profiles::Circle:
            return M_PI * _circle.radius * _circle.radius;
        case Profiles::Triangle:
            return 1./2. * abs((_triangle.points[1].x - _triangle.points[0].x)*(_triangle.points[2].y - _triangle.points[0].y) - (_triangle.points[2].x - _triangle.points[0].x)*(_triangle.points[1].y - _triangle.points[0].y));
        case Profiles::Rectangle:
            return distance(_rectangle.points[0], _rectangle.points[1]) * distance(_rectangle.points[0], _rectangle.points[3]); //FIXME
        case Profiles::Arc:
            return _arc.opening * _arc.radius * _arc.radius;
        case Profiles::Complex:
        {
            //Simple addition of areas... does not take into account overlap
            double value = 0;
            for (Shape* s = next ; s != nullptr ; s = s->next)
            {
                value += s->area();
            }
            return value;
        }
        default:
            return 0;
    }
}

double Shape::span()
{
    switch (profile)
    {
        case Profiles::Circle:
            return 2 * _circle.radius;
        // TODO: complete for other profiles
        default:
            return 0;
    }
}

void Shape::updateDirection(double direction)
{
    if (profile == Profiles::Line)
        line(_line.points[0], _line.length, direction, _line.thickness);
    else if (profile == Profiles::Arc)
        _arc.direction = direction;
}

pair<Vector2D<double>, Vector2D<double>> Shape::boundingBox() const
{
    return box;
}

void Shape::removeLastPointOfLine()
{
    if (profile == Line && _line.thickness > 0)
        circle(_line.points[0], _line.thickness);

    else if (profile == Line && _line.thickness == 0)
        point(_line.points[0]);

    else if (profile == Polyline && _polyline.points.size() == 3)
        line(_polyline.points[0], _polyline.points[1], _polyline.thickness);

    else if (profile == Polyline && _polyline.points.size() > 3)
        _polyline.points.pop_back();
}

void Shape::loadFromXML(XMLElement* elem)
{
    string type = elem->Attribute("type");

    if (type == "none")
    {
        profile = Profiles::None;
    }

    else if (type == "point")
    {
        point(Vector2D<double>(elem->DoubleAttribute("x"), elem->DoubleAttribute("y")));
    }

    else if (type == "circle")
    {
        Vector2d p(0, 0);
        double radius = 0;
        elem->QueryAttribute("xCenter", &p.x);
        elem->QueryAttribute("yCenter", &p.y);
        elem->QueryAttribute("radius", &radius);

        circle(p, radius);
    }

    else if (type == "triangle")
    {
        Vector2d p1(0, 0);
        Vector2d p2(0, 0);
        Vector2d p3(0, 0);

        elem->QueryAttribute("x1", &p1.x);
        elem->QueryAttribute("y1", &p1.y);
        elem->QueryAttribute("x2", &p2.x);
        elem->QueryAttribute("y2", &p2.y);
        elem->QueryAttribute("x3", &p3.x);
        elem->QueryAttribute("y3", &p3.y);

        triangle(p1, p2, p3);
    }

    else if (type == "rectangle")
    {
        Vector2d p1(0, 0);
        Vector2d p2(0, 0);
        Vector2d p3(0, 0);
        double xCenter = 0, yCenter = 0, xSize = 0, ySize = 0, angle = 0;

        elem->QueryAttribute("xCenter", &xCenter);
        elem->QueryAttribute("yCenter", &yCenter);
        elem->QueryAttribute("xSize", &xSize);
        elem->QueryAttribute("ySize", &ySize);
        elem->QueryAttribute("angle", &angle);

        if (xSize == 0 || ySize == 0)
        {
            double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
            elem->QueryAttribute("x1", &x1);
            elem->QueryAttribute("y1", &y1);
            elem->QueryAttribute("x2", &x2);
            elem->QueryAttribute("y2", &y2);
            xCenter = (x1 + x2) / 2.0;
            yCenter = (y1 + y2) / 2.0;
            xSize = x2 - x1;
            ySize = y2 - y1;
        }

        p1.x = xCenter + ((-xSize/2.0) * cos(angle) - (-ySize/2.0) * sin(angle));
        p1.y = yCenter + ((-xSize/2.0) * sin(angle) + (-ySize/2.0) * cos(angle));
        p2.x = xCenter + (xSize/2.0 * cos(angle) - (-ySize/2.0) * sin(angle));
        p2.y = yCenter + (xSize/2.0 * sin(angle) + (-ySize/2.0) * cos(angle));
        p3.x = xCenter + ((-xSize/2.0) * cos(angle) - ySize/2.0 * sin(angle));
        p3.y = yCenter + ((-xSize/2.0) * sin(angle) + ySize/2.0 * cos(angle));

        rectangle(p1, p2, p3);
    }

    else if (type == "parallelogram")
    {
        Vector2d p1(0, 0);
        Vector2d p2(0, 0);
        Vector2d p3(0, 0);

        elem->QueryAttribute("x1", &p1.x);
        elem->QueryAttribute("y1", &p1.y);
        elem->QueryAttribute("x2", &p2.x);
        elem->QueryAttribute("y2", &p2.y);
        elem->QueryAttribute("x3", &p3.x);
        elem->QueryAttribute("y3", &p3.y);

        rectangle(p1, p2, p3);
    }

    else if (type == "line")
    {
        Vector2d p(0, 0);
        double length = 0, angle = 0, thickness = 0;
        elem->QueryAttribute("xOrigin", &p.x);
        elem->QueryAttribute("yOrigin", &p.y);
        elem->QueryAttribute("length", &length);
        elem->QueryAttribute("angle", &angle);
        elem->QueryAttribute("thickness", &thickness);

        line(p, length, angle, thickness);
    }

    else if (type == "polyline")
    {
        vector<Vector2d> p;
        int n = 0, thickness = 0;
        elem->QueryAttribute("points", &n);
        elem->QueryAttribute("thickness", &thickness);
        for (int i = 1 ; i <= n ; ++i)
        {
            Vector2d v(0, 0);

            elem->QueryAttribute(("x" + intToChars(i)).c_str(), &v.x);
            elem->QueryAttribute(("y" + intToChars(i)).c_str(), &v.y);
            p.emplace_back(std::move(v));
        }

        polyline(p, thickness);
    }

    else if (type == "arc")
    {
        Vector2d p(0, 0);
        double radius = 0, direction = 0, opening = 0;
        elem->QueryAttribute("xCenter", &p.x);
        elem->QueryAttribute("yCenter", &p.y);
        elem->QueryAttribute("radius", &radius);
        elem->QueryAttribute("direction", &direction);
        elem->QueryAttribute("opening", &opening);

        arc(p, radius, direction, opening);
    }

    else if (type == "complex")
    {
        profile = Profiles::Complex;

        Shape *current = this;
        XMLHandle hdl(elem);
        XMLElement *elem2 = hdl.FirstChildElement().ToElement();
        while (elem2)
        {
            current->next = new Shape;
            current->next->loadFromXML(elem2);
            current->next->setOrigin(_origin);
            current = current->next;

            elem2 = elem2->NextSiblingElement();
        }

        current = this->next;
        box = current->box;
        while (current != nullptr)
        {
            box.first.x = minimum(box.first.x, current->box.first.x);
            box.first.y = minimum(box.first.y, current->box.first.y);
            box.second.x = maximum(box.second.x, current->box.second.x);
            box.second.y = maximum(box.second.y, current->box.second.y);
            current = current->next;
        }
    }
}

void Shape::saveToXML(XMLDocument& doc, XMLHandle& handle) const
{
    XMLElement* root = handle.ToElement();

    switch(profile)
    {
        case Profiles::None:
            root->SetAttribute("type", "none");
            break;
        case Profiles::Point:
            root->SetAttribute("type", "point");
            root->SetAttribute("x", _point.point.x);
            root->SetAttribute("y", _point.point.y);
            break;
        case Profiles::Circle:
            root->SetAttribute("type", "circle");
            root->SetAttribute("xCenter", _circle.center.x);
            root->SetAttribute("yCenter", _circle.center.y);
            root->SetAttribute("radius", _circle.radius);
            break;
        case Profiles::Triangle:
            root->SetAttribute("type", "triangle");
            root->SetAttribute("x1", _triangle.points[0].x);
            root->SetAttribute("y1", _triangle.points[0].y);
            root->SetAttribute("x2", _triangle.points[1].x);
            root->SetAttribute("y2", _triangle.points[1].y);
            root->SetAttribute("x3", _triangle.points[2].x);
            root->SetAttribute("y3", _triangle.points[2].y);
            break;
        case Profiles::Rectangle:
            root->SetAttribute("type", "parallelogram");
            root->SetAttribute("x1", _rectangle.points[0].x);
            root->SetAttribute("y1", _rectangle.points[0].y);
            root->SetAttribute("x2", _rectangle.points[1].x);
            root->SetAttribute("y2", _rectangle.points[1].y);
            root->SetAttribute("x3", _rectangle.points[3].x);
            root->SetAttribute("y3", _rectangle.points[3].y);
            break;
        case Profiles::Line:
            root->SetAttribute("type", "line");
            root->SetAttribute("xOrigin", _line.points[0].x);
            root->SetAttribute("yOrigin", _line.points[0].y);
            root->SetAttribute("length", _line.length);
            root->SetAttribute("angle", _line.direction);
            root->SetAttribute("thickness", _line.thickness);
            break;
        case Profiles::Polyline:
            root->SetAttribute("type", "polyline");
            root->SetAttribute("thickness", _polyline.thickness);
            root->SetAttribute("points", (int)_polyline.points.size());
            for (unsigned i = 0 ; i < _polyline.points.size() ; ++i)
            {
                root->SetAttribute(("x" + intToChars(i+1)).c_str(), _polyline.points[i].x);
                root->SetAttribute(("y" + intToChars(i+1)).c_str(), _polyline.points[i].y);
            }
            break;
        case Profiles::Arc:
            root->SetAttribute("type", "arc");
            root->SetAttribute("xCenter", _arc.center.x);
            root->SetAttribute("yCenter", _arc.center.y);
            root->SetAttribute("radius", _arc.radius);
            root->SetAttribute("direction", _arc.direction);
            root->SetAttribute("opening", _arc.opening);
            break;
        case Profiles::Complex:
            root->SetAttribute("type", "complex");
            for (Shape* s = next ; s != nullptr ; s = s->next)
            {
                XMLElement* elem = doc.NewElement("shape");
                XMLHandle hdl(elem);
                s->saveToXML(doc, hdl);
                root->InsertEndChild(elem);
            }
            break;
        default:
            break;
    }
}

void Shape::display(RenderTarget& target, const Color& color)
{
    switch (profile)
    {
        case Profiles::Circle:
            {
                CircleShape drawing(_circle.radius);
                drawing.setPosition(_origin->x + _circle.center.x - _circle.radius, _origin->y + _circle.center.y - _circle.radius);
                drawing.setFillColor(color);
                target.draw(drawing);
            }
            break;
        case Profiles::Triangle:
            {
                ConvexShape drawing;
                drawing.setPointCount(3);
                drawing.setPoint(0, Vector2f(_origin->x + _triangle.points[0].x, _origin->y + _triangle.points[0].y));
                drawing.setPoint(1, Vector2f(_origin->x + _triangle.points[1].x, _origin->y + _triangle.points[1].y));
                drawing.setPoint(2, Vector2f(_origin->x + _triangle.points[2].x, _origin->y + _triangle.points[2].y));
                drawing.setFillColor(color);
                target.draw(drawing);
            }
            break;
        case Profiles::Rectangle:
            {
                ConvexShape drawing;
                drawing.setPointCount(4);
                drawing.setPoint(0, Vector2f(_origin->x + _rectangle.points[0].x, _origin->y + _rectangle.points[0].y));
                drawing.setPoint(1, Vector2f(_origin->x + _rectangle.points[1].x, _origin->y + _rectangle.points[1].y));
                drawing.setPoint(2, Vector2f(_origin->x + _rectangle.points[2].x, _origin->y + _rectangle.points[2].y));
                drawing.setPoint(3, Vector2f(_origin->x + _rectangle.points[3].x, _origin->y + _rectangle.points[3].y));
                drawing.setFillColor(color);
                target.draw(drawing);
            }
            break;
        case Profiles::Line:
            {
                CircleShape drawing0(_line.thickness);
                drawing0.setFillColor(color);
                drawing0.setPosition(_origin->x + _line.points[0].x - _line.thickness, _origin->y + _line.points[0].y - _line.thickness);
                target.draw(drawing0);
                drawing0.setPosition(_origin->x + _line.points[1].x - _line.thickness, _origin->y + _line.points[1].y - _line.thickness);
                target.draw(drawing0);

                ConvexShape drawing;
                drawing.setPointCount(4);
                Vector2d A(_origin->x + _line.points[0].x, _origin->y + _line.points[0].y);
                Vector2d B(_origin->x + _line.points[1].x, _origin->y + _line.points[1].y);
                Vector2d perp(-(B-A).y, (B-A).x);
                perp = perp / perp.length();
                drawing.setPoint(0, Vector2f((A + _line.thickness * perp).x, (A + _line.thickness * perp).y));
                drawing.setPoint(1, Vector2f((B + _line.thickness * perp).x, (B + _line.thickness * perp).y));
                drawing.setPoint(2, Vector2f((B - _line.thickness * perp).x, (B - _line.thickness * perp).y));
                drawing.setPoint(3, Vector2f((A - _line.thickness * perp).x, (A - _line.thickness * perp).y));
                drawing.setFillColor(color);
                target.draw(drawing);
            }
            break;
        case Profiles::Polyline:
            {
                CircleShape drawing0(_polyline.thickness);
                drawing0.setFillColor(color);
                drawing0.setPosition(_origin->x + _polyline.points[0].x - _polyline.thickness, _origin->y + _polyline.points[0].y - _polyline.thickness);
                target.draw(drawing0);

                for (unsigned i = 1 ; i < _polyline.points.size() ; ++i)
                {
                    ConvexShape drawing;
                    drawing.setPointCount(4);
                    Vector2d A(_origin->x + _polyline.points[i-1].x, _origin->y + _polyline.points[i-1].y);
                    Vector2d B(_origin->x + _polyline.points[i].x, _origin->y + _polyline.points[i].y);
                    Vector2d perp(-(B-A).y, (B-A).x);
                    perp = perp / perp.length();
                    drawing.setPoint(0, Vector2f((A + _polyline.thickness * perp).x, (A + _polyline.thickness * perp).y));
                    drawing.setPoint(1, Vector2f((B + _polyline.thickness * perp).x, (B + _polyline.thickness * perp).y));
                    drawing.setPoint(2, Vector2f((B - _polyline.thickness * perp).x, (B - _polyline.thickness * perp).y));
                    drawing.setPoint(3, Vector2f((A - _polyline.thickness * perp).x, (A - _polyline.thickness * perp).y));
                    drawing.setFillColor(color);
                    target.draw(drawing);

                    drawing0.setPosition(_origin->x + _polyline.points[i].x - _polyline.thickness, _origin->y + _polyline.points[i].y - _polyline.thickness);
                    target.draw(drawing0);
                }
            }
            break;
        case Profiles::Arc:
            {
                ConvexShape drawing;
                drawing.setPointCount(6);
                drawing.setPoint(0, Vector2f(_origin->x + _arc.center.x, _origin->y + _arc.center.y));
                drawing.setPoint(1, Vector2f(_origin->x + _arc.center.x + _arc.radius * cos(_arc.direction - _arc.opening),       _origin->y + _arc.center.y + _arc.radius * sin(_arc.direction - _arc.opening)));
                drawing.setPoint(2, Vector2f(_origin->x + _arc.center.x + _arc.radius * cos(_arc.direction - 0.5 * _arc.opening), _origin->y + _arc.center.y + _arc.radius * sin(_arc.direction - 0.5 * _arc.opening)));
                drawing.setPoint(3, Vector2f(_origin->x + _arc.center.x + _arc.radius * cos(_arc.direction),                      _origin->y + _arc.center.y + _arc.radius * sin(_arc.direction)));
                drawing.setPoint(4, Vector2f(_origin->x + _arc.center.x + _arc.radius * cos(_arc.direction + 0.5 * _arc.opening), _origin->y + _arc.center.y + _arc.radius * sin(_arc.direction + 0.5 * _arc.opening)));
                drawing.setPoint(5, Vector2f(_origin->x + _arc.center.x + _arc.radius * cos(_arc.direction + _arc.opening),       _origin->y + _arc.center.y + _arc.radius * sin(_arc.direction + _arc.opening)));
                drawing.setFillColor(color);
                target.draw(drawing);
            }
            break;
        case Profiles::Complex:
        {
            Shape* current = next;
            while (current != nullptr)
            {
                current->display(target, color);
                current = current->next;
            }
            break;
        }
        default:
            break;
    }
}

} //namespace math

} //namespace tools
