
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <random>
#include <chrono>

#include "tools/debug.h"
#include "tools/math.h"
#include "tools/shape.h"

namespace tools{

namespace math{

default_random_engine randomGenerator;

void initLibrary()
{
    chrono::high_resolution_clock::time_point t = chrono::high_resolution_clock::now();
    chrono::high_resolution_clock::duration   d = t.time_since_epoch();
    randomGenerator.seed(d.count());
}

double angle(double x, double y)
{
    if (x > 0 && y >= 0)
        return atan(y / x);
    if (x > 0 && y < 0)
        return atan(y / x) + 2 * M_PI;
    if (x < 0)
        return atan(y / x) + M_PI;
    if (x == 0 && y > 0)
        return M_PI / 2;
    if (x == 0 && y < 0)
        return 3 * M_PI / 2;

    return 0;
}

// binomial_distribution

double randomNumber(double min, double max)
{
    double n = randomGenerator() / 1000.0;
    n -= floor(n);

    return min + (max - min) * n;
}

int randomNumber(int min, int max)
{
    return round(randomNumber((double)min, (double)max));
}

double randomNumber_BinomialLaw(double min, double max)
{
    binomial_distribution<int> distribution(1000*(max - min), 0.5);

    return min + distribution(randomGenerator) / 1000.0;
}

double square(double a)
{
    return a*a;
}

string intToChars(int value, int stringLength, bool sign, bool digitSeparator)
{
    if (value == 0) return string(max(1, stringLength), '0');

    int length = 0;
    for (int l = abs(value) ; l != 0 ; l /= 10)
        ++length;

    if (digitSeparator) length += (length-1)/3;

    if (value < 0 || sign) ++length;
    string res(max(length, stringLength), '0');
    int pos = res.size() - 1;

    if (value < 0)
    {
        res[0] = '-';
        value = -value;
    }
    else if (sign) res[0] = '+';

    static constexpr char digits[] = "0123456789";

    int count = 0;
    while (value != 0)
    {
        if (digitSeparator && count == 3)
        {
            res[pos] = ' ';
            --pos;
            count = 0;
        }
        int r = value % 10;
        res[pos] = digits[r];
        value /= 10;
        --pos;
        ++count;
    }

    return res;
}

bool intersection_circle_line(const Shape& shape1, const Shape& shape2);
bool intersection_line_line(const Shape& shape1, const Shape& shape2);

bool intersection_point_point(const Shape& shape1, const Shape& shape2)
{
    if (*shape1._origin + shape1._point.point == *shape2._origin + shape2._point.point) return true;

    return false;
}

bool intersection_point_circle(const Shape& shape1, const Shape& shape2)
{
    return (distanceSquare(*shape1._origin + shape1._point.point, *shape2._origin + shape2._circle.center) <= square(shape2._circle.radius));
}

bool intersection_point_triangle(const Shape& shape1, const Shape& shape2)
{
    Vector2d vec_Nab(shape2._triangle.points[1].y - shape2._triangle.points[0].y, shape2._triangle.points[0].x - shape2._triangle.points[1].x);
    Vector2d vec_Nbc(shape2._triangle.points[2].y - shape2._triangle.points[1].y, shape2._triangle.points[1].x - shape2._triangle.points[2].x);
    Vector2d vec_Nca(shape2._triangle.points[0].y - shape2._triangle.points[2].y, shape2._triangle.points[2].x - shape2._triangle.points[0].x);

    Vector2d vec_AH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._triangle.points[0]);
    Vector2d vec_BH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._triangle.points[1]);
    Vector2d vec_CH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._triangle.points[2]);

    double h1 = dotProduct(vec_AH, vec_Nab);
    double h2 = dotProduct(vec_BH, vec_Nbc);
    double h3 = dotProduct(vec_CH, vec_Nca);

    if (h1 >= 0 && h2 >= 0 && h3 >= 0) return true;
    if (h1 <= 0 && h2 <= 0 && h3 <= 0) return true;

    return false;
}

bool intersection_point_rectangle(const Shape& shape1, const Shape& shape2)
{
    Vector2d vec_AB = shape2._rectangle.points[1] - shape2._rectangle.points[0];
    Vector2d vec_AC = shape2._rectangle.points[3] - shape2._rectangle.points[0];
    Vector2d vec_AH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._rectangle.points[0]);

    // Covariant coordinates
    double x1 = dotProduct(vec_AH, vec_AB) / vec_AB.length();
    double x2 = dotProduct(vec_AH, vec_AC) / vec_AC.length();

    if (abs(dotProduct(vec_AB, vec_AC)) < 5)  // Almost rectangle, not parallelogram
    {
        if (x1 >= 0 && x1 <= vec_AB.length() && x2 >= 0 && x2 <= vec_AC.length())
            return true;
        else return false;
    }

    // Contravariant coordinates
    Vector2d vec_e1 = vec_AB / vec_AB.length();
    Vector2d vec_e2 = vec_AC / vec_AC.length();
    double det = dotProduct(vec_e1, vec_e1) * dotProduct(vec_e2, vec_e2) - dotProduct(vec_e2, vec_e1) * dotProduct(vec_e1, vec_e2);
    double y1 = (x1 * dotProduct(vec_e2, vec_e2) - x2 * dotProduct(vec_e2, vec_e1)) / det;
    double y2 = (- x1 * dotProduct(vec_e1, vec_e2) + x2 * dotProduct(vec_e1, vec_e1)) / det;

    if (y1 >= 0 && y1 <= vec_AB.length() && y2 >= 0 && y2 <= vec_AC.length())
        return true;

    return false;
}

bool intersection_point_line(const Shape& shape1, const Shape& shape2)
{
    Vector2d vec_AB = shape2._line.points[1] - shape2._line.points[0];
    Vector2d vec_AC(-vec_AB.y, vec_AB.x);
    Vector2d vec_AH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._line.points[0]);

    double h1 = dotProduct(vec_AH, vec_AB) / vec_AB.length();
    double h2 = dotProduct(vec_AH, vec_AC) / vec_AC.length();

    if (h1 >= 0 && h1 <= vec_AB.length() && abs(h2) <= shape2._line.thickness)
        return true;

    if (distance(*shape1._origin + shape1._point.point, *shape2._origin + shape2._line.points[0]) <= shape2._line.thickness)
        return true;

    if (distance(*shape1._origin + shape1._point.point, *shape2._origin + shape2._line.points[1]) <= shape2._line.thickness)
        return true;

    return false;
}

bool intersection_point_polyline(const Shape& shape1, const Shape& shape2)
{
    for (unsigned i = 1 ; i < shape2._polyline.points.size() ; ++i)
    {
        Vector2d vec_AB = shape2._polyline.points[i] - shape2._polyline.points[i-1];
        Vector2d vec_AC(-vec_AB.y, vec_AB.x);
        Vector2d vec_AH = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._polyline.points[i-1]);

        double h1 = dotProduct(vec_AH, vec_AB) / vec_AB.length();
        double h2 = dotProduct(vec_AH, vec_AC) / vec_AC.length();

        if (h1 >= 0 && h1 <= vec_AB.length() && abs(h2) <= shape2._polyline.thickness)
            return true;

        if (distance(*shape1._origin + shape1._point.point, *shape2._origin + shape2._polyline.points[i-1]) <= shape2._polyline.thickness)
            return true;

        if (distance(*shape1._origin + shape1._point.point, *shape2._origin + shape2._polyline.points[i]) <= shape2._polyline.thickness)
            return true;
    }

    return false;
}

bool intersection_point_arc(const Shape& shape1, const Shape& shape2)
{
    if (distanceSquare(*shape1._origin + shape1._point.point, *shape2._origin + shape2._arc.center) > square(shape2._arc.radius))
        return false;

    Vector2d vec_AB = *shape1._origin + shape1._point.point - (*shape2._origin + shape2._arc.center);
    if (angle(vec_AB.x, vec_AB.y) >= shape2._arc.direction - shape2._arc.opening && angle(vec_AB.x, vec_AB.y) <= shape2._arc.direction + shape2._arc.opening)
        return true;

    return false;
}

bool intersection_circle_circle(const Shape& shape1, const Shape& shape2)
{
    return (distanceSquare(*shape1._origin + shape1._circle.center, *shape2._origin + shape2._circle.center) <= square(shape1._circle.radius + shape2._circle.radius));
}

bool intersection_circle_triangle(const Shape& shape1, const Shape& shape2)
{
    Vector2d vec_Nab(shape2._triangle.points[1].y - shape2._triangle.points[0].y, shape2._triangle.points[0].x - shape2._triangle.points[1].x);
    Vector2d vec_Nbc(shape2._triangle.points[2].y - shape2._triangle.points[1].y, shape2._triangle.points[1].x - shape2._triangle.points[2].x);
    Vector2d vec_Nca(shape2._triangle.points[0].y - shape2._triangle.points[2].y, shape2._triangle.points[2].x - shape2._triangle.points[0].x);

    Vector2d vec_AH = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._triangle.points[0]);
    Vector2d vec_BH = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._triangle.points[1]);
    Vector2d vec_CH = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._triangle.points[2]);

    double h1 = dotProduct(vec_AH, vec_Nab) / vec_Nab.length();
    double h2 = dotProduct(vec_BH, vec_Nbc) / vec_Nbc.length();
    double h3 = dotProduct(vec_CH, vec_Nca) / vec_Nca.length();

    if (h1 >= -shape1._circle.radius && h2 >= -shape1._circle.radius && h3 >= -shape1._circle.radius) return true;
    if (h1 <= shape1._circle.radius && h2 <= shape1._circle.radius && h3 <= shape1._circle.radius) return true;

    return false;
}

bool intersection_circle_rectangle(const Shape& shape1, const Shape& shape2)
{
    Shape s;
    s.setOrigin(shape1._origin);
    s.point(shape1._circle.center);
    if (intersection_point_rectangle(s, shape2)) return true;

    s.setOrigin(shape2._origin);

    for (unsigned int i = 0 ; i < 4 ; ++i)
    {
        s.point(shape2._rectangle.points[i]);
        if (intersection_point_circle(s, shape1))
            return true;
    }

    for (unsigned int i = 0 ; i < 4 ; ++i)
    {
        s.line(shape2._rectangle.points[(i+1)%4], shape2._rectangle.points[i], 0);
        if (intersection_circle_line(shape1, s))
            return true;
    }

    return false;
}

bool intersection_circle_line(const Shape& shape1, const Shape& shape2)
{
    Vector2d vec_AB = shape2._line.points[1] - shape2._line.points[0];
    Vector2d vec_AC(-vec_AB.y, vec_AB.x);
    Vector2d vec_AH = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._line.points[0]);

    double h1 = dotProduct(vec_AH, vec_AB) / vec_AB.length();
    double h2 = dotProduct(vec_AH, vec_AC) / vec_AC.length();

    if (h1 >= 0 && h1 <= vec_AB.length() && abs(h2) <= (shape1._circle.radius + shape2._line.thickness))
        return true;

    if (distance(*shape1._origin + shape1._circle.center, *shape2._origin + shape2._line.points[0]) <= shape1._circle.radius + shape2._line.thickness)
        return true;

    if (distance(*shape1._origin + shape1._circle.center, *shape2._origin + shape2._line.points[1]) <= shape1._circle.radius + shape2._line.thickness)
        return true;

    return false;
}

bool intersection_circle_polyline(const Shape& shape1, const Shape& shape2)
{
    for (unsigned i = 1 ; i < shape2._polyline.points.size() ; ++i)
    {
        Vector2d vec_AB = shape2._polyline.points[i] - shape2._polyline.points[i-1];
        Vector2d vec_AC(-vec_AB.y, vec_AB.x);
        Vector2d vec_AH = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._polyline.points[i-1]);

        double h1 = dotProduct(vec_AH, vec_AB) / vec_AB.length();
        double h2 = dotProduct(vec_AH, vec_AC) / vec_AC.length();

        if (h1 >= 0 && h1 <= vec_AB.length() && abs(h2) <= (shape1._circle.radius + shape2._polyline.thickness))
            return true;

        if (distance(*shape1._origin + shape1._circle.center, *shape2._origin + shape2._polyline.points[i-1]) <= shape1._circle.radius + shape2._polyline.thickness)
            return true;

        if (distance(*shape1._origin + shape1._circle.center, *shape2._origin + shape2._polyline.points[i]) <= shape1._circle.radius + shape2._polyline.thickness)
            return true;
    }

    return false;
}

bool intersection_circle_arc(const Shape& shape1, const Shape& shape2)
{
    Shape s;
    s.setOrigin(shape2._origin);
    s.circle(shape2._arc.center, shape2._arc.radius);
    if (!intersection_circle_circle(shape1, s))
        return false;

    Vector2d vec_AB = *shape1._origin + shape1._circle.center - (*shape2._origin + shape2._arc.center);
    if (angle(vec_AB.x, vec_AB.y) >= shape2._arc.direction - shape2._arc.opening && angle(vec_AB.x, vec_AB.y) <= shape2._arc.direction + shape2._arc.opening)
        return true;

    Shape line1;
    line1.line(shape2._arc.center, shape2._arc.radius, shape2._arc.direction - shape2._arc.opening, 1);
    line1.setOrigin(shape2._origin);
    if (intersection_circle_line(shape1, line1))
        return true;

    Shape line2;
    line2.line(shape2._arc.center, shape2._arc.radius, shape2._arc.direction + shape2._arc.opening, 1);
    line2.setOrigin(shape2._origin);
    if (intersection_circle_line(shape1, line2))
        return true;

    return false;
}

bool intersection_triangle_triangle(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_triangle_triangle() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_triangle_rectangle(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_triangle_rectangle() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_triangle_line(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_triangle_line() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_triangle_polyline(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_triangle_polyline() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_triangle_arc(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_triangle_arc() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_rectangle_rectangle(const Shape& shape1, const Shape& shape2)
{
    for (int i = 0 ; i < 4 ; ++i)
    {
        Shape p;
        p.setOrigin(shape1._origin);
        p.point(shape1._rectangle.points[i]);
        if (intersection_point_rectangle(p, shape2))
            return true;

        p.setOrigin(shape2._origin);
        p.point(shape2._rectangle.points[i]);
        if (intersection_point_rectangle(p, shape1))
            return true;
    }

    for (int i = 0 ; i < 4 ; ++i)
    {
        Shape l1;
        l1.setOrigin(shape1._origin);
        l1.line(shape1._rectangle.points[i], shape1._rectangle.points[(i+1)%4], 0);

        for (int j = 0 ; j < 4 ; ++j)
        {
            Shape l2;
            l2.setOrigin(shape2._origin);
            l2.line(shape2._rectangle.points[j], shape2._rectangle.points[(j+1)%4], 0);

            if (intersection_line_line(l1, l2))
                return true;
        }
    }

    return false;
}

bool intersection_rectangle_line(const Shape& shape1, const Shape& shape2)
{
    Shape p;
    p.setOrigin(shape2._origin);
    for (unsigned i = 0 ; i < 2 ; ++i)
    {
        p.point(shape2._line.points[i]);
        if (intersection_point_rectangle(p, shape1))
            return true;
    }

    for (unsigned i = 0 ; i < 4 ; ++i)
    {
        Shape l1;
        l1.setOrigin(shape1._origin);
        l1.line(shape1._rectangle.points[i], shape1._rectangle.points[(i+1)%4], 0);

        if (intersection_line_line(l1, shape2))
            return true;
    }

    return false;
}

bool intersection_rectangle_polyline(const Shape& shape1, const Shape& shape2)
{
    Shape p;
    p.setOrigin(shape2._origin);
    for (unsigned i = 0 ; i < shape2._polyline.points.size() ; ++i)
    {
        p.point(shape2._polyline.points[i]);
        if (intersection_point_rectangle(p, shape1))
            return true;
    }

    for (unsigned i = 0 ; i < 4 ; ++i)
    {
        Shape l1;
        l1.setOrigin(shape1._origin);
        l1.line(shape1._rectangle.points[i], shape1._rectangle.points[(i+1)%4], 0);

        if (intersection_line_line(l1, shape2))
            return true;
    }

    return false;
}

bool intersection_rectangle_arc(const Shape& shape1, const Shape& shape2)
{
    Shape p;
    p.setOrigin(shape1._origin);
    for (int i = 0 ; i < 4 ; ++i)
    {
        p.point(shape1._rectangle.points[i]);
        if (intersection_point_arc(p, shape2)) return true;
    }

    p.setOrigin(shape2._origin);

    p.point(shape2._arc.center);
    if (intersection_point_rectangle(p, shape1)) return true;

    p.point(shape2._arc.center + Vector2d(shape2._arc.radius + cos(shape2._arc.direction - shape2._arc.opening), shape2._arc.radius + sin(shape2._arc.direction - shape2._arc.opening)));
    if (intersection_point_rectangle(p, shape1)) return true;

    p.point(shape2._arc.center + Vector2d(shape2._arc.radius + cos(shape2._arc.direction), shape2._arc.radius + sin(shape2._arc.direction)));
    if (intersection_point_rectangle(p, shape1)) return true;

    p.point(shape2._arc.center + Vector2d(shape2._arc.radius + cos(shape2._arc.direction + shape2._arc.opening), shape2._arc.radius + sin(shape2._arc.direction + shape2._arc.opening)));
    if (intersection_point_rectangle(p, shape1)) return true;

    return false;
}

bool intersection_line_line(const Shape& shape1, const Shape& shape2)
{
    if (shape1._line.thickness > 0 || shape2._line.thickness > 0)
    {
        for (unsigned i = 0 ; i < 2 ; ++i)
        {
            Shape s1b;
            s1b.setOrigin(shape1._origin);
            if (shape1._line.thickness > 0)
            {
                s1b.circle(shape1._line.points[i], shape1._line.thickness);
                if (intersection_circle_line(s1b, shape2)) return true;
            }
            else
            {
                s1b.point(shape1._line.points[i]);
                if (intersection_point_line(s1b, shape2)) return true;
            }
        }

        for (unsigned i = 0 ; i < 2 ; ++i)
        {
            Shape s2b;
            s2b.setOrigin(shape2._origin);
            if (shape2._line.thickness > 0)
            {
                s2b.circle(shape2._line.points[i], shape2._line.thickness);
                if (intersection_circle_line(s2b, shape1)) return true;
            }
            else
            {
                s2b.point(shape2._line.points[i]);
                if (intersection_point_line(s2b, shape1)) return true;
            }
        }
    }

    Vector2d l1 = shape1._line.points[1] - shape1._line.points[0];
    Vector2d l2 = shape2._line.points[1] - shape2._line.points[0];
    Vector2d c = *shape2._origin + shape2._line.points[0] - (*shape1._origin + shape1._line.points[0]);

    double cross = l1.x * l2.y - l1.y * l2.x;

    if (cross == 0) return false;  // Parallel or collinear, don't bother with that unlikely case

    double t = (c.x * l2.y - c.y * l2.x) / cross;
    double u = (c.x * l1.y - c.y * l1.x) / cross;

    if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
        return true;

    return false;
}

bool intersection_line_polyline(const Shape& shape1, const Shape& shape2)
{
    if (shape1._line.thickness > 0 || shape2._polyline.thickness > 0)
    {
        for (unsigned i = 0 ; i < 2 ; ++i)
        {
            Shape s1b;
            s1b.setOrigin(shape1._origin);
            if (shape1._line.thickness > 0)
            {
                s1b.circle(shape1._line.points[i], shape1._line.thickness);
                if (intersection_circle_polyline(s1b, shape2)) return true;
            }
            else
            {
                s1b.point(shape1._line.points[i]);
                if (intersection_point_polyline(s1b, shape2)) return true;
            }
        }

        for (unsigned i = 0 ; i < shape2._polyline.points.size() ; ++i)
        {
            Shape s2b;
            s2b.setOrigin(shape2._origin);
            if (shape2._polyline.thickness > 0)
            {
                s2b.circle(shape2._polyline.points[i], shape2._polyline.thickness);
                if (intersection_circle_line(s2b, shape1)) return true;
            }
            else
            {
                s2b.point(shape2._polyline.points[i]);
                if (intersection_point_line(s2b, shape1)) return true;
            }
        }
    }

    Vector2d l1 = shape1._polyline.points[1] - shape1._polyline.points[0];

    for (unsigned j = 0 ; j < shape2._polyline.points.size() - 1 ; ++j)
    {
        Vector2d l2 = shape2._polyline.points[j+1] - shape2._polyline.points[j];
        Vector2d c = *shape2._origin + shape2._polyline.points[j] - (*shape1._origin + shape1._polyline.points[0]);

        double cross = l1.x * l2.y - l1.y * l2.x;

        if (cross == 0) continue;  // Parallel or collinear, don't bother with that unlikely case

        double t = (c.x * l2.y - c.y * l2.x) / cross;
        double u = (c.x * l1.y - c.y * l1.x) / cross;

        if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
            return true;
    }

    return false;
}

bool intersection_line_arc(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_line_arc() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_polyline_polyline(const Shape& shape1, const Shape& shape2)
{
    if (shape1._polyline.thickness > 0 || shape2._polyline.thickness > 0)
    {
        for (unsigned i = 0 ; i < shape1._polyline.points.size() ; ++i)
        {
            Shape s1b;
            s1b.setOrigin(shape1._origin);
            if (shape1._polyline.thickness > 0)
            {
                s1b.circle(shape1._polyline.points[i], shape1._polyline.thickness);
                if (intersection_circle_polyline(s1b, shape2)) return true;
            }
            else
            {
                s1b.point(shape1._polyline.points[i]);
                if (intersection_point_polyline(s1b, shape2)) return true;
            }
        }

        for (unsigned i = 0 ; i < shape2._polyline.points.size() ; ++i)
        {
            Shape s2b;
            s2b.setOrigin(shape2._origin);
            if (shape2._polyline.thickness > 0)
            {
                s2b.circle(shape2._polyline.points[i], shape2._polyline.thickness);
                if (intersection_circle_polyline(s2b, shape1)) return true;
            }
            else
            {
                s2b.point(shape2._polyline.points[i]);
                if (intersection_point_polyline(s2b, shape1)) return true;
            }
        }
    }

    for (unsigned i = 0 ; i < shape1._polyline.points.size() - 1 ; ++i)
    {
        for (unsigned j = 0 ; j < shape2._polyline.points.size() - 1 ; ++j)
        {
            Vector2d l1 = shape1._polyline.points[i+1] - shape1._polyline.points[i];
            Vector2d l2 = shape2._polyline.points[j+1] - shape2._polyline.points[j];
            Vector2d c = *shape2._origin + shape2._polyline.points[j] - (*shape1._origin + shape1._polyline.points[i]);

            double cross = l1.x * l2.y - l1.y * l2.x;

            if (cross == 0) continue;  // Parallel or collinear, don't bother with that unlikely case

            double t = (c.x * l2.y - c.y * l2.x) / cross;
            double u = (c.x * l1.y - c.y * l1.x) / cross;

            if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
                return true;
        }
    }

    return false;
}

bool intersection_polyline_arc(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_polyline_arc() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection_arc_arc(const Shape& shape1, const Shape& shape2)
{
    tools::debug::warning("intersection_arc_arc() is not implemented", "tools::math", __FILENAME__, __LINE__);
    return false;
}

bool intersection(const Shape& shape1, const Shape& shape2)
{
    //Check if there is no 'none' profile
    if (shape1.profile == Shape::Profiles::None || shape2.profile == Shape::Profiles::None)
        return false;

    //If the boxes have no intersection, shapes will not have one either
    if (shape1._origin->x + shape1.box.first.x > shape2._origin->x + shape2.box.second.x) return false;
    if (shape1._origin->y + shape1.box.first.y > shape2._origin->y + shape2.box.second.y) return false;
    if (shape1._origin->x + shape1.box.second.x < shape2._origin->x + shape2.box.first.x) return false;
    if (shape1._origin->y + shape1.box.second.y < shape2._origin->y + shape2.box.first.y) return false;

    //if shape1 is a complex shape
    if (shape1.profile == Shape::Profiles::Complex)
    {
        Shape* current = shape1.next;
        while (current != nullptr)
        {
            if (intersection(*current, shape2))
                return true;
            current = current->next;
        }
        return false;
    }

    //if shape2 is a complex shape
    if (shape2.profile == Shape::Profiles::Complex)
    {
        Shape* current = shape2.next;
        while (current != nullptr)
        {
            if (intersection(*current, shape1))
                return true;
            current = current->next;
        }
        return false;
    }

    //Find the good function to test intersection
    switch (shape1.profile)
    {
        case Shape::Profiles::Point:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_point(shape1, shape2);
                case Shape::Profiles::Circle:
                    return intersection_point_circle(shape1, shape2);
                case Shape::Profiles::Triangle:
                    return intersection_point_triangle(shape1, shape2);
                case Shape::Profiles::Rectangle:
                    return intersection_point_rectangle(shape1, shape2);
                case Shape::Profiles::Line:
                    return intersection_point_line(shape1, shape2);
                case Shape::Profiles::Polyline:
                    return intersection_point_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_point_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Circle:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_circle(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_circle(shape1, shape2);
                case Shape::Profiles::Triangle:
                    return intersection_circle_triangle(shape1, shape2);
                case Shape::Profiles::Rectangle:
                    return intersection_circle_rectangle(shape1, shape2);
                case Shape::Profiles::Line:
                    return intersection_circle_line(shape1, shape2);
                case Shape::Profiles::Polyline:
                    return intersection_circle_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_circle_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Triangle:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_triangle(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_triangle(shape2, shape1);
                case Shape::Profiles::Triangle:
                    return intersection_triangle_triangle(shape1, shape2);
                case Shape::Profiles::Rectangle:
                    return intersection_triangle_rectangle(shape1, shape2);
                case Shape::Profiles::Line:
                    return intersection_triangle_line(shape1, shape2);
                case Shape::Profiles::Polyline:
                    return intersection_triangle_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_triangle_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Rectangle:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_rectangle(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_rectangle(shape2, shape1);
                case Shape::Profiles::Triangle:
                    return intersection_triangle_rectangle(shape2, shape1);
                case Shape::Profiles::Rectangle:
                    return intersection_rectangle_rectangle(shape1, shape2);
                case Shape::Profiles::Line:
                    return intersection_rectangle_line(shape1, shape2);
                case Shape::Profiles::Polyline:
                    return intersection_rectangle_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_rectangle_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Line:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_line(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_line(shape2, shape1);
                case Shape::Profiles::Triangle:
                    return intersection_triangle_line(shape2, shape1);
                case Shape::Profiles::Rectangle:
                    return intersection_rectangle_line(shape2, shape1);
                case Shape::Profiles::Line:
                    return intersection_line_line(shape1, shape2);
                case Shape::Profiles::Polyline:
                    return intersection_line_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_line_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Polyline:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_polyline(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_polyline(shape2, shape1);
                case Shape::Profiles::Triangle:
                    return intersection_triangle_polyline(shape2, shape1);
                case Shape::Profiles::Rectangle:
                    return intersection_rectangle_polyline(shape2, shape1);
                case Shape::Profiles::Line:
                    return intersection_line_polyline(shape2, shape1);
                case Shape::Profiles::Polyline:
                    return intersection_polyline_polyline(shape1, shape2);
                case Shape::Profiles::Arc:
                    return intersection_polyline_arc(shape1, shape2);
                default: return false;
            }
            break;
        case Shape::Profiles::Arc:
            switch(shape2.profile)
            {
                case Shape::Profiles::Point:
                    return intersection_point_arc(shape2, shape1);
                case Shape::Profiles::Circle:
                    return intersection_circle_arc(shape2, shape1);
                case Shape::Profiles::Triangle:
                    return intersection_triangle_arc(shape2, shape1);
                case Shape::Profiles::Rectangle:
                    return intersection_rectangle_arc(shape2, shape1);
                case Shape::Profiles::Line:
                    return intersection_line_arc(shape2, shape1);
                case Shape::Profiles::Polyline:
                    return intersection_polyline_arc(shape2, shape1);
                case Shape::Profiles::Arc:
                    return intersection_arc_arc(shape1, shape2);
                default: return false;
            }
        default: return false;
    }

    return false;
}

} //namespace math

} //namespace tools
