
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tools/debug.h"

#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>


namespace debug{

static const auto    beginTime {std::chrono::system_clock::now()};
static std::ofstream logfile   {};
static bool          newentry  {true};


void openDebugFile(const char* appName, const char* version, const char* debugFile)
{
    logfile.open(debugFile, std::ios_base::out);
    logfile.precision(4);
    logfile.setf(std::ios::fixed, std::ios::floatfield);

    auto now = std::time(nullptr);

    logfile << "=================================================\n";
    logfile << "=== " << appName << " logfile\n";
    logfile << "=== version : " << version << "\n";
    logfile << "=== debug file opened at " << std::put_time(std::localtime(&now), "%F %T") << "\n";
    logfile << "=================================================" << std::endl;
}

template<typename T>
void log(T value)
{
    if (newentry)
    {
        std::chrono::duration<double> duration = std::chrono::system_clock::now() - beginTime;
        logfile << "[" << duration.count() << "] ";
    }
    logfile << value;
    newentry = false;
}

Warn& Warn::operator<<(const char* value)
{
    log(value);
    return *this;
}

Warn& Warn::operator<<(const std::string& value)
{
    log(value);
    return *this;
}

Warn& Warn::operator<<(int value)
{
    log(value);
    return *this;
}

Warn& Warn::operator<<(End)
{
    logfile << std::endl;
    newentry = true;

    return *this;
}

}  // namespace debug



namespace tools{

namespace debug{

void openDebugFile(const string& appName, const string& version)
{
}

void addDebugCategory(string category)
{
}

void writeToDebugFile(const string& s, const string& file, int line)
{
    ::debug::warn << s << ::debug::end;
}

void fatal(const string& str, const string& category, const string& file, int line)
{
    error(str, category, file, line);
}

void error(const string& str, const string& category, const string& file, int line)
{
    writeToDebugFile("(EE) " + str, file, line);
}

void warning(const string& str, const string& category, const string& file, int line)
{
    writeToDebugFile("(WW) " + str, file, line);
}

void message(const string& str, const string& category, const string& file, int line)
{
    writeToDebugFile("(II) " + str, file, line);
}

} //namespace debug

} //namespace tools
