
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <unordered_map>

#include "tools/math.h"
#include "tools/aStar.h"

using namespace std;

namespace tools{

namespace aStar{

struct Node
{
    double costFromStart, costToEnd, costSum;
    math::Vector2D<int> parent;

    Node()
      : costFromStart(0),
        costToEnd(0),
        costSum(0),
        parent()
    {
    }
};

unordered_map < math::Vector2D<int>, Node> _openSet;
unordered_map < math::Vector2D<int>, Node> _closedSet;

math::Shape _finalPath;

math::Vector2D<int> _start, _end;

math::Shape _field;
vector<pair<const math::Shape*, int>> _obstacles;
double _nodeRadius;
unsigned _childrenNumber;
double _distance;
unsigned _maximumNodes;

void setPoints(const math::Vector2d& start, const math::Vector2d& end)
{
    _start.x = round(start.x);
    _start.y = round(start.y);
    _end.x = round(end.x);
    _end.y = round(end.y);
}

void setField(math::Shape& field)
{
    _field = field;
    _field.setOrigin(field.origin());
}

void setNodesProperties(double span, unsigned childrenNumber, double distance, unsigned maxNodes)
{
    _nodeRadius = span/2.0;
    _childrenNumber = childrenNumber;
    _distance = distance;
    _maximumNodes = maxNodes;
}

void setObstacles(vector<pair<const math::Shape*,int>>& obstacles)
{
    _obstacles = obstacles;
}

void toClosedSet(math::Vector2D<int>& p)
{
    Node& n = _openSet[p];
    _closedSet.emplace(p, std::move(n));
    _openSet.erase(p);
}

void addAdjacentNodes(math::Vector2D<int>& n)
{
    if (_closedSet.size() + _openSet.size() > _maximumNodes) return;

    for (unsigned i = 0 ; i < _childrenNumber ; ++i)
    {
        math::Vector2D<int> newNode;

        newNode.x = n.x + _distance * cos(i * (2 * M_PI) / _childrenNumber);
        newNode.y = n.y + _distance * sin(i * (2 * M_PI) / _childrenNumber);

        math::Shape newNodeShape;
        newNodeShape.point(math::Vector2D<double> {newNode.x, newNode.y});

        if (!math::intersection(_field, newNodeShape))
            continue;

        newNodeShape.line(n, newNode, _nodeRadius);

        bool free = true;
        for (auto& c : _closedSet)
            if (math::distanceSquare(newNode, c.first) < 0.9 * _distance * _distance)
            {
                free = false;
                break;
            }

        if (!free)
            continue;

        int additionalCost = 0;
        for (auto& o : _obstacles)
            if (math::intersection(*(o.first), newNodeShape))
            {
                additionalCost += o.second;
            }

        if (additionalCost >= 100000)
            continue;

        Node tmp;
        tmp.costFromStart = _closedSet[n].costFromStart + math::distanceSquare(newNode, n);
        tmp.costToEnd = math::distanceSquare(newNode, _end);
        tmp.costSum = tmp.costFromStart + tmp.costToEnd + additionalCost;

        tmp.parent = n;

        if (_openSet.find(newNode) != _openSet.end())
        {
            if (tmp.costSum < _openSet[newNode].costSum)
                _openSet[newNode] = tmp;
        }
        else
            _openSet.emplace(std::move(newNode), std::move(tmp));
    }
}

math::Shape aStar()
{
    Node startNode;
    startNode.parent.x = 0;
    startNode.parent.y = 0;

    _openSet.emplace(_start, startNode);
    toClosedSet(_start);
    addAdjacentNodes(_start);

    math::Vector2D<int> tmp = _start;

    while(math::distanceSquare(tmp, _end) > 4 * _distance * _distance && !_openSet.empty())
    {
        double f = _openSet.begin()->second.costSum;
        tmp = _openSet.begin()->first;

        for (auto& i : _openSet)
        {
            if (i.second.costSum < f)
            {
                f = i.second.costSum;
                tmp = i.first;
            }
        }

        toClosedSet(tmp);
        addAdjacentNodes(tmp);
    }

    if (math::distanceSquare(tmp, _end) <= 4 * _distance * _distance)
    {
        vector<math::Vector2d> path;

        Node& tmp2 = _closedSet[tmp];
        math::Vector2D<double> current = _end, previous = tmp2.parent;

        path.push_back(current);
        path.push_back(tmp);

        while (previous.x != 0 && previous.y != 0)
        {
            current = previous;
            path.push_back(current);

            tmp2 = _closedSet[tmp2.parent];
            previous = tmp2.parent;
        }

        _finalPath.polyline(path, _nodeRadius);
    }
    else
        _finalPath.none();

    return _finalPath;
}

void clear()
{
    _finalPath.none();
    _openSet.clear();
    _closedSet.clear();
}

} //namespace aStar

} //namespace tools
