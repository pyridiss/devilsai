
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_MATH_H
#define TOOLS_MATH_H

//M_PI seems to cause problems on some platforms; define it if not done yet.
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include <cstddef>
#include <string>

using std::string;

namespace tools{

namespace math{

struct Shape;

void initLibrary();
double angle(double x, double y);

template<typename T>
constexpr T signof(T x)
{
    return (x >= 0) ? 1 : -1;
}

template<typename T>
constexpr T minimum(T a, T b)
{
    return (a <= b) ? a : b;
}

template<typename T, typename... Args>
constexpr T minimum(T a, Args... args)
{
    return minimum(a, minimum(args...));
}

template<typename T>
constexpr T maximum(T a, T b)
{
    return (a >= b) ? a : b;
}


template<typename T, typename... Args>
constexpr T maximum(T a, Args... args)
{
    return maximum(a, maximum(args...));
}

double randomNumber(double min, double max);
int randomNumber(int min, int max);
double randomNumber_BinomialLaw(double min, double max);


/**
 * \brief Converts an integer value into a std::string.
 *
 * \param value value to convert
 * \param stringLength minimum length of the string (the character '0' is used for padding). 0 means 'no minimum'. Default is 0.
 * \param sign if true, the string will contain a '+' character if \a value is positive. Default is false.
 * \param digitSeparator if true, add a space character to group digits by 3. Default is false.
 */
string intToChars(int value, int stringLength = 0, bool sign = false, bool digitSeparator = false);

bool intersection(const Shape& shape1, const Shape& shape2);

} //namespace math

constexpr unsigned int hash(const char* str)
{
    unsigned int h = 0;
    for (unsigned i = 0 ; str[i] != '\0' ; ++i)
        h = str[i] + (h << 6) + (h << 16) - h;

    return h;
}

} //namespace tools

/**
 * Synonym of tools::hash()
 */
constexpr unsigned int operator ""_hash(const char* str, std::size_t l)
{
    unsigned int h = 0;
    for (std::size_t i = 0 ; i != l ; ++i)
        h = str[i] + (h << 6) + (h << 16) - h;

    return h;
}

#endif // TOOLS_MATH_H

