
/*
    devilsai - A game engine designed for the development of Action-RPG games
    Copyright (C) 2009-2019  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_DEBUG_XML
#define TOOLS_DEBUG_XML

#include <exception>

#include <tinyxml2.h>

#include "tools/debug.h"

namespace debug{

struct XMLException : public std::exception
{
    const char* what() const noexcept override
    {
        return "Invalid XML file. See the log for further information.";
    }
};

inline void XMLCheckAttribute(const char* file, tinyxml2::XMLElement* elem, const char* attr, int result)
{
    if (result == tinyxml2::XML_SUCCESS) return;
    if (result == tinyxml2::XML_NO_ATTRIBUTE)
    {
        warn << "Error in XML file " << file << ": "
             << "attribute '" << attr << "' not defined in element '" << elem->Name()
             << "' at line " << elem->GetLineNum()
             << end;
    }
    if (result == tinyxml2::XML_WRONG_ATTRIBUTE_TYPE)
    {
        warn << "Error in XML file " << file << ": "
             << "wrong type for attribute '" << attr << "' in element '" << elem->Name()
             << "' at line " << elem->GetLineNum()
             << end;
    }

    throw XMLException {};
}

template<typename T = const char*>
inline void XMLCheckAttribute(const char* file, tinyxml2::XMLElement* elem, const char* attr)
{
    if (elem->Attribute(attr)) return;
    XMLCheckAttribute(file, elem, attr, tinyxml2::XML_NO_ATTRIBUTE);
}

template<>
inline void XMLCheckAttribute<int>(const char* file, tinyxml2::XMLElement* elem, const char* attr)
{
    int buffer;
    int result = elem->QueryIntAttribute(attr, &buffer);
    return XMLCheckAttribute(file, elem, attr, result);
}

template<>
inline void XMLCheckAttribute<unsigned>(const char* file, tinyxml2::XMLElement* elem, const char* attr)
{
    unsigned buffer;
    int result = elem->QueryUnsignedAttribute(attr, &buffer);
    return XMLCheckAttribute(file, elem, attr, result);
}

}  // namespace debug

#endif  // TOOLS_DEBUG_XML
