
/*
    devilsai - An Action-RPG game engine
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_VECTOR2D
#define TOOLS_VECTOR2D

#include <cmath>
#include <functional>

namespace tools::math{

/**
 * \brief Template class to define vectors in a 2D world.
 */
template<typename T>
struct Vector2D
{
    T x {};
    T y {};

    Vector2D() = default;

    template<typename U>
    Vector2D(const Vector2D<U>& v)
      : x(v.x),
        y(v.y)
    {
    }

    template<typename U>
    Vector2D(const U& _x, const U& _y)
      : x(_x),
        y(_y)
    {
    }

    bool operator==(const Vector2D<T>& right) const
    {
        return (x == right.x && y == right.y);
    }

    double length() const
    {
        return std::sqrt(x * x + y * y);
    }
};

template<typename T>
inline Vector2D<T> operator+(const Vector2D<T>& left, const Vector2D<T>& right)
{
    return Vector2D<T>(left.x + right.x, left.y + right.y);
}

template<typename T>
inline Vector2D<T> operator-(const Vector2D<T>& left, const Vector2D<T>& right)
{
    return Vector2D<T>(left.x - right.x, left.y - right.y);
}

template<typename T, typename U>
inline Vector2D<T> operator*(U l, const Vector2D<T>& v)
{
    return Vector2D<T>(l * v.x, l * v.y);
}

template<typename T, typename U>
inline Vector2D<T> operator/(const Vector2D<T>& v, U l)
{
    return Vector2D<T>(v.x / l, v.y / l);
}

template<typename T>
inline double dotProduct(const Vector2D<T>& v1, const Vector2D<T>& v2)
{
    return (v1.x * v2.x + v1.y * v2.y);
}

template<typename T>
inline double distanceSquare(const Vector2D<T>& v1, const Vector2D<T>& v2)
{
    return ((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y));
}

template<typename T>
inline double distance(const Vector2D<T>& v1, const Vector2D<T>& v2)
{
    return std::sqrt(distanceSquare(v1, v2));
}

using Vector2d = Vector2D<double>;

}  // namespace tools::math

namespace std{

template<typename T>
struct hash<tools::math::Vector2D<T>>
{
    size_t operator()(const tools::math::Vector2D<T>& i) const
    {
        return hash<T>()(i.x) ^ hash<T>()(i.y);
    }
};

template<typename T>
struct equal_to<tools::math::Vector2D<T>>
{
    bool operator()(const tools::math::Vector2D<T>& left, const tools::math::Vector2D<T>& right) const
    {
        return (left == right);
    }
};

}  // std

#endif  // TOOLS_VECTOR2D
