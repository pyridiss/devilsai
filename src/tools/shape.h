
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_SHAPE_H
#define TOOLS_SHAPE_H

#include <vector>

#include "tools/vector2d.h"

using namespace std;

namespace tinyxml2{
    class XMLElement;
    class XMLHandle;
    class XMLDocument;
}

namespace sf{
    class RenderTarget;
    class Color;
}

namespace tools{

namespace math{

Vector2d* absoluteOrigin();

class Shape
{
    enum Profiles
    {
        None,
        Point,
        Circle,
        Triangle,
        Rectangle,
        Line,
        Polyline,
        Arc,
        Complex
    };

    struct S_Point
    {
        Vector2D<double> point;
    };
    struct S_Circle
    {
        Vector2D<double> center;
        double radius;
    };
    struct S_Triangle
    {
        Vector2D<double> points[3];
    };
    struct S_Rectangle
    {
        Vector2D<double> points[4];
    };
    struct S_Line
    {
        Vector2D<double> points[2];
        double thickness;
        double length;
        double direction;
    };
    struct S_Polyline
    {
        std::vector<Vector2D<double>> points;
        double thickness;
    };
    struct S_Arc
    {
        Vector2D<double> center;
        double radius;
        double direction;
        double opening;
    };

    private:
        Profiles profile {Profiles::None};
        const Vector2D<double>* _origin {absoluteOrigin()};
        pair<Vector2D<double>, Vector2D<double>> box {};
        Shape* next {nullptr};

        union
        {
            S_Point _point;
            S_Circle _circle;
            S_Triangle _triangle;
            S_Rectangle _rectangle;
            S_Line _line;
            S_Polyline _polyline;
            S_Arc _arc;
        };

    public:
        Shape();
        Shape(const Shape& s);
        Shape(Shape&& s) noexcept = default;
        Shape& operator=(const Shape& right);
        Shape& operator=(Shape&& right) noexcept = default;
        ~Shape();

    private:
        void setUnion(Profiles p);
        void setUnion(const Shape& other);

    public:
    void setOrigin(const Vector2d* o);
        const Vector2d* origin() const;
        void none();
        void point(const Vector2D<double> p);
        void circle(const Vector2D<double> p, double radius);
        void triangle(const Vector2D<double> p1, Vector2D<double> p2, Vector2D<double> p3);

    /**
     * Defines a parallelogram (despite its name) from three points.
     *
     * The three points define two vectors according to the following scheme:
     * \image html tools_math_shape_rectangle.png "Meaning of the parameters"
     *
     * \param p1 Origin of both vectors defining the parallelogram
     * \param p2 End of the first vector
     * \param p3 End of the second vector
     */
        void rectangle(const Vector2D<double> p1, const Vector2D<double> p2, const Vector2D<double> p3);
        void line(const Vector2D<double> p1, const Vector2D<double> p2, int thickness);
        void line(const Vector2D<double>, double length, double direction, int thickness);
        void polyline(const vector<Vector2d>& p, int thickness);
        void arc(const Vector2D<double> p, double radius, double direction, double opening);

        bool isLine();
        bool isPolyline();

        double lengthOfLine();
        std::vector<Vector2D<double>> pointsOfLine();
    double area();
        double span();
    void updateDirection(double direction);
        pair<Vector2D<double>, Vector2D<double>> boundingBox() const;

        void removeLastPointOfLine();

    void loadFromXML(tinyxml2::XMLElement* elem);
    void saveToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLHandle& handle) const;

    void display(sf::RenderTarget& target, const sf::Color& color);

    friend bool intersection_point_point(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_circle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_triangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_rectangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_line(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_point_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_circle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_triangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_rectangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_line(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_circle_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_triangle_triangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_triangle_rectangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_triangle_line(const Shape& shape1, const Shape& shape2);
    friend bool intersection_triangle_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_triangle_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_rectangle_rectangle(const Shape& shape1, const Shape& shape2);
    friend bool intersection_rectangle_line(const Shape& shape1, const Shape& shape2);
    friend bool intersection_rectangle_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_rectangle_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_line_line(const Shape& shape1, const Shape& shape2);
    friend bool intersection_line_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_line_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_polyline_polyline(const Shape& shape1, const Shape& shape2);
    friend bool intersection_polyline_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection_arc_arc(const Shape& shape1, const Shape& shape2);
    friend bool intersection(const Shape& shape1, const Shape& shape2);
};

} //namespace math

} //namespace tools

#endif // TOOLS_SHAPE_H

