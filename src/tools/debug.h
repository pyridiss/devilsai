
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEBUG_H
#define DEBUG_H

#include <cstring>
#include <string>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

namespace debug{

inline struct End{} end;

struct Warn
{
    Warn& operator<<(const char*);
    Warn& operator<<(const std::string&);
    Warn& operator<<(int);
    Warn& operator<<(End);
};

inline Warn warn;

void openDebugFile(const char* appName, const char* version, const char* debugFile);

}  // namespace debug

namespace tools{

namespace debug{

using namespace std;

[[deprecated]]
void openDebugFile(const string& appName, const string& version);
[[deprecated]]
void addDebugCategory(string category);
[[deprecated]]
void fatal(const string& str, const string& category, const string& file, int line);
[[deprecated]]
void error(const string& str, const string& category, const string& file, int line);
[[deprecated]]
void warning(const string& str, const string& category, const string& file, int line);
[[deprecated]]
void message(const string& str, const string& category, const string& file, int line);

} //namespace debug

} //namespace tools

#endif // DEBUG_H
