
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2014  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <lua.hpp>

#include "tools/debug.h"
#include "tools/signals.h"
#include "tools/timeManager.h"
#include "tools/filesystem.h"
#include "tools/math.h"
#include "textManager/richText.h"
#include "textManager/textManager.h"

#include "devilsai-resources/Carte.h"
#include "devilsai-resources/CheckPoint.h"
#include "devilsai-resources/manager.h"
#include "devilsai-resources/Place.h"
#include "devilsai-gui/conversation.h"
#include "../ElementsCarte/ElementsCarte.h"

#include "Jeu/Jeu.h"
#include "Jeu/options.h"

int LUA_cout(lua_State* L)
{
	cout << "LUA says: " << lua_tostring(L, 1) << endl;
	return 0;
}

int LUA_set(lua_State* L)
{
    Unit* elem = static_cast<Unit*>(lua_touserdata(L, 1));
	string field = lua_tostring(L, 2);
	double value = lua_tonumber(L, 3);

    Individu* ind  = dynamic_cast<Individu*>(elem);

    if (ind == NULL)
    {
        tools::debug::error("LUA_set() has been called with a non-individual pointer.", "lua", __FILENAME__, __LINE__);
        return 0;
    }

    if (field == "Diplomatie") ind->Diplomatie = (int)value;
    else if (field == "life") ind->setAttribute(Life, value);
    else if (field == "energy") ind->setAttribute(Energy, value);
    else if (field == "healing") ind->setAttribute(Healing, value);

	return 0;
}

int LUA_get(lua_State* L)
{
    double result = 0;

    Unit* elem = static_cast<Unit*>(lua_touserdata(L, 1));
	string field = lua_tostring(L, 2);

	Individu* ind = dynamic_cast<Individu*>(elem);

    if (ind == NULL)
    {
        tools::debug::error("LUA_get() has been called with a non-individual pointer.", "lua", __FILENAME__, __LINE__);
        lua_pushnumber(L, 1);
        return 1;
    }

    if (field == "PosX") result = ind->position().x;
    else if (field == "PosY") result = ind->position().y;
    else if (field == "angle") result = ind->angle();
    else if (field == "diplomacy") result = ind->Diplomatie;
    else
    {
        Attribute a = stringToAttribute(field);
        if (a != numberOfAttributes)
            result = ind->currentHealthStatus(a);
    }

	lua_pushnumber(L, result);
	return 1;
}

int LUA_useObject(lua_State* L)
{
    UnitId unit (lua_tonumber(L, 1));
    Individu* ind = dynamic_cast<Individu*>(unit.unit());
	string object = lua_tostring(L, 2);

    auto& objects = ind->inventory.objects;

    for (auto& i : objects)
    {
        if (i.stackable() && i.name() == object)
        {
            //TODO: Erase temporary objects instead of refusing the use of the object.
            if (ind->inventory.at(i.requiredSlot()) != nullptr)
                break;

            if (i.quantity() > 1)
            {
                i.setQuantity(i.quantity() - 1);
                ind->inventory.addObject(i.name(), i.requiredSlot());
                break;
            }
            else
            {
                i.setSlot(i.requiredSlot());
                objects.push_back(std::move(i));
                ind->inventory.deleteObject(objects.back());
                break;
            }
        }
    }

	return 0;
}

int LUA_getQuantityOf(lua_State* L)
{
    Individu* ind = static_cast<Individu*>(lua_touserdata(L, 1));
	string object = lua_tostring(L, 2);
    int result = ind->inventory.quantityOf(object);

	lua_pushnumber(L, result);
	return 1;
}

int LUA_pushDialog(lua_State* L)
{
    string container = lua_tostring(L, 1);
    string newDialog = lua_tostring(L, 2);
    textManager::PlainText plain = textManager::getText(container, newDialog);
    multimedia::RichText rich;
    rich.setSize(options::option<unsigned>(tools::hash("dialog-width")), 0);
    rich.setDefaultFormatting("liberation", 12, Color(255, 255, 255));
    rich.addFlags(textManager::HAlignJustify);
    rich.create(plain);

    addDialog(std::move(rich));
	return 0;
}

int LUA_dialogDisplayed(lua_State* L)
{
    lua_pushboolean(L, isConversationDone());
	return 1;
}

int LUA_interact(lua_State* L)
{
    Individu* individual = (Individu*)lua_touserdata(L, 1);
    Unit* element = static_cast<Unit*>(lua_touserdata(L, 2));

    bool result = false;

    if (individual != nullptr && element != nullptr)
        result = individual->interact(element);
    else
        tools::debug::error("LUA_interact() has been called with a non-existent element.", "lua", __FILENAME__, __LINE__);

	lua_pushboolean(L, result);
	return 1;
}

int LUA_loadWorld(lua_State* L)
{
    tools::debug::message("LUA_loadWorld() called", "lua", __FILENAME__, __LINE__);

    string world = lua_tostring(L, 1);
    string file = lua_tostring(L, 2);
    string tag = lua_tostring(L, 3);

    Carte* w = devilsai::addResource<Carte>(world, world);  // just get it, if it already exists
    w->loadFromFile(tools::filesystem::dataDirectory() + file, tag);

    return 0;
}

int LUA_I(lua_State* L)
{
	lua_pushnumber(L, tools::timeManager::I(1./60.));
	return 1;
}

int LUA_addCheckPoint(lua_State* L)
{
    string world = lua_tostring(L, 1);
    int x = lua_tonumber(L, 2);
    int y = lua_tonumber(L, 3);
    int w = lua_tonumber(L, 4);
    int h = lua_tonumber(L, 5);

    Carte* wo = devilsai::getResource<Carte>(world);
    if (wo == nullptr)
    {
        tools::debug::error("LUA_addCheckPoint() has been called from a Lua state that require an unknown world: " + world, "lua", __FILENAME__, __LINE__);
        lua_pushnumber(L, 0);
        return 1;
    }

    CheckPoint *c = wo->addCheckPoint("CheckPoints", x, y);
    tools::math::Shape s;
    s.rectangle(tools::math::Vector2d(-w, -h), tools::math::Vector2d(w, -h), tools::math::Vector2d(-w, h));
    c->setShape(s);
    lua_pushnumber(L, c->id().id());

    return 1;
}

int LUA_setActivity(lua_State* L)
{
    Individu* ind = static_cast<Individu*>(lua_touserdata(L, 1));

	if (ind != NULL) ind->Set_Activite(lua_tostring(L, 2));

	return 0;
}

int LUA_possess(lua_State* L)
{
    Individu* ind = static_cast<Individu*>(lua_touserdata(L, 1));

    string_view object = lua_tostring(L, 2);

	bool result = false;
	for (auto& i : ind->inventory.objects)
	{
        if (i.name() == object)
		{
			result = true;
			break;
		}
	}

	lua_pushboolean(L, result);
	return 1;
}

int LUA_transferObject(lua_State* L)
{
    Individu* indA = static_cast<Individu*>(lua_touserdata(L, 1));
    Individu* indB = static_cast<Individu*>(lua_touserdata(L, 2));

    string_view object = lua_tostring(L, 3);

    auto iObj = indA->inventory.objects.begin();

    WearableItem tmp;

    //Retrieve the item in the tmp variable
    while (iObj != indA->inventory.objects.end() && iObj->name() != object)
        ++iObj;
    if (iObj == indA->inventory.objects.end()) return 0;

    tmp = std::move(*iObj);
    indA->inventory.objects.erase(iObj);

    //Now, we check is the same object exists in the player inventory and can be stacked.
    for (auto& i : indB->inventory.objects)
    {
        if (i.stackable() && i.name() == tmp.name())
        {
            i.setQuantity(i.quantity() + tmp.quantity());
            //We're done, tmp will be destroyed
            return 0;
        }
    }

    //The item cannot be stacked, we try to find an empty slot
    int key = 1;
    while (indB->inventory.at("inventory" + tools::math::intToChars(key, 2)) != nullptr)
        ++key;
    //TODO: this function may push an item outside the inventory range; fix this

    tmp.setSlot("inventory" + tools::math::intToChars(key, 2));
    indB->inventory.objects.push_back(std::move(tmp));

    return 0;
}

int LUA_moveItemTo(lua_State* L)
{
    tools::debug::message("LUA_moveItemTo() called", "lua", __FILENAME__, __LINE__);

    Individu* ind = (Individu*)lua_touserdata(L, 1);
    string w = lua_tostring(L, 2);
    string p = lua_tostring(L, 3);

    Carte* newWorld = devilsai::getResource<Carte>(w);
    if (newWorld == nullptr)
    {
        tools::debug::error("World not found: " + w, "lua", __FILENAME__, __LINE__);
        return 0;
    }

    const tools::math::Shape* zone = nullptr;
    for (auto & place : newWorld->places)
    {
        if (place->name() == p) zone = &(place->shape());
    }

    if (zone == nullptr)
    {
        tools::debug::error("This place does not exist: " + p, "lua", __FILENAME__, __LINE__);
        return 0;
    }

    auto box = zone->boundingBox();
    tools::math::Vector2d originalPosition = ind->position();

    int debugCounter = 0;
    do
    {
        ++debugCounter;

        //Set a new random position
        double x = box.first.x + (box.second.x - box.first.x)/10000.0 * (double)(rand()%10000);
        double y = box.first.y + (box.second.y - box.first.y)/10000.0 * (double)(rand()%10000);
        ind->move(x - ind->position().x, y - ind->position().y);

        //1. The item must collide with the zone
        if (!tools::math::intersection(ind->shape(), *zone)) continue;

        //2. The item must not collide with anything else
        if (newWorld->findFirstCollidingItem(ind, ind->shape(), false).first == nullptr)
            break;
    }
    while (debugCounter < 100);

    if (debugCounter == 100)
    {
        //Refuse to do the move: there is no place left
        ind->move(originalPosition.x - ind->position().x, originalPosition.y - ind->position().y);
        lua_pushboolean(L, false);
    }
    else
    {
        for (auto& world : devilsai::getAllResources<Carte>())
        {
            world->removeItem(ind);
            world->stopManagement();
        }

        Joueur* pl = dynamic_cast<Joueur*>(ind);
        if (pl == nullptr)
        {
            newWorld->insertItem(ind);
        }
        else
        {
            pl->setWorld(newWorld);
            pl->Set_Activite(Behaviors::Random);
            pl->stopAutomoving();
            pl->stopHunting();
        }

        newWorld->resetClock();

        lua_pushboolean(L, true);
    }

    return 1;
}
