
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-gui/conversation.h"

#include <queue>

#include <SFML/Graphics.hpp>

#include "tools/debug.h"
#include "tools/signals.h"
#include "tools/math.h"
#include "tools/timeManager.h"
#include "multimedia/RichText.h"
#include "gui/widget.h"
#include "gui/window.h"

#include "Jeu/options.h"


using namespace std;
using namespace sf;

gui::Window Gui;

multimedia::RichText* _currentDialog = nullptr;
double _currentOffset = 0;
int _currentSpeed = 0;
gui::Widget *_restart = nullptr, *_ok = nullptr, *_faster = nullptr, *_slower = nullptr;

static queue<multimedia::RichText> Conversation_Dialogs;


void addDialog(multimedia::RichText&& t)
{
    Conversation_Dialogs.emplace(std::move(t));
}

bool isConversationDone()
{
    return Conversation_Dialogs.empty();
}

void clearConversation()
{
    while (!Conversation_Dialogs.empty())
        Conversation_Dialogs.pop();
}

void initConversation(RenderWindow& target)
{
    Gui.startWindow(target);
    Gui.loadFromFile("gui/conversation.xml");

    _restart = Gui.widget("restart");
    _ok = Gui.widget("ok");
    _faster = Gui.widget("faster");
    _slower = Gui.widget("slower");

    gui::Widget* text = Gui.widget("text");

    if (text != nullptr)
        options::addOption<unsigned>(tools::hash("dialog-width"), text->width());
    else tools::debug::error("Cannot find the widget 'text' in the conversation gui", "devilsai", __FILENAME__, __LINE__);

    _currentSpeed = options::option<unsigned>(tools::hash("dialog-speed"));
}

void manageConversation(Event& event)
{
    if (_currentDialog != nullptr)
    {
        if (_restart != nullptr && _restart->activated(event))
        {
            _currentOffset = -100;
        }
        if (_ok != nullptr && _ok->activated(event))
        {
            _currentDialog = nullptr;
            Conversation_Dialogs.pop();
            tools::signals::addSignal("disable-cinematic-mode");
        }
        if (_faster != nullptr && _faster->activated(event))
        {
            if (_currentSpeed < 50)
            {
                _currentSpeed += 2;
                options::addOption<unsigned>(tools::hash("dialog-speed"), _currentSpeed);
            }
        }
        if (_slower != nullptr && _slower->activated(event))
        {
            if (_currentSpeed > 2)
            {
                _currentSpeed -= 2;
                options::addOption<unsigned>(tools::hash("dialog-speed"), _currentSpeed);
            }
        }
    }

    Gui.checkTriggers();
}

void manageConversation()
{
    if (!Conversation_Dialogs.empty() && _currentDialog == nullptr)
    {
        _currentDialog = &Conversation_Dialogs.front();
        _currentOffset = -100;
        tools::signals::addSignal("enable-cinematic-mode");
    }

    if (_currentDialog != nullptr)
    {
        _currentOffset += tools::timeManager::I((double)_currentSpeed/100.0);

        if (_currentOffset > _currentDialog->height() + 5)
        {
            _currentDialog = nullptr;
            Conversation_Dialogs.pop();
            tools::signals::addSignal("disable-cinematic-mode");
        }
    }
}

void displayConversation(RenderWindow& target)
{
    if (_currentDialog == nullptr)
        return;

    Gui.display(target);

    gui::Widget* text = Gui.widget("text");

    float x = text->left();
    float y = text->top();
    float w = text->width() + 2;
    float h = text->height();

    _currentDialog->display(target, x, y, w, h, 0, _currentOffset);
}
