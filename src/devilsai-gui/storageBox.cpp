
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-gui/storageBox.h"

#include "tools/math.h"
#include "textManager/textManager.h"
#include "gui/widget.h"
#include "gui/window.h"
#include "imageManager/imageManager.h"

#include "devilsai-resources/pack.h"
#include "devilsai-resources/StorageBox.h"
#include "devilsai-resources/wearableItem.h"


void manageStorageBoxScreen(gui::Window& window, sf::Event& event, Pack& playerInventory, StorageBox* box)
{
    auto& storageBox = box->objects();

    const auto& slots = window.getWidgets();

    for (const auto& slot : slots)
    {
        if (!slot.second->activated(event)) continue;

        //Click founded in the player inventory; we will try to transfer the object in the storage box
        if (slot.second->embeddedData<string>("owner") == "player")
        {
            if (storageBox.isFull()) break;

            WearableItem item = playerInventory.pickItemAt(slot.first);
            if (!item.valid()) break;

            storageBox.insertItem(std::move(item));
        }

        //Click founded in the storage box; we will try to transfer the object in the player inventory
        if (slot.second->embeddedData<string>("owner") == "storage-box")
        {
            if (playerInventory.isFull()) break;

            WearableItem item = storageBox.pickItemAt(slot.first);
            if (!item.valid()) break;

            playerInventory.insertItem(std::move(item));
        }
        break;
    }

    window.checkTriggers();
}

void displayStorageBoxScreen(gui::Window& window, sf::RenderWindow& target, Pack& playerInventory, StorageBox* box)
{
    gui::Widget* boxName = window.widget("storagebox-name");
    gui::optionType o;
    o.set<textManager::PlainText>(box->name());
    boxName->setValue(o);

    window.display(target);

    auto& storageBox = box->objects();

    WearableItem* hovering = nullptr;

    const auto& slots = window.getWidgets();

    for (const auto& slot : slots)
    {
        WearableItem* obj = nullptr;
        if (slot.second->embeddedData<string>("owner") == "player") obj = playerInventory.at(slot.first);
        if (slot.second->embeddedData<string>("owner") == "storage-box") obj = storageBox.at(slot.first);
        if (obj != nullptr)
        {
            imageManager::display(target, tools::hash("objectsIcons"), obj->name(), slot.second->left(), slot.second->top());

            if (obj->stackable())
            {
                multimedia::RichText number;
                number.setSize(50, 0);
                number.setDefaultFormatting("liberation", 12, Color(255, 255, 255));
                number.create(obj->quantity());
                number.display(target, slot.second->left() + 30, slot.second->top() + 30);
            }

            if (slot.second->mouseHovering())
                hovering = obj;
        }
    }

    if (hovering != nullptr)
        hovering->displayDescription(target);
}
