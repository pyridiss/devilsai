
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-gui/skillPanel.h"

#include <unordered_map>

#include <tinyxml2.h>

#include "tools/math.h"
#include "imageManager/imageManager.h"
#include "textManager/textManager.h"
#include "gui/style.h"
#include "gui/window.h"

#include "devilsai-resources/shaders.h"
#include "ElementsCarte/ElementsCarte.h"


gui::Window SkillPanel_Gui;
gui::Window SkillPanel_Bar;
string SkillPanel_SelectedSkillName;


using Skillbar = unordered_map<string, string>;

unordered_map<UnitId, Skillbar> SkillPanel_PlayerData;
Player* currentPlayer = nullptr;


void initSkillPanel(RenderWindow& target)
{
    SkillPanel_Gui.startWindow(target);
    SkillPanel_Gui.loadFromFile("gui/skill-panel.xml");
    SkillPanel_Bar.startWindow(target);
    SkillPanel_Bar.loadFromFile("gui/ingame-skillbar.xml");
}

void checkSkillPanelTriggers()
{
    SkillPanel_Gui.checkTriggers();
    SkillPanel_Bar.checkTriggers();
}

void updatePanels(Player* player)
{
    for (const auto& slot : SkillPanel_Bar.getWidgets())
    {
        slot.second->addEmbeddedData<string>("value", SkillPanel_PlayerData[player->id()][slot.first]);
    }
    currentPlayer = player;
}

void placeItemInSkillbar(Event& event, Player* player, WearableItem& item)
{
    for (const auto& slot : SkillPanel_Bar.getWidgets())
    {
        if (slot.second->activated(event))
        {
            // Search for a skill corresponding to the selected object
            Character::SkillAccess skill = player->skill("use-" + item.name());
            if (!skill.none())
            {
                slot.second->addEmbeddedData<string>("value", "use-" + item.name());
                SkillPanel_PlayerData[player->id()][slot.first] = "use-" + item.name();
                // Put the object in the inventory again; it will returns at its old place.
                player->inventory.objects.push_back(std::move(item));
            }
        }
    }
}

void manageSkillbar(Event& event, Player* player)
{
    if (player != currentPlayer) updatePanels(player);

    if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Button::Right)
    {
        for (const auto& slot : SkillPanel_Bar.getWidgets())
        {
            if (slot.second->activated(event))
            {
                slot.second->addEmbeddedData<string>("value", string());
                SkillPanel_PlayerData[player->id()][slot.first].clear();
            }
        }
    }

    SkillPanel_Bar.manage(event);
}

void manageSkillPanel(sf::Event& event, Joueur* player)
{
    if (player != currentPlayer) updatePanels(player);

    for (const auto& slot : SkillPanel_Gui.getWidgets())
    {
        const string& skillName = slot.second->embeddedData<string>("skill");
        if (skillName.empty())
            continue;

        if (slot.second->activated(event))
        {
            if (skillName == SkillPanel_SelectedSkillName)
                SkillPanel_SelectedSkillName.clear();
            else
                SkillPanel_SelectedSkillName = skillName;
        }
    }

    if (!SkillPanel_SelectedSkillName.empty())
    {
        for (const auto& slot : SkillPanel_Bar.getWidgets())
        {
            if (slot.second->activated(event))
            {
                slot.second->addEmbeddedData<string>("value", SkillPanel_SelectedSkillName);
                SkillPanel_PlayerData[player->id()][slot.first] = SkillPanel_SelectedSkillName;
                SkillPanel_SelectedSkillName.clear();
            }
        }
    }

    manageSkillbar(event, player);
}

void displaySkillbar(sf::RenderWindow& target, Joueur* player)
{
    if (player != currentPlayer) updatePanels(player);

    SkillPanel_Bar.display(target);

    for (const auto& slot : SkillPanel_Bar.getWidgets())
    {
        const string& skillName = slot.second->embeddedData<string>("value");
        if (!skillName.empty())
        {
            Shader* shader = nullptr;

            Individu::SkillAccess currentSkill = player->skill(skillName);

            if (!currentSkill.none() && currentSkill.unavailability() > 0)
                shader = multimedia::shader("contrast", devilsai::skillPanelShaderInstance);

            imageManager::display(target, tools::hash("skills"), skillName, slot.second->left(), slot.second->top(), false, shader);
        }

        string relatedObject;
        if (size_t i = skillName.find("use-") ; i != string::npos)
            relatedObject = skillName.substr(i+4);

        if (!relatedObject.empty())
        {
            multimedia::RichText number;
            number.setSize(50, 0);
            number.setDefaultFormatting("liberation", 12, Color(255, 255, 255));
            number.create(player->inventory.quantityOf(relatedObject));
            number.display(target, slot.second->left() + 30, slot.second->top() + 30);
        }
    }
}

void displaySkillPanel(RenderWindow& target, Joueur* player)
{
    if (player != currentPlayer) updatePanels(player);

    SkillPanel_Gui.display(target);

    Individu::SkillAccess hovering;

    for (const auto& slot : SkillPanel_Gui.getWidgets())
    {
        const string& skillName = slot.second->embeddedData<string>("skill");
        if (skillName.empty()) continue;

        Individu::SkillAccess currentSkill = player->skill(skillName);
        if (currentSkill.none()) continue;

        if (currentSkill.level() != 0)
        {
            slot.second->show();
        }
        else
        {
            slot.second->hide();
            continue;
        }

        imageManager::display(target, tools::hash("skills"), skillName, slot.second->left(), slot.second->top());

        multimedia::RichText level;
        level.setSize(50, 0);
        level.setDefaultFormatting("liberation-bold", 13, Color(192, 192, 192));
        level.addFlags(textManager::Shaded);
        level.create(currentSkill.level());
        level.display(target, slot.second->left() + 30, slot.second->top() + 30);

        if (slot.second->mouseHovering())
            hovering = currentSkill;
    }

    displaySkillbar(target, player);

    if (!SkillPanel_SelectedSkillName.empty())
    {
        imageManager::display(target, tools::hash("skills"), SkillPanel_SelectedSkillName, gui::mousePosition().x, gui::mousePosition().y, true);
    }
    else if (!hovering.none())
    {
        hovering->displayDescription(target);
    }
}

const string& leftClickSkill()
{
    return SkillPanel_PlayerData[currentPlayer->id()]["left-click"];
}

const string& rightClickSkill()
{
    return SkillPanel_PlayerData[currentPlayer->id()]["right-click"];
}

void loadSkillbarFromXML(tinyxml2::XMLElement* elem)
{
    using namespace tinyxml2;

    XMLElement *player = elem->FirstChildElement();

    while (player)
    {
        UnitId id;
        Skillbar bar;
        const XMLAttribute* attr = player->FirstAttribute();
        while (attr)
        {
            if (strcmp(attr->Name(), "id") == 0)
                id = UnitId(attr->UnsignedValue());
            else
            {
                bar.emplace(attr->Name(), attr->Value());
            }
            attr = attr->Next();
        }

        if (id) SkillPanel_PlayerData.emplace(id, std::move(bar));

        player = player->NextSiblingElement();
    }
}

void saveSkillbarToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement* root)
{
    using namespace tinyxml2;

    for (auto& player : SkillPanel_PlayerData)
    {
        XMLElement* elem = doc.NewElement("player");
        root->InsertEndChild(elem);
        elem->SetAttribute("id", player.first.id());
        for (auto& attr : player.second)
        {
            if (!attr.second.empty())
                elem->SetAttribute(attr.first.c_str(), attr.second.c_str());
        }
    }
}
