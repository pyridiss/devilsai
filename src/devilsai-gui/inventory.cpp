
/*
    Devilsai - A game written using the SFML library
    Copyright (C) 2009-2017  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "devilsai-gui/inventory.h"

#include "tools/math.h"
#include "gui/style.h"
#include "gui/window.h"
#include "imageManager/imageManager.h"

#include "devilsai-resources/wearableItem.h"
#include "devilsai-resources/pack.h"
#include "ElementsCarte/ElementsCarte.h"

#include "devilsai-gui/skillPanel.h"


WearableItem InventoryPanel_selectedObject;
string InventoryPanel_correspondingSkill;

void manageInventoryScreen(gui::Window& window, sf::Event& event, Player* player)
{
    const auto& slots = window.getWidgets();

    for (const auto& slot : slots)
    {
        if (slot.second->activated(event))
        {
            if (!InventoryPanel_selectedObject.valid())
            {
                if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left)
                {
                    InventoryPanel_selectedObject = player->inventory.pickItemAt(slot.first);
                }
                else if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Right)
                {
                    WearableItem* currentObject = player->inventory.at(slot.first);
                    if (player->inventory.at(currentObject->requiredSlot()) != nullptr)
                        continue;

                    // If the player has a skill corresponding to the object, use the skill;
                    // otherwise, put the object directly in the right slot

                    Character::SkillAccess skill = player->skill("use-" + currentObject->name());
                    if (!skill.none())
                    {
                        player->Set_Activite("use-" + currentObject->name());
                    }
                    else if (currentObject->stackable() && currentObject->quantity() > 1)
                    {
                        WearableItem d = currentObject->duplicate();
                        d.setQuantity(1);
                        currentObject->setQuantity(currentObject->quantity() - 1);
                        if (!player->inventory.insertItem(std::move(d), d.requiredSlot()))
                            currentObject->setQuantity(currentObject->quantity() + 1);
                    }
                    else
                    {
                        currentObject->setSlot(currentObject->requiredSlot());
                    }
                }
            }
            else if (player->inventory.at(slot.first) == nullptr &&
                     (slot.second->embeddedData<string>("allowed-object") == "all" ||
                      slot.second->embeddedData<string>("allowed-object") == InventoryPanel_selectedObject.requiredSlot()))
            {
                player->inventory.insertItem(std::move(InventoryPanel_selectedObject), slot.first);
            }
            break;
        }
    }

    if (InventoryPanel_selectedObject.valid()) placeItemInSkillbar(event, player, InventoryPanel_selectedObject);

    window.checkTriggers();
    manageSkillbar(event, player);
}

void displayInventoryScreen(gui::Window& window, sf::RenderWindow& target, Player* player)
{
    window.display(target);

    WearableItem* hovering = nullptr;

    const auto& slots = window.getWidgets();

    for (const auto& slot : slots)
    {
        auto obj = player->inventory.at(slot.first);
        if (obj != nullptr)
        {
            const string& objectName = obj->name();
            const string& imageContainer = slot.second->embeddedData<string>("image-container");
            imageManager::display(target, tools::hash(imageContainer.c_str()), objectName, slot.second->left(), slot.second->top());

            if (obj->stackable())
            {
                multimedia::RichText number;
                number.setSize(50, 0);
                number.setDefaultFormatting("liberation", 12, Color(255, 255, 255));
                number.create(obj->quantity());
                number.display(target, slot.second->left() + 30, slot.second->top() + 30);
            }

            if (slot.second->mouseHovering())
                hovering = obj;
        }
    }

    displaySkillbar(target, player);

    if (hovering != nullptr)
        hovering->displayDescription(target);

    if (InventoryPanel_selectedObject.valid())
    {
        string objectName = InventoryPanel_selectedObject.name();
        imageManager::display(target, tools::hash("objects"), objectName, gui::mousePosition().x, gui::mousePosition().y, true);
    }
}
