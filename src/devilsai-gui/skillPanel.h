
/*
    devilsai - An Action-RPG game
    Copyright (C) 2009-2018  Quentin Henriet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEVILSAI_GUI_SKILLSPANEL
#define DEVILSAI_GUI_SKILLSPANEL

#include <string>

namespace sf{
    class RenderWindow;
    class Event;
}

namespace tinyxml2{
    class XMLElement;
    class XMLDocument;
}

class Joueur;
class WearableItem;

void initSkillPanel(sf::RenderWindow& target);
void checkSkillPanelTriggers();
void placeItemInSkillbar(sf::Event& event, Joueur* player, WearableItem& item);
void manageSkillbar(sf::Event& event, Joueur* player);
void manageSkillPanel(sf::Event& event, Joueur* player);
void displaySkillbar(sf::RenderWindow& target, Joueur* player);
void displaySkillPanel(sf::RenderWindow& target, Joueur* player);
const std::string& leftClickSkill();
const std::string& rightClickSkill();
void loadSkillbarFromXML(tinyxml2::XMLElement* elem);
void saveSkillbarToXML(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement* root);

#endif // DEVILSAI_GUI_SKILLSPANEL
